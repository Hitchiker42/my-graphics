struct single_list_node {
  void *data;
  struct single_list_node *next;
};
struct double_list_node {
  void *data;
  struct double_list_node *prev;
  struct double_list_node *next;
};
struct xor_list_node {
  void *data;
  void *xor_node;
};
struct single_list {
  li
