#ifndef __LIGHTING_H__
#define __LIGHTING_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
/*
  I got this from documentation for unity, so it's only really a guess
  Diffuse: For non-metals the color of the object, for metals black
  Specular: Non-metals dark grey, for metals the color of the object
*/
struct material {
  gl_colorf ambient;
  gl_colorf diffuse;
  gl_colorf specular;
  gl_colorf default_color;//can be used instead of the per-vertex celor
  float shininess;
/* Could store shininess in the alpha channel of one of the 
   other fields to save space.
*/
};

//vectors are 4 components so that the C struct has the same
//layout as the glsl struct
/*
  maybe add an ambient component
*/
struct light {
  gl_position location;
  gl_position direction;
  union {
    gl_colorf diffuse;
    float diffuse_vector[4];
  };
  union {
    gl_colorf specular;
    float specular_vector[4];
  };
  //not part of the equivelent glsl struct
  gl_scene *scene;//set to NULL for a light outside of a scene
};
//structure that is binary compatable to the uniform used to store
//lights in shaders
#define num_total_lights 10
struct u_light_block {
  float ambient[4];
  float eye_direction[4];
  uint32_t num_lights_used;
  uint32_t lights_bitmask;
  uint32_t transform_flag;
  uint32_t padding;//pad to 16 bytes
  gl_light lights[num_total_lights];
};
gl_uniform_block *init_material_uniform(gl_material *mat);
gl_uniform_block *init_light_uniform(float *ambient, float *eye_direction,
                                     gl_light **lights, uint32_t num_lights, 
                                     uint32_t lights_bitmask);
void set_num_lights_used(gl_scene *scene, int num_lights);
void gouraud_lighting(gl_shape *shape, gl_position eye,
                            gl_light **lights, int num_lights,
                            gl_colorf ambient);
/*
  This should probably move somewhere else
*/
#ifdef __cplusplus
}
#endif
#endif /* __LIGHTING_H__ */
