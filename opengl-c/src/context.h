#ifndef __CONTEXT_H__
#define __CONTEXT_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
//define this in the main file of the program using the library
extern thread_local global_context *thread_context;
struct global_context {
  void *userdata;
  void(*update_userdata)(void*);
  gl_scene **scenes;
  SDL_Thread *thread;
  SDL_sem *sem;
//gl_window is only non 0 in the main thread, unless you have
//multiple concurrent gl_contexts active
  gl_window window;
  int num_scenes;
};
//default size of a vertex buffer object, most vbos should be allocated
//with the same size to allow the gpu to eaisly reuse memory
//1 megabyte is a good default, considering the size of video memory these days
static const size_t default_vbo_size = 4*1024*1024;//1M
struct gl_VBO {
  gl_scene *scene;
  GLuint id;
  GLuint VAO;
  uint32_t size;//total size, usually default_vbo_size
  uint32_t offset;//current byte offset, (i.e where to store new drawables)
  uint32_t used;//ammount of space used, if no drawables have been removed
                //this will be the same as offset
  uint32_t empty_drawables;
  /* 
     Switch to using a linked list?
   */
  svector drawables;
  //Possibly use a linked list to use multible VBOS.
  //or use a dynamic array in the scene
  //gl_VBO *next;
};
/*
  Doesn't have a pointer to a scene since uniforms aren't necessarly
  bound to a single scene.
*/
struct gl_uniform_block {
  const char *name;
  /* Data associted with the uniform buffer, this can be NULL, or
     smaller than the buffer, in fact, this generally isn't used*/
  void *data;
  int data_size;
  /* Index of the buffer used for uniform variables */
  GLuint buffer;
  int buffer_size;
  /* Location in the program of the uniform block */
  int loc;
  /* index in uniform buffer array at which this uniform buffer is located */
  int array_index;
  int refcount;
};
//this is the number of matrices, not bytes
//essentially this is the max nesting depth for composite objects
//this is currently restricted to the maximum value use_stack can hold
static const size_t default_matrix_stack_size = 255;
//the first 16*32 bytes are for the model matrix
static const int scene_transform_projection_offset = 16*sizeof(float);
static const int scene_transform_view_offset = 32*sizeof(float);
static const int scene_transform_scene_offset = 48*sizeof(float);
struct gl_scene {
  global_context *ctx;
  void *scene_userdata;
  void(*update_scene)(gl_scene*,void*);
  union {
    float proj_view_scene[48];//useful for updating uniform blocks
    struct {
      float projection[16];
      float view[16];
      float scene[16];
    };
  };
  //this needs to become an array at some point
  gl_VBO VBO;
  //uniform block containing the projection, model, scene, and view matrices
  gl_uniform_block transform;
  //array containing all other uniform blocks
  gl_uniform_block **uniforms;
  gl_camera *camera;
  float *matrix_stack;
  float *stack_ptr;
  size_t matrix_stack_sz;
  char *name;//Optional
  int num_uniform_blocks;
  GLuint program;
  int scene_index;
  uint8_t is_active;
  uint8_t use_stack;
  uint16_t padding;
};
#define ORTHOGRAPHIC_PROJECTION 0
#define PERSPECTIVE_PROJECTION 1

struct gl_camera {
  union {
    gl_position view[3];
    struct {
      gl_position eye;
      gl_position center;
      gl_position up;
    };
  };
  union {
    float proj[6];
    struct {
      float left;
      float right;
      float bottom;
      float top;
      float near;
      float far;
    };
  };
  int proj_type;//0 = ortho, 1 = perspective
  int refcount;
};
/*
  Functions to make the different structures, these allocate (and zero) the struct
  and initialize a few basic parts, most of the initializion is left up to the
  client program.
*/
/*
  Allocate and zero objects only
*/
global_context *allocate_global_context();
gl_scene *allocate_gl_scene();
//other shape creation functions are in shapes.h
gl_shape *allocate_gl_shape();
/*
  Allocates space for a global context, creates a window and sets the
  userdata pointer, the rest of the initialization is upto the program.
*/
global_context *make_global_context(int window_width, int window_height,
                                    const char *name);
void* context_allocate_userdata(global_context *ctx, size_t sz,
                               void *update_userdata);
void context_init_userdata(global_context *ctx, void *userdata,
                           void *update_userdata);
gl_scene** context_allocate_scenes(global_context *ctx, int num_scenes);
/*
  Allocates a gl_scene, compiles a program using the given vertex and
  fragment shader sources (which are strings containing the literal source)
  and initializes the VAO.
*/
gl_scene *make_scene_from_program(GLuint program);
gl_scene *make_simple_scene(const char *vertex_shader,
                            const char *fragment_shader);
gl_scene *make_simple_scene_from_files(const char *vertex_shader,
                                       const char *fragment_shader);
gl_drawable **scene_get_drawables(gl_scene *scene, int *num_drawables);
//relies on the user knowing the order of the drawables in the scene
gl_drawable *scene_get_drawable(gl_scene *scene, int idx);
gl_drawable *scene_get_drawable_by_name(gl_scene *scene, const char *name);

/*
  These funcitons all set scene->uniforms to a newly allocated array
    of 'num_uniforms' uniform block pointers

  init_uniforms creates & allocateds uniform blocks using names and sizes.
  add_uniforms simply copies uniforms into the scene
  bind_uniforms sets the location field of each uniform based on
    the scene's program
*/
void scene_init_uniforms(gl_scene *scene, char **names,
                         int *sizes, int num_uniforms);
void scene_add_uniforms(gl_scene *scene, gl_uniform_block **uniforms,
                        int num_uniforms);
void scene_bind_uniforms(gl_scene *scene, gl_uniform_block **uniforms,
                         int num_uniforms);
void scene_bind_camera(gl_scene *scene, gl_camera *camera);

#define scene_update_camera(s,c) scene_bind_camera(s,c)
gl_camera *make_gl_camera(gl_position eye, gl_position center,
                          gl_position up, float boundries[6], int proj);
static inline
gl_camera *make_gl_camera_ortho(gl_position eye, gl_position center,
                                gl_position up, float boundries[6]){
  return make_gl_camera(eye, center, up, boundries, ORTHOGRAPHIC_PROJECTION);
}
static inline
gl_camera *make_gl_camera_frustum(gl_position eye, gl_position center,
                                  gl_position up, float boundries[6]){
  return make_gl_camera(eye, center, up, boundries, PERSPECTIVE_PROJECTION);
}
  
static inline
gl_camera *make_gl_camera_perspective(gl_position eye, gl_position center,
                                      gl_position up, float fov, float aspect,
                                      float near, float far){
  float top = near * tan(fov * M_PI / 360.0);
  float right = top * aspect;
  float boundries[6] = {-right, right, -top, top, near, far};
  return make_gl_camera_frustum(eye, center, up, boundries);
}  
gl_uniform_block* make_uniform_block(const char *name, int size,
                                     void *data, int data_size);
/*
  (Potentially) allocate space for the scene's userdata, and set
  the update scene function
*/
void scene_allocate_userdata(gl_scene *scene, size_t sz,
                             void *update_scene);
void init_vbo(gl_VBO *vbo, const gl_vertex_attrib* attribs, int num_attribs);
#define SCENE_ADD_DRAWABLES(scene,...)                  \
  ({gl_drawable *drawables[] = {__VA_ARGS__};           \
    scene_add_drawables(scene, drawables,               \
                        __NARG__(__VA_ARGS__));})
//get the svector containing the drawables in the given scene
#define SCENE_GET_DRAWABLES(scene) ((scene)->VBO.drawables)
void scene_add_drawable(gl_scene *scene, gl_drawable *drawable);
void scene_add_drawables(gl_scene *scene,
                         gl_drawable **drawable, int num_drawables);
/* void gl_scene_add_drawables_va(gl_scene *scene, */
/*                                gl_shape **drawable, int num_drawables); */
#define init_scene_uniform_block(scene, name, size)                     \
  init_uniform_block(&scene->uniforms, scene->program, name, size)
void init_uniform_block(gl_uniform_block *block, GLuint program,
                        char *name, int size);
//modifides the data store in the data field of block
int update_uniform_block_data(gl_uniform_block *block, void *data,
                              int offset, int size);
//syncs the data field of block with the gpu
void sync_uniform_block(gl_uniform_block *block);
//updates the uniform buffer on the gpu, without changing the
//data field of block
void update_uniform_block(gl_uniform_block *block, void *data,
                         int offset, int size);

void bind_scene(gl_scene *scene);
void __attribute__((noreturn)) gl_main_loop(global_context *ctx);
void __attribute__((noreturn)) 
gl_main_loop_w_callbacks(global_context *ctx,
                         void(*scene_callback)(gl_scene*),
                         void(*drawable_callback)(gl_drawable*));
void gl_main_loop_once(global_context *ctx);
void gl_main_loop_internal(global_context *ctx);
/*
  these functions seperate the rendering and event processing of the main loop,
  to allow them to be done in seperate threads.
  they bind the thread local variable thread_context the ctx argument.
  
*/
void __attribute__((noreturn)) gl_main_loop_render(global_context *ctx);
void __attribute__((noreturn)) gl_main_loop_event(global_context *ctx);
/*
  Initialize the window manager independent part of the opengl context, a
  context must already exist when this is called
*/
void init_glew(void);
/*
  some trivial functions to help with C++ compatibility, since
  writing explicit casts to function pointers is ugly
*/
static inline void set_update_userdata(global_context *ctx,
                                       void *update_userdata){
  ctx->update_userdata = (void(*)(void*))update_userdata;
}
static inline void set_update_scene(gl_scene *scene,
                                    void *update_scene){
  scene->update_scene = (void(*)(gl_scene*,void*))update_scene;
}
/*
  A struct representing the default opengl camera
*/
static struct gl_camera default_camera = {
  .eye = POSITION(0,0,0),
  .center = POSITION(0,0,-1),
  .up = POSITION(0,1,0),
  .left = -1, .right = 1,
  .bottom = -1, .top = 1,
  .near = -1, .far = 1,
  .proj_type = ORTHOGRAPHIC_PROJECTION
};
#ifdef __cplusplus
}
#endif
#endif /* __BASIC_LOOP_H__ */
