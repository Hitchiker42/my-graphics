#include "gl_text.h"
/*
  TODO: Move texture related stuff to seperate file.
*/
typedef struct gl_font_vertex gl_font_vertex;
struct gl_font {
  //freetype library and font face info
  FT_Library ft;
  FT_Face face;
  gl_color color;
  GLuint program;
  //currently unused but will be necessary when we start to use other textures
  int    texture_id;
  GLuint tex;//texture
  GLuint vbo;
  GLuint vao;
};
/*
  A single vec4, with an xy position and a uv texture coordinate
*/
struct gl_font_vertex {
  float x;
  float y;
  float u;
  float v;
};
static const char *vertex_shader =
  "#version 330 core\n"
  "layout(location = 0) in vec4 pos;\n"
  "uniform vec4 ucolor;\n"//only used in fragement shader
  "out vec2 tex_coord;\n"
  "void main(){\n"
  "  gl_Position = vec4(pos.x,pos.y,0,1);\n"
  "  tex_coord = vec2(pos.z, pos.w);\n"
  "}\n";
static const char *fragment_shader =
  "#version 330 core\n"
  "uniform vec4 ucolor;\n"
  "uniform sampler2D tex;\n"
  "in vec2 v_tex_coord;\n"
  "out vec4 f_color;\n"
  "void main(){\n"
  /*
    The tutorial I'm following says to only use an alpha value for the font color,
    which does seem to make sense.
  */
  "  f_color = vec4(1,1,1,texture(tex, tex_coord).r)*ucolor;\n"
  "}\n";
//I'll make this more flexable later
gl_font* ft_init(void){
  gl_font *font = zmalloc(sizeof(gl_font));
  /*
    Initialize freetype + font
   */
  int err = FT_Init_FreeType(&font->ft);
  if(err){
    fprintf(stderr, "Freetype initialization error (error no. %d)\n", err);
    return NULL;
  }
  err = FT_New_Face(font->ft, "unifont.ttf", 0, &font->face);
  if(err){
    fprintf(stderr, "Freetype font load error (error no. %d)\n",err);
    return NULL;
  }
  //set font size to 16x16
  FT_Set_Pixel_Sizes(font->face, 0, 16);
  /*
    Initialize texture + vao + vbo + program.
  */
  font->program = create_shader_program(vertex_shader, fragment_shader);
  //this will need to change when we start to use other textures
  glActiveTexture(GL_TEXTURE0 + font->texture_id);
  glGenTextures(1, &font->tex);
  glBindTexture(GL_TEXTURE_2D, &font->tex);
  GLint tex_loc = glGetUniformLocation(font->program, "tex");
  glUniform1i(tex_loc, font->texture_id);
  /*
    Set some sampling parameters for the texture to make text look nicer
  */
  //Use linear interpolation
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  //clamp texture coordinates to [0,1]
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  //Tell openGL that the texure is only aligned to byte boundries
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  return 0;
}
/*
  A quick note on freetype types, font sizes are stored in 1/64th of a pixel,
  using a 26.6 fixed point type. This means to convert a freetype size into
  a pixel we shift right by 6.
*/
/*
  This takes time relative to the number of characters in the font. In general
  it is very slow, as it needs to render each glyph. If you know the font you want
  you should call this from a seperate thread at the start of the program to avoid
  a performance hit.


struct character_info {
  //number of pixels to advance
  uint16_t ax;
  uint16_t ay;

  ///bitmap height and width
  uint16_t bw;
  uint16_t bh;

  //top left coordinate of bitmap
  uint16_t bl;
  uint16_t bt;

  //offset (in bytes) from the start of the bitmap
  uint32_t offset;
};

struct font_atlas {
  uint8_t *bitmap;
  character_info *char_info;
  gl_font *font_info;
  uint32_t bitmap_size;
  uint32_t num_characters;
};
*/
//just a function to set all the fields of a char_info struct
static inline void set_char_info(character_info *ci, int ax, int ay,
                                 int bw, int bh, int bl, int bt, uint32_t offset){
  ci->ax = ax; ci->ay = ay;
  ci->bw = bw; ci->bh = bh;
  ci->bl = bl; ci->bt = bt;
  ci->offset = offset;
  return;
}
struct font_atlas* generate_font_atlas(FT_Face face, 
                                       struct font_atlas *atlas){
  FT_GlyphSlot g = face->glyph;
  uint32_t w = 0, h = 0, i;
  long num_glyphs = face->num_glyphs;
  atlas = zmalloc(sizeof(font_atlas));
  atlas->font = face;
  atlas->char_info = xmalloc(num_glyphs * sizeof(struct character_info));
  character_info *ci = atlas->char_info;//save some typing
  /*
    For a bitmap font creating a font atlas is almost trivial, we just iterate
    through the font copying the characters to the bitmap and the glyph
    parameters to the char_info field.

    A scalable font (i.e ttf, oft, basically any modern font) is a more
    complicated because of font hinting, basically altering the character so
    it looks right at the size it's being rendered. This means any scalable
    font has glyphs with varying size, so we can't eaisly pre-allocate a bitmap.

    There are two ways we can do this, with a tradeoff between speed and memory,
    we can compute the maximum possible glyph size, which is 1 row and column
    of pixels larger than what the font metrics specify. This will almost
    certainly give us a bit map that is much too large, but we'll only have to
    do one loop. The alternative is to do a loop to compute the bitmap size
    then allocate it, and then loop again to fill it up.
   */

  if(FT_IS_SCALABLE(face)){
    //the slower, but more memory efficent and accurate way.
    for(i=0;i<num_glyphs;i++){
      //hopefully freetype can cache the bitmap, but who knows
      if(FT_Load_Glyph(face, i, FT_LOAD_RENDER)){continue;}
      w += g->bitmap.width;
      h = MAX(h, g->bitmap.rows);
    }
    //Round to nearest power of 2?
    atlas->bitmap = zmalloc(w*h);
    /*
      faster, less accurate
      w_max = (face_size.max_advance>>6)+1;
      h_max = (face_size.height>>6)+1;
      //this is almost certainly way too big
      atlas->bitmap = zmalloc(w_max*h_max*num_glyphs);
     */
  } else {
    //bitmap font, this is easy
    FT_Size_Metrics face_size = face->size->metrics;
    atlas->bitmap = zmalloc(face_size.height * 
                             face_size.max_advance * num_glyphs);
    h = face_size.height;
  }
  for(i=0, w=0;i<num_glyphs;i++){
    if(FT_Load_Glyph(face, i, FT_LOAD_RENDER)){
      fprintf(stderr, "Failed to load glyph #%u\n", i);
      continue;
    }
    set_char_info(ci + i, g->advance.x >> 6, g->advance.y >> 6,
                  g->bitmap.width, g->bitmap.rows,
                  g->bitmap_left, g->bitmap_top, h*w);
    memcpy(atlas->bitmap + w*h, g->bitmap.buffer,
           g->bitmap.rows*g->bitmap.width);
    w += g->bitmap.width;
  }
  return atlas;
}
//TODO: Define a proper texture type to return from this
/*
GLuint font_atlas_to_texture(font_atlas *atlas){
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  */
/*
  Turn a 2D array of pixels into a texture
  glTexImage2D(target = GL_TEXTURE_2D, level = 0,
               internalFormat = GL_RED (for text), GL_RGBA(otherwise)
               width, height, border = 0(always),
               format = GL_RED(text), GL_RGBA|GL_BGRA(otherwise)
               type = GL_UNSIGNED_BYTE(text) GL_UNSIGNED_INT_8_8_8_8[_REV] (otherwise)
               data)

*/
