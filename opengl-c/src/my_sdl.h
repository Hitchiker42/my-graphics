#ifndef __MY_SDL_H__
#define __MY_SDL_H__
#ifdef __cplusplus
extern "C" {
#endif
#include <SDL2/SDL.h>
/*
  These next function pointers have default versions, but can be
  overridden by user defined versions.
*/
typedef SDL_Window* gl_window;
/*
  The default function just prints the type of event
*/
extern void sdl_handle_event(SDL_Event *);
/*
  By default just calls sdl_handle_event for each event
*/
extern void sdl_handle_events(SDL_Event **, int);
/*
  Called whenever there's an error in an sdl function, arguments are
  the type of error, and the call that generated the error, both
  as strings.
*/
extern void sdl_handle_error(char *,char *);

char *sdl_event_to_string(SDL_Event *event);
gl_window init_sdl_gl_context(int w, int h, const char *name);

static inline void gl_swap_buffers(gl_window win){
  SDL_GL_SwapWindow(win);
}

#ifdef __cplusplus
}
#endif
#endif /* __MY_SDL_H__ */
