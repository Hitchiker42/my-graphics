#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
thread_local global_context *thread_context;

int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "basic test");
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;
  gl_scene *scene = make_simple_scene(basic_vertex, basic_fragment);
  ctx->scenes[0] = scene;
  mat4_rotateX(scene->scene, M_PI/4, NULL);
  gl_drawable *oct = make_colored_default_convex_polygon(9,0.5,
                                                         gl_color_red);
  gl_position oct_translate = POSITION(0.5, 0.5, 0);
  mat4_translate(oct->transform, (float*)&oct_translate, NULL);
  gl_drawable *tetra = make_colored_default_tetrahedron(0.5, gl_color_white);
  gl_drawable *cube = make_colored_default_cube(0.5, (gl_color){0x7fff00ff});
//  gl_drawable *octa = make_colored_default_octahedron(gl_color_cyan);
  gl_drawable *elements[2] = {oct, tetra};
  gl_drawable *composite = make_composite_drawable(elements, 2,
                                                   COMPOSITE_NONE);
  gl_drawable *elements2[] = {composite, cube};
  gl_drawable *composite2 = make_composite_drawable(elements, 2,
                                                    COMPOSITE_NONE);
  gl_position trans = POSITION(0.25, 0.5, -0.25);
  mat4_translate(composite2->transform, (float*)&trans, NULL);
  //  gl_drawable *axes = init_line_obj(axes_vertices, 6);
  //  set_draw_mode(axes, GL_LINES);
  gl_drawable *drawables[] = {cube, composite, composite2};
  scene_add_drawables(scene, drawables, 3);
  gl_check_error();
  gl_main_loop(ctx);
  return 0;
}
