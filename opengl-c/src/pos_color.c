#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
#include "interaction.h"
char *gl_shape_to_string(gl_shape *shape, char *buf);
/*
  This should remain fairly uncomplicated, it draws a single shape,
  and has the ability to rotate the scene, and move the shape.  
  
  It also draws axes and a 3d grid to help show depth
*/
thread_local global_context *thread_context;
static gl_keystate transform_keybindings[25] =
  {{.keycode = GLFW_KEY_RIGHT},{.keycode = GLFW_KEY_LEFT},//translate x
   {.keycode = GLFW_KEY_UP},{.keycode = GLFW_KEY_DOWN},//translate y
   {.keycode = GLFW_KEY_UP, .modmask = GLFW_MOD_SHIFT},
   {.keycode = GLFW_KEY_DOWN, .modmask = GLFW_MOD_SHIFT}, //translate z
   //rotate
   {.keycode = 'x'}, {.keycode = 'x', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'c'}, {.keycode = 'c', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'z'}, {.keycode = 'z', .modmask = GLFW_MOD_CONTROL},
   //scale width, height, depth
   {.keycode = 'w'}, {.keycode = 'w', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'h'}, {.keycode = 'h', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'd'}, {.keycode = 'd', .modmask = GLFW_MOD_CONTROL},
   //shear ',', '.', '/' because why not
   {.keycode = ','}, {.keycode = ',', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '.'}, {.keycode = '.', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '/'}, {.keycode = '/', .modmask = GLFW_MOD_CONTROL},
   //reset
   {.keycode = 'r'}};
gl_position y_equals_x(float x){
  gl_position ret = POSITION(x,x,0,1);
  return ret;
}
gl_color gradient(int i){
  //this is cheating a bit, since it's using info from make_sphere
  const int step = 10*M_PI;
  double max = 5*step;
  int intensity = (i/max) * 255;
  gl_color ret;
  ret.g = ret.r = ret.b = intensity;
  ret.a = 0xff;
  return ret;
}
double fxy(double x, double y){
  //  return sin(x*2*M_PI)/2+cos(z*2*M_PI)/2;
  return (-cos(y*M_PI)+cos(x*M_PI))/2;
  //return signbit(x)*(x*x + y*y);
  //return (x+y)/2;
}
gl_color gen_color(gl_position p){
  gl_color ret;  
  if(p.z < 0){
    ret.b = ((-p.z))*0xff;
    ret.g = 0xff - ret.b;
    ret.r = 0;
  } else {
    ret.r = ((p.z))*0xff;
    ret.g = 0xff-ret.r;
    ret.b = 0;
  }
  ret.a = 0xff;
  return ret;
  
}
static gl_drawable *init_drawable(){
  gl_drawable *x = make_colored_default_cube(0.25, gl_color_white);
  //gl_drawable *x = make_colored_sphere(0.5, gl_color_white);
  //gl_drawable *x = make_colored_default_cube(0.05, gl_color_red);
 
  return x;
  /*  gl_drawable **drawables = xmalloc(1000*sizeof(gl_drawable*));
  int i, j;
  gl_position offset = POSITION(0,0,0);
  for(i=0;i<100;i++){
    offset.x = (i-50)*0.1;
    for(j=0;j<10;j++){
      offset.y = (j-5)*0.1;
      drawables[i*100 + j] = copy_gl_drawable(x);
      mat4_translate(drawables[i*100 +j]->transform, offset.vec, NULL);
    }
  }
  gl_drawable *ret = init_composite_drawable(drawables, 1000, COMPOSITE_INIT_NONE);
  return ret;
  //    gl_position trans = POSITION(0,0,-.75);
    //    translate_shape(x, &trans);
  //gl_drawable *x = make_cylinder(0.25, 0.5, gradient);
  //gl_drawable *x = make_surface_pos_color(-1.0,1.0,100,
  //-2.0,2.0,100, fxy, gen_color);
  return x;
  //    gl_position offset = POSITION(0.25,0.25,0.25,0);
  //    mat4_translate(x->transform, offset.vec, x->transform);
  //gl_drawable *x = make_colored_default_tetrahedron(0.5, gl_color_blue);
//  gl_drawable *x = make_colored_default_octahedron(0.5, gl_color_blue);
  return x;
/*  gl_drawable *primitives[4];
  primitives[0] = make_colored_default_cube(0.5,gl_color_transparent_cyan);
  primitives[1] = make_colored_default_tetrahedron(0.5,gl_color_transparent_yellow);
  primitives[2] = make_colored_default_octahedron(0.5,gl_color_transparent_magenta);
  primitives[3] = make_colored_sphere(0.5, gl_color_transparent_white);
  gl_position offsets[4] = {{0.25,0.25,0},{-0.25,0.25,0},
                         {0.25,-0.25,0},{-0.25, -0.25, 0}};
  int i;
  for(i=0;i<4;i++){
    mat4_translate(primitives[i]->transform, (float*)&offsets[i], NULL);
  }
  gl_drawable *composite = init_composite_drawable(primitives, 4,
                                                   COMPOSITE_INIT_NONE);
                                                   return composite;*/
}
static const int grid_step = 20;
gl_position grid_gen(float x, float y, float z){
  float d = z/20.0;
  gl_position ret = POSITION(x+d,y+d,z);
  return ret;
}
gl_color grid_color(int i){
  int z = i/(grid_step*grid_step);
  int y = (i%(grid_step*grid_step))/grid_step;
  int x = (i%(grid_step*grid_step))%grid_step;
  gl_color ret = {.r = z*(0xff/grid_step),
                  .g = y*(0xff/grid_step),
                  .b = x*(0xff/grid_step), .a = 0xff};

  return ret;
}

void update_scene(gl_scene *s, void *data){
  interactive_transform_update(data);
}
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "simple test");
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;
  gl_scene *scene = make_simple_scene(pos_color_vertex, pos_color_fragment);
  window_set_keymap(ctx->window, default_keymap);
  interactive_transform *trans = 
    make_interactive_transform(0.05, M_PI/60, 0.05, 0.05, 
                               transform_keybindings, default_keymap);
  ctx->userdata = trans;
  //These 2 transformations are the default
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  //  mat4_ortho(-1,1,-1,1,-1,1,scene->projection);
  
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  //mat4_perspective(135.0, 1, 0.25, 100, scene->projection);
  
  scene->update_scene = (void*)update_scene;
  ctx->scenes[0] = scene;

  /*
   */
  //  glClearColor(1,0,0,1);
  gl_check_error();
  gl_drawable *drawable = init_drawable();
    DEBUG_PRINTF(gl_shape_to_string(drawable, NULL));
  //  pthread_create(&ctx->thread_id, NULL, (void*)gl_main_loop_render, ctx);
    //  return;
  glEnable(GL_LINE_SMOOTH);
//  glLineWidth(2.0);
  gl_drawable *axes = make_colored_axes_drawable(gl_color_cyan,
                                                 gl_color_magenta,
                                                 gl_color_yellow);
  gl_check_error();
  /*  gl_drawable *grid = make_grid(-1.0, 1.0, -1.0, 1.0,
                                -1.0, 1.0, grid_gen,
                                grid_step, grid_step,
                                grid_step, grid_color);*/
  gl_check_error();
  transform_set_drawable(trans, drawable);
  glPointSize(2.0);  
  //  SCENE_ADD_DRAWABLES(scene, drawable, axes);//, grid);
  scene_add_drawable(scene, drawable);
  gl_check_error();
  glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  gl_main_loop(ctx);
}
