#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
#include "interaction.h"
#include "my_sdl.h"
#include <SDL2/SDL.h>
thread_local global_context *thread_context;
gl_color gradient(int i){
  //this is cheating a bit, since it's using info from make_sphere
  const int step = 10*M_PI;
  double max = 5*step;
  int intensity = (i/max) * 255;
  gl_color ret;
  ret.g = ret.r = ret.b = intensity;
  ret.a = 0xff;
  return ret;
}
static gl_drawable *init_drawable(){
    gl_drawable *x = make_sphere_indexed(0.5, gradient);
    return x;
}
static float alpha = 0.0;
static float dalpha = M_PI/20;
void update_scene(gl_scene *s, void *data){
  gl_shape *shape = data;
  alpha+=dalpha;
  mat4_rotateX((float*)const_id_matrix, alpha, shape->transform);
  mat4_rotateY(shape->transform, alpha/2, shape->transform);
}
int main(){ 
  global_context *ctx = zmalloc(sizeof(global_context));//zero it to be safe
  SDL_Window *win = init_sdl_gl_context(800, 800, "sdl test");
  ctx->window = (void*)win;
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;
  gl_scene *scene = make_simple_scene(basic_vertex, basic_fragment);
  ctx->scenes[0] = scene;
  scene->update_scene = update_scene;
  gl_drawable *drawable = init_drawable();
  ctx->userdata = drawable;
  scene_add_drawable(scene, drawable);
  sdl_main_loop(ctx);  
}
