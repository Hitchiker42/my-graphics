#include "common.h"  
/*
  Binds the data/vertex layout for vertex attribute loc to the
  current VAO
*/
void enable_vertex_attribs(GLuint buf, gl_vertex_attrib *attribs, int n){
  int i;
  glBindBuffer(GL_ARRAY_BUFFER, buf);
  for(i=0;i<n;i++){
    glEnableVertexAttribArray(attribs[i].location);
    glVertexAttribPointer(attribs[i].location, attribs[i].size, attribs[i].type,
                          attribs[i].normalized, attribs[i].stride,
                          (void*)attribs[i].offset);
  }
}
/*
  Creates a gl buffer and binds the data given in data to it.
*/
GLuint make_data_buffer(GLenum buffer_type, void *data,
                        size_t size, int usage){
  GLuint buffer;
  glGenBuffers(1,&buffer);
  glBindBuffer(GL_ARRAY_BUFFER, buffer);//make buffer current
  glBufferData(GL_ARRAY_BUFFER, size, data, usage);
  return buffer;
}
/*
GLuint font_atlas_to_texture(font_atlas *atlas){
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  */
/*
  Turn a 2D array of pixels into a texture
  glTexImage2D(target = GL_TEXTURE_2D, level = 0,
               internalFormat = GL_RED (for text), GL_RGBA(otherwise)
               width, height, border = 0(always),
               format = GL_RED(text), GL_RGBA|GL_BGRA(otherwise)
               type = GL_UNSIGNED_BYTE(text) GL_UNSIGNED_INT_8_8_8_8[_REV] (otherwise)
               data)

*/
gl_texture *allocate_gl_texture2D(int w, int h, int l, int comp){
  gl_texture *tex = xmalloc(sizeof(gl_texture));
  *tex = (gl_texture){.w = w, .h = h, .d = 0, .levels = l,
                      .comp = comp,
                      .filter_min = GL_NEAREST_MIPMAP_LINEAR,
                      .filter_max = GL_LINEAR,
                      .wrap_s = GL_REPEAT,
                      .wrap_t = GL_REPEAT,
                      .wrap_r = GL_REPEAT,
                      .target = GL_TEXTURE_2D,
                      .idx = 0,
                      .refcount = 1};
  glGenTextures(1, &tex->id);
  glBindTexture(GL_TEXTURE_2D, tex->id);
  gl_check_error();
  /*
    Use the newer immutable texture storage functions if possible, but emulate
    the behavior of the new functions if necessary.
   */
  if(GL_ARB_texture_storage){
    glTexStorage2D(GL_TEXTURE_2D, l, sized_format_by_components[comp-1], w, h);
    gl_check_error();
  } else {
    int i;
    for(i=0;i<l;i++){
      glTexImage2D(GL_TEXTURE_2D, i, sized_format_by_components[comp-1],
                   (w>>i), (h>>i), 0, format_by_components[comp-1],
                   GL_UNSIGNED_BYTE, NULL);
      gl_check_error();
    }
  }
  return tex;
}
gl_texture *gen_texture_from_bitmap(uint8_t *data, int w, int h,
                                   int l, int comp){
  gl_texture *tex = allocate_gl_texture2D(w,h,l,comp);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h,
                  format_by_components[comp-1], GL_UNSIGNED_BYTE, data);
  gl_check_error();
  glGenerateMipmap(GL_TEXTURE_2D);
  return tex;
}
gl_texture *gen_texture_from_image(uint8_t *data, int size, int level){
  int x, y, comp;
  uint8_t *buf = stbi_load_from_memory(data, size, &x, &y, &comp, 0);
  if(!buf){return NULL;}
  return gen_texture_from_bitmap(buf, x, y, level, comp);
}
  
gl_texture *gen_texture_from_file(const char *file, int level){
  int x, y, comp;
  uint8_t *buf = stbi_load(file, &x, &y, &comp, 0);
  if(!buf){return NULL;}
  return gen_texture_from_bitmap(buf, x, y, level, comp);
}

int __attribute__((const)) gl_type_size(GLenum type){
  switch(type){
    case GL_UNSIGNED_BYTE:
    case GL_BYTE:
      return sizeof(uint8_t);
    case GL_UNSIGNED_SHORT:
    case GL_SHORT:
      return sizeof(uint16_t);
    case GL_UNSIGNED_INT:
    case GL_INT:
    case GL_FLOAT:
    case GL_FIXED:
      return sizeof(uint32_t);
    case GL_DOUBLE:
      return sizeof(uint64_t);
  }
  __builtin_unreachable();
}
gl_color gl_rand_color(int __attribute__((unused)) dummy){
  gl_color ret = {lrand48()};
  return ret;
}
mat4_t randomize_transform(mat4_t trans){
  gl_position rpos = rand_pos();
  mat4_translate((float*)const_id_matrix, rpos.vec, trans);
  rpos = rand_pos();
  float rangle = 2*M_PI*drand48();
  mat4_rotate(trans, rangle, rpos.vec, trans);
  return trans;
}
  
void init_glew(void){
  glewExperimental = GL_TRUE;
  int err = glewInit();
  glGetError();
  if(err != GLEW_OK){
    fprintf(stderr, "Error, glew init failure\n");
    exit(EXIT_FAILURE);
  }
  if(!(GLEW_VERSION_3_3)){
    fprintf(stderr, "Error, OpenGL 3.3 not supported\n");
    exit(EXIT_FAILURE);
  }
  /*
    Enable antialising
   */
  if(GLEW_ARB_multisample){
    glEnable(GL_MULTISAMPLE);
  }
  /*
    This makes dealing with primitive restarts much eaiser,
    the primitive restart index is set to the maximum of
    whatever type is being used for indices (i.e 0xff for a byte,
    0xffff for a short,etc...). Meaning the restart index doesn't
    need to be changed for different types.
   */
  /*
  if(GLEW_ARB_ES3_compatibility){
    glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  } else {
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(0xff);
  }
  */
  /*
    Enable basic depth testing
   */
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  /*
    Enable basic color blending
  */
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#if (defined DEBUG) && (defined __linux__)
  enable_backtraces();
#endif
  srand48(nano_time());
  return;
}
