#include "common.h"
#include "context.h"
#include "drawable.h"
#include "interaction.h"
/*
  There are a lot of values for GLEnum, and creating strings for all of them
  would be a huge waste of memory, gl_enum_to_string.c only defines functions
  to convert user specified (via macros) types of enums.
*/
#define __GL_SHADER_TYPE_TO_STRING__
#define __GL_ERROR_CODE_TO_STRING__
#define __GL_PRIMITIVE_TYPE_TO_STRING__
#include "gl_enum_to_string.c"
/*
  Functions to format various types to strings.
  Functions take an optional buffer to output to, and allocate memory
  if a buffer isn't given.
*/
char *gl_position_to_string(gl_position pos, char *buf){
  if(buf == NULL){
    buf = xmalloc(gl_component_str_buf_sz);
  }
  snprintf(buf, gl_component_str_buf_sz, "(% .5f, % .5f, % .5f)",
           pos.x, pos.y, pos.z);
  return buf;
}
char *gl_normal_to_string(gl_normal norm, char *buf){
  /*
    This prints the normal as it's integer components
   */
  if(buf == NULL){
    buf = xmalloc(gl_component_str_buf_sz);
  }
  snprintf(buf, gl_component_str_buf_sz, "(%d, %d, %d)",
           norm.nx, norm.ny, norm.nz);
  return buf;
}
char *gl_tex_coord_to_string(gl_tex_coord coord, char *buf){
  //convert normalized unsigned 16 bit int to float in the range [0.0,1.0]
  const unsigned int coord_max = (1<<16)-1;
  float u,v;
  u = coord.u/coord_max;
  v = coord.v/coord_max;
  if(buf == NULL){
    buf = xmalloc(gl_component_str_buf_sz);
  }
  snprintf(buf, gl_component_str_buf_sz, "(%.5f, %.5f)", u, v);
  return buf;
}
char *gl_color_to_string(gl_color color, char *buf){
  if(buf == NULL){
    buf = xmalloc(gl_component_str_buf_sz);
  }
  snprintf(buf, gl_component_str_buf_sz, "(%.2hhx, %.2hhx, %.2hhx, %.2hhx)",
           color.r, color.g, color.b, color.a);
  return buf;
}
char *gl_vertex_to_string(gl_vertex vert, char *buf, int num_components){
  if(num_components > 4 || num_components < 1){
    return NULL;
  }
  if(buf == NULL){
    buf = xmalloc(gl_vertex_str_buf_sz);
  }
  char *components = alloca(gl_vertex_str_buf_sz);
  gl_position_to_string(vert.pos, components);
  int offset = snprintf(buf, gl_vertex_str_buf_sz,
                        "(position = %s", components);
  if(num_components < 2){goto END;}
  components += gl_component_str_buf_sz;
  gl_color_to_string(vert.color, components);
  offset += snprintf(buf+offset, gl_vertex_str_buf_sz-offset,
                     ", color = %s", components);
  if(num_components < 3){goto END;}
  components += gl_component_str_buf_sz;
  gl_normal_to_string(vert.normal, components);
  offset += snprintf(buf+offset, gl_vertex_str_buf_sz-offset,
                     ",\n normal = %s", components);
  if(num_components < 4){goto END;}
  components += gl_component_str_buf_sz;
  gl_tex_coord_to_string(vert.tex_coord, components);
  offset += snprintf(buf+offset, gl_vertex_str_buf_sz-offset,
                     ", texture coordinates = %s", components);
  //could add a ')' to the above string and return here
 END:
  snprintf(buf+offset, gl_vertex_str_buf_sz - offset, ")");
  return buf;
}

char* gl_camera_to_string(gl_camera *cam){
  const char *fmt = "eye    = (%f, %f, %f)\n"
                    "center = (%f, %f, %f)\n"
                    "up     = (%f, %f, %f)\n"
                    "left = %f, right = %f\n"
                    "bottom = %f, top = %f\n"
                    "near = %f, far = %f\n"
                    "proj = %d\n";
  char *buf;
  asprintf(&buf, fmt,
           cam->eye.x, cam->eye.y, cam->eye.z,
           cam->center.x, cam->center.y, cam->center.z,
           cam->up.x, cam->up.y, cam->up.z,
           cam->left, cam->right,
           cam->bottom, cam->top,
           cam->near, cam->far, cam->proj_type);
  return buf;
}
//char *polygon_to_string(gl_polygon *poly){
static int format_vertices(gl_vertices *vertices,
                           int start, int end,
                           char *buf, int sz){
  end = MIN(end, vertices->num_vertices);
  int offset = 0, i;
  gl_vertex *verts = vertices->vertices;
  for(i=0;i<end-start;i++){
    gl_position pos = verts[i+start].pos;
    if(!(i % 3) && i != 0){//put a newline after every 3 positions
      offset += snprintf(buf+offset, sz - offset, "\n");
    }
    offset += snprintf(buf+offset, sz - offset, "(% .9f, % .9f, % .9f)",
                       pos.x, pos.y, pos.z);
  }
  return offset;
}
//This needs to be a macro since I concatenate other string literals to it
#define drawable_format_string                                \
  "Printing Drawable:\nscene = %p\nvbo = %p\ntransform =\n%s"   \
  "\nnum %s = %d\nvbo_offset = %u\nsize = %u\n"
char *gl_shape_to_string(gl_shape *shape, char *buf){
  static char *static_buf = NULL;
  if(static_buf){
    free(static_buf);
  }
  if(buf == NULL){
    buf = static_buf;
  }
  char *transform_buf = mat4_str(shape->transform);
  //far more than enough space to store 10 positions
  int vertex_buf_sz = 512;
  char *vertex_buf = alloca(vertex_buf_sz);
  gl_vertices *vertices = shape->vertices;
  /*
    Print The first and last 6 vertices of the shape, or all of
    the vertices if the shape has less than 12.
  */
  if(vertices->num_vertices < 12){
    format_vertices(vertices, 0, vertices->num_vertices,
                    vertex_buf, vertex_buf_sz);
  } else {
    char *temp_buf = vertex_buf;
    int temp_sz = vertex_buf_sz;
    int used = format_vertices(vertices, 0, 6,
                                     temp_buf, temp_sz);
    temp_buf += used; temp_sz -= used;
    used = snprintf(temp_buf, temp_sz, "\n...\n");
    temp_buf += used; temp_sz -= used;
    int start = vertices->num_vertices - 6, end = vertices->num_vertices;
    format_vertices(vertices, start, end,
                          temp_buf, temp_sz);
  }
  int draw_count = MAX(vertices->num_vertices,
                       (shape->indices ? shape->indices->num_indices : 0));
  int sz = asprintf(&buf, drawable_format_string
                    "draw_count = %u\ndraw_mode = %s\n"
                    "vertices =\n%s\n",
                    shape->scene, shape->vbo, transform_buf,
                    "vertices", vertices->num_vertices,
                    vertices->vbo_offset, VERTICES_SIZE(vertices),
                    draw_count, draw_mode_to_string(shape->draw_mode),
                    vertex_buf);
  free(transform_buf);
  return buf;
}
/*
char *generic_drawable_to_string(gl_drawable *drawable){
  char *buf;
  char *transform_buf = mat4_str(drawable->transform);  
  int sz = asprintf(&buf, drawable_format_string,
                    drawable->scene, drawable->vbo, transform_buf,
                    "components", drawable->num_components,
                    *drawable->vbo_offset, drawable->size,
                    drawable_type_to_string(drawable->type));
  free(transform_buf);
  return buf;
  }*/
char *drawable_to_string(gl_drawable *drawable){
  if(IS_COMPOSITE(drawable)){
    return "Composite Drawable";
  } else {
    return gl_shape_to_string(AS_SHAPE(drawable), NULL);
  }
}
char *keystate_to_string(gl_keystate k){
  char *buf;
  int nbytes = asprintf(&buf, "keycode, char, index = (%d, '%c', %u)\n"
                        "modmask = %s\n", k.keycode, k.keycode,
                        KEY_TO_KEYMAP_INDEX(k.keycode),
                        format_binary_int(k.modmask, 4));
  return buf;
}
char *keypress_closures_to_string(keypress_closures *kc){
  char *buf;
  int nbytes = asprintf(&buf, "default = %p\nshift = %p\n"
                        "control = %p\nalt = %p\n",
                        kc->default_callback, kc->shift_callback,
                        kc->ctrl_callback, kc->alt_callback);
  return buf;
}
#define WM_KEY(key) CAT(GLFW_KEY_,key)
#define GET_KEYNAME_CASE(key)                   \
  case WM_KEY(key):                             \
    *s = #key; return 2;
int gl_get_keyname(int key, char *c, char **s){
  if(isgraph(key)){
    *c = key;
    return 1;
  }
  switch(key){
    GET_KEYNAME_CASE(UNKNOWN);
    GET_KEYNAME_CASE(SPACE);
    GET_KEYNAME_CASE(ESCAPE);
    GET_KEYNAME_CASE(ENTER);
    GET_KEYNAME_CASE(TAB);
    GET_KEYNAME_CASE(BACKSPACE);
    GET_KEYNAME_CASE(INSERT);
    GET_KEYNAME_CASE(DELETE);
    GET_KEYNAME_CASE(RIGHT);
    GET_KEYNAME_CASE(LEFT);
    GET_KEYNAME_CASE(DOWN);
    GET_KEYNAME_CASE(UP);
    GET_KEYNAME_CASE(PAGE_UP);
    GET_KEYNAME_CASE(PAGE_DOWN);
    GET_KEYNAME_CASE(HOME);
    GET_KEYNAME_CASE(END);
    GET_KEYNAME_CASE(CAPS_LOCK);
    GET_KEYNAME_CASE(SCROLL_LOCK);
    GET_KEYNAME_CASE(NUM_LOCK);
    GET_KEYNAME_CASE(PRINT_SCREEN);
    GET_KEYNAME_CASE(PAUSE);
    GET_KEYNAME_CASE(F1);
    GET_KEYNAME_CASE(F2);
    GET_KEYNAME_CASE(F3);
    GET_KEYNAME_CASE(F4);
    GET_KEYNAME_CASE(F5);
    GET_KEYNAME_CASE(F6);
    GET_KEYNAME_CASE(F7);
    GET_KEYNAME_CASE(F8);
    GET_KEYNAME_CASE(F9);
    GET_KEYNAME_CASE(F10);
    GET_KEYNAME_CASE(F11);
    GET_KEYNAME_CASE(F12);
    GET_KEYNAME_CASE(F13);
    GET_KEYNAME_CASE(F14);
    GET_KEYNAME_CASE(F15);
    GET_KEYNAME_CASE(F16);
    GET_KEYNAME_CASE(F17);
    GET_KEYNAME_CASE(F18);
    GET_KEYNAME_CASE(F19);
    GET_KEYNAME_CASE(F20);
    GET_KEYNAME_CASE(F21);
    GET_KEYNAME_CASE(F22);
    GET_KEYNAME_CASE(F23);
    GET_KEYNAME_CASE(F24);
    GET_KEYNAME_CASE(F25);
    GET_KEYNAME_CASE(KP_0);
    GET_KEYNAME_CASE(KP_1);
    GET_KEYNAME_CASE(KP_2);
    GET_KEYNAME_CASE(KP_3);
    GET_KEYNAME_CASE(KP_4);
    GET_KEYNAME_CASE(KP_5);
    GET_KEYNAME_CASE(KP_6);
    GET_KEYNAME_CASE(KP_7);
    GET_KEYNAME_CASE(KP_8);
    GET_KEYNAME_CASE(KP_9);
    GET_KEYNAME_CASE(KP_DECIMAL);
    GET_KEYNAME_CASE(KP_DIVIDE);
    GET_KEYNAME_CASE(KP_MULTIPLY);
    GET_KEYNAME_CASE(KP_SUBTRACT);
    GET_KEYNAME_CASE(KP_ADD);
    GET_KEYNAME_CASE(KP_ENTER);
    GET_KEYNAME_CASE(KP_EQUAL);
    GET_KEYNAME_CASE(SHIFT_L);
    GET_KEYNAME_CASE(CTRL_L);
    GET_KEYNAME_CASE(ALT_L);
    GET_KEYNAME_CASE(SUPER_L);
    GET_KEYNAME_CASE(SHIFT_R);
    GET_KEYNAME_CASE(CTRL_R);
    GET_KEYNAME_CASE(ALT_R);
    GET_KEYNAME_CASE(SUPER_R);
    GET_KEYNAME_CASE(MENU);
    default:
      return 3;
  }
}
