#include "common.h"
#include "gl_toolkit.h"
/*
  IDEA: Divide this into seperate files for glut and glfw?
*/
/*
  Toolkit independent initalization code, specifically glew and
  various opengl features.
*/
static inline void init_glew(void){
  glewExperimental = GL_TRUE;
  int err = glewInit();
  glGetError();
  if(err != GLEW_OK){
    fprintf(stderr, "Error, glew init failure\n");
    exit(EXIT_FAILURE);
  }
  if(!(GLEW_VERSION_3_3)){
    fprintf(stderr, "Error, OpenGL 3.3 not supported\n");
    exit(EXIT_FAILURE);
  }
  /*
    Enable antialising
   */
  if(GLEW_ARB_multisample){
    glEnable(GL_MULTISAMPLE);
  }
  /*
    This makes dealing with primitive restarts much eaiser,
    the primitive restart index is set to the maximum of
    whatever type is being used for indices (i.e 0xff for a byte,
    0xffff for a short,etc...). Meaning the restart index doesn't
    need to be changed for different types.
   */
  if(GLEW_ARB_ES3_compatibility){
    glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  } else {
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(0xff);
  }
  /*
    Enable basic depth testing
   */
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  /*
    Enable basic color blending
  */
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  srand48(nano_time());
  return;
}
#if 1
struct glfw_userdata {
  gl_key_callback key_callback;
  gl_button_callback button_callback;
  void *userdata;
};
/*
  Basic error handling function to help diganose glfw errors.
*/
static void handle_error(int err_code, const char *err_string){
  fprintf(stderr,"Glfw error %d:\n%s\n",err_code, err_string);
  return;
}
void gl_set_window_should_close(gl_window win, int value){
  return glfwSetWindowShouldClose(win, value);
}
int gl_window_should_close(gl_window window){
  return glfwWindowShouldClose(window);
}
void gl_swap_buffers(gl_window win){
  glfwSwapBuffers(win);
}
void gl_poll_events(){
  glfwPollEvents();
}
void gl_set_window_userdata(gl_window window, void *userdata){
  struct glfw_userdata *data = glfwGetWindowUserPointer(window);
  data->userdata = userdata;
}
void* gl_get_window_userdata(gl_window window){
  struct glfw_userdata *data = glfwGetWindowUserPointer(window);
  return data->userdata;
}
static void glfw_key_callback(gl_window window, int key, int scancode,
                              int action, int modmask){
  struct glfw_userdata *data = glfwGetWindowUserPointer(window);
  //we need to get the mouse coordinates ourselves for glfw
  double x,y;
  glfwGetCursorPos(window, &x, &y);
  return data->key_callback(window, key, action, modmask, x, y);
}
static void glfw_button_callback(gl_window window, int button,
                                 int action, int modmask){
  struct glfw_userdata *data = glfwGetWindowUserPointer(window);
  //we need to get the mouse coordinates ourselves for glfw
  double x,y;
  glfwGetCursorPos(window, &x, &y);
  data->button_callback(window, button, action, modmask, x, y);
  return;
}

gl_mouse_pos_callback gl_set_mouse_pos_callback(gl_window window,
                                                gl_mouse_pos_callback cb){
  return glfwSetCursorPosCallback(window, cb);
}
gl_key_callback gl_set_key_callback(gl_window window,
                                    gl_key_callback callback){
  struct glfw_userdata *data = glfwGetWindowUserPointer(window);
  gl_key_callback old_callback = data->key_callback;
  if(old_callback == NULL){
    //We only need to set this once
    glfwSetKeyCallback(window, glfw_key_callback);
  }
  data->key_callback = callback;
  return old_callback;
}
gl_button_callback gl_set_button_callback(gl_window window,
                                          gl_button_callback callback){
  struct glfw_userdata *data = glfwGetWindowUserPointer(window);
  gl_button_callback old_callback = data->button_callback;
  if(old_callback == NULL){
    //We only need to set this once
    glfwSetMouseButtonCallback(window, glfw_button_callback);
  }
  data->button_callback = callback;
  return old_callback;
}
gl_window init_gl_context(int w, int h, const char* name){
  //set error callback first to allow logging initialization errors
  glfwSetErrorCallback(handle_error);
  if(!glfwInit()){
    fprintf(stderr, "Error, failed to initialize glfw\n");
    exit(EXIT_FAILURE);
  }
//  atexit(glfwTerminate);
  glfwWindowHint(GLFW_SAMPLES, 4);//4xAA
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
  GLFWwindow *win = glfwCreateWindow(w, h, name, NULL, NULL);
  if(!win){
    fprintf(stderr, "Error creating window\n");
    exit(EXIT_FAILURE);
  }
  glfwMakeContextCurrent(win);
  init_glew();
  struct glfw_userdata *data = zmalloc(sizeof(struct glfw_userdata));
  glfwSetWindowUserPointer(win, data);
  glfwSwapInterval(1);
  return win;
}
#else //GLUT
struct glut_userdata {
  gl_key_callback key_callback;
  gl_button_callback button_callback;
  gl_mouse_pos_callback pos_callback;
  int should_close;
  void *userdata;
};
/*
  Since glut uses the global value of the current window implicitly,
  we need to temporally make a window current to get/set values for it.
*/
//CPP macro overloading is a bit clunky, but it does work
//unhygenic macro since it creates a binding for __old_window
//CPP macros are a far cry from lisp macros, but they're better than
//nothing (try doing this in java)
#define glut_window_begin_w_data(win,dataptr)           \
  gl_window __old_window = glutGetWindow();             \
  if(win != __old_window){                              \
    glutSetWindow(win);                                 \
  }                                                     \
  struct glut_userdata *dataptr = glutGetWindowData();
#define glut_window_begin_wo_data(win)          \
  gl_window __old_window = glutGetWindow();     \
  if(win != __old_window){                      \
    glutSetWindow(win);                         \
  }
#define glut_window_begin(...)                          \
  GET_MACRO(2, __VA_ARGS__, glut_window_begin_w_data,   \
            glut_window_begin_wo_data)(__VA_ARGS__)
//relies on the binding for __old_window set by glut_window_begin
#define glut_window_end(win)                    \
  if(win != __old_window){                      \
    glutSetWindow(__old_window);                \
  }
//it's kinda weird that glut gives you a va_list
static void handle_error(const char *fmt, va_list args){
  fputs("Glut Error:\n",stderr);
  vfprintf(stderr,fmt,args);
  return;
}
static void handle_warning(const char *fmt, va_list args){
  fputs("Glut Warning:\n",stderr);
  vfprintf(stderr,fmt,args);
  return;
}
void gl_set_window_should_close(gl_window win, int value){
  glut_window_begin(win, data);
  data->should_close = value;
  glut_window_end(win)
}
int gl_window_should_close(gl_window window){
  glut_window_begin(window, data);
  int ret = data->should_close;
  glut_window_end(window);
  return ret;
}
void gl_swap_buffers(gl_window win){
  glut_window_begin(win);
  glutSwapBuffers();
  glutPostRedisplay();
  glut_window_end(win);
}
void gl_poll_events(){
  glutMainLoopEvent();
}
void gl_set_window_userdata(gl_window window, void *userdata){
  glut_window_begin(window, data);
  data->userdata = userdata;
  glut_window_end(window);
}
void* gl_get_window_userdata(gl_window window){
  glut_window_begin(window, data);
  void *userdata = data->userdata;
  glut_window_end(window);
  return data;
}
/*
  We need 4 callbacks for glut to implement our callback
*/
static inline int glut_translate_key(unsigned char c){
  int key = c;
  //check if the character is printable
  if(c < ' ' || c == 0x7F){
    switch(c){
      case '\b':
        key = GL_key_backspace; break;
      case '\t':
        key = GL_key_tab; break;
      case '\r':
        key = GL_key_return; break;
      case '\x1b':
        key = GL_key_escape; break;
      case '\x7b':
        key = GL_key_delete; break;
      default:
        return GL_key_unknown;
    }
  }
  return c;
}
static void glut_keyboard_callback(unsigned char c, int x, int y){
  gl_window win = glutGetWindow();
  struct glut_userdata *data = glutGetWindowData();
  int modmask = glutGetModifiers();
  int key = glut_translate_key(c);
  if(c != (unsigned char)GL_key_unknown){
    return data->key_callback(win, key, GL_action_press, modmask, 
                              (double)x, (double)y);
  }
}
static void glut_keyboard_up_callback(unsigned char c, int x, int y){
  gl_window win = glutGetWindow();
  struct glut_userdata *data = glutGetWindowData();
  int modmask = glutGetModifiers();
  int key = glut_translate_key(c);
  if(c != (unsigned char)GL_key_unknown){
    return data->key_callback(win, key, GL_action_release, modmask, 
                              (double)x, (double)y);
  }
}
const int glut_fkey_offset = GL_KEY_F1 - GLUT_KEY_F1;
static inline int glut_translate_special(int c){
  //This only works so long as the values of GLUT_KEY_FN are
  //sequential, they are now, and I can't imagine that changing
  if(c >= GLUT_KEY_F1 && c <= GLUT_KEY_F12){
    return c + glut_fkey_offset;
  }
  switch(c){
    case GLUT_KEY_LEFT:
      return GL_KEY_LEFT;
    case GLUT_KEY_RIGHT:
      return GL_KEY_RIGHT;
    case GLUT_KEY_UP:
      return GL_KEY_UP;
    case GLUT_KEY_DOWN:
      return GL_KEY_DOWN;
    case GLUT_KEY_PAGE_UP:
      return GL_KEY_PAGE_UP;
    case GLUT_KEY_PAGE_DOWN:
      return GL_KEY_PAGE_DOWN;
    case GLUT_KEY_HOME:
      return GL_KEY_HOME;
    case GLUT_KEY_END:
      return GL_KEY_END;
    case GLUT_KEY_INSERT:
      return GL_KEY_INSERT;
      //freeglut extensions
    case GLUT_KEY_NUM_LOCK:
      return GL_KEY_NUMLOCK;
    case GLUT_KEY_SHIFT_L:
      return GL_KEY_SHIFT_L;
    case GLUT_KEY_SHIFT_R:
      return GL_KEY_SHIFT_R;
    case GLUT_KEY_CTRL_L:
      return GL_KEY_CTRL_L;
    case GLUT_KEY_CTRL_R:
      return GL_KEY_CTRL_R;
    case GLUT_KEY_ALT_L:
      return GL_KEY_ALT_L;
    case GLUT_KEY_ALT_R:
      return GL_KEY_ALT_R;
    default:
      return GL_KEY_UNKNOWN;
  }
}
static void glut_special_callback(int c, int x, int y){
  gl_window win = glutGetWindow();
  struct glut_userdata *data = glutGetWindowData();
  int modmask = glutGetModifiers();
  int key = glut_translate_special(c);
  if(c != GL_key_unknown){
    return data->key_callback(win, key, GL_action_press, modmask, 
                              (double)x, (double)y);
  }
}
static void glut_special_up_callback(int c, int x, int y){
  gl_window win = glutGetWindow();
  struct glut_userdata *data = glutGetWindowData();
  int modmask = glutGetModifiers();
  int key = glut_translate_special(c);
  if(c != GL_key_unknown){
    return data->key_callback(win, key, GL_action_release, modmask,
                              (double)x, (double)y);
  }
}

static void glut_mousebutton_callback(int button, int action, int x, int y){
  gl_window win = glutGetWindow();
  struct glut_userdata *data = glutGetWindowData();
  int modmask = glutGetModifiers();
  //glut values for press and release are swapped
  action = (action == GLUT_UP ? GL_ACTION_RELEASE : GL_ACTION_PRESS);
  //ditto for right and middle buttons
  if(button == GL_MOUSE_BUTTON_MIDDLE){
    button = GL_MOUSE_BUTTON_RIGHT;
  } else if(button == GL_MOUSE_BUTTON_RIGHT){
    button = GL_MOUSE_BUTTON_MIDDLE;
  }
  return data->button_callback(win, button, action, modmask, 
                               (double)x, (double)y);
}
void glut_mouse_pos_callback(int x, int y){
  gl_window win = glutGetWindow();
  struct glut_userdata *data = glutGetWindowData();
  data->pos_callback(win, (double)x, (double)y);
}

gl_key_callback gl_set_key_callback(gl_window window,
                                    gl_key_callback callback){
  glut_window_begin(window, data);
  gl_key_callback old_callback = data->key_callback;
  if(old_callback == NULL){
    glutKeyboardFunc(glut_keyboard_callback);
    glutSpecialFunc(glut_special_callback);
    glutKeyboardUpFunc(glut_keyboard_up_callback);
    glutSpecialUpFunc(glut_special_up_callback);
  }
  data->key_callback = callback;
  glut_window_end(window);
  return old_callback;
}
gl_button_callback gl_set_button_callback(gl_window window,
                                          gl_button_callback callback){
  glut_window_begin(window, data);
  gl_button_callback old_callback = data->button_callback;
  if(old_callback == NULL){
    glutMouseFunc(glut_mousebutton_callback);
  }
  data->button_callback = callback;
  glut_window_end(window);
  return old_callback;
}
gl_mouse_pos_callback gl_set_mouse_pos_callback(gl_window window,
                                                gl_mouse_pos_callback cb){
  glut_window_begin(window, data);
  gl_mouse_pos_callback old_callback = data->pos_callback;
  if(old_callback == NULL){
    glutMotionFunc(glut_mouse_pos_callback);
    glutPassiveMotionFunc(glut_mouse_pos_callback);
  }
  data->pos_callback = cb;
  glut_window_end(window);
  return old_callback;
}
gl_window init_gl_context(int w, int h, const char* name){
  /*
    Call these before glutInit to allow printing errors caused by
    failed initialization. This is perfectly legal to do.
  */
  DEBUG_PRINTF("Using glut\n");
  glutInitErrorFunc(handle_error);
  glutInitWarningFunc(handle_warning);
  int dummy_argc = 0;
  glutInit(&dummy_argc,0);//need to pass a non-null pontier as first arg
  glutInitContextVersion(3,3);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
  glutInitContextProfile(GLUT_CORE_PROFILE);
  glutSetOption(GLUT_MULTISAMPLE, 4);//4xAA
//  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
//                GLUT_ACTION_GLUTMAINLOOP_RETURNS);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA |
                      GLUT_DEPTH | GLUT_MULTISAMPLE);
  glutInitWindowSize(w,h);
  int window = glutCreateWindow(name);
  glutSetWindow(window);
  glutDisplayFunc(glutPostRedisplay);
  atexit(glutExit);
//  glutCloseFunc(glut_set_window_should_close);
  init_glew();
  struct glut_userdata *userdata = zmalloc(sizeof(struct glut_userdata));
  glutSetWindowData(userdata);
  return window;
}
#endif

/*
  toolkit independent but it's long so it goes at the end
*/
int gl_get_keyname(enum gl_keyboard_key key, char *c, char **s){
  if(isgraph(key)){
    *c = key;
    return 1;
  }
  switch(key){
    case GL_KEY_UNKNOWN:
      *s = "unknown"; return 2;
    case GL_KEY_SPACE:
      *s = "space"; return 2;
    case GL_KEY_ESCAPE:
      *s = "escape"; return 2;
    case GL_KEY_ENTER:
      *s = "enter"; return 2;
    case GL_KEY_TAB:
      *s = "tab"; return 2;
    case GL_KEY_BACKSPACE:
      *s = "backspace"; return 2;
    case GL_KEY_INSERT:
      *s = "insert"; return 2;
    case GL_KEY_DELETE:
      *s = "delete"; return 2;
    case GL_KEY_RIGHT:
      *s = "right"; return 2;
    case GL_KEY_LEFT:
      *s = "left"; return 2;
    case GL_KEY_DOWN:
      *s = "down"; return 2;
    case GL_KEY_UP:
      *s = "up"; return 2;
    case GL_KEY_PAGE_UP:
      *s = "page_up"; return 2;
    case GL_KEY_PAGE_DOWN:
      *s = "page_down"; return 2;
    case GL_KEY_HOME:
      *s = "home"; return 2;
    case GL_KEY_END:
      *s = "end"; return 2;
    case GL_KEY_CAPS_LOCK:
      *s = "caps_lock"; return 2;
    case GL_KEY_SCROLL_LOCK:
      *s = "scroll_lock"; return 2;
    case GL_KEY_NUMLOCK:
      *s = "numlock"; return 2;
    case GL_KEY_PRINT_SCREEN:
      *s = "print_screen"; return 2;
    case GL_KEY_PAUSE:
      *s = "pause"; return 2;
    case GL_KEY_F1:
      *s = "f1"; return 2;
    case GL_KEY_F2:
      *s = "f2"; return 2;
    case GL_KEY_F3:
      *s = "f3"; return 2;
    case GL_KEY_F4:
      *s = "f4"; return 2;
    case GL_KEY_F5:
      *s = "f5"; return 2;
    case GL_KEY_F6:
      *s = "f6"; return 2;
    case GL_KEY_F7:
      *s = "f7"; return 2;
    case GL_KEY_F8:
      *s = "f8"; return 2;
    case GL_KEY_F9:
      *s = "f9"; return 2;
    case GL_KEY_F10:
      *s = "f10"; return 2;
    case GL_KEY_F11:
      *s = "f11"; return 2;
    case GL_KEY_F12:
      *s = "f12"; return 2;
    case GL_KEY_F13:
      *s = "f13"; return 2;
    case GL_KEY_F14:
      *s = "f14"; return 2;
    case GL_KEY_F15:
      *s = "f15"; return 2;
    case GL_KEY_F16:
      *s = "f16"; return 2;
    case GL_KEY_F17:
      *s = "f17"; return 2;
    case GL_KEY_F18:
      *s = "f18"; return 2;
    case GL_KEY_F19:
      *s = "f19"; return 2;
    case GL_KEY_F20:
      *s = "f20"; return 2;
    case GL_KEY_F21:
      *s = "f21"; return 2;
    case GL_KEY_F22:
      *s = "f22"; return 2;
    case GL_KEY_F23:
      *s = "f23"; return 2;
    case GL_KEY_F24:
      *s = "f24"; return 2;
    case GL_KEY_F25:
      *s = "f25"; return 2;
    case GL_KEY_KP_0:
      *s = "kp_0"; return 2;
    case GL_KEY_KP_1:
      *s = "kp_1"; return 2;
    case GL_KEY_KP_2:
      *s = "kp_2"; return 2;
    case GL_KEY_KP_3:
      *s = "kp_3"; return 2;
    case GL_KEY_KP_4:
      *s = "kp_4"; return 2;
    case GL_KEY_KP_5:
      *s = "kp_5"; return 2;
    case GL_KEY_KP_6:
      *s = "kp_6"; return 2;
    case GL_KEY_KP_7:
      *s = "kp_7"; return 2;
    case GL_KEY_KP_8:
      *s = "kp_8"; return 2;
    case GL_KEY_KP_9:
      *s = "kp_9"; return 2;
    case GL_KEY_KP_DECIMAL:
      *s = "kp_decimal"; return 2;
    case GL_KEY_KP_DIVIDE:
      *s = "kp_divide"; return 2;
    case GL_KEY_KP_MULTIPLY:
      *s = "kp_multiply"; return 2;
    case GL_KEY_KP_SUBTRACT:
      *s = "kp_subtract"; return 2;
    case GL_KEY_KP_ADD:
      *s = "kp_add"; return 2;
    case GL_KEY_KP_ENTER:
      *s = "kp_enter"; return 2;
    case GL_KEY_KP_EQUAL:
      *s = "kp_equal"; return 2;
    case GL_KEY_SHIFT_L:
      *s = "shift_l"; return 2;
    case GL_KEY_CTRL_L:
      *s = "ctrl_l"; return 2;
    case GL_KEY_ALT_L:
      *s = "alt_l"; return 2;
    case GL_KEY_SUPER_L:
      *s = "super_l"; return 2;
    case GL_KEY_SHIFT_R:
      *s = "shift_r"; return 2;
    case GL_KEY_CTRL_R:
      *s = "ctrl_r"; return 2;
    case GL_KEY_ALT_R:
      *s = "alt_r"; return 2;
    case GL_KEY_SUPER_R:
      *s = "super_r"; return 2;
    case GL_KEY_MENU:
      *s = "menu"; return 2;
    default:
      return 3;
  }
}
