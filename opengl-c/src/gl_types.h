/*
  Types for use in opengl code.
*/
#ifndef __MY_GL_TYPES_H__
#define __MY_GL_TYPES_H__
//#include <GL/glew.h>
#include "common.h"
/*
  GL code uses camel case so using a gl_ prefix should avoid
  namespace clashes. Since this is C structs have their own namespace
  so they have fairly simple names.
*/

typedef struct vertex gl_vertex;
typedef union  position gl_position;//xyzw
typedef union  color gl_color; //rgba, as bytes in a 32 bit int
typedef union  colorf gl_colorf; //rgba, as bytes in a 32 bit int
typedef union  normal gl_normal; //xyz, each 10 bits in a 32 bit int
typedef union  tex_coord gl_tex_coord;//16 bit uv texuture coordinates
typedef union  gl_index_t gl_index_t;
typedef struct vertex_attrib gl_vertex_attrib;
typedef struct gl_texture gl_texture;
//defined in C_util.h
typedef struct svector svector;//basic dynamic array
//defined in context.h
typedef struct gl_scene gl_scene;
typedef struct gl_VBO gl_VBO;
typedef struct global_context global_context;
typedef struct gl_uniform_block gl_uniform_block;
typedef struct gl_camera gl_camera;
//defined in drawable.h
typedef struct gl_vertices gl_vertices;
typedef struct gl_indices gl_indices;
typedef union gl_drawable gl_drawable;
typedef struct gl_shape gl_shape;
typedef struct gl_composite_drawable gl_composite;
typedef struct gl_composite_drawable gl_composite_drawable;
//defined in lighting.h
typedef struct material gl_material;
typedef struct light gl_light;
//has same layout as the glsl uniform used for lights
typedef struct u_light_block u_light_block;
// defined in interaction.h
typedef struct keystate gl_keystate;
typedef struct keypress_closure keypress_closure;
typedef struct mouse_button_closure mouse_button_closure;
typedef union keypress_closures keypress_closures;
typedef struct interactive_camera interactive_camera;
typedef struct interactive_transform interactive_transform;
/*
  I define a lot of unions to provide several ways to refer to the same
  data, this helps avoid casting, and makes code cleaner.
*/
//since this is a union literals of it require 2 sets of braces
//i.e gl_position p = {{x,y,z,w}}; which is ugly, so wrap that in a macro
//also overload the macro so I don't always need to set w to 1 or z to 0

//GL_POSITION is already taken
#define POSITION_2(x,y) {{x,y,0,1}}
#define POSITION_3(x,y,z) {{x,y,z,1}}
#define POSITION_4(x,y,z,w) {{x,y,z,w}}
#define POSITION(...) VFUNC(POSITION_, __VA_ARGS__)
#define POSITION_CAST(...) (gl_position)POSITION(__VA_ARGS__)
/* //Identical to position but with w component set to 0 */
/* #define VECTOR_2(x,y) {{x,y,0,0}} */
/* #define VECTOR_3(x,y,z) {{x,y,z,0}} */
/* #define VECTOR_4(x,y,z,w) {{x,y,z,w}} */

/* #define VECTOR(...) VFUNC(VECTOR_, __VA_ARGS__) */
/* #define VECTOR_CAST(...) (gl_position)VECTOR(__VA_ARGS__) */
union position {
  float vec[4];
  struct {
    float x;
    float y;
    float z;
    float w;
  };
};
union color {
  uint32_t argb;
  struct {
    uint8_t b;
    uint8_t g;
    uint8_t r;
    uint8_t a;
  };
};
union colorf {
  float rgba[4];
  struct {
    float r;
    float g;
    float b;
    float a;
  };
};
//This uses the opengl GL_UNSIGNED_INT_2_10_10_10_REV format, it
//stores 3 10 bit integers in 32 bits (+ 2 bits for whatever)
union normal {
  uint32_t normal;
  struct {
    signed int nx : 10;
    signed int ny : 10;
    signed int nz : 10;
    signed int tag : 2;//no real defined use
  };
};

union tex_coord {
  uint32_t uv;
  struct {
    union {
      uint16_t v;//depreciated
      uint16_t s;
    };
    union {
      uint16_t u;//depreciated
      uint16_t t;
    };
  };
};
/*
  This should be everything you might want to put in a vertex, it's
  an ugly definition, but makes access to different attributes easy.
*/
struct vertex {
  union {
    gl_position pos;
    struct {
      float x;
      float y;
      float z;
      float w;
    };
  };//128 bits
  union {
    union normal normal;
    struct {
      signed int nx : 10;
      signed int ny : 10;
      signed int nz : 10;
      signed int tag : 2;//no real defined use
    };
  };//160 bits
  union {
    uint32_t argb;
    union color color;
    struct {
      uint8_t b;
      uint8_t g;
      uint8_t r;
      uint8_t a;
    };
  };//192 bits
  union {
    union tex_coord tex_coord;
    struct {
      union {
        uint16_t v;//depricated
        uint16_t s;
      };
      union {
        uint16_t u;//depricated
        uint16_t t;
      };
    };
  };//224 bits
  //just in case we need to add another field, we can without
  //changing the size of the struct, also this aligns the struct
  //to an 8 byte boundry, so even if we didn't add it the compilier would.
  uint32_t padding;//256 bits
};//total size 256 bits / 32 bytes

union gl_index_t {
  uint32_t index32;
  uint16_t index16[2];
  uint8_t  index8[4];
};
struct vertex_attrib {
  GLuint location;
  GLuint size;
  GLenum type;
  int normalized;
  int stride;
  size_t offset;
};
/*
  Some useful positions
*/
static gl_position pos_origin = POSITION(0,0,0);
static gl_position pos_x_min = POSITION(-1,0,0);
static gl_position pos_x_max = POSITION(1,0,0);
static gl_position pos_y_min = POSITION(0,-1,0);
static gl_position pos_y_max = POSITION(0,1,0);
static gl_position pos_z_min = POSITION(0,0,-1);
static gl_position pos_z_max = POSITION(0,0,1);
static gl_position pos_xyz_min = POSITION(-1,-1,-1);
static gl_position pos_xyz_max = POSITION(1,1,1);

static gl_position pos_xyz_normal =
  POSITION(0.577350269,0.577350269,0.577350269,0);
static gl_position pos_neg_xyz_normal =
  POSITION(0.577350269,0.577350269,0.577350269,0);
/*
  It's assumed that the locations in the glsl program are given
  in the same order as the vertex struct.
*/
static const struct vertex_attrib position_attrib =
  {.location = 0, .size = 4, .type = GL_FLOAT, .normalized = 0,
   .stride = sizeof(gl_vertex), .offset = 0};
static const struct vertex_attrib normal_attrib =
  {.location = 1, .size = 4, .type = GL_UNSIGNED_INT_2_10_10_10_REV,
   .normalized = 1, .stride = sizeof(gl_vertex), .offset = offsetof(gl_vertex, normal)};
static const struct vertex_attrib color_attrib =
  {.location = 2, .size = GL_BGRA, .type = GL_UNSIGNED_BYTE, .normalized = 1,
   .stride = sizeof(gl_vertex), .offset = offsetof(gl_vertex, color)};
static const struct vertex_attrib tex_coord_attrib =
  {.location = 3, .size = 2, .type = GL_UNSIGNED_SHORT, .normalized = 1,
   .stride = sizeof(gl_vertex), .offset = offsetof(gl_vertex, tex_coord)};
//this needs to use the literal values rather than variables to
//ensure this is a compile time constant
static const struct vertex_attrib default_attribs[4] =
  {{.location = 0, .size = 4, .type = GL_FLOAT, .normalized = 0,
    .stride = sizeof(gl_vertex), .offset = 0},
   {.location = 1, .size = 4, .type = GL_UNSIGNED_INT_2_10_10_10_REV,
    .normalized = 1, .stride = sizeof(gl_vertex), .offset = offsetof(gl_vertex, normal)},
   {.location = 2, .size = GL_BGRA, .type = GL_UNSIGNED_BYTE, .normalized = 1,
    .stride = sizeof(gl_vertex), .offset = offsetof(gl_vertex, color)},
   {.location = 3, .size = 2, .type = GL_UNSIGNED_SHORT, .normalized = 1,
    .stride = sizeof(gl_vertex), .offset = offsetof(gl_vertex, tex_coord)}};
/* Some basic colors*/
static const gl_color gl_color_blue = {0xff0000ff};
static const gl_color gl_color_cyan = {0xff00ffff};
static const gl_color gl_color_green =  {0xff00ff00};
static const gl_color gl_color_magenta = {0xffff00ff};
static const gl_color gl_color_red = {0xffff0000};
static const gl_color gl_color_yellow = {0xffffff00};
static const gl_color gl_color_white = {0xffffffff};
static const gl_color gl_color_black = {0xff000000};

static const gl_color gl_color_transparent_blue = {0x7f0000ff};
static const gl_color gl_color_transparent_cyan = {0x7f00ffff};
static const gl_color gl_color_transparent_green =  {0x7f00ff00};
static const gl_color gl_color_transparent_magenta = {0x7fff00ff};
static const gl_color gl_color_transparent_red = {0x7fff0000};
static const gl_color gl_color_transparent_yellow = {0x7fffff00};
static const gl_color gl_color_transparent_white = {0x7fffffff};
static const gl_color gl_color_transparent_black = {0x7f000000};

#define GL_COLOR_BLUE  {0xff0000ff}
#define GL_COLOR_CYAN  {0xff00ffff}
#define GL_COLOR_GREEN   {0xff00ff00}
#define GL_COLOR_MAGENTA  {0xffff00ff}
#define GL_COLOR_RED  {0xffff0000}
#define GL_COLOR_YELLOW  {0xffff00ff}
#define GL_COLOR_WHITE  {0xffffffff}
#define GL_COLOR_GREY(i)  {CAT(0xff,CAT3(i,i,i))}
#define GL_COLOR_BLACK  {0xff000000}

#define GL_COLOR_BLUE_A(a)  {CAT3(0x, a, ff0000ff)}
#define GL_COLOR_CYAN_A(a)  {CAT3(0x, a, ff00ffff)}
#define GL_COLOR_GREEN_A(a)   {CAT3(0x,a, ff00ff00)}
#define GL_COLOR_MAGENTA_A(a)  {CAT3(0x,a,ffff00ff)}
#define GL_COLOR_RED_A(a)  {CAT3(0x,a,ffff0000)}
#define GL_COLOR_YELLOW_A(a)  {CAT3(0x,a,ffff00ff)}
#define GL_COLOR_WHITE_A(a)  {CAT3(0x,a,ffffffff)}
#define GL_COLOR_BLACK_A(a)  {CAT3(0x,a,ff000000)}

#endif
