#ifndef __GL_TOOLKIT_H__
#define __GL_TOOLKIT_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
/*
  This file provides a uniform interface to the different toolkits
  commonly used with open gl (currently only glfw and freeglut are
  supported). The idea is the same code can be used regardless
  of which library is used. The version of the functions used
  is decided at compile time, so switching the library used
  requires recompiling the application.

  Most of the interface is identical to glfw, so most of the
  work is done to make glut behave like glfw, not the other way
  around.
*/
//borrowed from glfw, and made into an enum
//enum values have a capatilized prefix and a equivlent macro
//which is all caps
enum gl_keyboard_key {
  GL_key_unknown = -1,
#define GL_KEY_UNKNOWN            -1
  /* Printable keys, enum value equal to ascii value of character */
  GL_key_space  = 0x20,
#define GL_KEY_SPACE              0x20
  GL_key_apostrophe = 0x27,
#define GL_KEY_APOSTROPHE         0x27  /* ' */
  GL_key_comma = 0x2c,
#define GL_KEY_COMMA              0x2c  /* , */
  GL_key_minus = 0x2d,
#define GL_KEY_MINUS              0x2d  /* - */
  GL_key_period = 0x2e,
#define GL_KEY_PERIOD             0x2e  /* . */
  GL_key_slash = 0x2f,
#define GL_KEY_SLASH              0x2f  /* / */
  GL_key_0 = 0x30,
#define GL_KEY_0                  0x30
  GL_key_1 = 0x31,
#define GL_KEY_1                  0x31
  GL_key_2 = 0x32,
#define GL_KEY_2                  0x32
  GL_key_3 = 0x33,
#define GL_KEY_3                  0x33
  GL_key_4 = 0x34,
#define GL_KEY_4                  0x34
  GL_key_5 = 0x35,
#define GL_KEY_5                  0x35
  GL_key_6 = 0x36,
#define GL_KEY_6                  0x36
  GL_key_7 = 0x37,
#define GL_KEY_7                  0x37
  GL_key_8 = 0x38,
#define GL_KEY_8                  0x38
  GL_key_9 = 0x39,
#define GL_KEY_9                  0x39
  GL_key_semicolon = 0x3b,
#define GL_KEY_SEMICOLON          0x3b  /* ; */
  GL_key_equal = 0x3d,
#define GL_KEY_EQUAL              0x3d  /* = */
  GL_key_a = 0x41,
#define GL_KEY_A                  0x41
  GL_key_b = 0x42,
#define GL_KEY_B                  0x42
  GL_key_c = 0x43,
#define GL_KEY_C                  0x43
  GL_key_d = 0x44,
#define GL_KEY_D                  0x44
  GL_key_e = 0x45,
#define GL_KEY_E                  0x45
  GL_key_f = 0x46,
#define GL_KEY_F                  0x46
  GL_key_g = 0x47,
#define GL_KEY_G                  0x47
  GL_key_h = 0x48,
#define GL_KEY_H                  0x48
  GL_key_i = 0x49,
#define GL_KEY_I                  0x49
  GL_key_j = 0x4a,
#define GL_KEY_J                  0x4a
  GL_key_k = 0x4b,
#define GL_KEY_K                  0x4b
  GL_key_l = 0x4c,
#define GL_KEY_L                  0x4c
  GL_key_m = 0x4d,
#define GL_KEY_M                  0x4d
  GL_key_n = 0x4e,
#define GL_KEY_N                  0x4e
  GL_key_o = 0x4f,
#define GL_KEY_O                  0x4f
  GL_key_p = 0x50,
#define GL_KEY_P                  0x50
  GL_key_q = 0x51,
#define GL_KEY_Q                  0x51
  GL_key_r = 0x52,
#define GL_KEY_R                  0x52
  GL_key_s = 0x53,
#define GL_KEY_S                  0x53
  GL_key_t = 0x54,
#define GL_KEY_T                  0x54
  GL_key_u = 0x55,
#define GL_KEY_U                  0x55
  GL_key_v = 0x56,
#define GL_KEY_V                  0x56
  GL_key_w = 0x57,
#define GL_KEY_W                  0x57
  GL_key_x = 0x58,
#define GL_KEY_X                  0x58
  GL_key_y = 0x59,
#define GL_KEY_Y                  0x59
  GL_key_z = 0x5a,
#define GL_KEY_Z                  0x5a
  GL_key_left_bracket = 0x5b,
#define GL_KEY_LEFT_BRACKET       0x5b  /* [ */
  GL_key_backslash = 0x5c,
#define GL_KEY_BACKSLASH          0x5c  /* \ */
  GL_key_right_bracket = 0x5d,
#define GL_KEY_RIGHT_BRACKET      0x5d  /* ] */
  GL_key_grave_accent = 0x60,
#define GL_KEY_GRAVE_ACCENT       0x60  /* ` */

  /* Function/Special keys */
/*
  /Some of these have nonprintable ascii representations, so
   are non 'special' in glut, but  glfw has a much saner way of
   dealing with keys so we follow them. There are also a lot of
   glfw specific keys which should perhaps be removed.
*/
  GL_KEY_ESCAPE = 0x100,    //0x1b ('\e' (nonstandard))
#define GL_key_escape 0x100
  GL_key_enter = 0x101,     //0x0d ('\r')
#define GL_KEY_ENTER              0x101
  GL_key_return = 0x101,     //0x0d ('\r')
#define GL_KEY_RETURN              0x101
  GL_key_tab = 0x102,       //0x09 ('\t')
#define GL_KEY_TAB                0x102
  GL_key_backspace = 0x103, //0x08 ('\b')
#define GL_KEY_BACKSPACE          0x103
  GL_key_insert = 0x104,
#define GL_KEY_INSERT             0x104
  GL_key_delete = 0x105,    //0x7f
#define GL_KEY_DELETE             0x105
  GL_key_right = 0x106,
#define GL_KEY_RIGHT              0x106
  GL_key_left = 0x107,
#define GL_KEY_LEFT               0x107
  GL_key_down = 0x108,
#define GL_KEY_DOWN               0x108
  GL_key_up = 0x109,
#define GL_KEY_UP                 0x109
  GL_key_page_up = 0x10a,
#define GL_KEY_PAGE_UP            0x10a
  GL_key_page_down = 0x10b,
#define GL_KEY_PAGE_DOWN          0x10b
  GL_key_home = 0x10c,
#define GL_KEY_HOME               0x10c
  GL_key_end = 0x10d,
#define GL_KEY_END                0x10d
//glfw only
  GL_key_caps_lock = 0x118,
#define GL_KEY_CAPS_LOCK          0x118
  GL_key_scroll_lock = 0x119,
#define GL_KEY_SCROLL_LOCK        0x119
  GL_key_num_lock = 0x11a, //freeglut extension
#define GL_KEY_NUMLOCK           0x11a
  GL_key_print_screen = 0x11b,
#define GL_KEY_PRINT_SCREEN       0x11b
  GL_key_pause = 0x11c,
#define GL_KEY_PAUSE              0x11c
//both again
  GL_key_f1 = 0x122,
#define GL_KEY_F1                 0x122
  GL_key_f2 = 0x123,
#define GL_KEY_F2                 0x123
  GL_key_f3 = 0x124,
#define GL_KEY_F3                 0x124
  GL_key_f4 = 0x125,
#define GL_KEY_F4                 0x125
  GL_key_f5 = 0x126,
#define GL_KEY_F5                 0x126
  GL_key_f6 = 0x127,
#define GL_KEY_F6                 0x127
  GL_key_f7 = 0x128,
#define GL_KEY_F7                 0x128
  GL_key_f8 = 0x129,
#define GL_KEY_F8                 0x129
  GL_key_f9 = 0x12a,
#define GL_KEY_F9                 0x12a
  GL_key_f10 = 0x12b,
#define GL_KEY_F10                0x12b
  GL_key_f11 = 0x12c,
#define GL_KEY_F11                0x12c
  GL_key_f12 = 0x12d,
#define GL_KEY_F12                0x12d
  GL_key_f13  = 0x12e,
//glfw only
#define GL_KEY_F13                0x12e
  GL_key_f14  = 0x12f,
#define GL_KEY_F14                0x12f
  GL_key_f15  = 0x130,
#define GL_KEY_F15                0x130
  GL_key_f16  = 0x131,
#define GL_KEY_F16                0x131
  GL_key_f17  = 0x132,
#define GL_KEY_F17                0x132
  GL_key_f18  = 0x133,
#define GL_KEY_F18                0x133
  GL_key_f19  = 0x134,
#define GL_KEY_F19                0x134
  GL_key_f20  = 0x135,
#define GL_KEY_F20                0x135
  GL_key_f21  = 0x136,
#define GL_KEY_F21                0x136
  GL_key_f22  = 0x137,
#define GL_KEY_F22                0x137
  GL_key_f23  = 0x138,
#define GL_KEY_F23                0x138
  GL_key_f24  = 0x139,
#define GL_KEY_F24                0x139
  GL_key_f25  = 0x13a,
#define GL_KEY_F25                0x13a
  GL_key_kp_0  = 0x140,
#define GL_KEY_KP_0               0x140
  GL_key_kp_1 = 0x141,
#define GL_KEY_KP_1               0x141
  GL_key_kp_2 = 0x142,
#define GL_KEY_KP_2               0x142
  GL_key_kp_3 = 0x143,
#define GL_KEY_KP_3               0x143
  GL_key_kp_4 = 0x144,
#define GL_KEY_KP_4               0x144
  GL_key_kp_5 = 0x145,
#define GL_KEY_KP_5               0x145
  GL_key_kp_6 = 0x146,
#define GL_KEY_KP_6               0x146
  GL_key_kp_7 = 0x147,
#define GL_KEY_KP_7               0x147
  GL_key_kp_8 = 0x148,
#define GL_KEY_KP_8               0x148
  GL_key_kp_9 = 0x149,
#define GL_KEY_KP_9               0x149
  GL_key_kp_decimal = 0x14a,
#define GL_KEY_KP_DECIMAL         0x14a
  GL_key_kp_divide = 0x14b,
#define GL_KEY_KP_DIVIDE          0x14b
  GL_key_kp_multiply = 0x14c,
#define GL_KEY_KP_MULTIPLY        0x14c
  GL_key_kp_subtract = 0x14d,
#define GL_KEY_KP_SUBTRACT        0x14d
  GL_key_kp_add = 0x14e,
#define GL_KEY_KP_ADD             0x14e
  GL_key_kp_enter = 0x14f,
#define GL_KEY_KP_ENTER           0x14f
  GL_key_kp_equal = 0x150,
#define GL_KEY_KP_EQUAL           0x150
//glfw + freeglut extensions
  GL_key_shift_l = 0x154,
#define GL_KEY_SHIFT_L         0x154
  GL_key_ctrl_l = 0x155,
#define GL_KEY_CTRL_L       0x155
  GL_key_alt_l = 0x156,
#define GL_KEY_ALT_L           0x156
  GL_key_left_super = 0x157,             //glfw only
#define GL_KEY_SUPER_L         0x157
  GL_key_shift_r = 0x158,
#define GL_KEY_SHIFT_R        0x158
  GL_key_control_r = 0x159,
#define GL_KEY_CTRL_R      0x159
  GL_key_alt_r = 0x15a,
#define GL_KEY_ALT_R          0x15a
//glfw only
  GL_key_super_r = 0x15b,
#define GL_KEY_SUPER_R        0x15b
  GL_key_menu = 0x180,
#define GL_KEY_MENU               0x180
  GL_key_last = GL_KEY_MENU,
#define GL_KEY_LAST               GL_KEY_MENU
};
enum GL_modifier_flag {
  GL_mod_shift = 0x1,
#define GL_MOD_SHIFT 0x1
  GL_mod_ctrl = 0x2,
#define GL_MOD_CTRL 0x2
  GL_mod_alt = 0x4,
#define GL_MOD_ALT 0x4
//glfw only
  GL_mod_super = 0x8
#define GL_MOD_SUPER  0x8
};
//compatable w/glfw
enum GL_input_action {
  GL_action_press = 1,
#define GL_ACTION_PRESS 1
  GL_action_release = 0,
#define GL_ACTION_RELEASE 0
  GL_action_repeat = 2
#define GL_ACTION_REPEAT 2
};
//again taken from glfw, but also identical to glut
enum GL_mouse_button {
  GL_mouse_button_1  = 0,
#define GL_MOUSE_BUTTON_1         0
  GL_mouse_button_2  = 1,
#define GL_MOUSE_BUTTON_2         1
  GL_mouse_button_3  = 2,
#define GL_MOUSE_BUTTON_3         2
  GL_mouse_button_4  = 3,
#define GL_MOUSE_BUTTON_4         3
  GL_mouse_button_5  = 4,
#define GL_MOUSE_BUTTON_5         4
  GL_mouse_button_6  = 5,
#define GL_MOUSE_BUTTON_6         5
  GL_mouse_button_7  = 6,
#define GL_MOUSE_BUTTON_7         6
  GL_mouse_button_8  = 7,
#define GL_MOUSE_BUTTON_8         7
  GL_mouse_button_last  = GL_MOUSE_BUTTON_8,
#define GL_MOUSE_BUTTON_LAST      GL_MOUSE_BUTTON_8
  GL_mouse_button_left  = GL_MOUSE_BUTTON_1,
#define GL_MOUSE_BUTTON_LEFT      GL_MOUSE_BUTTON_1
  GL_mouse_button_right  = GL_MOUSE_BUTTON_2,
#define GL_MOUSE_BUTTON_RIGHT     GL_MOUSE_BUTTON_2
  GL_mouse_button_middle  = GL_MOUSE_BUTTON_3
#define GL_MOUSE_BUTTON_MIDDLE    GL_MOUSE_BUTTON_3
};
//TODO: change positons to use doubles, rather than truncating
//doubles to ints.
/*
  Type of keyboard key callbacks, function signature should be:
  void callback(gl_window window, int key, int action, int modifiers,
                int x, int y);
  Takes a combination of the arguments accepted by glfw and glut callbacks,
  the functions to register callbacks take care of getting the necessary
  arguments depending on the toolkit being used.
*/
typedef void(*gl_key_callback)(gl_window, int, int, int, double, double);
/*
  type of mouse button press callbacks:
  void callback(gl_window window, int button, int action, int modifiers,
                int x, int y);
  Yes it is exactly the same as the key callback type.
*/
typedef void(*gl_button_callback)(gl_window, int, int, int, double, double);
//this is kind of a weird one, since glfw uses doubles for positons
//TODO: add an argument giving the state of mouse buttons
typedef void(*gl_mouse_pos_callback)(gl_window, double, double);
/*
  functions to control glut/glfw
*/
/*
  Initialize glut/glfw, create a window and opengl context, then
  initialize glew and insure opengl3.3 is supported
*/
gl_window init_gl_context(int w, int h, const char* name);
void gl_swap_buffers(gl_window window);
void gl_poll_events();
void gl_set_window_should_close(gl_window win, int value);
int gl_window_should_close(gl_window window);
//TODO:
//specify where to log errors/warnings, defaults to stderr-
//if given NULL as an argument disables logging errors
//void gl_set_log_file(FILE *out);
void gl_set_window_userdata(gl_window window, void *data);
void* gl_get_window_userdata(gl_window window);
/*
  This is equivlent to glfwSetKeyCallback, but for glut it combines
  glutKeyboardFunc, glutKeyboardUpFunc, glutSpecialFunc, glutSpecialUpFunc,
  into one callback.
*/
gl_key_callback gl_set_key_callback(gl_window window,
                                    gl_key_callback callback);
gl_button_callback gl_set_button_callback(gl_window window,
                                          gl_button_callback callback);
gl_mouse_pos_callback gl_set_mouse_pos_callback(gl_window window,
                                                gl_mouse_pos_callback cb);

#ifdef __cplusplus
}
#endif
#endif /* __GL_TOOLKIT_H__ */
