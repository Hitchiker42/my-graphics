/*
  So I'm sure I'm going to lose a bunch of points here, due to the
  whole not object oriented thing, but let me try to explain at least.

  I'm planning on writing most of the code which actually does work,
  i.e transformations, calculating tessellations, interacting with 
  opengl, system interaction, etc..., in C. There are too many things
  C can do which C++ can't, at least with the way I program. But I will
  (begrudgingly) implement the necessary object oriented stuff on top of
  that. That's what I was planning on doing for this program,
  but I didn't have enough time.

  There are a suprising number of incompatabilities between C and C++,
  including some features which C++ just doesn't support but C does 
  (namely restrict and designated initalizers). 

  As for constructing shapes, I'm still working on that (see shapes.c),
  but I don't have enough time to fully integrate shapes into my existing
  framework in time for this program, but I should have that done by
  the next program.
*/
#include "common.h"
#include "context.h"
#include "shapes.h"
/*
  Really generic shader, takes position, color and an arbitary
  transformation matrix.
*/
/*
  Each vertex has a color assoicated with it, but if use_ucolor is
  nonzero the uniform ucolor will be used instead.
*/
const char *vertex_shader =
  "#version 330 core\n"
  "layout(std140) uniform;\n"
  "layout(row_major) uniform;\n"
  "layout(location = 0) in vec3 position;\n"
  "layout(location = 1) in vec4 color;\n"
  "out vec4 v_color;\n"
  "uniform uniform_block {\n"
  "  uniform mat4 transform;\n"
  "  uniform vec4 ucolor;\n"
  "  uniform uint use_ucolor;\n"
  "};"
  "void main(){\n"
  "  gl_Position = vec4(position,1.0f);\n"
  "  gl_Position *= transform;\n"
  "  if(bool(use_ucolor)){\n"
  "    v_color = ucolor;\n"
  "  } else {\n"
  "    v_color = color;\n"
  "  }\n"
  "}\n";
/*
  Just sets the fragment's color based on the output from
  the vertex shader
*/
const char *fragment_shader =
  "#version 330 core\n"
  "in vec4 v_color;\n"
  "out vec4 color;\n"
  "void main(){\n"
  "  color = v_color;\n"
  "}\n";
struct userdata {
  float theta_1;
  float theta_2;
  int frame;
  mat4_t transform_0;
  mat4_t transform_1;
  mat4_t transform_2;
};
struct uniforms {
  float transform[16];
  float ucolor[4];
  uint32_t use_ucolor;
};
#define oct_color {0.1, 0.1, 0.4, 0.5}
#define pent_color {0.2, 0.05, 0.05, 0.5}
static gl_vertex shapes[] = 
  {{.pos = {-0.7, -0.5,-0.3}, .color = {1.0, 1.0, 0.0, 0.5}},
   {.pos = {-0.4, -0.5,-0.3}, .color = {0.0, 1.0, 1.0, 0.5}},
   {.pos = {-0.7, 0.0,-0.3}, .color = {1.0, 0.0, 1.0, 0.5}},
   {.pos = {0.5, 0.0,0.5}, .color = {0.0, 1.0, 1.0, 0.5}},
   {.pos = {0.3, 0.0,0.5}, .color = {1.0, 0.0, 1.0, 0.5}},
   {.pos = {0.5, 1.0,0.5}, .color = {1.0, 1.0, 0.0, 0.5}},
   //square (more like a diamond
   {.pos = {-0.5,  0, -0.1}, .color = {0,0,0,0.5}},
   {.pos = {0.0, 0.5, -0.1}, .color = {1.0, 1.0, 1.0, 0.5}},
   {.pos = {0.0, -0.5, -0.1}, .color = {1.0, 1.0, 1.0, 0.5}},
   {.pos = {0.5,  0, -0.1}, .color = {0,0,0,0.5}},
   //octahedron
   {.pos = {-0.7, 0.5,-0.3}, .color = oct_color},
   {.pos = {-0.5, 0.5,-0.3}, .color = oct_color},
   {.pos = {-0.3, 0.3,-0.3}, .color = oct_color},
   {.pos = {-0.3, 0.1,-0.3}, .color = oct_color},
   {.pos = {-0.5, -0.1,-0.3}, .color = oct_color},
   {.pos = {-0.7, -0.1,-0.3}, .color = oct_color},
   {.pos = {-0.9, 0.1,-0.3}, .color = oct_color},
   {.pos = {-0.9, 0.3,-0.3}, .color = oct_color},
   //lopsided pentagon
   {.pos = {0.5, -0.5, -0.2}, .color = pent_color},
   {.pos = {0.9, -0.75, -0.2}, .color = pent_color},
   {.pos = {0.7, -1.0, -0.2}, .color = pent_color},
   {.pos = {0.35, -1.0, -0.2}, .color = pent_color},
   {.pos = {0.2, -0.75, -0.2}, .color = pent_color}};
#define cube_color_1 {.r = 0.0, .g = 0.5, .b = 1.0, .a = 0.5}
#define cube_color_2 {.r = 0.0, .g = 1.0, .b = 0.5, .a = 0.5}
static gl_vertex cube[] =
  {{.pos = {-0.25, 0.25, 0.5}, .color = cube_color_1},
   {.pos = {0.25, 0.25, 0.5}, .color = cube_color_1},
   {.pos = {-0.25, -0.25, 0.5}, .color = cube_color_1},
   {.pos = {0.25, -0.25, 0.5}, .color = cube_color_1},
   {.pos = {-0.25, 0.25, -0.5}, .color = cube_color_2},
   {.pos = {0.25, 0.25, -0.5}, .color = cube_color_2},
   {.pos = {-0.25, -0.25, -0.5}, .color = cube_color_2},
   {.pos = {0.25, -0.25, -0.5}, .color = cube_color_2}};
uint8_t cube_indices[] =
  {0, 1, 2, 3, 6, 7, 4, 5, 0xff,
   7, 3, 5, 1, 4, 0, 6, 2};
uint8_t shape_indices[] =
  {0, 1, 2, 0xff, 
   3, 4, 5, 0xff,
   6, 7, 8, 9, 0xff,
   //0,1,7,2,6,3,5,4, all + 10
   10,11,17,12,16,13,15,14,0xff,
   //0,1,4,2,3 + 18
   18,19,22,20,21};
struct userdata *init_userdata(){
  struct userdata *ret = (struct userdata*)xmalloc(sizeof(struct userdata));
  ret->theta_1 = 0.0;
  ret->theta_2 = M_PI/4;
  ret->transform_0 = mat4_identity(NULL);
  ret->transform_1 = mat4_identity(NULL);
  ret->transform_2 = mat4_identity(NULL);
  ret->frame = 0;
  return ret;
}

void update_userdata(struct userdata *data){
  data->theta_1 = fmod(data->theta_1+2*M_PI/120, 2*M_PI);
  data->theta_2 = fmod(data->theta_2+2*M_PI/240, 2*M_PI);
  data->frame++;
  mat4_identity(data->transform_1);
  mat4_rotateX(data->transform_1, data->theta_1, NULL);
  mat4_rotateY(data->transform_1, data->theta_1, NULL);
  mat4_rotateZ(data->transform_1, data->theta_1, NULL);
/*
  Not acutally used, since it makes everything move in one giant
  blob, which looks awful
*/
  float s,c;
  sincosf(data->theta_2, &s, &c);
  float pos[3] = {s/2,c/2,0};
  mat4_identity(data->transform_2);
  mat4_translate(data->transform_2, pos, NULL);
  mat4_rotateZ(data->transform_2, data->theta_2, NULL);
}

void update_shape_buffer(gl_buffer *buf, struct userdata *data){
  bind_vertex_attrib(buf->array_buffer, 0, 3, GL_FLOAT, 0,
                     sizeof(struct vertex), 0);
  bind_vertex_attrib(buf->array_buffer, 1, 4, GL_FLOAT, 0,
                     sizeof(struct vertex), offsetof(struct vertex, color));
  //grab a pointer to the uniform buffer
  struct uniforms *u = buf->scene->uniform_block;
  memcpy(u->transform, data->transform_0, 16*sizeof(float));
  u->use_ucolor = 0;
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(struct uniforms), u);
}

gl_buffer *init_shape_buffer(gl_scene *scene){
  gl_buffer *buf = make_gl_buffer(shapes, sizeof(shapes)/sizeof(gl_vertex),
                                  sizeof(gl_vertex), GL_STATIC_DRAW,
                                  shape_indices, sizeof(shape_indices),
                                  GL_UNSIGNED_BYTE, GL_STATIC_DRAW);
  buf->draw_mode = GL_TRIANGLE_STRIP;
  set_update_buffer(buf, (void*)update_shape_buffer);
  buf->draw_count = sizeof(shape_indices)/sizeof(uint8_t);
  buf->scene = scene;
  return buf;
}

void update_cube_buffer(gl_buffer *buf, struct userdata *data){
  bind_vertex_attrib(buf->array_buffer, 0, 3, GL_FLOAT, 0,
                     sizeof(struct vertex), 0);
  bind_vertex_attrib(buf->array_buffer, 1, 4, GL_FLOAT, 0,
                     sizeof(struct vertex), offsetof(struct vertex, color));
  //grab a pointer to the uniform buffer
  struct uniforms *u = buf->scene->uniform_block;
  memcpy(u->transform, data->transform_1, 16*sizeof(float));
  u->use_ucolor = 1;
  if(!(data->frame % 10)){
    int i;
    for(i=0;i<4;i++){
      u->ucolor[i] =  drand48();
    }
  }
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(struct uniforms), u);
}

gl_buffer *init_cube_buffer(gl_scene *scene){
  gl_buffer *buf = make_gl_buffer(cube, sizeof(cube)/sizeof(gl_vertex),
                                  sizeof(gl_vertex), GL_STATIC_DRAW,
                                  cube_indices, sizeof(cube_indices), 
                                  GL_UNSIGNED_BYTE, GL_STATIC_DRAW);
  assert(buf->array_buffer != 0 && buf->index_buffer != 0);
  set_update_buffer(buf,(void*)update_cube_buffer);
  buf->draw_mode = GL_TRIANGLE_STRIP;
  buf->draw_count = sizeof(cube_indices)/sizeof(uint8_t);
  buf->scene = scene;
  return buf;
}
//logarithmic spiral
gl_position line_fun(float x){
  gl_position ret = {.x = exp(-0.1*x) * cos(x),
                     .y = exp(-0.1*x) * sin(x)};
  return ret;
}
void update_line_buffer(gl_buffer *buf, struct userdata *data){
  bind_vertex_attrib(buf->array_buffer, 0, 3, GL_FLOAT, 0,
                     sizeof(struct vertex), 0);
  bind_vertex_attrib(buf->array_buffer, 1, 4, GL_FLOAT, 0,
                     sizeof(struct vertex), offsetof(struct vertex, color));
  struct uniforms *u = buf->scene->uniform_block;
  memcpy(u->transform, data->transform_0, 16*sizeof(float));
  u->use_ucolor = 0;
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(struct uniforms), u);
}
gl_buffer *init_line_buffer(gl_scene *scene){
  int stride = sizeof(gl_vertex)/sizeof(float);
  int num_points = 500;
//this is how things should be created  
  gl_vertex *line = (gl_vertex*)make_line(0, 25.0, num_points, line_fun,
                                          sizeof(gl_vertex)/sizeof(float));
  int i;
  uint8_t *indices = (uint8_t*)xmalloc(num_points*sizeof(uint8_t));
  for(i=0;i<num_points;i++){
    indices[i] = i;
  }
  for(i=0;i<num_points;i++){
    line[i].color = (gl_color){drand48(),drand48(),drand48(),drand48()};
  }
  gl_buffer *buf = make_gl_buffer(line, num_points, sizeof(gl_vertex),
                                  GL_STATIC_DRAW, indices, num_points,
                                  GL_UNSIGNED_BYTE, GL_STATIC_DRAW);
  set_update_buffer(buf,(void*)update_line_buffer);
  buf->draw_mode = GL_LINES;
  buf->scene = scene;
  return buf;
}

gl_scene* init_scene(){
  gl_scene *scene = make_simple_scene(vertex_shader, fragment_shader);
  //3 buffers, one for some shapes, one for lines, and one for a cube
  scene->buffers = (gl_buffer**)zmalloc(3*sizeof(gl_buffer*));
  scene->num_buffers = 3;
  scene->buffers[0] = init_line_buffer(scene);
  scene->buffers[1] = init_cube_buffer(scene);
  scene->buffers[2] = init_shape_buffer(scene);
  /*
    Initialize uniform block, it's rather complicated
   */
  scene->scene_index = 1;
  glGenBuffers(1, &scene->uniform_buffer);
  glBindBuffer(GL_UNIFORM_BUFFER, scene->uniform_buffer);
  scene->uniform_block_index = glGetUniformBlockIndex(scene->program,
                                                      "uniform_block");
  glUniformBlockBinding(scene->program, scene->uniform_block_index,
                        scene->scene_index);
  scene->uniform_block = zmalloc(sizeof(struct uniforms));
  scene->uniform_buffer_size = sizeof(struct uniforms);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(struct uniforms),
               scene->uniform_block, GL_STREAM_DRAW);
  gl_check_error();
  return scene;
}

global_context* init_global_context(int width, int height){
  global_context *ctx = make_global_context(width, height, "p0");
  gl_check_error();
  ctx->scenes = init_scene();
  ctx->num_scenes = 1;
  ctx->userdata = init_userdata();
  set_update_userdata(ctx, (void*)update_userdata);
  return ctx;
}
static global_context *global_ctx;
void loop_once(){
  gl_main_loop_once(global_ctx);  
}
int main(int argc, char *argv[]){
  global_context *ctx = init_global_context(800,800);
  glEnable(GL_PRIMITIVE_RESTART);
  glPrimitiveRestartIndex(0xff);
  gl_main_loop(ctx);
}
