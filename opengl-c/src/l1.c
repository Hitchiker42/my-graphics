/*
  This doesn't work, I'll fix it asap, but I wanted to submit something
  so that I don't get a 0.

  I re-wrote the whole object heirarchy in my library, which entailed
  re-writing a lot of code, but it should be a more stable base for future
  programs, being as it supports composite objects, has a fixed vertex
  type that is used for all vertices, and a lot of other things.
*/
#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
thread_local global_context *thread_context;
const char *vertex_shader =
  "#version 330 core\n"
  "layout(row_major) uniform;\n"
  "layout(location = 0) in vec4 position;\n"
  "layout(location = 1) in vec4 color;\n"
  //unused but part of the default vertex attribute layout
  "layout(location = 2) in vec4 normals;\n"
  "layout(location = 3) in vec2 tex_coords;\n"
  "out vec4 v_color;\n"
  "uniform transform {\n"
  "  mat4 model;\n"
  "  mat4 proj;\n"
  "};"
  "void main(){\n"
  "  gl_Position = position * model * proj;"
  "  v_color = color;\n"
  "}\n";
const char *fragment_shader =
  "#version 330 core\n"
  "in vec4 v_color;\n"
  "layout(location = 0) out vec4 f_color;\n"
  "void main(){\n"
  "  f_color = v_color;\n"
  "}\n";
//in terms of world coordinates -1...1
#define mouse_movement_delta 0.05
thread_local global_context *thread_context;
struct userdata {
  int current_sides;
  gl_color current_color;
  //this might be general enough to be worth putting into gl_toolkit.c
  int mouse_button_pressed;
  gl_position initial_position;
  //non-null only when drawing re-sizable shape
  gl_shape *current_shape;
  gl_shape *default_triangle;
  gl_shape *default_square;
  gl_shape *default_polygon;
};
static inline gl_drawable *
clone_current_default_shape(struct userdata *userdata){
  if(userdata->current_sides == 3){
    return clone_gl_drawable(AS_DRAWABLE(userdata->default_triangle));
  } else if(userdata->current_sides == 4){
    return clone_gl_drawable(AS_DRAWABLE(userdata->default_square));
  } else {
    return clone_gl_drawable(AS_DRAWABLE(userdata->default_polygon));
  }
}
void keypress_callback(gl_window win, int key, int scancode,
                       int action, int modmask){
  //only act when key is released
  if(action != GLFW_RELEASE){
    return;
  }
  double x,y;
  glfwGetCursorPos(win, &x, &y);
  printf("key %s released at location (%f,%f)\n",
           gl_key_to_string(key),x,y);
  struct userdata *userdata = ((struct userdata*)thread_context->userdata);
  if(!(modmask & GLFW_MOD_SHIFT)){
    key |= 0x20;
  }
  switch(key){
    case 'b':
    case 'B':
      userdata->current_color = gl_color_blue; return;
    case 'c':
    case 'C':
      userdata->current_color = gl_color_cyan; return;
    case 'g':
    case 'G':
      userdata->current_color = gl_color_green; return;
    case 'm':
    case 'M':
      userdata->current_color = gl_color_magenta; return;
    case 'R':
      userdata->current_color = gl_color_red; return;
    case 'y':
    case 'Y':
      userdata->current_color = gl_color_yellow; return;
    case 'p':
      userdata->current_sides = 10; return;
    case 'r':
      userdata->current_sides = 4; return;
    case 't':
      userdata->current_sides = 3; return;
  }
}

void mouse_button_callback(gl_window win, int button, int action,
                           int modmask){
  if(button != GLFW_MOUSE_BUTTON_1){
    return;
  }  struct userdata *userdata = ((struct userdata*)thread_context->userdata);
  if(action == GLFW_PRESS){
    userdata->mouse_button_pressed = 1;
    userdata->initial_position = get_cursor_pos_world();
    return;
  } else if(action == GLFW_RELEASE){
    if(userdata->current_shape == NULL){
      gl_shape *temp = (gl_shape*)clone_current_default_shape(userdata);
//given that nothing is moving it'd probably be more efficent to
//transform the vertices directly
      mat4_identity(temp->transform);
      mat4_translate(temp->transform,
                     (float*)&userdata->initial_position,
                     NULL);
      shape_set_uniform_color(temp, userdata->current_color);
      fprintf(stderr, "adding new shape at position %s\n",
              format_gl_position(userdata->initial_position));
      scene_add_drawable(thread_context->scenes[0], AS_DRAWABLE(temp));
    }
    userdata->current_shape = NULL;
    userdata->mouse_button_pressed = 0;
  }
}
mat4_t update_pos_scale(struct userdata *userdata, gl_position pos){
  mat4_t transform = userdata->current_shape->transform;
  gl_position init = userdata->initial_position;
  gl_position delta = position_sub(pos, init);
  gl_position mov = position_scale(delta, 0.5);
  mat4_identity(transform);
  mat4_translate(transform, (float*)&mov, NULL);
  mat4_scale(transform, (float*)&delta, NULL);
  return transform;
}

void mouse_position_callback(gl_window win, double x, double y){
  struct userdata *userdata = ((struct userdata*)thread_context->userdata);
  if(userdata->mouse_button_pressed != 1){
    return;
  }
  gl_position pos = screen_coords_to_world(x,y);
  if(userdata->current_shape == NULL){
    fprintf(stderr, "adding new resizable shape at position %s\n",
            format_gl_position(userdata->initial_position));
    float delta = position_distance(pos, userdata->initial_position);
    if(delta < mouse_movement_delta){
      return;
    } else {
      userdata->current_shape =
        (gl_shape*)clone_current_default_shape(userdata);
      int i;
      shape_set_uniform_color(userdata->current_shape,
                              userdata->current_color);
      scene_add_drawable(thread_context->scenes[0],
                         AS_DRAWABLE(userdata->current_shape));
    }
  }
  //we only get here if the current shape exists
  update_pos_scale(userdata, pos);
}
gl_position point_five_x = POSITION(0,0.5);
gl_position point_five_x_y = POSITION(0.5,0.5);
global_context *init_ctx(void){
  global_context *ctx = make_global_context(800,800,"l1");
  glfwSetKeyCallback(ctx->window, keypress_callback);
  glfwSetMouseButtonCallback(ctx->window, mouse_button_callback);
  glfwSetCursorPosCallback(ctx->window, mouse_position_callback);
  struct userdata *data = context_allocate_userdata(ctx,
                                                    sizeof(struct userdata),
                                                    NULL);
  context_allocate_scenes(ctx, 1);
  data->default_triangle =
    (gl_shape*)make_colored_convex_polygon(&point_five_x, 3,
                                           NULL, gl_color_white);
  data->default_square =
    (gl_shape*)make_colored_convex_polygon(&point_five_x_y, 4,
                                                   NULL, gl_color_white);
  data->default_polygon =
    (gl_shape*)make_colored_convex_polygon(&point_five_x, 10,
                                                   NULL, gl_color_white);
  data->current_sides = 3;
  data->current_color = gl_color_white;
  ctx->scenes[0] = make_simple_scene(vertex_shader, fragment_shader);
  return ctx;
}
int main(){
  thread_context = init_ctx();
  gl_main_loop(thread_context);
}
