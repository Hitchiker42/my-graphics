#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
#include "lighting.h"
#include "interaction.h"
thread_local global_context *thread_context;
static gl_keystate transform_keybindings[25] =
  {{.keycode = GLFW_KEY_RIGHT},{.keycode = GLFW_KEY_LEFT},//translate x
   {.keycode = GLFW_KEY_UP},{.keycode = GLFW_KEY_DOWN},//translate y
   {.keycode = GLFW_KEY_UP, .modmask = GLFW_MOD_SHIFT},
   {.keycode = GLFW_KEY_DOWN, .modmask = GLFW_MOD_SHIFT}, //translate z
   //rotate
   {.keycode = 'x'}, {.keycode = 'x', .modmask = GLFW_MOD_CONTROL},
   //rotate y, it's much eaiser to put all 3 rotations next ot each other
   {.keycode = 'c'}, {.keycode = 'c', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'z'}, {.keycode = 'z', .modmask = GLFW_MOD_CONTROL},
   //scale width, height, depth
   {.keycode = 'w'}, {.keycode = 'w', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'h'}, {.keycode = 'h', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'd'}, {.keycode = 'd', .modmask = GLFW_MOD_CONTROL},
   //shear ',', '.', '/' because why not
   {.keycode = ','}, {.keycode = ',', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '.'}, {.keycode = '.', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '/'}, {.keycode = '/', .modmask = GLFW_MOD_CONTROL},
   //reset
   {.keycode = 'r'}};
static gl_material test_mat_1[1] = 
  {{.ambient = {.r = 0.2, .g = 0.2, .b = 0.2},
    .diffuse = {.r = 1, .g = 1, .b = 1},
    .specular = {.r = 0.2, .g = 0.2, .b = 0.2},
    .shininess = 1}};
static gl_material test_mat_2[1] = 
  {{.ambient = {.r = 0, .g = 0, .b = 0},
    .diffuse = {.r = 1, .g = 1, .b = 1},
    .specular = {.r = 0, .g = 0, .b = 0},
    .shininess = 1}};
static gl_material test_mat_3[1] = 
  {{.specular = {.r = 0.5, .g = 0.5, .b = 0.1},
    .diffuse = {.r = 0.1, .g = 0.1, .b = 0.1},
    .ambient = {.r = 3, .g = 3, .b = 3},
    .shininess = 16}};
static gl_material test_mat_4[1] = 
  {{.specular = {.r = 0.1, .g = 0.1, .b = 0.1},
    .diffuse = {.r = 0.1, .g = 0.5, .b = 0.1},
    .ambient = {.r = 3, .g = 3, .b = 3},
    .shininess = 4}};
static gl_material mirror[1] = 
  {{.specular = {.r = 1, .g = 1, .b = 1},
    .diffuse = {.r = 0, .g = 0, .b = 0},
    .ambient = {.r = 3, .g = 3, .b = 3},
    .shininess = 64}};
static gl_material diffuse_red[1] =
  {{.specular = {.r = 0, .g = 0, .b = 0},
    .diffuse = {.r = 1, .g = 0, .b = 0},
    .ambient = {.r = .3, .g = .3, .b = .3},
    .shininess = 8}};
static gl_material diffuse_blue[1] =
  {{.specular = {.r = 0, .g = 0, .b = 0},
    .diffuse = {.r = 0, .g = 0, .b = 1},
    .ambient = {.r = .3,.g = .3,.b = .3},
    .shininess = 8}};
static gl_material diffuse_green[1] =
  {{.specular = {.r = 0, .g = 0, .b = 0},
    .diffuse = {.r = 0, .g = 1, .b = 0},
    .ambient = {.r = .3, .g = .3, .b = .3},
    .shininess = 8}};
static gl_material specular_red[1] =
  {{.diffuse = {.r = 0, .g = 0, .b = 0},
    .specular = {.r = 1, .g = 0, .b = 0},
    .ambient = {.r = .3, .g = .3, .b = .3},
    .shininess = 8}};
static gl_material specular_blue[1] =
  {{.diffuse = {.r = 0, .g = 0, .b = 0},
    .specular = {.r = 0, .g = 0, .b = 1},
    .ambient = {.r = .3, .g = .3, .b = .3},
    .shininess = 8}};
static gl_material specular_green[1] =
  {{.diffuse = {.r = 0, .g = 0, .b = 0},
    .specular = {.r = 0, .g = 1, .b = 0},
    .ambient = {.r = .3, .g = .3, .b = .3},
    .shininess = 8}};
/*
  These are all positioned at the most negitive x y and z, pointing
  toward the most positive x,y and z
*/
static gl_light test_light_1[1] = 
  {{.location = POSITION(0,0,-1,0),
    .direction = POSITION(0,0,1,0),
    .diffuse = {.r = 0.6,.g = 0.6,.b = 0.6, .a = 1},
    .specular = {.r = 0.2,.g = 0.2,.b = 0.2, .a = 1}}};
static gl_light test_light_2[1] = 
  {{.location = POSITION(0,0,1,0),
    .direction = POSITION(0,0,-1,0),
    .diffuse = {.r = 0.2,.g = 0.2,.b = 0.2, .a = 1},
    .specular = {.r = 0.6,.g = 0.6,.b = 0.6, .a = 1}}};
static gl_light max_light[1] = 
  {{.location = POSITION(-1,-1,-1,1),
    .direction = POSITION(M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 1,.g = 1,.b = 1, .a = 1},
    .specular = {.r = 1,.g = 1,.b = 1, .a = 1}}};
static gl_light red_light[1] = 
  {{.location = POSITION(1,-1,-1,1),
    .direction = POSITION(-M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 1,.g = 0,.b = 0, .a = 1},
    .specular = {.r = 1,.g = 0,.b = 0, .a = 1}}};
static gl_light blue_light[1] = 
  {{.location = POSITION(-1,1,-1,1),
    .direction = POSITION(-M_SQRT2_2, -M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 0,.g = 0,.b = 1, .a = 1},
    .specular = {.r = 0,.g = 0,.b = 1, .a = 1}}};
static gl_light green_light[1] = 
  {{.location = POSITION(1,1,-1,1),
    .direction = POSITION(-M_SQRT2_2, -M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 0,.g = 1,.b = 0, .a = 1},
    .specular = {.r = 0,.g = 1,.b = 0, .a = 1}}};
static gl_light diffuse_only_light[1] = 
  {{.location = POSITION(-1,-1,-1,1),
    .direction = POSITION(M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 1,.g = 1,.b = 1, .a = 1},
    .specular = {.r = 0, .g = 0, .b = 0}}};
static gl_light specular_only_light[1] = 
  {{.location = POSITION(-1,-1,-1,1),
    .direction = POSITION(M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {. r = 0, .b = 0, .g = 0},
    .specular = {.r = 1,.g = 1,.b = 1, .a = 1}}};
static gl_position eye_direction = POSITION(0,0,1,1);
static gl_colorf ambient = {.r = 0.5, .g = 0.5, .b = 0.5};
struct transform_switch_info {
  interactive_transform **transforms;
  int num_transforms;
  int transform_idx;
};
  
void switch_transform(gl_window win, int key, void *data,
                      int action, int modmask){
  if(action == GLFW_RELEASE){
    struct transform_switch_info *info = data;
    info->transform_idx %= info->transform_idx + info->num_transforms;
    interactive_transform_set_keys(info->transforms[info->transform_idx]);
  }
}
/*
  TODO: use a more elegent solution for determining how to move/rotate
  than bitmasks (or at least make it look nicer). 
*/
static void update_scene(gl_scene *scene, void *data){
  interactive_transform_update(data);
}
gl_color gradient(int i){
  //this is cheating a bit, since it's using info from make_sphere
  const int step = 10*M_PI;
  double max = 5*step;
  int intensity = (i/max) * 255;
  gl_color ret;
  ret.g = ret.b = intensity;
  ret.r = 0;
  ret.a = 0xdd;
  return ret;
}
void bind_drawable_material(gl_drawable *drawable){
  update_uniform_block(drawable->scene->uniforms[0],
                       drawable->material_properties,
                       0, sizeof(gl_material));
  bind_drawable_default(drawable);
}
gl_scene *init_test_scene(GLuint program){
  gl_scene *scene = make_scene_from_program(program);
  scene->name = "Basic Lighting Test";
  gl_drawable *cube = make_colored_default_cube(0.25, gl_color_white);
  gl_drawable *tetra = make_colored_default_tetrahedron(0.25, gl_color_white);
  gl_drawable *oct = make_colored_default_octahedron(0.25, gl_color_white);
  gl_drawable *cylinder = make_cylinder(0.25, 0.25, gradient);
  gl_drawable *sphere = make_sphere(0.25, gradient);

  gl_drawable *shapes[] = {cube, tetra, oct, cylinder, sphere};
  cube->material_properties = test_mat_1;
  tetra->material_properties = test_mat_2;
  oct->material_properties = test_mat_3;
  cylinder->material_properties = test_mat_4;
  sphere->material_properties = mirror;
  gl_position offset = POSITION(0.25,0.25,0.25,0);
  mat4_translate(cube->transform, offset.vec, cube->transform);
  offset.x = -0.25;
  mat4_translate(tetra->transform, offset.vec, tetra->transform);
  offset.y = offset.z = -0.25;
  mat4_translate(oct->transform, offset.vec, oct->transform);
  offset.x = 0.25;
  mat4_translate(cylinder->transform, offset.vec, cylinder->transform);
  //  gl_drawable *circle = make_colored_default_convex_polygon(40, 0.5, gl_color_yellow);
  //  circle->material_properties = test_mat;
  gl_drawable *axes = make_axes_drawable(gl_color_white);
  int i;
  for(i=0;i<5;i++){
    shapes[i]->bind_drawable = bind_drawable_material;
  }
  gl_uniform_block *uniforms[2];
  gl_light *lights[6] = {test_light_1, test_light_2, red_light, blue_light, green_light};
  uniforms[0] = init_material_uniform(test_mat_1);
  uniforms[1] = init_light_uniform(ambient.rgba, eye_direction.vec,
                                   lights, 5, (0x4 | 0x8 | 0x10));
  scene_bind_uniforms(scene, uniforms, 2);
  scene->update_scene = (void*)update_scene;
  SCENE_ADD_DRAWABLES(scene,  cube, tetra, oct, cylinder, sphere, axes);
  return scene;
}
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "p3");
  struct userdata *data = ctx->userdata;
  GLuint program = create_shader_program(lighting_vertex, lighting_fragment);
  context_allocate_scenes(ctx, 1);
  ctx->scenes[0] = init_test_scene(program);
  glfwSetKeyCallback(ctx->window, default_keypress_callback);
  window_set_keymap(ctx->window, default_keymap_ptr);
  interactive_transform *trans = 
    make_interactive_transform(0.05, M_PI/60, 0.05, 0.05, 
                               transform_keybindings, default_keymap_ptr);
  transform_set_scene(trans, ctx->scenes[0]);
  ctx->userdata = trans;
  thread_context = ctx;
  gl_main_loop(thread_context);
}
