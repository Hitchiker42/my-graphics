#include "common.h"
#include "interaction.h"
#include "context.h"
#include <SDL2/SDL_keycode.h>
void __attribute__((weak)) sdl_handle_error(char *errstr, char *call){
  fprintf(stderr,"SDL error: %s\nFrom function call: %s\n", errstr, call); \
  exit(1);
}
#define SDL_CHECK_ERR(call)                     \
  char *errstr = SDL_GetError();                \
  if(errstr){                                   \
    sdl_handle_error(errstr, call);             \
  }
/*
  This should call fun with args, then check if there was an sdl error,
  (and call sdl_handle_error if there was).
  this returns whatever the function
*/
#define SDL_CHECK_CALL(fun,args...)                                     \
  ({__typeof(fun(args)) _ret = fun(args);                               \
    SDL_CHECK_ERR(#fun "(" #args ")");                                  \
    _ret;})
#define SDL_CHECK_CALL_VOID(fun,args...)                                \
  ({fun(args);                                                          \
    SDL_CHECK_ERR(#fun "(" #args ")");})
/*
  This relies on the ordering and values of sdl scancodes and the
  method to convent scancodes to keycodes, The alternative would
  be to use hashtable as a keymap, which could work.
*/
#ifndef SDL_KEYCODE_TO_SCANCODE
#define SDL_KEYCODE_TO_SCANCODE(X) (X & ~(SDLK_SCANCODE_MASK))
#endif

//CAPSLOCK is the smallest scancode whose keycode doesn't corrspond
//to an ascii key
#define SDL_SCANCODE_OFFSET (128 - SDL_SCANCODE_CAPSLOCK)
#define SDL_KEY_TO_KEYMAP_INDEX(key)                                        \
  ({int ret;                                                            \
    if(key<128){                                                        \
      ret = key;                                                        \
    } else {                                                            \
      ret = (SDL_KEYCODE_TO_SCANCODE(key) + SDL_SCANCODE_OFFSET);       \
    }                                                                   \
    ret;})
#define SDL_KEYMAP_GET_KEY(keymap, key) (keymap[SDL_KEY_TO_KEYMAP_INDEX(key)])
#define SDL_KEYMAP_GET_KEYPTR(keymap, key) (&(keymap[SDL_KEY_TO_KEYMAP_INDEX(key)]))
#define SDL_MODIFIER_TO_INDEX(mod)                  \
  ({((mod & KMOD_CTRL)   ? 1 :                  \
     ((mod & KMOD_SHIFT) ? 2 :                  \
      ((mod & KMOD_ALT)  ? 3 : 0)));})

void default_sdl_key_callback(gl_window win, SDL_KeyboardEvent *event){
  SDL_Keysym key = event->keysym;
  gl_keymap *map = SDL_GetWindowData(win, "keymap") || current_keymap;
  keypress_closures *callbacks = SDL_KEYMAP_GET_KEYPTR(map, key.sym);
  int index = SDL_MODIFIER_TO_INDEX(key.mod);
  keypress_closure closure = callbacks[index].closures[index];
  if(closure.callback){
    closure.callback(win, key.sym, key.scancode,
                     key.mod, event->type, closure.data);
  }
}
/*
  These functions are weak, meaning they'll get used if the user doesn't
  redefine them, but they'll get overridden by user defined versions.
*/
void __attribute__((weak)) sdl_handle_event(SDL_Event *event){
  printf(sdl_event_to_string(event));
}
void __attribute__((weak)) sdl_handle_events(SDL_Event **events, int num_events){
  while(num_events-- > 0){
    sdl_handle_event(*events++);
  }
}
//void gl_poll_events()

SDL_Window *init_sdl_gl_context(int w, int h, const char *name){
  //init sdl here
  SDL_Window *window;
  SDL_GLContext context;
  SDL_Init(SDL_INIT_VIDEO);
  //set opengl attributes before creating the context
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GL_UTIL_CONTEXT_VERSION_MAJOR);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GL_UTIL_CONTEXT_VERSION_MINOR);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, GL_UTIL_MULTISAMPLE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,
                      SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  atexit(SDL_Quit);

  window = SDL_CHECK_CALL(SDL_CreateWindow, name, SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED, w, h,
                          SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  context = SDL_CHECK_CALL(SDL_GL_CreateContext, window);
  //No event handling for now
  init_glew();
  return window;
}
#define enum_case(val)                          \
  case(val):                                    \
  return #val;
char * __attribute__((pure)) sdl_event_type_to_string(int event_type){
  switch(event_type){
    enum_case(SDL_FIRSTEVENT);
    enum_case(SDL_QUIT);
    enum_case(SDL_APP_TERMINATING);
    enum_case(SDL_APP_LOWMEMORY);
    enum_case(SDL_APP_WILLENTERBACKGROUND);
    enum_case(SDL_APP_DIDENTERBACKGROUND);
    enum_case(SDL_APP_WILLENTERFOREGROUND);
    enum_case(SDL_APP_DIDENTERFOREGROUND);
    enum_case(SDL_WINDOWEVENT);
    enum_case(SDL_SYSWMEVENT);
    enum_case(SDL_KEYDOWN);
    enum_case(SDL_KEYUP);
    enum_case(SDL_TEXTEDITING);
    enum_case(SDL_TEXTINPUT);
    enum_case(SDL_KEYMAPCHANGED);
    enum_case(SDL_MOUSEMOTION);
    enum_case(SDL_MOUSEBUTTONDOWN);
    enum_case(SDL_MOUSEBUTTONUP);
    enum_case(SDL_MOUSEWHEEL);
    enum_case(SDL_JOYAXISMOTION);
    enum_case(SDL_JOYBALLMOTION);
    enum_case(SDL_JOYHATMOTION);
    enum_case(SDL_JOYBUTTONDOWN);
    enum_case(SDL_JOYBUTTONUP);
    enum_case(SDL_JOYDEVICEADDED);
    enum_case(SDL_JOYDEVICEREMOVED);
    enum_case(SDL_CONTROLLERAXISMOTION);
    enum_case(SDL_CONTROLLERBUTTONDOWN);
    enum_case(SDL_CONTROLLERBUTTONUP);
    enum_case(SDL_CONTROLLERDEVICEADDED);
    enum_case(SDL_CONTROLLERDEVICEREMOVED);
    enum_case(SDL_CONTROLLERDEVICEREMAPPED);
    enum_case(SDL_FINGERDOWN);
    enum_case(SDL_FINGERUP);
    enum_case(SDL_FINGERMOTION);
    enum_case(SDL_DOLLARGESTURE);
    enum_case(SDL_DOLLARRECORD);
    enum_case(SDL_MULTIGESTURE);
    enum_case(SDL_CLIPBOARDUPDATE);
    enum_case(SDL_DROPFILE);
    enum_case(SDL_AUDIODEVICEADDED);
    enum_case(SDL_AUDIODEVICEREMOVED);
    enum_case(SDL_RENDER_TARGETS_RESET);
    enum_case(SDL_RENDER_DEVICE_RESET);
    enum_case(SDL_USEREVENT);
    enum_case(SDL_LASTEVENT);
    default:
      if(event_type > SDL_USEREVENT && event_type < SDL_LASTEVENT){
        return "SDL_USEREVENT";
      } else {
        return "UNKNOWN_EVENT";
      }
  }
}
char *sdl_event_to_string(SDL_Event *event){
  //this needs to be updated
  return sdl_event_type_to_string(event->type);
}
