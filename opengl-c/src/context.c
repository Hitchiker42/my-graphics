#include "common.h"
#include "context.h"
#include "drawable.h"
/*
  TODO: Seperate scene and model transformation matrices into seperate
  uniforms, store all model matrices in one uniform buffer and update
  them using asynchronous mapping

*/
//monotonic counter for uniform buffer array indices
static int32_t uniform_buffer_index = 0;
//void init_scene_uniforms(gl_scene *scene, char *name,
static void bind_vbo(gl_VBO *vbo){
  glBindVertexArray(vbo->VAO);
  gl_check_error();
  glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
}
static void bind_uniform(GLuint program, gl_uniform_block *uniform){
  if(uniform->loc == GL_INVALID_INDEX){
    int loc = glGetUniformBlockIndex(program, uniform->name);
    glUniformBlockBinding(program, loc, uniform->array_index);
  } else {
    glUniformBlockBinding(program, uniform->loc, uniform->array_index);
  }
  glBindBuffer(GL_UNIFORM_BUFFER, uniform->buffer);
  glBindBufferBase(GL_UNIFORM_BUFFER, uniform->array_index, uniform->buffer);
}
void bind_scene(gl_scene *scene){
//  gl_print_location();
  gl_check_error();
  glUseProgram(scene->program);
  gl_check_error();
  bind_vbo(&scene->VBO);
  gl_check_error();
  bind_uniform(scene->program, &scene->transform);
  int i;
  for(i=0;i<scene->num_uniform_blocks;i++){
    bind_uniform(scene->program, scene->uniforms[i]);
  }
  return;
}
void bind_texture(gl_texture *tex){
  glActiveTexture(GL_TEXTURE0 + tex->idx);
  glBindTexture(tex->target, tex->id);
  gl_check_error();
  int loc = glGetUniformLocation(GL_GET(GL_CURRENT_PROGRAM), tex->name);
  gl_check_error();
  glUniform1i(loc, 0);//GL_TEXTURE0 + tex->idx);
  gl_check_error();
}
global_context *allocate_global_context(){
  global_context *ctx = zmalloc(sizeof(global_context));
  return ctx;
}
gl_scene *allocate_gl_scene(){
  gl_scene *scene = zmalloc(sizeof(gl_scene));
  return scene;
}
global_context *make_global_context(int window_width, int window_height,
                                    const char *name){
  gl_window win = init_gl_context(window_width, window_height, name);
  global_context *ctx = zmalloc(sizeof(global_context));//zero it to be safe
  ctx->window = win;
  return ctx;
}
gl_scene** context_allocate_scenes(global_context *ctx, int num_scenes){
  ctx->scenes = xmalloc(sizeof(gl_scene*)*num_scenes);
  ctx->num_scenes = num_scenes;
  return ctx->scenes;
}
void* context_allocate_userdata(global_context *ctx, size_t sz,
                               void *update_userdata){
  ctx->userdata = zmalloc(sz);
  ctx->update_userdata = update_userdata;
  return ctx->userdata;
}
void context_init_userdata(global_context *ctx, void *userdata,
                           void *update_userdata){
  ctx->userdata = userdata;
  ctx->update_userdata = update_userdata;
}
/*
  Initializes a vertex buffer object, this entails:
    -Allocating a buffer on the gpu
    -Allocating a vertex array object
    -Binding the given, or default, vertex attributes to the VAO
    -Allocating space for the array of drawables
    -Setting the elements of the vbo object itself

  vbo should point to zeroed memory the size of a gl_VBO, attribs should ba an
  array of num_attribs vertex atributes, or NULL to use default attributes.
*/
void bind_vbo_attribs(gl_VBO *vbo,
                      const gl_vertex_attrib *attribs, int num_attribs){
  if(attribs == NULL){
    attribs = default_attribs;
  }
  int i;
  for(i=0;i<num_attribs;i++){
    enable_vertex_attrib(attribs[i]);
    gl_check_error();
  }
}
void init_vbo(gl_VBO *vbo, const gl_vertex_attrib* attribs, int num_attribs){
  glGenBuffers(1, &vbo->id);
  glGenVertexArrays(1, &vbo->VAO);
  glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
  glBindVertexArray(vbo->VAO);
  gl_check_error();
  bind_vbo_attribs(vbo, attribs, num_attribs);
  /*
    The buffer is modified whenever a new drawable is added to it, so
    GL_DYNAMIC_DRAW is probably the best usage hint.
   */
  glBufferData(GL_ARRAY_BUFFER, default_vbo_size, NULL, GL_DYNAMIC_DRAW);

  vbo->size = default_vbo_size;
  vbo->drawables = make_svector(16);
  return;
}
/*
  Initialize all aspects of block that don't need an explicit program
*/
static void init_uniform_block_internal(gl_uniform_block *block,
                                        const char *name, int size,
                                        void *data, int data_size){
  glGenBuffers(1,&block->buffer);
  glBindBuffer(GL_UNIFORM_BUFFER, block->buffer);
  glBufferData(GL_UNIFORM_BUFFER, size, NULL, GL_STREAM_DRAW);
//maybe add an offset parameter
  if(data != NULL){
    glBufferSubData(GL_UNIFORM_BUFFER, 0, data_size, data);
    block->data = data;
  } else {
    //passing 0 as data size implies that data_size = size
    data_size = (data_size ? data_size : size);
    block->data = zmalloc(data_size);
  }
  gl_check_error();

  int32_t array_index = uniform_buffer_index++;
//  glUniformBlockBinding(program, loc, array_index);
//  glBindBufferBase(GL_UNIFORM_BUFFER, array_index, block->buffer);
//  gl_check_error();
  block->name = name;
  block->buffer_size = size;
  block->data_size = data_size;
  block->array_index = array_index;
  //by default uniforms aren't bound to a specific program
  block->loc = GL_INVALID_INDEX;
}

gl_uniform_block* make_uniform_block(const char *name, int size,
                                     void *data, int data_size){
  gl_uniform_block *block = zmalloc(sizeof(gl_uniform_block));
  init_uniform_block_internal(block, name, size, data, data_size); 
 return block;
}
void init_uniform_block(gl_uniform_block *block, GLuint program,
                        char *name, int size){
  init_uniform_block_internal(block, name, size, NULL, 0);
  int loc = glGetUniformBlockIndex(program, name);
  block->loc = loc;
  glUniformBlockBinding(program, block->loc, block->array_index);
  glBindBufferBase(GL_UNIFORM_BUFFER, block->array_index, block->buffer);
  gl_check_error();
  return;
}
int update_uniform_block_data(gl_uniform_block *block, void *data,
                              int offset, int size){
  if(offset + size > block->buffer_size){
    fprintf(stderr, "Error Attempt to write past the end of uniform buffer");
    abort();
    return -1;
  }
  memcpy(block->data + offset, data, size);
  return 0;
}
void sync_uniform_block(gl_uniform_block *block){
  bind_uniform(GL_GET(GL_CURRENT_PROGRAM), block);
  glBufferData(GL_UNIFORM_BUFFER, block->buffer_size, NULL, GL_STREAM_DRAW);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, block->buffer_size, block->data);

}
void update_uniform_block(gl_uniform_block *block, void *data,
                         int offset, int size){
  sync_uniform_block(block);
  glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data);
}

/*
  Allocates a gl_scene, compiles a program using the given vertex and
  fragment shader sources (which are strings containing the literal source)
  and initializes the VAO.
*/
gl_scene *make_scene_from_program(GLuint program){
  gl_check_error();
  gl_scene *scene = zmalloc(sizeof(gl_scene));
  scene->program = program;
  scene->is_active = 1;
  scene->matrix_stack_sz = default_matrix_stack_size * 16*sizeof(float);
  scene->matrix_stack = xmalloc(scene->matrix_stack_sz);
  scene->stack_ptr = scene->matrix_stack + scene->matrix_stack_sz;
  mat4_identity(scene->projection);
  mat4_identity(scene->view);
  mat4_identity(scene->scene);
  init_vbo(&scene->VBO, NULL, 4);
  scene->VBO.scene = scene;
  init_uniform_block(&scene->transform, scene->program,
                     "transform", 64*sizeof(float));
  gl_check_error();
  return scene;
}
gl_scene *make_simple_scene(const char *vertex_shader, const char *fragment_shader){
  GLuint prog = create_shader_program(vertex_shader, fragment_shader);
  return make_scene_from_program(prog);
}
gl_scene *make_simple_scene_from_files(const char *vertex_shader_path,
                                       const char *fragment_shader_path){
  size_t vertex_shader_sz, fragment_shader_sz;
  char *vertex_shader = mmap_filename(vertex_shader_path, 1,
                                            PROT_READ, &vertex_shader_sz);
  if(vertex_shader == NULL){
    return NULL;
  }
  char *fragment_shader = mmap_filename(vertex_shader_path, 1,
                                            PROT_READ, &fragment_shader_sz);
  if(fragment_shader == NULL){
    munmap(vertex_shader, vertex_shader_sz);
  }
  gl_scene *scene = make_simple_scene(vertex_shader, fragment_shader);
  munmap(vertex_shader, vertex_shader_sz);
  munmap(fragment_shader, fragment_shader_sz);
  return scene;
}

/*
  Add a drawable to a scene
*/
void scene_add_drawable(gl_scene *scene, gl_drawable *drawable){
  vbo_add_drawable(&scene->VBO, drawable);
}
void scene_add_drawables(gl_scene *scene, gl_drawable **drawables, int n){
  vbo_add_drawables(&scene->VBO, drawables, n);
}
gl_drawable **scene_get_drawables(gl_scene *scene, int *num_drawables){
  svector drawables = scene->VBO.drawables;
  if(num_drawables != NULL){
    *num_drawables = drawables.len;
  }
  return (gl_drawable**)drawables.data;
}
gl_drawable *scene_get_drawable(gl_scene *scene, int idx){
  svector *drawables = &scene->VBO.drawables;
  return svector_ref(drawables, idx);
}
static int string_equal(const char *a, const char *b){
  return !(strcmp(a,b));
}
gl_drawable *scene_get_drawable_by_name(gl_scene *scene, const char *name){
  svector *drawables = &scene->VBO.drawables;
  int idx = svector_search2(drawables, (void*)string_equal, (void*)name);
  if(idx >= 0){
    return svector_ref(drawables, idx);
  } else {
    return NULL;
  }
}
void scene_init_uniforms(gl_scene *scene, char **names,
                         int *sizes, int num_uniforms){
  scene->uniforms = zmalloc(num_uniforms * sizeof(gl_uniform_block*));
  int i;
  for(i=0;i<num_uniforms;i++){
    scene->uniforms[i] = make_uniform_block(names[i], sizes[i],
                                            NULL, 0);
  }
}
void scene_add_uniforms(gl_scene *scene, gl_uniform_block **uniforms,
                        int num_uniforms){
  scene->uniforms = zmalloc(num_uniforms * sizeof(gl_uniform_block*));
  int i;
  for(i=0;i<num_uniforms;i++){
    scene->uniforms[i] = uniforms[i];
  }
}
void scene_bind_uniforms(gl_scene *scene, gl_uniform_block **uniforms,
                         int num_uniforms){
  scene->uniforms = zmalloc(num_uniforms * sizeof(gl_uniform_block*));
  int i;
  for(i=0;i<num_uniforms;i++){
    int loc = glGetUniformBlockIndex(scene->program, uniforms[i]->name);
    uniforms[i]->loc = loc;
    glUniformBlockBinding(scene->program, uniforms[i]->loc, uniforms[i]->array_index);
    glBindBufferBase(GL_UNIFORM_BUFFER, uniforms[i]->array_index, uniforms[i]->buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, uniforms[i]->data_size, uniforms[i]->data);
    scene->uniforms[i] = uniforms[i];
  }
}
void scene_bind_camera(gl_scene *scene, gl_camera *camera){
  scene->camera = camera;
  mat4_identity(scene->projection);
  mat4_look_at(camera->eye.vec, camera->center.vec, camera->up.vec,
               scene->projection);
  if(camera->proj_type == ORTHOGRAPHIC_PROJECTION){
    mat4_ortho(camera->left, camera->right,
               camera->bottom, camera->top,
               camera->near, camera->far, scene->projection);
  } else {
    mat4_frustum(camera->left, camera->right,
                 camera->bottom, camera->top,
                 camera->near, camera->far, scene->projection);
  }
  update_uniform_block_data(&scene->transform, scene->view,
                            scene_transform_view_offset, 16*sizeof(float));
}

gl_camera *make_gl_camera(gl_position eye, gl_position center,
                          gl_position up, float boundries[6], int proj){
  gl_camera *camera = zmalloc(sizeof(gl_camera));
  camera->eye = eye;
  camera->center = center;
  camera->up = up;
  camera->left = boundries[0], camera->right = boundries[1];
  camera->bottom = boundries[2], camera->top = boundries[3];
  camera->near = boundries[4], camera->far = boundries[5];
  camera->proj_type = proj;
  return camera;
}
/* static int loop_count = 0; */
/* inline void gl_main_loop_internal(global_context *ctx){ */
/*   int i,j,k; */
/*   //presumably swaping buffers means things are synchronized */
/*   //but there's no guarantee of this, */
/*   gl_check_error(); */
/*   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); */
/*   if(ctx->update_userdata){ */
/*     ctx->update_userdata(ctx->userdata); */
/*   } */
/*   for(i=0;i<ctx->num_scenes;i++){ */
/*     gl_scene *scene = ctx->scenes[i]; */
/*     if(!scene->is_active){ */
/*       continue; */
/*     } */
/*     bind_scene(scene); */
/*     if(scene->update_scene){ */
/*       scene->update_scene(scene, ctx->userdata); */
/*     } */
/*     //    if(scene_callback){ */
/*     //      scene_callback(scene); */
/*     //    } */
/*     update_uniform_block_data(&scene->transform, scene->proj_view_scene, */
/*                               16*sizeof(float), 48*sizeof(float)); */
/*     gl_check_error(); */
/*     //might put this is a seprate function eventually */
/*     svector drawables = scene->VBO.drawables; */
/*     int num_drawables = drawables.len; */
/*     for(j=0;j<num_drawables;j++){ */
/*       gl_drawable *drawable = svector_ref(&drawables,j); */
/*       if(drawable != NULL){ */
/*         //if(drawable_callback){ */
/*         //drawable_callback(drawable); */
/*         //} */
/*         gl_draw_drawable(drawable); */
/*       } */
/*     } */
/*   } */
/* } */
/* /\* */
/*   TODO: Change current gl_main_loop to gl_main_loop_simple */
/*   and gl_main_loop_w_callbacks to just gl_main_loop; */
/*  *\/ */
/* void __attribute__((noreturn)) gl_main_loop(global_context *ctx){ */
/*   gl_check_error(); */
/*   while(!glfwWindowShouldClose(ctx->window)){ */
/*     gl_check_error(); */
/*     gl_main_loop_internal(ctx); */
/*     glfwSwapBuffers(ctx->window); */
/*     glfwPollEvents(); */
/*   } */
/*   exit(0);//need a way to specify an exit value */
/* } */
/* void __attribute__((noreturn)) gl_main_loop_render(global_context *ctx){ */
/*   thread_context = ctx; */
/*   glfwMakeContextCurrent(thread_context->window); */
/*   while(!glfwWindowShouldClose(thread_context->window)){ */
/*     gl_check_error(); */
/*     gl_main_loop_internal(thread_context); */
/*     glfwSwapBuffers(thread_context->window); */
/*   } */
/*   exit(0);//need a way to specify an exit value */
/* } */
/* void __attribute__((noreturn)) gl_main_loop_render_fps(global_context *ctx){ */
/*   thread_context = ctx; */
/*   glfwMakeContextCurrent(thread_context->window); */
/*   while(!glfwWindowShouldClose(ctx->window)){ */
/*     double last_time = glfwGetTime(); */
/*     gl_check_error(); */
/*     gl_main_loop_internal(ctx); */
/*     glfwSwapBuffers(ctx->window); */
/*   } */
/*   exit(0);//need a way to specify an exit value */
/* } */
/* void __attribute__((noreturn)) gl_main_loop_event(global_context *ctx){ */
/*   thread_context = ctx; */
/*   while(!glfwWindowShouldClose(ctx->window)){ */
/*     glfwWaitEvents(); */
/*   } */
/*   exit(0);//need a way to specify an exit value */
/* } */
/* void gl_main_loop_once(global_context *ctx){ */
/*   if(!glfwWindowShouldClose(ctx->window)){ */
/*     gl_main_loop_internal(ctx); */
/*   } */
/* } */
