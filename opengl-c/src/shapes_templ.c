/*
  Templates for generating functions to create polygons/shapes. They
  are parameterized by macros, so this file needs to be #included multiple
  times to generate all the functions.

  Parameters:
   PREFIX: All functions in this file are of the form make_PREFIX_thing,
   (the second _ needs to be part of prefix). Thus PREFIX is used to
   identify the different versions of the functions.

   COLOR_ARG: The formal argument used to specify how to set vertex color
   GEN_COLOR(i,p): Generates a vertex color for the vertex at index i
*/

/*
  Generate the vertices of a regular polygon with n sides, with the
  first point at the position start and lying on the plane given by plane,
  or if plane is NULL on the xy plane.
e
  really generic (and thus somewhat slow) way to make a convex polygon
  given an array of positions and a function which takes an index
  and returns the color that the vertex at the index should be.
  The positions are stored in an array with the given stride, and
  are copied into the shape object.
*/
#define MAKE_NAME(name) _MAKE_NAME(make, PREFIX, name)
#define _MAKE_NAME(start, middle, end) CAT3(start, middle, end)


gl_drawable *MAKE_NAME(convex_polygon)(gl_position *start, int n,
                                       gl_position *norm, COLOR_ARG){
  gl_position axis;
  float theta = (2*M_PI/n);
  int i;
  if(norm == NULL){
    //default to xy plane
    axis = POSITION_CAST(0,0,-1,0);
  } else {
    axis = *norm;
  }
  mat4_t rot = alloca(16*sizeof(float));
  mat4_rotate(mat4_identity(rot), -theta, axis.vec, NULL);
  gl_vertex *vertices = zmalloc(n*sizeof(gl_vertex));
  for(i=0;i<n;i++){
    vertices[i].pos = *start;
    vertices[i].normal = gl_position_to_normal(axis);
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
    start = (gl_position*)mat4_multiply_vec3(rot, (float*)start, NULL);
  }
  gl_shape *shape = init_gl_shape(vertices, n);
  shape->indices = compute_indices(n);
  return AS_DRAWABLE(shape);
}

gl_drawable *MAKE_NAME(default_convex_polygon)(int n, float r,
                                               COLOR_ARG){
  float rot[16];
  gl_position pos = POSITION(0,r);
  gl_normal norm = gl_position_to_normal(POSITION_CAST(0,0,1,0));
  mat4_rotateZ(mat4_identity(rot), -2*M_PI/n, NULL);
  gl_vertex *vertices = zmalloc(n*sizeof(gl_vertex));
  int i;
  for(i=0;i<n;i++){
    vertices[i].pos = pos;
    vertices[i].normal = norm;
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
    pos = *((gl_position*)mat4_multiply_vec3(rot, pos.vec, NULL));
  }
  gl_shape *shape = init_gl_shape(vertices, n);
  shape->indices = compute_indices(n);
  return AS_DRAWABLE(shape);
}
/*
  This makes a cylinder by drawing a top and then a bottom, then a single
  strip of sides to connect them.
  
*/
gl_drawable *MAKE_NAME(psuedo_cylinder)(int n, float h, float r, COLOR_ARG){
  gl_position bottom = POSITION(0,-h/2,-r);
  gl_position top = POSITION(0,h/2,-r);
  gl_position norm = POSITION(0,1,0);
  float theta = (2*M_PI/n);
  mat4_t rot = alloca(16*sizeof(float));
  mat4_rotate(mat4_identity(rot), -theta, norm.vec, NULL);
  gl_vertex *vertices = zmalloc(2*n*sizeof(gl_vertex));
  int i,j,k;
  for(i=0;i<n;i++){
    vertices[i].pos = bottom;
    vertices[i].normal = gl_position_to_normal(POSITION_CAST(0,-1,0));
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
    vertices[i+n].pos = top;
    vertices[i+n].normal = gl_position_to_normal(POSITION_CAST(0,1,0));
    vertices[i+n].color = GEN_COLOR(i+n, (&vertices[i]));
    mat4_multiply_vec3(rot, top.vec, top.vec);
    bottom.x = top.x; bottom.z = top.z;
  }
  uint16_t *indices = xmalloc(((4*n)+2)*sizeof(uint16_t));
  /*compute top and bottom indices*/
  i = 1, j = 1, k = n-1;
  indices[0] = 0;
  indices[n] = 0xffff;
  indices[n+1] = n;
  while(j < k){
    indices[i] = j;
    indices[i+n+1] = j+n;
    i++, j++;
    if(j > k){break;}
    indices[i] = k;
    indices[i+n+1] = k+n;
    i++,k--;
  }  
  indices[2*n+1] = 0xffff;
  /*compute edge indices*/
  j = 2*n+2;
  for(i=0;i<n;i++){
    indices[j++] = i;
    indices[j++] = i + n;
  }
    
  gl_shape *shape = init_gl_shape_indexed(vertices, n,
                                          (gl_index_t*)indices, 4*n + 2, 2);
  return AS_DRAWABLE(shape);
}
  
/*
  Takes a square and a vector and creates a cube with square as
  the front face and square + vector as the rear face (or oppisite
  depending on the directon of the vector).

*/
gl_drawable *MAKE_NAME(cube)(gl_position *square, gl_position shift,
                             COLOR_ARG){
  /*

   */
  static int  indices_initialized = 0;
  static gl_indices indices;
  if(indices_initialized == 0){
    uint8_t cube_indices[17] =
      {0, 1, 2, 3, 6, 7, 4, 5, 0xff, 7, 3, 5, 1, 4, 0, 6, 2};
    indices.num_indices = 17;
    indices.index_size = 1;
    indices.index_type = GL_UNSIGNED_BYTE;
    indices.index_packing = 4;
    indices.indices = (gl_index_t*)cube_indices;
    init_index_buffer(&indices);
  }
  gl_shape *cube = make_gl_shape(8);
  cube->indices = &indices;
  gl_vertex *vertices = SHAPE_VERTICES(cube);
  int i;
  //set position and color of vertices
  for(i=0;i<4;i++){
    vertices[i].pos = square[i];
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
    vertices[i+4].pos = position_add(square[i], shift);
    vertices[i+4].color = GEN_COLOR(i+4, (&vertices[i+4]));
  }
  return (gl_drawable*)cube;
}
/*
  This is inefficent, but necessary for class
*/
/*
gl_drawable *MAKE_NAME(explicit_cube)(float edge_len, COLOR_ARG){
  gl_shape *cube = AS_SHAPE(make_gl_shape(16));
    Front face
  gl_normal pos_x, neg_x, pos_y, neg_y, pos_z, neg_z;
  pos_x = gl_position_to_normal(pos_x_max);
  neg_x = gl_position_to_normal(pos_x_min);

  pos_y = gl_position_to_normal(pos_y_may);
  neg_y = gl_position_to_normal(pos_y_min);

  pos_z = gl_position_to_normal(pos_z_maz);
  neg_z = gl_position_to_normal(pos_z_min);

  vertices[0].pos = POSITION_CAST(a,-a,a);
  vertices[0].normal = neg_z;
  vertices[0].color = GEN_COLOR(0);

  vertices[1].pos = POSITION_CAST(-a,-a,a);
  vertices[1].normal = gl_position_to_normal(vertices[1].pos);
  vertices[1].color = GEN_COLOR(1);

  vertices[2].pos = POSITION_CAST(a,a,a);
  vertices[2].normal = gl_position_to_normal(vertices[2].pos);
  vertices[2].color = GEN_COLOR(2);

  vertices[3].pos = POSITION_CAST(-a,a,a);
  vertices[3].normal = gl_position_to_normal(vertices[3].pos);
  vertices[3].color = GEN_COLOR(3);


  vertices[4].pos = POSITION_CAST(a,-a,-a);
  vertices[4].normal = gl_position_to_normal(vertices[4].pos);
  vertices[4].color = GEN_COLOR(4);

  vertices[5].pos = POSITION_CAST(-a,-a,-a);
  vertices[5].normal = gl_position_to_normal(vertices[5].pos);
  vertices[5].color = GEN_COLOR(5);

  vertices[6].pos = POSITION_CAST(a,a,-a);
  vertices[6].normal = gl_position_to_normal(vertices[6].pos);
  vertices[6].color = GEN_COLOR(6);

  vertices[7].pos = POSITION_CAST(-a,a,-a);
  vertices[7].normal = gl_position_to_normal(vertices[7].pos);
  vertices[7].color = GEN_COLOR(7);

//still need to restart here, or else figure out a way to avoid this
//but still use triangle strips


  vertices[4].pos = POSITION_CAST(a,-a,-a);
  vertices[4].normal = gl_position_to_normal(vertices[4].pos);
  vertices[4].color = GEN_COLOR(4);

  vertices[0].pos = POSITION_CAST(a,-a,a);
  vertices[0].normal = neg_z;
  vertices[0].color = GEN_COLOR(0);

  vertices[6].pos = POSITION_CAST(a,a,-a);
  vertices[6].normal = gl_position_to_normal(vertices[6].pos);
  vertices[6].color = GEN_COLOR(6);

  vertices[2].pos = POSITION_CAST(a,a,a);
  vertices[2].normal = gl_position_to_normal(vertices[2].pos);
  vertices[2].color = GEN_COLOR(2);

  vertices[7].pos = POSITION_CAST(-a,a,-a);
  vertices[7].normal = gl_position_to_normal(vertices[7].pos);
  vertices[7].color = GEN_COLOR(7);

  vertices[3].pos = POSITION_CAST(-a,a,a);
  vertices[3].normal = gl_position_to_normal(vertices[3].pos);
  vertices[3].color = GEN_COLOR(3);

  vertices[5].pos = POSITION_CAST(-a,-a,-a);
  vertices[5].normal = gl_position_to_normal(vertices[5].pos);
  vertices[5].color = GEN_COLOR(5);

  vertices[1].pos = POSITION_CAST(-a,-a,a);
  vertices[1].normal = gl_position_to_normal(vertices[1].pos);
  vertices[1].color = GEN_COLOR(1);
*/
#ifndef CUBE_POSITIONS
#define CUBE_POSITIONS
static const gl_position cube_positions[8] = 
  {POSITION(1,-1,1), POSITION(-1,-1,1), POSITION(1,1,1), POSITION(-1,1,1),
   POSITION(1,-1,-1), POSITION(-1,-1,-1), POSITION(1,1,-1), POSITION(-1,1,-1)};
#endif
gl_drawable *MAKE_NAME(default_cube)(float edge_len, COLOR_ARG){
  static int initialized = 0;
  static gl_indices indices = {0};
  static uint8_t cube_indices[17] =
    {0, 1, 2, 3, 6, 7, 4, 5, 0xff, 7, 3, 5, 1, 4, 0, 6, 2};
  if(initialized == 0){
    indices.num_indices = 17;
    indices.index_size = 1;
    indices.index_type = GL_UNSIGNED_BYTE;
    indices.index_packing = 4;
    indices.indices = (gl_index_t*)cube_indices;
    indices.is_static = 1;
    init_index_buffer(&indices);
  }
  float a = edge_len / 2;
  gl_shape *cube = (void*)make_gl_shape(8);
  cube->indices = &indices;
  gl_vertex *vertices = SHAPE_VERTICES(cube);
  int i;
  for(i=0;i<6;i++){
    vertices[i].pos = position_scale(cube_positions[i], a);
    vertices[i].normal = gl_position_to_normal(vertices[i].pos);
    vertices[i].color = GEN_COLOR(0, (&vertices[i]));
  }
  return (gl_drawable*)cube;
}
/*
  Non perturbed vertices are for edge length a are:
  (0,0,1/3 *sqrt(3) * a),(1/2*a,0,-1/6*sqrt(3)*a),(-1/2*a,0,-1/6*sqrt(3)*a,
  (0,1/3*sqrt(6)*a,0).

  We shift down by 1/2 the edge_length to put the tetrahedron at approximately the center,
  this needs to be fixed, so we put the actual center at (0,0,0);

*/
#ifndef TETRA_POSITIONS
#define TETRA_POSITIONS
#define M_SQRT6_3 0.8164965809277295
#define M_SQRT3_3 0.5773502691896257
#define M_SQRT3_6 0.28867513459481287
static const gl_position tetrahedron_positions[4] = 
  {POSITION(0, -0.5, M_SQRT3_3), POSITION(0.5, -0.5, M_SQRT3_6),
   POSITION(-0.5, -0.5, M_SQRT3_6), POSITION(0, M_SQRT6_3-0.5, 0)};
#endif
gl_drawable *MAKE_NAME(default_tetrahedron)(float edge_len, COLOR_ARG){
  static const float sqrt3_3 = sqrt(3)/3.0;
  static const float sqrt3_6 = sqrt(3)/6.0;
  static const float sqrt6_3 = sqrt(6)/3.0;
  static int initialized = 0;
  static gl_indices indices;
  if(initialized == 0){
    uint8_t tetra_indices[6] = {3, 0, 1, 2, 3, 0};
    indices.num_indices = 6;
    indices.index_size = 1;
    indices.index_type = GL_UNSIGNED_BYTE;
    indices.index_packing = 4;
    indices.indices = (gl_index_t*)tetra_indices;
    indices.is_static = 1;
    init_index_buffer(&indices);
  }
  float a = edge_len;
  
  gl_shape *tetra = (void*)make_gl_shape(4);
  tetra->indices = &indices;
  gl_vertex *vertices = SHAPE_VERTICES(tetra);
  int i;
  for(i=0;i<4;i++){
    vertices[i].pos = position_scale(tetrahedron_positions[i], a);
    vertices[i].normal = gl_position_to_normal(vertices[i].pos);
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
  }
//I'm not 100% sure that the normals here are correct
  
  return (gl_drawable*)tetra;
}
#ifndef OCT_POSITIONS
#define OCT_POSITIONS
static const gl_position octahedron_positions[6] =
  {POSITION(0,1,0), POSITION(0,0,-1), POSITION(1,0,0),
   POSITION(0,0,1), POSITION(-1,0,0), POSITION(0,-1,0)};
#endif
gl_drawable *MAKE_NAME(default_octahedron)(float edge_len, COLOR_ARG){
  static int initialized = 0;
  static gl_indices indices;
  if(initialized == 0){
    uint8_t oct_indices[15] =
      {0, 1, 2, 3, 0, 4, 1, 0xff, 5, 3, 4, 1, 5, 2, 3};
    indices.num_indices = 15;
    indices.index_size = 1;
    indices.index_type = GL_UNSIGNED_BYTE;
    indices.index_packing = 4;
    indices.indices = (gl_index_t*)oct_indices;
    indices.is_static = 1;
    init_index_buffer(&indices);
  }

  float a = edge_len/2;
  gl_shape *oct = (void*)make_gl_shape(6);
  oct->indices = &indices;
  gl_vertex *vertices = SHAPE_VERTICES(oct);
  //As with the cube the normals here are just the position
  //converted to a vector and normalized
  int i;
  for(i=0;i<6;i++){
    vertices[i].pos = position_scale(octahedron_positions[i], a);
    vertices[i].normal = gl_position_to_normal(vertices[i].pos);
    vertices[i].color = GEN_COLOR(0, (&vertices[i]));
  }
  return (gl_drawable*)oct;
}
/*

*/
/*
  Given a range [x_0,x_n]⊆[-1.0,1.0], a number of points n
  and an equation of the form f(x) -> (x,y,z), return a set
  of n vertices that plot f from x_0 to x_n.
*/

gl_drawable* MAKE_NAME(line)(float x_0, float x_n, int n,
                             gl_position(*fx)(float), COLOR_ARG){
  int i;
  float dx = (x_n-x_0)/n, x = x_0;
  gl_shape *line = make_gl_shape(n);
  gl_vertex *vertices = SHAPE_VERTICES(line);
  for(i=0, x=x_0; i<n; i++,x+=dx){
    vertices[i].pos = fx(x);
    DEBUG_PRINTF("%s\n", format_gl_position(vertices[i].pos));
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
  }
  line->draw_mode = GL_LINE_STRIP;
  return AS_DRAWABLE(line);
}
/*
  Draws a n x m x l grid of points. The top, left, front of the grid is
  located at (x_0, y_0, z_0) and the bottom, right, back at (x_n, y_m, z_l),
  I might add an argument / make a seperate function with that takes a function
  pointer f(x,y,z) -> (x,y,z)

*/

gl_drawable* MAKE_NAME(grid)(float x_0, float x_n,
                             float y_0, float y_m,
                             float z_0, float z_l,
                             gl_position(*f)(float,float,float),
                             int n, int m, int l, COLOR_ARG){
  int i,j,k;
  double x,y,z;
  double dx = (x_n - x_0)/n;
  double dy = (y_m - y_0)/m;
  double dz = (z_l - z_0)/l;
  gl_shape *grid = (gl_shape*)make_gl_shape(n*m*l);
  grid->draw_mode = GL_POINTS;
  gl_vertex *vertices = SHAPE_VERTICES(grid);
  //we use the integer sizes as loop limits, this means we might
  //not get to the float limits exactly
  for(k=0, z=z_0; k<l; z+=dz, k++){
    for(j=0, y=y_0; j<m; y+=dy, j++){
      for(i=0, x=x_0; i<n; x+=dx, i++){
        int index = (k*n*m) + (j*n) + i;
        vertices[index].pos = f(x,y,z);
        vertices[index].color = GEN_COLOR(index, (&vertices[index]));
        //by default the grid is static wrt. the scene it's it
        vertices[index].tag = 1;
      }
    }
  }
  return (gl_drawable*)grid;
}
/*
  Generates a surface on the x-z plane with height (aka y) determined
  by a function f(x,z)->y
*/
gl_drawable* MAKE_NAME(surface)(double x_0, double x_n, int n,
                               double y_0, double y_m, int m,
                               double(*fxy)(double,double), COLOR_ARG){
  int i=0,j=0,k=0;
  double dx = (x_n-x_0)/n, dy = (y_m-y_0)/m;
  double x,z,y, zx[2], zy[2];
  gl_vertex *vertices = zmalloc(m*n*sizeof(gl_vertex));
  //we need 2 indices for every 1 vertex + 1 extra index for
  //every row, for restarting drawing
  int num_indices = m*(2*n+1);
  //I'm just using ints as indices, at least for now
  uint16_t *indices = alloca(num_indices*sizeof(uint16_t));
  /*
    The normal at a point on the surface is equal to the gradient
    at that point, the x and z components of the gradient are
    just dx and dz, the y component we need to compute numerically
    using finite differences.
   */
  zy[0] = fxy(x_0,y_0-dy);
  zy[1] = fxy(x_0,y_0+dy);
  zx[0] = fxy(x_0-dx,y_0);
  zx[1] = fxy(x_0+dx,y_0);
  for(j=0,y=y_0;j<m;j++,y+=dy){
    for(i=0,x=x_0;i<n;i++,x+=dx){
      z = fxy(x,y);
      double dz_y = (z-zy[0] + zy[1]-y + 2*(zy[0]-zy[1]))/4*dy;
      double dz_x = (y-zx[0] + zx[1]-y + 2*(zx[0]-zx[1]))/4*dx;
      gl_position grad = POSITION(dx, (dz_x+dz_y)/2,dy);
      int index = j*n + i;
      vertices[index].pos = POSITION_CAST(x,z,y);
      //      DEBUG_PRINTF("Setting color to %s\n",format_gl_color(GEN_COLOR(index)));
      vertices[index].normal = gl_position_to_normal(grad);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));

      indices[k++] = index;
      indices[k++] = index + n;
    }
    //primitive restart
    indices[k++] = (uint16_t)-1;
  }
  return (void*)init_gl_shape_indexed(vertices, m*n,
                               (gl_index_t*)indices, m*(2*n+1), 2);
}
/*
  A tetrahedron is specified by 4 verticies, 3 vertices which specify
  a 'base' triangle and one other vertex which specifies the 'top' of
  the tetrahedron
*/
#if 0
gl_drawable *MAKE_NAME(tetrahedron)(gl_position *base, gl_position apex,
                                    COLOR_ARG){
  /*
    Order of vertices for triangle strips:
    apex, base[0], base[1], base[2], apex, base[0]
  */
  static uint8_t tetra_indices[6] = {3, 0, 1, 2, 3, 0};
  gl_polygon *tetra = (void*)make_polygon_indexed(4, 6);
  tetra->type = GL_drawable_3d_polygon;
  gl_vertex *vertices = SHAPE_VERTICES(tetra);
  int i;
  //set position and color of vertices
  for(i=0;i<3;i++){
    vertices[i].pos = base[i];
    vertices[i].color = GEN_COLOR(i);
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
  }
  vertices[i].pos = apex;
  vertices[i].color = GEN_COLOR(i, (&vertices[i]));
  memcpy(tetra->indices, tetra_indices, sizeof(tetra_indices));
  return (gl_drawable*)tetra;
}
/*
  An octahedron needs 6 vertices, a square base, the vertex at the top,
  and the vertex at the bottom.
  since the octahedron is regular we 'can' compute the bottom vertex
  from the top, but that's complicated.
*/
gl_drawable *MAKE_NAME(octahedron)(gl_position *base, gl_position apex,
                                   gl_position nadir, COLOR_ARG){
  /*
    base is assumed to be clockwise.
    Order of vertices for triangle strips:
    apex, base[0], base[1], base[2], apex, base[3], base[0], restart
    nadir, base[2], base[3], base[0], nadir, base[1], base[2]
  */
  static uint8_t oct_indices[15] =
    {0, 1, 2, 3, 0, 4, 1, 0xff, 5, 3, 4, 1, 5, 2, 3};
  gl_polygon *oct = (void*)make_polygon_indexed(6, 15);
  oct->type = GL_drawable_3d_polygon;
  gl_vertex *vertices = SHAPE_VERTICES(oct);
  int i = 0;
  //set position and color of vertices
  vertices[i].pos = apex;
  vertices[i].color = GEN_COLOR(i);
  for(i=1;i<5;i++){
    vertices[i].pos = base[i];
    vertices[i].color = GEN_COLOR(i, (&vertices[i]));
  }
  vertices[i].pos = nadir;
  vertices[i].color = GEN_COLOR(i, (&vertices[i]));
  memcpy(oct->indices, oct_indices, sizeof(oct_indices));
  return (gl_drawable*)oct;
}
#endif
/*
  This might not work, it's a translation of some
  public domain code to draw a sphere using
  old opengl

  It shouldn't be too hard to calculate indices to use for this
 */
gl_drawable *MAKE_NAME(sphere)(float radius, COLOR_ARG){
/*
  Not every value of step works, due to floating point rounding issues,
  Honestly it's pretty hard to pick a good value for it.
 */
  const int step = 10*M_PI;
  double a,b, da = M_PI/step, db = M_PI/step;
  float x,y,z;
  int i = 0,j = 0;
  int h = step+1, w = 4*(step+1);
  gl_vertex *vertices = zmalloc(w*h*sizeof(gl_vertex));
  double sa, sb, ca, cb, sa_1, ca_1;
  sincos(-M_PI/2, &sa_1, &ca_1);

  for(a = -M_PI_2, i=0; i<h;a = MIN(a+da, M_PI_2-da),i++){
    assert(i<h);
    sa = sa_1; ca = ca_1;
    sincos((a + da), &sa_1, &ca_1);
    for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
      if(j>=w){
        DEBUG_PRINTF("j < w\nj = %d, w = %d, 2pi-b > 0 = %d\n",j,w,2*M_PI-b>=0);
        abort();
      }
      //I could be wrong about the normals here
      gl_position pos;
      int index = i*w + j++;
      sincos(b, &sb, &cb);
      pos.w = 1;

      pos.x = radius * cb * ca;
      pos.y = radius * sb * ca;
      pos.z = radius * sa;
      vertices[index].pos = pos;
      vertices[index].normal =
        gl_position_to_normal(POSITION_CAST(cb*ca, sb*ca, sa));
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));

      index = i*w + j++;
      pos.x = radius * cb * ca_1;
      pos.y = radius * sb * ca_1;
      pos.z = radius * sa_1;
      vertices[index].pos = pos;
//Maybe subtracting 1 from the index here will fix normals?
      vertices[index].normal =
        gl_position_to_normal(POSITION_CAST(cb*ca_1, sb*ca_1, sa_1));
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));
    }
  }
  DEBUG_PRINTF("w = %d, j = %d\nh = %d, i = %d\n",w,j,h,i);
  gl_shape *sphere = (gl_shape*)init_gl_shape(vertices, w*h);
  return (void*)sphere;
}
/*
  Using indices cuts the number of vertices in half, which is a reduction of
  1000s of vertices, so it's probably worth it.
*/
gl_drawable *MAKE_NAME(sphere_indexed)(float radius, COLOR_ARG){
  const int step = 10*M_PI;
  double a,b, da = M_PI/step, db = M_PI/step;
  float x,y,z;
  int i = 0,j = 0;
  int h = step+1,w = 2*(step+1);
  int num_indices = 2*(w*h);
  int num_vertices = w*h;
  gl_vertex *vertices = zmalloc(num_vertices*sizeof(gl_vertex));
  uint16_t *indices = xmalloc(num_indices * sizeof(uint16_t));
  uint16_t *index_ptr = indices;
  double sa, sb, ca, cb;
  for(a = -M_PI_2, i=0; i<h;a = MIN(a+da, M_PI_2),i++){
    sincos(a, &sa, &ca);
    for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
      *index_ptr++ = i*w + j;
      *index_ptr++ = (i+1)*w + j;
      gl_position pos;
      int index = i*w + j++;
      sincos(b, &sb, &cb);
      pos.w = 1;

      pos.x = radius * cb * ca;
      pos.y = radius * sb * ca;
      pos.z = radius * sa;
      vertices[index].pos = pos;
      vertices[index].normal =
        gl_position_to_normal(POSITION_CAST(cb*ca, sb*ca, sa));
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));
    }
  }
  gl_shape *sphere = init_gl_shape_indexed(vertices, num_vertices,
                                           (gl_index_t*)indices,
                                           num_indices, 2);
  //  free(indices);
  sphere->draw_mode = GL_TRIANGLE_STRIP;
  return AS_DRAWABLE(sphere);
}
/*
  This is basically the same as the sphere, except the width of the generated
  circles doesn't vary with height
*/
gl_drawable *MAKE_NAME(cylinder)(float radius, float height, COLOR_ARG){
/*
  Not every value of step works, due to floating point rounding issues
 */
  const int step = 10*M_PI;
  double a,b, da = M_PI/step, db = M_PI/step;
  float x,y,z;
  int i = 0, j = 0;
  int h = step+1, w = 4*(step+1);
  //need 2 extra rows for the top/bottom
  gl_vertex *vertices = zmalloc(w*(h+2)*sizeof(gl_vertex));
  double sb, cb;
  //do the bottom
  gl_position center_base = POSITION(0,-height/2, 0);
  for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){

      int index = j++;
      sincos(b, &sb, &cb);
      vertices[index].pos = center_base;
      vertices[index].normal = gl_position_to_normal(center_base);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));

      index = j++;
      gl_position pos = POSITION(cb*radius, -height/2, sb*radius);

      vertices[index].pos = pos;
      vertices[index].normal = gl_position_to_normal(center_base);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));
  }
  for(i=0; i<h; i++){
    float y = ((float)i/h)*height - height/2.0;
    float y_1 = ((float)(i+1)/h)*height - height/2.0;
    for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
      if(j>=w){
        DEBUG_PRINTF("j < w\nj = %d, w = %d, b = %f\n",j,w,b);
        abort();
      }
      gl_position pos;
      int index = (i+1)*w + j++;
      sincos(b, &sb, &cb);
      pos.w = 1;
      pos.x = radius * cb;
      pos.y = y;
      pos.z = radius * sb;
      vertices[index].pos = pos;
      vertices[index].normal = gl_position_to_normal(vertices[index].pos);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));

      index = (i+1)*w + j++;
      pos.x = radius * cb;
      pos.y = y_1;
      pos.z = radius * sb;
      vertices[index].pos = pos;
      vertices[index].normal = gl_position_to_normal(vertices[index].pos);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));
    }
  }
  //do the top
  gl_position center_top = POSITION(0,height/2,0);
  for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
      int index = (h+1)*w + j++;
      sincos(b, &sb, &cb);
      vertices[index].pos = center_top;
      vertices[index].normal = gl_position_to_normal(center_top);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));

      index = (h+1)*w + j++;
      gl_position pos = POSITION(cb*radius, height/2, sb*radius);

      vertices[index].pos = pos;
      vertices[index].normal = gl_position_to_normal(center_top);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));
  }
  //need to change the name of this functions
  gl_shape *cylinder = (gl_shape*)init_gl_shape(vertices, w*(h+2));
  cylinder->draw_mode = GL_TRIANGLE_STRIP;
  return AS_DRAWABLE(cylinder);
}
gl_drawable *MAKE_NAME(cylinder_indexed)(float radius, float height, COLOR_ARG){
/*
  Not every value of step works, due to floating point rounding issues
 */
  const int step = 10*M_PI;
  double a,b, da = M_PI/step, db = M_PI/step;
  float x,y,z;
  int i = 0, j = 0;
  int h = step+1, w = 2*(step+1);
  //need 2 extra rows for the top/bottom
  int num_indices = 2*(w*(h+2));
  int num_vertices = w*(h+2);
  gl_vertex *vertices = zmalloc(num_vertices*sizeof(gl_vertex));
  uint16_t *indices = xmalloc(num_indices * sizeof(uint16_t));
  uint16_t *index_ptr = indices;
  
  double sb, cb;
  //do the bottom
  gl_position center_base = POSITION(0,-height/2, 0);
  for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
    int index = j++;
    *index_ptr++ = 0;
    *index_ptr++ = i++;

    sincos(b, &sb, &cb);
    vertices[index].pos = center_base;
    vertices[index].normal = gl_position_to_normal(center_base);
    vertices[index].color = GEN_COLOR(index, (&vertices[index]));

    index = j++;
    gl_position pos = POSITION(cb*radius, -height/2, sb*radius);

    vertices[index].pos = pos;
    vertices[index].normal = gl_position_to_normal(center_base);
    vertices[index].color = GEN_COLOR(index, (&vertices[index]));
  }
  for(i=1; i<h; i++){
    float y = ((float)i/h)*height - height/2.0;
    for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
      *index_ptr++ = i*w + j;
      *index_ptr++ = (i+1)*w+j;
      gl_position pos;
      int index = i*w + j++;
      sincos(b, &sb, &cb);
      pos.w = 1;
      pos.x = radius * cb;
      pos.y = y;
      pos.z = radius * sb;
      vertices[index].pos = pos;
      vertices[index].normal = gl_position_to_normal(vertices[index].pos);
      vertices[index].color = GEN_COLOR(index, (&vertices[index]));
    }
  }
  //do the top
  gl_position center_top = POSITION(0,height/2,0);
  for(b = 0.0, j=0; j<w;b = MIN(b+db,2*M_PI)){
    *index_ptr++ = i*w + j;//i = h
    *index_ptr++ = (i+1)*w+j;
    int index = (i+1)*w + j++;
    sincos(b, &sb, &cb);
    vertices[index].pos = center_top;
    vertices[index].normal = gl_position_to_normal(center_top);
    vertices[index].color = GEN_COLOR(index, (&vertices[index]));

    index = (i+1)*w + j++;
    gl_position pos = POSITION(cb*radius, height/2, sb*radius);

    vertices[index].pos = pos;
    vertices[index].normal = gl_position_to_normal(center_top);
    vertices[index].color = GEN_COLOR(index, (&vertices[index]));
  }
  gl_shape *cylinder = (gl_shape*)init_gl_shape(vertices, w*(h+2));
  cylinder->draw_mode = GL_TRIANGLE_STRIP;
  return AS_DRAWABLE(cylinder);
}
#undef MAKE_NAME
#undef _MAKE_NAME
#undef PREFIX
#undef COLOR_ARG
#undef GEN_COLOR
#undef COLOR_ARG_NAME

/*
*/
/* Vertices/normals/indices taken from freeglut's fg_geometry.c */
#if 0
static const gl_position icosahedron_v[ICOSAHEDRON_NUM_VERT*3] =
  {POSITION(1.0f, 0.0f, 0.0f),
   POSITION(0.447213595500f, 0.894427191000f, 0.0f,),
   POSITION(0.447213595500f, 0.276393202252f, 0.850650808354f),
   POSITION(0.447213595500f, -0.723606797748f, 0.525731112119f),
   POSITION(0.447213595500f, -0.723606797748f, -0.525731112119f),
   POSITION(0.447213595500f, 0.276393202252f, -0.850650808354f),
   POSITION(-0.447213595500f, -0.894427191000f, 0.0f),
   POSITION(-0.447213595500f, -0.276393202252f, 0.850650808354f),
   POSITION(-0.447213595500f, 0.723606797748f, 0.525731112119f),
   POSITION(-0.447213595500f, 0.723606797748f, -0.525731112119f),
   POSITION(-0.447213595500f, -0.276393202252f, -0.850650808354f),
   POSITION(-1.0f, 0.0f, 0.0f)};
gl_drawable *MAKE_NAME(default_icosahedron)(COLOR_ARG){
  static int initialized = 0;
  static gl_indices indices;
  if(initialized == 0){
    /* Vertex indices */
    static GLubyte icosahedron_indices[60] =
      {0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 1, 1, 8, 2, 2, 7, 3, 3, 6,
       4, 4, 10, 5, 5, 9, 1, 1, 9, 8, 2, 8, 7, 3, 7, 6, 4, 6, 10, 5, 10, 9,
       11, 9, 10, 11, 8, 9, 11, 7, 8, 11, 6, 7, 11, 10, 6};
    indices.num_indices = 60;
    indices.index_size = 1;
    indices.index_type = GL_UNSIGNED_BYTE;
    indices.index_packing = 4;
    indices.indices = (gl_index_t*)icosahedron_indices;
    indices.is_static = 1;
    init_index_buffer(&indices);
  }
  
/* -- Icosahedron -- */
#define ICOSAHEDRON_NUM_VERT           12
#define ICOSAHEDRON_NUM_FACES          20
#define ICOSAHEDRON_NUM_EDGE_PER_FACE  3
#define ICOSAHEDRON_VERT_PER_OBJ       (ICOSAHEDRON_NUM_FACES*ICOSAHEDRON_NUM_EDGE_PER_FACE)
#define ICOSAHEDRON_VERT_ELEM_PER_OBJ  (ICOSAHEDRON_VERT_PER_OBJ*3)
#define ICOSAHEDRON_VERT_PER_OBJ_TRI   ICOSAHEDRON_VERT_PER_OBJ
/* Vertex Coordinates */

/* Normal Vectors:
 * icosahedron_n[i][0] = ( icosahedron_v[icosahedron_vi[i][1]][1] - icosahedron_v[icosahedron_vi[i][0]][1] ) * ( icosahedron_v[icosahedron_vi[i][2]][2] - icosahedron_v[icosahedron_vi[i][0]][2] ) - ( icosahedron_v[icosahedron_vi[i][1]][2] - icosahedron_v[icosahedron_vi[i][0]][2] ) * ( icosahedron_v[icosahedron_vi[i][2]][1] - icosahedron_v[icosahedron_vi[i][0]][1] ) ;
 * icosahedron_n[i][1] = ( icosahedron_v[icosahedron_vi[i][1]][2] - icosahedron_v[icosahedron_vi[i][0]][2] ) * ( icosahedron_v[icosahedron_vi[i][2]][0] - icosahedron_v[icosahedron_vi[i][0]][0] ) - ( icosahedron_v[icosahedron_vi[i][1]][0] - icosahedron_v[icosahedron_vi[i][0]][0] ) * ( icosahedron_v[icosahedron_vi[i][2]][2] - icosahedron_v[icosahedron_vi[i][0]][2] ) ;
 * icosahedron_n[i][2] = ( icosahedron_v[icosahedron_vi[i][1]][0] - icosahedron_v[icosahedron_vi[i][0]][0] ) * ( icosahedron_v[icosahedron_vi[i][2]][1] - icosahedron_v[icosahedron_vi[i][0]][1] ) - ( icosahedron_v[icosahedron_vi[i][1]][1] - icosahedron_v[icosahedron_vi[i][0]][1] ) * ( icosahedron_v[icosahedron_vi[i][2]][0] - icosahedron_v[icosahedron_vi[i][0]][0] ) ;
*/
static GLfloat icosahedron_n[ICOSAHEDRON_NUM_FACES*3] =
{
     0.760845213037948f,  0.470228201835026f,  0.341640786498800f,
     0.760845213036861f, -0.179611190632978f,  0.552786404500000f,
     0.760845213033849f, -0.581234022404097f,                0.0f,
     0.760845213036861f, -0.179611190632978f, -0.552786404500000f,
     0.760845213037948f,  0.470228201835026f, -0.341640786498800f,
     0.179611190628666f,  0.760845213037948f,  0.552786404498399f,
     0.179611190634277f, -0.290617011204044f,  0.894427191000000f,
     0.179611190633958f, -0.940456403667806f,                0.0f,
     0.179611190634278f, -0.290617011204044f, -0.894427191000000f,
     0.179611190628666f,  0.760845213037948f, -0.552786404498399f,
    -0.179611190633958f,  0.940456403667806f,                0.0f,
    -0.179611190634277f,  0.290617011204044f,  0.894427191000000f,
    -0.179611190628666f, -0.760845213037948f,  0.552786404498399f,
    -0.179611190628666f, -0.760845213037948f, -0.552786404498399f,
    -0.179611190634277f,  0.290617011204044f, -0.894427191000000f,
    -0.760845213036861f,  0.179611190632978f, -0.552786404500000f,
    -0.760845213033849f,  0.581234022404097f,                0.0f,
    -0.760845213036861f,  0.179611190632978f,  0.552786404500000f,
    -0.760845213037948f, -0.470228201835026f,  0.341640786498800f,
    -0.760845213037948f, -0.470228201835026f, -0.341640786498800f,
};


DECLARE_SHAPE_CACHE(icosahedron,Icosahedron,ICOSAHEDRON)
/* -- Dodecahedron -- */
/* Magic Numbers:  It is possible to create a dodecahedron by attaching two
 * pentagons to each face of of a cube. The coordinates of the points are:
 *   (+-x,0, z); (+-1, 1, 1); (0, z, x )
 * where x = (-1 + sqrt(5))/2, z = (1 + sqrt(5))/2 or
 *       x = 0.61803398875 and z = 1.61803398875.
 */
#define DODECAHEDRON_NUM_VERT           20
#define DODECAHEDRON_NUM_FACES          12
#define DODECAHEDRON_NUM_EDGE_PER_FACE  5
#define DODECAHEDRON_VERT_PER_OBJ       (DODECAHEDRON_NUM_FACES*DODECAHEDRON_NUM_EDGE_PER_FACE)
#define DODECAHEDRON_VERT_ELEM_PER_OBJ  (DODECAHEDRON_VERT_PER_OBJ*3)
#define DODECAHEDRON_VERT_PER_OBJ_TRI   (DODECAHEDRON_VERT_PER_OBJ+DODECAHEDRON_NUM_FACES*4)    /* 4 extra edges per face when drawing pentagons as triangles */
/* Vertex Coordinates */
static GLfloat dodecahedron_v[DODECAHEDRON_NUM_VERT*3] =
{
               0.0f,  1.61803398875f,  0.61803398875f,
    -          1.0f,            1.0f,            1.0f,
    -0.61803398875f,            0.0f,  1.61803398875f,
     0.61803398875f,            0.0f,  1.61803398875f,
               1.0f,            1.0f,            1.0f,
               0.0f,  1.61803398875f, -0.61803398875f,
               1.0f,            1.0f, -          1.0f,
     0.61803398875f,            0.0f, -1.61803398875f,
    -0.61803398875f,            0.0f, -1.61803398875f,
    -          1.0f,            1.0f, -          1.0f,
               0.0f, -1.61803398875f,  0.61803398875f,
               1.0f, -          1.0f,            1.0f,
    -          1.0f, -          1.0f,            1.0f,
               0.0f, -1.61803398875f, -0.61803398875f,
    -          1.0f, -          1.0f, -          1.0f,
               1.0f, -          1.0f, -          1.0f,
     1.61803398875f, -0.61803398875f,            0.0f,
     1.61803398875f,  0.61803398875f,            0.0f,
    -1.61803398875f,  0.61803398875f,            0.0f,
    -1.61803398875f, -0.61803398875f,            0.0f
};
/* Normal Vectors */
static GLfloat dodecahedron_n[DODECAHEDRON_NUM_FACES*3] =
{
                0.0f,  0.525731112119f,  0.850650808354f,
                0.0f,  0.525731112119f, -0.850650808354f,
                0.0f, -0.525731112119f,  0.850650808354f,
                0.0f, -0.525731112119f, -0.850650808354f,

     0.850650808354f,             0.0f,  0.525731112119f,
    -0.850650808354f,             0.0f,  0.525731112119f,
     0.850650808354f,             0.0f, -0.525731112119f,
    -0.850650808354f,             0.0f, -0.525731112119f,

     0.525731112119f,  0.850650808354f,             0.0f,
     0.525731112119f, -0.850650808354f,             0.0f,
    -0.525731112119f,  0.850650808354f,             0.0f,
    -0.525731112119f, -0.850650808354f,             0.0f,
};

/* Vertex indices */
static GLubyte dodecahedron_vi[DODECAHEDRON_VERT_PER_OBJ] =
{
     0,  1,  2,  3,  4,
     5,  6,  7,  8,  9,
    10, 11,  3,  2, 12,
    13, 14,  8,  7, 15,

     3, 11, 16, 17,  4,
     2,  1, 18, 19, 12,
     7,  6, 17, 16, 15,
     8, 14, 19, 18,  9,

    17,  6,  5,  0,  4,
    16, 11, 10, 13, 15,
    18,  1,  0,  5,  9,
    19, 14, 13, 10, 12
};
DECLARE_SHAPE_CACHE_DECOMPOSE_TO_TRIANGLE(dodecahedron,Dodecahedron,DODECAHEDRON)
#endif
