#ifndef __TREE_H__
#define __TREE_H__
#ifdef __cplusplus
extern "C" {
#endif
/*
  All kinds of linked lists/binary trees, some have their own headers
  which get included here.
*/
//regular old cons cells
#include "cons.h"

//red-black trees
struct rb_node {
  uint64_t parent_color;
  struct rb_node *left;
  struct rb_node *right;
};
//skip lists
//macros that can be used to traverse a skip list like a linked list
#define SLIST_XCAR(node) (node->data)
#define SLIST_XCDR(node) (node->links[0].next)
//Returns the first element of the underlying linked list of ls
#define SLIST_GET_LIST(ls) (ls->head[0].next)

struct skip_list {
  //The head node is a dummy node and is used to simplify insertion/deletion
  //and does not contain any data;
  struct skip_node *head;
  int (*cmp)(void*,void*);
  int max_height;
  int height;
  int length;
};
struct skip_link {
  struct skip_node *next;
  int width;//number of elements b/t current node and next
};
struct skip_node {
  void *data;
  struct skip_link *links;
  int height;//length of links array
};
#ifdef __cplusplus
}
#endif
#endif /* __TREE_H__ */
