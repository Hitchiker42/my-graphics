#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
static gl_material gold_ish = 
  {{.specular = {.r = 0.5, .g = 0.5, .b = 0.1}},
   {.diffuse = {.r = 0.1, .g = 0.1, .b = 0.1}},
   {.ambient = {.r = 3, .g = 3, .b = 3}},
   .shininess = 16};
static gl_material grass_ish = 
  {{.specular = {.r = 0.1, .g = 0.1, .b = 0.1}},
   {.diffuse = {.r = 0.1, .g = 0.5, .b = 0.1}},
   {.ambient = {.r = 3, .g = 3, .b = 3}},
   .shininess = 4};
static gl_material mirror = 
  {{.specular = {.r = 1, .g = 1, .b = 1}},
   {.diffuse = {.r = 0, .g = 0, .b = 0}},
   {.ambient = {.r = 3, .g = 3, .b = 3}},
   .shininess = 64};
static gl_material diffuse_red =
  {{.specular = {0}}, {.diffuse = {.r = 1, .g = 0, .b = 0}},
   {.ambient = {.3,.3,.3}}, .shininess = 8};
static gl_material diffuse_blue =
  {{.specular = {0}}, {.diffuse = {.r = 0, .g = 0, .b = 1}},
   {.ambient = {.3,.3,.3}}, .shininess = 8};
static gl_material diffuse_green =
  {{.specular = {0}}, {.diffuse = {.r = 0, .g = 1, .b = 0}},
   {.ambient = {.3,.3,.3}}, .shininess = 8};
/*
  These are all positioned at the most negitive x y and z, pointing
  toward the most positive x,y and z
*/
static gl_light max_light = 
  {{.location = POSITION(-1,-1,-1,1),
    .direction = POSITION(M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 1,.g = 1,.b = 1, .a = 1},
    .specular = {.r = 1,.g = 1,.b = 1, .a = 1}}};
static gl_light diffuse_only_light = 
  {{.location = POSITION(-1,-1,-1,1),
    .direction = POSITION(M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {.r = 1,.g = 1,.b = 1, .a = 1},
    .specular = {0}}};
static gl_light specular_only_light = 
  {{.location = POSITION(-1,-1,-1,1),
    .direction = POSITION(M_SQRT2_2, M_SQRT2_2, M_SQRT2_2, 0),
    .diffuse = {0},
    .specular = {.r = 1,.g = 1,.b = 1, .a = 1}}};
/*
  This should remain fairly uncomplicated, it draws a single shape,
  and has the ability to rotate the scene, and move the shape.  
  
  It also draws axes and a 3d grid to help show depth
*/
thread_local global_context *thread_context;
struct userdata {
  float alpha_x;
  float alpha_y;
  float alpha_z;
  float translate[3];
  float movement_delta;  
  int rotate;
  int movement;
};
/*
  TODO: use a more elegent solution for determining how to move/rotate
  than bitmasks (or at least make it look nicer). 
*/
void update_scene(gl_scene *scene, struct userdata *data){
  gl_drawable *drawable = scene_get_drawable(scene, 0);
  mat4_identity(drawable->transform);
  if(data->rotate){
    /*
      This really ought to be done by rotating around some vector, rather
      than rotating 3 times. 
    */
    if(data->rotate & 1){
      data->alpha_x = fmod(data->alpha_x + (2*M_PI)/60, 2*M_PI);
    }
    if(data->rotate & 2){
      data->alpha_y = fmod(data->alpha_y + (2*M_PI)/60, 2*M_PI);
    }
    if(data->rotate & 4){
      data->alpha_z = fmod(data->alpha_z + (2*M_PI)/60, 2*M_PI);
    }
  }
  if(data->movement){
    if(data->movement & 1){
      data->translate[0] += data->movement_delta;
    } else if(data->movement & 2){
      data->translate[0] -= data->movement_delta;
    }
    if(data->movement & 4){
      data->translate[1] += data->movement_delta;
    } else if(data->movement & 8){
      data->translate[1] -= data->movement_delta;
    }
    if(data->movement & 0x10){
      data->translate[2] += data->movement_delta;
    } else if(data->movement & 0x20){
      data->translate[2] -= data->movement_delta;
    }
  }
  mat4_translate(drawable->transform, data->translate, NULL);
  mat4_rotateX(drawable->transform,
               data->alpha_x, NULL);
  mat4_rotateY(drawable->transform,
               data->alpha_y, NULL);
  mat4_rotateZ(drawable->transform,
               data->alpha_z, NULL);
}
/*
  I'm not sure how efficent it'd be but it'd sure be fun to make a 
  dispatch table for key actions and then this could just be:
  key_callbacks[key](key, scancode, action, modmask);
  or similar
*/
#define case_set_bitmask(thing, bit, action)                            \
  if(action == GLFW_RELEASE){                                           \
    thing &= ~(bit);                                                    \
  } else if(action == GLFW_PRESS){                                      \
    thing |= bit;                                                       \
  }                                                                     \
  break;

//TODO: At least change the numeric constants for the bitmask to an enum
void key_callback(gl_window win, int key, int scancode,
                  int action, int modmask){
  struct userdata *data = thread_context->userdata;
  switch(key){
    case 'x':
    case 'X':
      case_set_bitmask(data->rotate, 1, action);
    case 'y':
    case 'Y':
      case_set_bitmask(data->rotate, 2, action);
    case 'z':
    case 'Z':
      case_set_bitmask(data->rotate, 4, action);
    case 'r':
    case 'R':
      if(action == GLFW_RELEASE){        
        //mat4_identity(thread_context->scenes[0]->projection);
        data->rotate = data->alpha_x = data->alpha_y = data->alpha_z = 0;
        data->translate[0] = data->translate[1] = data->translate[2] = 0;
        data->movement = 0;
      }
      break;
    case GLFW_KEY_RIGHT:
      case_set_bitmask(data->movement, 1, action);
    case GLFW_KEY_LEFT:
      case_set_bitmask(data->movement, 2, action);
    case GLFW_KEY_UP:
      if(modmask & GLFW_MOD_SHIFT){
        case_set_bitmask(data->movement, 0x10, action);
      } else {
        case_set_bitmask(data->movement, 4, action);
      }
    case GLFW_KEY_DOWN:
      if(modmask & GLFW_MOD_SHIFT){
        case_set_bitmask(data->movement, 0x20, action);
      } else {
        case_set_bitmask(data->movement, 8, action);
      }
  }
}
gl_position y_equals_x(float x){
  gl_position ret = POSITION(x,x,0,1);
  return ret;
}
gl_color gradient(int i){
  //this is cheating a bit, since it's using info from make_sphere
  const int step = 10*M_PI;
  double max = 5*step;
  int intensity = (i/max) * 255;
  gl_color ret;
  ret.g = ret.r = ret.b = intensity;
  ret.a = 0xff;
  return ret;
}
static gl_drawable *init_drawable(){
    gl_drawable *x = make_sphere(0.5, gradient);
    gl_position trans = POSITION(0,0,-.75);
    //    translate_shape(x, &trans);
//  gl_drawable *x = make_cylinder(0.5, 1, gradient);
//  gl_drawable *x = make_colored_default_tetrahedron(0.5, gl_color_blue);
//  gl_drawable *x = make_colored_default_octahedron(0.5, gl_color_blue);
  return x;
/*  gl_drawable *primitives[4];
  primitives[0] = make_colored_default_cube(0.5,gl_color_transparent_cyan);
  primitives[1] = make_colored_default_tetrahedron(0.5,gl_color_transparent_yellow);
  primitives[2] = make_colored_default_octahedron(0.5,gl_color_transparent_magenta);
  primitives[3] = make_colored_sphere(0.5, gl_color_transparent_white);
  gl_position offsets[4] = {{0.25,0.25,0},{-0.25,0.25,0},
                         {0.25,-0.25,0},{-0.25, -0.25, 0}};
  int i;
  for(i=0;i<4;i++){
    mat4_translate(primitives[i]->transform, (float*)&offsets[i], NULL);
  }
  gl_drawable *composite = init_composite_drawable(primitives, 4,
                                                   COMPOSITE_INIT_NONE);
  return composite;*/
}
static const int grid_step = 20;
gl_position grid_gen(float x, float y, float z){
  float d = z/20.0;
  gl_position ret = POSITION(x+d,y+d,z);
  return ret;
}
gl_color grid_color(int i){
  int z = i/(grid_step*grid_step);
  int y = (i%(grid_step*grid_step))/grid_step;
  int x = (i%(grid_step*grid_step))%grid_step;
  gl_color ret = {.r = z*(0xff/grid_step),
                  .g = y*(0xff/grid_step),
                  .b = x*(0xff/grid_step), .a = 0xff};

  return ret;
}
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "simple test");
  context_allocate_userdata(ctx, sizeof(struct userdata), NULL);
  struct userdata *data = ctx->userdata;
  data->movement_delta = 0.05;
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;
  gl_scene *scene = make_simple_scene(basic_vertex, basic_fragment);
  //These 2 transformations are the default
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  //  mat4_ortho(-1,1,-1,1,-1,1,scene->projection);
  
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  mat4_perspective(135.0, 1, 0.25, 100, scene->projection);
  
  scene->update_scene = (void*)update_scene;
  ctx->scenes[0] = scene;
  glfwSetKeyCallback(ctx->window, key_callback);
  gl_drawable *drawable;
  /*
   */
  glClearColor(1,0,0,1);
  drawable = init_drawable();
  glEnable(GL_LINE_SMOOTH);
//  glLineWidth(2.0);
  gl_drawable *axes = make_colored_axes_drawable(gl_color_cyan,
                                                 gl_color_magenta,
                                                 gl_color_yellow);
  gl_drawable *grid = make_grid(-1.0, 1.0, -1.0, 1.0,
                                        -1.0, 1.0, grid_gen,
                                        grid_step, grid_step,
                                        grid_step, grid_color);
  glPointSize(2.0);
  SCENE_ADD_DRAWABLES(scene, drawable, axes, grid);
  DEBUG_PRINTF(drawable_to_string(drawable));
  gl_main_loop(ctx);
}
