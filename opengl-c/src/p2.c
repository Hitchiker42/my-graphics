#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
thread_local global_context *thread_context;
static const char *vertex_shader =
  "#version 330 core\n"
  "layout(location = 0) in vec4 position;\n"
  "layout(location = 1) in vec4 color;\n"
  //unused but part of the default vertex attribute layout
  "layout(location = 2) in vec4 normals;\n"
  "layout(location = 3) in vec2 tex_coords;\n"
  "layout(row_major) uniform;\n"
  "layout(std140) uniform;\n"
  "out vec4 v_color;\n"
  "uniform transform {\n"
  "  mat4 model;\n"
  "  mat4 proj_view;\n"
  "};"
  "void main(){\n"
  "  if(normals[3] != 0){\n"
  "    gl_Position = position;\n"
  "  } else {\n"
  "  gl_Position = position * model * proj_view;\n"
  "  }\n"
  "  v_color = color;\n"
  "}\n";
static const char *fragment_shader =
  "#version 330 core\n"
  "in vec4 v_color;\n"
  "out vec4 f_color;\n"
  "void main(){\n"
  "  f_color = v_color;\n"
  "}\n";
/* This shader generates x,y and z axes using instanced rendering,
   the only input data used is the instance number and the
   max number of instances
*/
static const char *axes_vertex_shader =
  "#version 330 core\n"
  "layout(row_major) uniform;\n"
  "layout(std140) uniform;\n"
  "flat out vec4 v_color;\n"
  "uniform transform {\n"
  "  mat4 model;\n"
  "  mat4 proj_view;\n"
//instance_max/3, to minimize division in the shader
  "  int instance_max_3;\n"
  "};"
  "void main(){\n"
  "  float coord = ((2*gl_InstanceID)/instance_max_3)-1;\n"
  "  int is_even = gl_InstanceID & 1;\n"
  "  if(gl_InstanceID < instance_max_3){\n"
  "    glPosition = vec4(coord,0,0,1);\n"
  "    v_color = vec4(1);\n"
  "  } else if(gl_InstanceID > 2*instance_max_3){\n"
  "    glPosition = vec4(0,coord,0,1;\n"
  "    v_color = vec4(1);\n"
  "  } else {\n"
//the Z axis is drawn as a dotted line with y = x = z
  "    glPosition = vec4(coord);\n"
  "    v_color = vec4(is_even, is_even, is_even, 1);\n"
  "  }\n"
  "}\n";
struct userdata {
  float alpha_x;
  float alpha_y;
  float alpha_z;
  float angles[3];
  float psi;
  float psi_inc;
  int frames_per_orbit;
  int count;//simple monotonic frame counter
  int rotate;//which axes to rotate
  unsigned int scene_index;//which scene is active
};
static const char *scene_descriptions[] =
  {"Scene Rotation, transforms and composite objects",
   "Deeply nested objects", "Animation...kinda"};
void key_callback(gl_window win, int key, int scancode, 
                  int action, int modmask){
//  double x,y;
//  glfwGetCursorPos(win, &x, &y);
#if 0
  if(action == GLFW_PRESS){
    printf("key %s pressed at location (%f,%f)\n",
           gl_key_to_string(key),x,y);
  } else if(action == GLFW_RELEASE){
    printf("key %s released at location (%f,%f)\n",
           gl_key_to_string(key),x,y);
  }
#endif
  struct userdata *data = thread_context->userdata;
  if(key == 'x' || key == 'X'){
    if(action == GLFW_PRESS){
      data->rotate |= 1;
    } else if(action == GLFW_RELEASE){
      data->rotate &= (~1);
    }
  }
  if(key == 'y' || key == 'Y'){
    if(action == GLFW_PRESS){
      data->rotate |= 2;
    } else if(action == GLFW_RELEASE){
      data->rotate &= (~2);
    }
  }
  if(key == 'z' || key == 'Z'){
    if(action == GLFW_PRESS){
      data->rotate |= 4;
    } else if(action == GLFW_RELEASE){
      data->rotate &= (~4);
    }
  }
  if(action == GLFW_RELEASE){
    if(key == 'r' || key == 'R'){
      mat4_identity(thread_context->scenes[data->scene_index]->view);
      data->rotate = data->alpha_x = data->alpha_y = data->alpha_z = 0;
    }
    if(key == GLFW_KEY_RIGHT){
      thread_context->scenes[data->scene_index]->is_active = 0;
      data->scene_index = (data->scene_index + 1) % thread_context->num_scenes;
      thread_context->scenes[data->scene_index]->is_active = 1;
      puts(scene_descriptions[data->scene_index]);
    }
    if(key == GLFW_KEY_LEFT){
      thread_context->scenes[data->scene_index]->is_active = 0;
      if(data->scene_index == 0){
        data->scene_index = thread_context->num_scenes-1;
      } else {
        data->scene_index -=1;
      }
      thread_context->scenes[data->scene_index]->is_active = 1;
      puts(scene_descriptions[data->scene_index]);
    }
  }
}
gl_color gradient(int i){
  //this is cheating a bit, since it's using info from make_sphere
  const int step = 10*M_PI;
  double max = 5*step;
  int intensity = (i/max) * 255;
  gl_color ret;
  ret.g = ret.r = ret.b = intensity;
  ret.a = 0xdd;
  return ret;
}
void update_orbit(gl_scene *scene, struct userdata *data){
  static gl_position axes[3] = {POSITION(M_SQRT1_2, M_SQRT1_2),
                                POSITION(-M_SQRT1_2, M_SQRT1_2),
                                POSITION(1,0,0)};
  float *angles = data->angles;
  gl_drawable **orbiters = scene->scene_userdata;
  int i;
  //just have everything orbit at the same rate
  if(fabsf(data->psi + data->psi_inc) > M_PI){
    data->psi_inc = -data->psi_inc;
  }
  data->psi += data->psi_inc;
  float psi = data->psi - M_PI_2;
  float s, c, ps, pc;
  sincosf(psi, &ps, &pc);
  for(i=0;i<3;i++){
    angles[i] = fmod(angles[i] + 2*M_PI/data->frames_per_orbit, 2*M_PI);
    sincosf(angles[i], &s, &c);
    gl_position offset;
    switch(i){
      case(0):
        offset = POSITION_CAST(c*0.5*ps, s*0.5*ps, 0.5*pc); break;
      case(1):
          offset = POSITION_CAST(0.5*pc, c*0.5*ps, s*0.5*ps); break;
      case(2):
          offset = POSITION_CAST(s*0.5*ps, 0.5*pc, c*0.5*ps); break;
    }
    mat4_identity(orbiters[i]->transform);
    mat4_translate(orbiters[i]->transform,
                                            offset.vec, NULL);
    mat4_rotate(orbiters[i]->transform,
                                         angles[i], axes[i].vec,NULL);

  }
  return;
}
void update_rotate_scene(gl_scene *scene, struct userdata *data){
  if(data->rotate){
    /*
      This really ought to be done by rotating around some vector, rather
      than rotating 3 times.
     */
    if(data->rotate & 1){
      data->alpha_x = fmod(data->alpha_x + (2*M_PI)/60, 2*M_PI);
    }
    if(data->rotate & 2){
      data->alpha_y = fmod(data->alpha_y + (2*M_PI)/60, 2*M_PI);
    }
    if(data->rotate & 4){
      data->alpha_z = fmod(data->alpha_z + (2*M_PI)/60, 2*M_PI);
    }
    mat4_identity(scene->view);
    mat4_rotateX(scene->view,
                 data->alpha_x, NULL);
    mat4_rotateY(scene->view,
                 data->alpha_y, NULL);
    mat4_rotateZ(scene->view,
                 data->alpha_z, NULL);
  }
}
gl_scene* init_rotate_scene(GLuint program){
  gl_drawable *primitives[4];
  gl_scene *scene = make_scene_from_program(program);
  primitives[0] = make_default_cube(0.5,gl_rand_color);
  primitives[1] = make_default_tetrahedron(0.5,gl_rand_color);
  primitives[2] = make_default_octahedron(0.5,gl_rand_color);
  primitives[3] = make_sphere(0.25, gradient);
  gl_position offsets[4] = {POSITION(0.25,0.25,0),
                            POSITION(-0.25,0.25,0),
                            POSITION(0.25,-0.25,0),
                            POSITION(-0.25, -0.25, 0)};
  int i;
  for(i=0;i<4;i++){
    mat4_translate(primitives[i]->transform, (float*)&offsets[i], NULL);
  }
  gl_drawable *composite = init_composite_drawable(primitives, 4,
                                                   COMPOSITE_INIT_REF);
  mat4_rotateZ(composite->transform, M_PI/4, NULL);
  gl_drawable *axes = make_axes_drawable(gl_color_white);
  gl_drawable *composite2 = INIT_COMPOSITE_DRAWABLE(COMPOSITE_INIT_REF,
                                                    composite, axes);
  float scale[3] = {1.5,1.5,1.5};
  float translate[3] = {-0.2,-0.2,-0.5};
  mat4_translate(composite2->transform, translate, NULL);
  mat4_scale(composite2->transform, scale, NULL);
  SCENE_ADD_DRAWABLES(scene, primitives[0],primitives[1],primitives[2],
                      primitives[3], composite, composite2, axes);
//  scene_add_drawables(scene, drawables, 7);
  scene->update_scene = (void*)update_rotate_scene;
  return scene;
}
gl_scene* init_orbit_scene(GLuint program){
  gl_scene *scene = make_scene_from_program(program);
  gl_position start = POSITION(0,0.5,0);
  gl_drawable *center = make_colored_sphere(0.5, gl_color_transparent_yellow);
  gl_drawable *orbiter = make_colored_default_cube(0.5, gl_color_transparent_red);
  gl_drawable *orbiter2 = make_colored_default_cube(0.5, gl_color_transparent_green);
  gl_drawable *orbiter3 = make_colored_default_cube(0.5, gl_color_transparent_blue);
  gl_drawable **orbiters = xmalloc(3*sizeof(gl_drawable*));
  orbiters[0] = orbiter;
  orbiters[1] = orbiter2;
  orbiters[2] = orbiter3;
  scene->scene_userdata = orbiters;
  gl_drawable *axes = make_axes_drawable(gl_color_blue);
  SCENE_ADD_DRAWABLES(scene, orbiter, orbiter2, orbiter3, axes, center);
  scene->update_scene = (void*)update_orbit;
//  assert(scene->VBO.drawables.len == 4);
  return scene;
}
gl_scene* init_boring_scene(GLuint program){
  gl_scene *scene = make_scene_from_program(program);
  gl_drawable *base = make_colored_sphere(0.25, gl_color_transparent_cyan);
  gl_drawable *composites[16];
  composites[0] = init_composite_drawable(&base, 1, COMPOSITE_INIT_NONE);
  int i;
  float s,c;
  for(i=1;i<16;i++){
    sincosf(i*((M_PI*2)/16),&s,&c);
    float translate[] = {c*(.125*i)/M_SQRT2,s*(.125*i)/M_SQRT2,
                         s*(.125*i)/M_SQRT2};
    composites[i] = INIT_COMPOSITE_DRAWABLE(COMPOSITE_INIT_REF, base, composites[i-1]);
    mat4_translate(composites[i]->transform, translate, NULL);
  }
  gl_drawable *axes = make_axes_drawable(gl_color_blue);
  scene_add_drawables(scene, composites, 16);
  scene_add_drawable(scene, axes);
  return scene;
}
global_context *init_context(int w, int h){
  thread_context = make_global_context(w, h, "p2");
  global_context *ctx = thread_context;
  context_allocate_scenes(ctx, 3);
  context_allocate_userdata(ctx, sizeof(struct userdata), NULL);
  glfwSetKeyCallback(ctx->window, key_callback);
  GLuint basic_program = create_shader_program(vertex_shader,
                                               fragment_shader);
  gl_check_error();
  ctx->scenes[0] = init_rotate_scene(basic_program);
  ctx->scenes[0]->is_active = 1;
  ctx->scenes[2] = init_boring_scene(basic_program);
  ctx->scenes[2]->is_active = 0;
//this scene doesn't work
  ctx->scenes[1] = init_orbit_scene(basic_program);
  ctx->scenes[1]->is_active = 0;

  struct userdata *data = (struct userdata*)ctx->userdata;
  data->scene_index = 0;
  data->angles[0] = data->angles[1] = data->angles[2] = M_PI;
  data->frames_per_orbit = 120;
  data->psi_inc = data->frames_per_orbit/M_PI;
//not sure if I should do this or not
  puts(scene_descriptions[data->scene_index]);
  return ctx;
}
int main(){
  thread_context = init_context(800,800);
  gl_check_error();
  gl_main_loop(thread_context);
  return 0;
}
