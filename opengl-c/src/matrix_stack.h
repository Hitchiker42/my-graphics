#ifndef __MATRIX_STACK_H__
#define __MATRIX_STACK_H__
#ifdef __cplusplus
extern "C" {
#endif
/*
  Matrix stack functions, not really needed outside of drawable.c
  Matrices are all 4x4 matrixes of floats.

  About the matrix stack:
    By default the matrix stack is not active, since it would just
    add unecessary copying and matrix multiplication for non-composite
    drawables. A composite drawable increments a flag in the scene to
    indicate the matrix stack should be used.
*/
static inline void scene_matrix_stack_push(gl_scene *scene, float *mat){
  scene->stack_ptr -= sizeof(float[16]);
  if(scene->stack_ptr < scene->matrix_stack){
    abort();//this needs to be changed...probably
  }
  memcpy(scene->stack_ptr, mat, sizeof(float[16]));
}
//does this really need to return a value?
static inline void scene_matrix_stack_pop(gl_scene *scene){
  scene->stack_ptr += sizeof(float[16]);
}
static inline void scene_matrix_stack_deinit(gl_scene *scene){
  scene->stack_ptr += sizeof(float[16]);
  //scene->use_stack should never be decremented if it's 0, but
  //it doesn't hurt to be careful
  scene->use_stack = MIN(0, scene->use_stack - 1);
}
static inline void scene_matrix_stack_dup(gl_scene *scene){
  scene_matrix_stack_push(scene, scene->stack_ptr);
}
static inline void scene_matrix_stack_push_mul(gl_scene *scene, float *mat){
  scene_matrix_stack_push(scene, mat);
  mat4_multiply(scene->stack_ptr, scene->stack_ptr+sizeof(float[16]),NULL);
}
static inline void scene_matrix_stack_mul(gl_scene *scene, float *mat){
  mat4_multiply(scene->stack_ptr, mat, NULL);
}
static inline void scene_matrix_stack_update_uniform(gl_scene *scene){
  update_uniform_block(&scene->transform, scene->stack_ptr,
                       0, 16*sizeof(float));
}

#define MATRIX_STACK_PUSH(obj) matrix_stack_push(AS_DRAWABLE(obj))
static inline void matrix_stack_push(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  scene->stack_ptr -= 16*sizeof(float);
  if(scene->stack_ptr < scene->matrix_stack){
    abort();//this needs to be changed...probably
  }
  memcpy(scene->stack_ptr, obj->transform, 16*sizeof(float));
}
#define MATRIX_STACK_POP(obj) matrix_stack_pop(AS_DRAWABLE(obj))
static inline float* matrix_stack_pop(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  float *ret = scene->stack_ptr;
  scene->stack_ptr += 16*sizeof(float);
  return ret;
}
#define MATRIX_STACK_DEINIT(obj) matrix_stack_deinit(AS_DRAWABLE(obj))
static inline void matrix_stack_deinit(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  scene->stack_ptr += 16*sizeof(float);
  //scene->use_stack should never be decremented if it's 0, but
  //it doesn't hurt to be careful
  scene->use_stack = MIN(0, scene->use_stack - 1);
}
#define MATRIX_STACK_DUP(obj) matrix_stack_dup(AS_DRAWABLE(obj))
static inline void matrix_stack_dup(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  matrix_stack_push(obj);
}
#define MATRIX_STACK_PUSH_MUL(obj) matrix_stack_push_mul(AS_DRAWABLE(obj))
static inline void matrix_stack_push_mul(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  float *temp = scene->stack_ptr;
  matrix_stack_push(obj);
  mat4_multiply(temp, scene->stack_ptr, scene->stack_ptr);
}
#define MATRIX_STACK_MUL(obj) matrix_stack_mul(AS_DRAWABLE(obj))
static inline void matrix_stack_mul(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  mat4_multiply(scene->stack_ptr, obj->transform, NULL);
}
#define MATRIX_STACK_INIT(obj) matrix_stack_init(AS_DRAWABLE(obj))
static inline void matrix_stack_init(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  if(scene->use_stack){
    matrix_stack_push_mul(obj);
  } else {
    matrix_stack_push(obj);
  }
  scene->use_stack++;
}
#define MATRIX_STACK_UPDATE_UNIFORM(obj)        \
  matrix_stack_update_uniform(AS_DRAWABLE(obj))
static inline void matrix_stack_update_uniform(gl_drawable *obj){
  gl_scene *scene = obj->scene;
  update_uniform_block(&scene->transform, scene->stack_ptr,
                       0, 16*sizeof(float));
}

#ifdef __cplusplus
}
#endif
#endif /* __MATRIX_STACK_H__ */
