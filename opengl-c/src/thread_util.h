#ifndef __THREAD_UTIL_H__
#define __THREAD_UTIL_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "atomic.h"//C11 atomics/equivlents using builtins
//we need stdlib.h for malloc, but that should be it
#include <stdlib.h>
//for now only pthreads are supported
#include <pthread.h>
/*
  OSX doesn't support unamed posix semaphores, and named posix semaphores
  are more trouble than they're worth for communication between threads
  (rather than between processes), so we need to create a semaphore interface,
  using the unamed semaphores apple does provide.
*/
#if !(defined(__APPLE__))
#include <semaphore.h>
#include <time.h> //needed for timed wait
typedef sem_t* semaphore;
static inline semaphore semaphore_create(unsigned int value){
  sem_t *sem = (sem_t*)malloc(sizeof(sem_t));
  int err = sem_init(sem, 0, value);
  if(err < 0){
    return NULL;
  } else {
    return sem;
  }
}
static inline void semaphore_destroy(semaphore sem){
  int err = sem_destroy(sem);
  //err < 0 only happens if sem isn't a semaphore, if you really need
  //to know if there was an error check errno
  if(err >= 0){
    free(sem);
  }
} 
static inline int semaphore_post(semaphore sem){
  return sem_post(sem);
}
static inline int semaphore_timed_wait(semaphore sem, long nsecs){
  struct timespec t;
  //1000000000 = 10e9, number of nanoseconds in a second
  t.tv_sec = nsecs/1000000000;
  t.tv_nsec = nsecs % 1000000000;
  return sem_timedwait(sem, &t);
  
}
static inline int semaphore_wait(semaphore sem){
  return sem_wait(sem);
}
static inline int semahpore_try_wait(semaphore sem){
  return sem_trywait(sem);
}
static inline int semaphore_value(semaphore sem){
  int val = -1;//this shouldn't change if sem_getvalue fails
  sem_getvalue(sem, &val);
  return val;
}
#else
#include <dispatch/dispatch.h>
/*
  dispatch semaphores provide a much smaller interface than posix 
  semaphores, so we have to implement some features ourselves,
  specifically we need to keep track of the value of the semaphore
  ourselves in order to retrieve the value.
  
  This is done atomically, but since it is a seperate operation from
  the actual modification of the semaphore it may not be exactly correct.
*/
//this is effectively a void*
typedef dispatch_semaphore_t semaphore;
static inline semaphore semaphore_create(unsigned int value){
  dispatch_semaphore_t sem = dispatch_semaphore_create((long)value);
  atomic_int *cntr = (atomic_int*)malloc(sizeof(int));
  *cntr = 0;
  dispatch_set_context(sem, cntr);
  return sem;
}
static inline int semaphore_destroy(semaphore sem){
  atomic_int *cntr = dispatch_get_context(sem);
  free(cntr);
  dispatch_release((dispatch_object_t)sem);
} 
static inline int semaphore_post(semaphore sem){
  atomic_int *cntr = dispatch_get_context(sem);
  atomic_inc(cntr);
  return dispatch_semaphore_signal(sem);
}

static inline int semaphore_wait(semaphore sem){
  int ret = dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
  atomic_int *value = dispatch_get_context(sem);
  atomic_dec(cntr);
  return ret;
}
static inline int semahpore_try_wait(semaphore sem){
  int ret = dispatch_semaphore_wait(sem, DISPATCH_TIME_NOW);
  if(ret != 0){
    return -1;
  } else {
    atomic_int *value = dispatch_get_context(sem);
    atomic_dec(cntr);
    return 0;
  }
}
static inline int semaphore_wait(semaphore sem, long nsecs){
  dispatch_time_t t = dispatch_time(DISPATCH_TIME_NOW, nsecs);
  int ret = dispatch_semaphore_wait(sem, t);
  if(ret != 0){
    return -1;
  } else {
    atomic_int *value = dispatch_get_context(sem);
    atomic_dec(cntr);
    return 0;
  }
}

static inline int semaphore_value(semaphore sem){
  atomic_int *cntr = dispatch_get_context(sem);
  return atomic_load(cntr);
}  
#endif /*__APPLE__*/

#ifdef __cplusplus
}
#endif
#endif /* __THREAD_UTIL_H__ */
