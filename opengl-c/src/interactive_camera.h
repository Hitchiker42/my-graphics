#ifndef __INTERACTION_H__
#error "Do not include interactive_camera.h directly, use interaction.h"
#endif
/*
  There are 3 types of transformations for the interactive camera,
  panning, rotating and zooming. 
  Panning moves the viewer in the scene while keeping the focal point fixed.
    you can pan in any direction. 
  Rotating moves the focal point relative to the viewer, while keeping the distance
    between the two fixed. Becasue the distance is fixed there are only 2 degrees
    of freedom, thus you can only rotate in two directions
  Zooming changes the distance between the viewer and the focal point

  Moving is a type of transformation that is a composition of the other 3 and
  is provided as a convience. Moving moves both the viewer and the focal point
  in the scene while keeping the relation between the two fixed. This can be
  achieved by panning to move the viewer, rotating to re-align the viewer and focal
  point and zooming to normalized the distance between the two. However it can
  be performed much more efficently by simply moving both the viewer and
  focal point the same ammount in the scene.

  From a human perspective rotation is akin to moving your head from 
  side to side or up and down, moving is akin to moving your body without 
  moving your head, and zooming is akin to changing where you focus your eyes.
  Panning would be akin to moving your body while moving your head at the
  same time in order to compensate.

  TODO: Possibly add perspective transformations
*/

#define CAMERA_MIN_BIT 0x1

#define CAMERA_DEC_BIT 0x1
#define CAMERA_INC_BIT 0x2
#define CAMERA_X_DEC_BIT 0x1
#define CAMERA_X_INC_BIT 0x2
#define CAMERA_Y_DEC_BIT 0x4
#define CAMERA_Y_INC_BIT 0x8
#define CAMERA_Z_DEC_BIT 0x10
#define CAMERA_Z_INC_BIT 0x20

#define CAMERA_MAX_BIT 0x20


#define CAMERA_PAN 0x1
#define CAMERA_MOVE 0x2
#define CAMERA_ROTATE 0x4
#define CAMERA_ZOOM 0x8
#define CAMERA_ALL                                           \
  (CAMERA_PAN | CAMERA_MOVE | CAMERA_ROTATE | CAMERA_ZOOM)
#define ZOOM_MIN 0
#define ZOOM_MAX 10
struct interactive_camera {
  gl_camera *camera;
  //Cameras themselves are not bound to scenes but interactive cameras are
  gl_scene *scene;
  //2 keys for each transformation + 1 key to reset everything
  gl_keystate keybindings[19];
  gl_keymap map;
  float theta_xz_default;
  float theta_yz_default;
  union {
    float transforms[9];
    struct {
      union {
        float pan[3];
        struct {
          float pan_x;
          float pan_y;
          float pan_z;//this might not actually make any sense
        };
      };
      union {
        float move[3];
        struct {
          float move_x;
          float move_y;
          float move_z;
        };
      };
      /*
        It's not like I'm `trying` to see how many structs/unions I can nest.
       */
      union {
        float eye[3];
        struct {
          union {
            float rotate[2];
            struct {
              float rotate_xz;
              float rotate_yz;
            };
          };
          float zoom;
        };
      };
    };
  };

  union {
    float deltas[4];
    struct {
      float pan_delta;//delta for camera->center
      float move_delta;
      float rotate_delta;
      float zoom_delta;
    };
  };
  union {
    uint8_t masks[4];
    struct {
      uint8_t pan_mask;
      uint8_t move_mask;
      uint8_t rotate_mask;
      uint8_t zoom_mask;
    };
  };
};

#define gen_camera_add_function(name)                   \
  static inline void                                                    \
  CAT(interactive_camera_add_,name)(interactive_camera *camera, uint8_t bit){ \
    camera->name##_mask |= bit;                                           \
  }
#define gen_camera_remove_function(name)                \
  static inline void                                                    \
  CAT(interactive_camera_remove_,name)(interactive_camera *camera, uint8_t bit){       \
    camera->name##_mask &= ~(bit);                                        \
  }
#define gen_camera_funs(name)                                \
  gen_camera_add_function(name);                             \
  gen_camera_remove_function(name);

gen_camera_funs(pan);
gen_camera_funs(move);
gen_camera_funs(rotate);
gen_camera_funs(zoom);

static inline
void camera_set_delta(interactive_camera *cam, float val, int what){
  switch(what){
    case CAMERA_PAN:
      cam->pan_delta = val; return;
    case CAMERA_MOVE:
      cam->move_delta = val; return;
    case CAMERA_ROTATE:
      cam->rotate_delta = val; return;
    case CAMERA_ZOOM:
      cam->zoom_delta = val; return;
    default:
      fprintf(stderr,
              "Unknown transformation %d passed to camera set delta", what);
      return;
  }
  __builtin_unreachable();
}
static inline
void camera_reset(interactive_camera *cam, int what){
  int i;
  if(what & CAMERA_PAN){
    cam->pan_mask = 0;
    for(i=0;i<3;i++){
      cam->pan[i] = 0;
    }
  }
  if(what & CAMERA_MOVE){
    cam->move_mask = 0;
    for(i=0;i<3;i++){
      cam->move[i] = 0;
    }
  }
  if(what & CAMERA_ROTATE){
    cam->rotate_mask = 0;
    for(i=0;i<2;i++){
      cam->rotate[i] = 0;
    }
  }
  if(what & CAMERA_ZOOM){
    cam->zoom_mask = 0;
    cam->zoom = 0;
  }
}
static inline
void camera_reset_all(interactive_camera *cam){
  int i;
  for(i=0;i<4;i++){
    cam->masks[i] = 0;
  }
  for(i=0;i<3;i++){
    cam->pan[i] = 0;
    cam->move[i] = 0;
    cam->eye[i] = 0;
  }
  //  cam->zoom = 1;
}
/*
  Rotation moves the focal point of the viewer while keeping the distance
  between the two constant. Effectively this moves the eye position around
  a sphere. Since we are keeping distance fixed there are only 2 degrees of
  freedom and so only 2 functions
*/
static inline void camera_rotate_horizontal(gl_position *center,
                                            gl_position *eye,
                                            gl_position *up, float theta){
  gl_position cen = *center, e = *eye, u = *up;
  float r = position_distance(cen, e);
  float c,s;
  sincos(theta, &c, &s);
  float e_x = (r * c) + e.x;
  float e_z = (r * s) + e.z;
  //somehow modify up
  *eye = POSITION_CAST(e_x, e.y, e_z);
}

static inline void camera_rotate_vertical(gl_position *center,
                                          gl_position *eye,
                                          gl_position *up,float theta){
  gl_position cen = *center, e = *eye, u = *up;
  float r = position_distance(cen, e);
  float c,s;
  sincos(theta, &c, &s);
  float e_y = (r * c) + e.y;
  float e_z = (r * s) + e.z;
  //somehow modify up
  *eye = POSITION_CAST(e.x, e_y, e_z);
}

interactive_camera *make_interactive_camera(float d_pan, float d_move,
                                            float d_rotate, float d_zoom,
                                            gl_camera *camera, gl_keystate *keys,
                                            gl_keymap map);
void interactive_camera_bind_keys(interactive_camera *cam,
                                  gl_keystate keys[19], gl_keymap map);
void interactive_camera_update(interactive_camera *cam);
//Assumes scene has a camera
static inline void interactive_camera_set_scene(interactive_camera *cam,
                                                gl_scene *scene){
  cam->scene = scene;
  cam->camera = scene->camera;
}
