#ifndef __GL_TEXT_H__
#define __GL_TEXT_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "glew.h"
#include "common.h"
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
//opaque type contaning info needed to render text
typedef struct gl_font gl_font;
typedef struct character_info character_info;
typedef struct font_atlas font_atlas;
struct character_info {
  //number of pixels to advance
  uint16_t ax;
  uint16_t ay;

  ///bitmap height and width
  uint16_t bw;
  uint16_t bh;

  //top left coordinate of bitmap
  uint16_t bl;
  uint16_t bt;

  //offset (in bytes) from the start of the bitmap
  uint32_t offset;
};
  
struct font_atlas {
  uint8_t *bitmap;
  character_info *char_info;
  FT_Face *font;
  uint32_t bitmap_size;
  uint32_t num_characters;
};
#ifdef __cplusplus
}
#endif
#endif /* __GL_TEXT_H__ */
