#include "lighting.h"
#include "context.h"
#include "drawable.h"
#include "shapes.h"
gl_uniform_block *init_light_uniform(float *ambient, float *eye_direction,
                                     gl_light **lights, uint32_t num_lights,
                                     uint32_t lights_bitmask){
  u_light_block *u_lights = zmalloc(sizeof(u_light_block));
  memcpy(u_lights->ambient, ambient, 4*sizeof(float));
  memcpy(u_lights->eye_direction, eye_direction, 4*sizeof(float));
  int i;
  for(i=0;i<num_lights;i++){
    memcpy(&(u_lights->lights[i]), lights[i], sizeof(gl_light));
  }
  u_lights->num_lights_used = num_lights;
  u_lights->lights_bitmask = lights_bitmask;
  /* As things currently stand we always allocate enough memory for
     holding num_total_lights, so we don't save any space when
     num_lights < num_total_lights, but we might be able to save
     a bit of time by copying less memory to the gpu
   */
  int data_size = 8*sizeof(float) + 4*sizeof(uint32_t) + num_lights*sizeof(gl_light);
  //hardcoding the name is bad, but eh
  gl_uniform_block *uniform = make_uniform_block("u_light_block",
                                                 sizeof(u_light_block),
                                                 u_lights, data_size);
  return uniform;
}

gl_uniform_block *init_material_uniform(gl_material *mat){
  gl_uniform_block *uniform = make_uniform_block("u_material_block",
                                                 sizeof(gl_material),
                                                 mat, sizeof(gl_material));
  return uniform;
}

/*
  Software implementation of lighting, this uses gouraud shading,
  i.e we compute the lighting per vertex and interpolate that,
  vs phong shading, which is what the hardware implementation uses,
  which interpolates normals and computes lighting per fragment
*/
/*
  Computes the per vertex rgb lighting components for shape and assigns
  them to the shape component of the vertex.
*/
void gouraud_lighting(gl_shape *shape, gl_position eye,
                            gl_light **lights, int num_lights,
                            gl_colorf ambient){
  gl_colorf v_specular, v_diffuse, v_ambient;
  int i,j;
  gl_vertex *vertices = SHAPE_VERTICES(shape);
  gl_material *material = shape->material_properties;
  for(i=0;i<shape->vertices->num_vertices;i++){
    vec3_multiply(ambient.rgba, material->ambient.rgba,
                  v_ambient.rgba);
    v_specular = v_diffuse = (gl_colorf){{0,0,0,0}};
    gl_position normal = gl_normal_to_position(vertices[i].normal);
    DEBUG_PRINTF("Normals are %s\n%s\n",
                 format_gl_normal(vertices[i].normal),
                 format_gl_position(normal));
    for(j=0;j<num_lights;j++){
      float lambertian = MAX(position_dot(normal,
                                          lights[j]->direction), 0.0f);
      DEBUG_PRINTF("diffuse_coeff = %f\n", lambertian);
      if(lambertian > 0){
        float temp[3], H[3];
        //diffuse
        vec3_multiply(material->diffuse.rgba, 
                      lights[j]->diffuse.rgba, temp);
        vec3_scale(temp, lambertian, temp);
        vec3_add(v_diffuse.rgba, temp, v_diffuse.rgba);
        //We may as well use the more accurate formulation, since we're
        //losing out on a bunch of accuracy due to interpolation
        vec3_normalize(vec3_add(eye.vec, 
                                lights[j]->direction.vec, H),NULL);
        DEBUG_PRINTF("H = (%f,%f,%f)\n",H[0],H[1],H[2]);
        float specular_coeff = pow(vec3_dot(normal.vec, H),
                                   material->shininess);
        DEBUG_PRINTF("specular_coeff = %f\n", specular_coeff);
        vec3_multiply_scale(material->specular.rgba,
                            lights[j]->specular.rgba, specular_coeff, temp);
        vec3_add(v_specular.rgba, temp, NULL);
      }
    }
    gl_colorf temp_color;
    temp_color.a = 1;
    vec3_add3(v_specular.rgba, v_diffuse.rgba,
              v_ambient.rgba, temp_color.rgba);
    DEBUG_PRINTF("Lighting at vertex %d:\ntotal = (%f,%f,%f\n"
                 "specular = (%f,%f,%f)\ndiffuse = (%f,%f,%f)\n"
                 "ambient = (%f,%f,%f)\n",
                 i, temp_color.r, temp_color.g, temp_color.b,
                 v_specular.r, v_specular.g, v_specular.b,
                 v_diffuse.r, v_diffuse.g, v_diffuse.b,
                 v_ambient.r, v_ambient.g, v_ambient.b);
    //    gl_colorf vertex_color = gl_color_to_colorf(vertices[i].color);
    //    vec3_multiply(vertex_color.rgba, temp_color.rgba, NULL);
    vertices[i].color = gl_colorf_to_color(temp_color);
  }
  return;
}
