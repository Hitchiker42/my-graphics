#include "common.h"
#include "drawable.h"
#include "context.h"
#include "interaction.h"
#include "shapes.h"
/*
b: GPU buffer bsj 1 shared joint buffer for vertex positions and normals
buj unshared joint buffers positions/normals
bsa 2 shared separate (apart) buffers for vertex positions and normals
bua 2 unshared separate (apart) buffers for positions and normals
d: draw mode da use glDrawArrays for drawing object
de use glDrawElements for drawing object
m: PSV matrix mc cpu: multiply PSV in cpu and download product
mg gpu: download P, S, V and do product in shader
Optional features
a: attribute storage ab block: all position data stored first then all normal data
ai interleaved: pos0 norm0 pos1 norm1 pos2 norm2 ...
c: coordinate size c3 all positions and normals are 3-vectors
c4 all positions have 4th (homogeneous coord of 1); all normals have 0.
l: lighting lc cpu does lighting
lv vertex shader does lighting
lf fragment shader does lighting
h: object hierarchy ht 1 hierarchical object implementation traverses the data generation tree to
produce one position array and one normal array as if they had been
defined by hand. All instances of that object use that data.
r: run mode rb “batch” mode in which the testing framework terminates the program
after about 30 seconds. This is good for running a bunch of tests in a
shell or python script
*/
/*
  This will be slow but only needs to be done once.
*/
//the offset parameter needs to be set before these get bound
static struct vertex_attrib deinterleaved_attribs[4] =
  {{.location = 0, .size = 4, .type = GL_FLOAT, .normalized = 0},
   {.location = 1, .size = 4, .type = GL_UNSIGNED_INT_2_10_10_10_REV, .normalized = 1},
   {.location = 2, .size = GL_BGRA, .type = GL_UNSIGNED_BYTE, .normalized = 1},
   {.location = 3, .size = 2, .type = GL_UNSIGNED_SHORT, .normalized = 1}};
static struct vertex_attrib dual_buffer_attribs[4] =
  {{.location = 0, .size = 4, .type = GL_FLOAT, .normalized = 0},
   {.location = 1, .size = 4, .type = GL_UNSIGNED_INT_2_10_10_10_REV,
    .normalized = 1, .stride = sizeof(gl_vertex) - sizeof(gl_position)},
   {.location = 2, .size = GL_BGRA, .type = GL_UNSIGNED_BYTE, .normalized = 1,
    .stride = sizeof(gl_vertex) - sizeof(gl_position),
    .offset = sizeof(gl_normal)},
   {.location = 3, .size = 2, .type = GL_UNSIGNED_SHORT, .normalized = 1,
    .stride = sizeof(gl_vertex) - sizeof(gl_position),
    .offset = sizeof(gl_normal) + sizeof(gl_color)}};
/*
  Bind vertex attributes for deinterleaved attributes using gl_draw elements
*/
void bind_deinterleaved_attribs(GLuint buf, int nverts){
  deinterleaved_attribs[0].offset = 0;
  deinterleaved_attribs[1].offset = nverts * sizeof(gl_position);
  deinterleaved_attribs[2].offset = deinterleaved_attribs[1].offset +
    (nverts * sizeof(gl_normal));
  deinterleaved_attribs[3].offset = deinterleaved_attribs[2].offset +
    (nverts * sizeof(gl_color));
  enable_vertex_attribs(buf, deinterleaved_attribs, 4);
}
/*
  Drawing arrays will need to be done using glMultiDrawArrays like:
  int count[2] = {8,8}, first[2] = {0,8};
  glMultiDrawArrays(GL_TRIANGLE_STRIP, first, count, 2);
*/
void deinterleave_shape_attributes(gl_shape *shape){
  gl_vertices *verts = shape->vertices;
  void *data = glMapBufferRange(GL_ARRAY_BUFFER, verts->vbo_offset,
                                verts->num_vertices*sizeof(gl_vertex),
                                GL_MAP_WRITE_BIT);
  int i, normal_offset, color_offset, tex_offset;
  normal_offset = verts->num_vertices * sizeof(gl_position);
  color_offset = normal_offset + (verts->num_vertices * sizeof(gl_normal));
  tex_offset = color_offset + (verts->num_vertices * sizeof(gl_color));
  for(i=0;i<verts->num_vertices;i++){
    gl_vertex *vert = &(verts->vertices[i]);
    memcpy(data + i*sizeof(gl_vertex), vert, sizeof(gl_position));
    memcpy(data + normal_offset + (i*sizeof(gl_vertex)),
           vert+offsetof(gl_vertex, normal), sizeof(gl_normal));
    memcpy(data + color_offset + (i*sizeof(gl_vertex)),
           vert+offsetof(gl_vertex, color), sizeof(gl_color));
    memcpy(data + tex_offset + (i*sizeof(gl_vertex)),
           vert+offsetof(gl_vertex, tex_coord), sizeof(gl_tex_coord));
  }
  glUnmapBuffer(GL_ARRAY_BUFFER);
}
/*
  I the only way I can think to do triangle strips with arrays is to
  useg glMultiDrawArrays, but integrating that into my framework would be
  a lot of work, so just switch to doing triangles
*/
void unindex_gl_shape(gl_shape *shape){
  gl_vertices *verts = shape->vertices;
  gl_indices *inds = shape->indices;
  gl_vertex *elts = verts->vertices;
  int i,j,count = 0;
  int restart = (inds->index_size == 1 ? 0xff :
                 inds->index_size == 2 ? 0xffff : 0xffffffff);
  uint32_t *indices = indices_to_uint32(inds);
  //this allocates more vertices than necessary, but saves an
  //extra loop which would be needed to compute the exact number
  verts->vertices = zmalloc((inds->num_indices-2)*3* sizeof(gl_vertex));
  for(i=2;i<(inds->num_indices);i++){
    if(indices[i] == restart){
      i+=2;
      continue;
    }
    for(j=i-2;j<=i;j++){
      memcpy(&(verts->vertices[count++]),
             &(elts[indices[j]]), sizeof(gl_vertex));
    }
  }
  fprintf(stderr, "count = %d\n",count);
  verts->num_vertices = count;
  if(!(inds->is_static)){
    free(inds);
  }
  shape->indices = NULL;
  shape->draw_mode = GL_TRIANGLES;
  return;
}
/*
  Generate 2 buffers and put the positions in one and everything else in another.
  Copies the vertices, ignores the indices.
*/
void bind_seperate_buffers(gl_shape *shape){
  gl_vertices *verts = shape->vertices;
  //prevent this from being rebound
  verts->vertex_buffer = -1;
  GLuint bufs[2];
  glGenBuffers(2, bufs);
  int sz = sizeof(gl_vertex)-sizeof(gl_position);
  gl_position *positions = alloca(verts->num_vertices *sizeof(gl_position));
  void *everything_else = alloca(verts->num_vertices * sz);
  int i;
  for(i=0;i<verts->num_vertices;i++){
    positions[i] = verts->vertices[i].pos;
    memcpy(everything_else + i * sz,
           &(verts->vertices[i])+offsetof(gl_vertex, normal), sz);
  }
  glBindBuffer(GL_ARRAY_BUFFER, bufs[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(gl_position)*verts->num_vertices,
               positions, GL_STATIC_DRAW);
  enable_vertex_attribs(bufs[0], dual_buffer_attribs, 1);
  glBindBuffer(GL_ARRAY_BUFFER, bufs[1]);
  glBufferData(GL_ARRAY_BUFFER, sz*verts->num_vertices,
               everything_else, GL_STATIC_DRAW);
  enable_vertex_attribs(bufs[1], &(dual_buffer_attribs[1]), 3);
}
  
void shape_cpu_mat_mul(gl_shape *shape){
  gl_scene *scene = shape->scene;
  float transform[16];
  mat4_multiply(scene->projection, scene->view, transform);
  mat4_multiply(transform, scene->scene, transform);
  mat4_multiply(transform, shape->transform, transform);
  update_uniform_block(&scene->transform, transform, 0, 16*sizeof(float));
  return;
}
