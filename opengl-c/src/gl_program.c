#include "common.h"
/*
  Code to simplify compiling and linking glsl programs
*/
//doesn't print empty logs
#define print_gl_info_log(objtype, obj, msg)                    \
  ({GLint log_size;                                             \
    glGet##objtype##iv(obj, GL_INFO_LOG_LENGTH, &log_size);     \
    if(log_size > 0){                                           \
      char *buf = alloca(log_size*sizeof(char));                \
      glGet##objtype##InfoLog(obj, log_size, NULL, buf);        \
      fprintf(stderr, "%s"#objtype" info log:\n%s", msg, buf);  \
    };})
#define print_shader_info_log(shader, msg)      \
  print_gl_info_log(Shader, shader, msg)
#define print_program_info_log(program, msg)    \
  print_gl_info_log(Program, program, msg)

#define check_shader_error(obj, val)                                    \
  ({GLint status, type;                                                 \
    glGetShaderiv(obj, val, &status);                                   \
    if(!status){                                                        \
      glGetShaderiv(obj, GL_SHADER_TYPE, &type);                        \
      char *str = asprintf_alloca("Error when compiling %s shader.\n",  \
                                  gl_shader_type_to_string(type));      \
      print_gl_info_log(Shader, obj, str);                              \
    };                                                                  \
  })
#define check_program_error(obj, val, act)                              \
  ({GLint status;                                                       \
    glGetProgramiv(obj, val, &status);                                  \
    if(!status){                                                        \
      print_gl_info_log(Program, obj, "Error when " #act "program.\n"); \
    };                                                                  \
  })

static inline GLuint compile_shader(GLenum type, const char *source, int len){
  GLuint shader = glCreateShader(type);
  if(len == 0){
    len = strlen(source);
  }
  glShaderSource(shader, 1, &source, &len);
  glCompileShader(shader);
  check_shader_error(shader, GL_COMPILE_STATUS);
  return shader;
}
/*
  Same as above but allow the shader to be composed of multiple seperate strings,
  this is how the actual opengl call takes it's arguments.

  len can be NULL to indicate the lengths of the source strings should be
  calculated, but if it is not NULL it must contain the lengths of all the
  source strings.
*/
static inline GLuint compile_shader_multi(GLenum type, int num_sources,
                                          const char **source, int *len){
  GLuint shader = glCreateShader(type);
  int i;
  if(len == NULL){
    len = alloca(num_sources*sizeof(int));
    for(i=0;i<num_sources;i++){
      len[i] = strlen(source[i]);
    }
  }
  glShaderSource(shader, num_sources, source, len);
  glCompileShader(shader);
  check_shader_error(shader, GL_COMPILE_STATUS);
  return shader;
}
//IDEA: change this to also create the program and return it as the return value
static inline void link_program_internal(GLuint program){
  glLinkProgram(program);
  check_program_error(program, GL_LINK_STATUS, "linking");
  
  glValidateProgram(program);
  check_program_error(program, GL_VALIDATE_STATUS, "validating");
  return;
}

GLuint create_shader_program(const char *vertex_shader_source,
                             const char *fragment_shader_source){
  GLuint vertex_shader, fragment_shader;
  vertex_shader = compile_shader(GL_VERTEX_SHADER,
                                 vertex_shader_source, 0);
  fragment_shader = compile_shader(GL_FRAGMENT_SHADER,
                                   fragment_shader_source, 0);
  GLuint program = glCreateProgram();
  //this should be optimized out, but I may need to rewrite things to
  //just use this array if not
  //GLuint shaders[2] = {vertex_shader, fragment_shader};
    //the compiler should be able to unroll this loop
  glAttachShader(program, vertex_shader);
  glAttachShader(program, fragment_shader);

  link_program_internal(program);

  glDetachShader(program, vertex_shader);
  glDetachShader(program, fragment_shader);

  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);
  glUseProgram(program);
  return program;
}
/*
  Currently not used, would be used to allow creating a shader
  composed of multiple seperate strings.
*/
typedef struct shader_element {
  char *str;
  int len;
  GLenum type;
} gl_shader_element;

struct gl_program {
  GLuint prog;
  struct cons_t *shaders;
};

gl_program *make_gl_program(void){
  gl_program *prog = zmalloc(sizeof(gl_program));
  prog->prog = glCreateProgram();
  return prog;
}

void program_attach_shader(gl_program *prog, const char *shader, GLenum type){
  GLuint shader_id = compile_shader(type, shader, 0);
  glAttachShader(prog->prog, shader_id);
  prog->shaders = make_cons((void*)(unsigned long)shader_id, prog->shaders);
  return;
}
GLuint link_program(gl_program *prog){
  link_program_internal(prog->prog);
  struct cons_t *c = prog->shaders;
  do {
    GLuint shader = (unsigned long)XCAR(c);
    glDetachShader(prog->prog, shader);
    glDeleteShader(shader);
    void *temp = c;
    c = XCDR(c);
    free(temp);
  }  while(c != NULL);
  GLuint ret = prog->prog;
  free(prog);
  return ret;
}
