/*
  Timer/profiling code adapted from evalDemo.cpp
*/
#include "common.h"
#include "drawable.h"
#include "context.h"
#include "interaction.h"
#include "shapes.h"
#include "thread_util.h"
#include <stdarg.h>
void bind_deinterleaved_attribs(GLuint buf, int nverts);
void deinterleave_shape_attributes(gl_shape *shape);
void unindex_gl_shape(gl_shape *shape);
void bind_seperate_buffers(gl_shape *shape);
void shape_cpu_mat_mul(gl_shape *shape);
thread_local global_context *thread_context;
static char *config_code = "bsj.mg.rb";
static float log_interval = 3; // report every 3 secs
static int max_logs = INT_MAX;
static int n_shapes = 10000;
static FILE *log_file;
static sem_t sem_memory;
static sem_t *timer_sem = &sem_memory;
//defined such that 0 is the default for all the options
struct config_info {
  int unshared_buffers;
  int dual_buffers;
  int draw_mode;//0=elements, 1 = arrays
  int cpu_mat_mul;
};
void print_config(struct config_info *config){
  int *temp = (int*)config;
  fprintf(stderr, "unshared_buffers = %d\ndual_buffers = %d\n"
          "draw_mode = %d\ncpu_mat_mul = %d\n",
          temp[0], temp[1], temp[2], temp[3]);
}
static gl_keystate transform_keybindings[25] =
  {{.keycode = GLFW_KEY_RIGHT},{.keycode = GLFW_KEY_LEFT},//translate x
   {.keycode = GLFW_KEY_UP},{.keycode = GLFW_KEY_DOWN},//translate y
   {.keycode = GLFW_KEY_UP, .modmask = GLFW_MOD_SHIFT},
   {.keycode = GLFW_KEY_DOWN, .modmask = GLFW_MOD_SHIFT}, //translate z
   //rotate
   {.keycode = 'x'}, {.keycode = 'x', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'c'}, {.keycode = 'c', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'z'}, {.keycode = 'z', .modmask = GLFW_MOD_CONTROL},
   //scale width, height, depth
   {.keycode = 'w'}, {.keycode = 'w', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'h'}, {.keycode = 'h', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'd'}, {.keycode = 'd', .modmask = GLFW_MOD_CONTROL},
   //shear ',', '.', '/' because why not
   {.keycode = ','}, {.keycode = ',', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '.'}, {.keycode = '.', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '/'}, {.keycode = '/', .modmask = GLFW_MOD_CONTROL},
   //reset
   {.keycode = 'r'}};
void log_fmt(FILE *fd, char *fmt, ...){
  fd = (fd ? fd : log_file);
  va_list ap;
  va_start(ap, fmt);
  int nbytes = vfprintf(fd, fmt, ap);
  va_end(ap);
}
void log_err(FILE *fd, char *fmt, ...){
  fd = (fd ? fd : log_file);
  va_list ap,aq;
  va_start(ap, fmt);
  va_copy(ap, aq);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputs("***Error: ", fd);
  vfprintf(fd, fmt, aq);
  va_end(aq);
}
void render_main(void *ctx){
  thread_context = ctx;
  glfwMakeContextCurrent(thread_context->window);

  glfwSetTime(0.0);
  gl_main_loop_once(thread_context);
  double cur_time = glfwGetTime();
  log_fmt(log_file, "Initial redraw: %6.3f\n", cur_time);
  while(!glfwWindowShouldClose(thread_context->window)){
    glfwSwapBuffers(thread_context->window);
    gl_main_loop_once(thread_context);
    sem_post(timer_sem);
  }
}
void timer_main(void *ctx){

  //turn off vsync, render as fast as possible
  //  glfwSwapInterval(0);
  //set the time to 0, the timer is started when the library is initialized
  double cur_time = 0.0;
  int frame_count = 0;
  int num_logs = 0;
  float next_log = log_interval;
  while(1){
    sem_wait(timer_sem);
    frame_count++;
    cur_time = glfwGetTime();
    if(cur_time >= next_log){
      float elapsed_time = cur_time - (next_log-log_interval);
      next_log += log_interval;
      float fps = frame_count / elapsed_time;
      float spf = 1/fps;
      log_fmt(log_file, "Average redraw (sec): %6.4f    %8.3f FPS\n",
              spf, fps);
      if(++num_logs > max_logs){
        log_fmt(log_file,
                "========= batch termination: time expired =================\n");
        exit(0);
      }
      frame_count = 0;
    }
  }
}
struct config_info parse_options(char *optstr){
  struct config_info info = {0,0,0,0};
  //I really don't like using the c string funcions
  //also I feel the need to point out that this is a horrible
  //system for specifying command line options
  if(!strstr(optstr,"bsj")){
    if(strstr(optstr,"bsa")){
      info.dual_buffers = 1;      
    } else if(strstr(optstr, "buj")){
      info.unshared_buffers = 1;
    } else if(strstr(optstr, "bua")){
      info.unshared_buffers = 1;
      info.dual_buffers = 1;
    }
  }
  if(strstr(optstr, "da") && !strstr(optstr,"de")){
    info.draw_mode = 1;
  }
  if(strstr(optstr, "rb")){
    max_logs = 10;
  }
  if(strstr(optstr,"mc") && !strstr(optstr,"mg")){
    info.cpu_mat_mul = 1;
  }
  return info;
}
const char *optstring = "su12eagcb:n:";
struct config_info my_parse_options(int argc, char *argv[]){
  //How I would've done option parsing
  int opt;
  struct config_info info = {0,0,0,0};
  while((opt = getopt(argc, argv, optstring)) != -1){
    switch(opt){
      case 's':
        info.unshared_buffers = 0; break;
      case 'u':
        info.unshared_buffers = 0; break;
      case '1':
        info.dual_buffers = 0; break;
      case '2':
        info.dual_buffers = 1; break;
      case 'e':
        info.draw_mode = 1; break;
      case 'a':
        info.draw_mode = 0; break;
      case 'c':
        info.cpu_mat_mul = 1; break;
      case 'g':
        info.cpu_mat_mul = 0; break;
      case 'b':{//batch mode
        errno = 0;
        max_logs = strtoul(optarg, NULL, 0);
        if(errno != 0){max_logs = 10;}
        break;
      }
      case 'n':{//number of shapes
        errno = 0;
        n_shapes = strtoul(optarg, NULL, 0);
        if(errno != 0){n_shapes = 10000;}
        break;
      }
    }
  }
  return info;
}
/*
b: GPU buffer bsj 1 shared joint buffer for vertex positions and normals
buj unshared joint buffers positions/normals
bsa 2 shared separate (apart) buffers for vertex positions and normals
bua 2 unshared separate (apart) buffers for positions and normals
d: draw mode da use glDrawArrays for drawing object
de use glDrawElements for drawing object
m: PSV matrix mc cpu: multiply PSV in cpu and download product
mg gpu: download P, S, V and do product in shader
Optional features
a: attribute storage ab block: all position data stored first then all normal data
ai interleaved: pos0 norm0 pos1 norm1 pos2 norm2 ...
c: coordinate size c3 all positions and normals are 3-vectors
c4 all positions have 4th (homogeneous coord of 1); all normals have 0.
l: lighting lc cpu does lighting
lv vertex shader does lighting
lf fragment shader does lighting
h: object hierarchy ht 1 hierarchical object implementation traverses the data generation tree to
produce one position array and one normal array as if they had been
defined by hand. All instances of that object use that data.
r: run mode rb “batch” mode in which the testing framework terminates the program
after about 30 seconds. This is good for running a bunch of tests in a
shell or python script
*/
static int max_vertex = 7;
gl_color gen_color(int i){
  uint8_t intensity = (double)0xff * ((double)i/(double)max_vertex);
  fprintf(stderr, "%d\n",intensity);
  gl_color ret = {.r = intensity, .g = intensity, .b = intensity, .a = 0xff};  
  return ret;
}

gl_shape** gen_shapes(struct config_info *config){
  gl_shape **shapes = xmalloc(n_shapes*sizeof(gl_shape*));
  gl_shape *proto;
  if(config->draw_mode){
    proto = (gl_shape*)make_colored_sphere(1/(sqrt(n_shapes)),
                                           gl_color_red);
  } else {
    proto = (gl_shape*)make_colored_sphere_indexed(1/(sqrt(n_shapes)),
                                                   gl_color_red);
  }
  //gl_shape *proto = (gl_shape*)make_default_cube(1/sqrt(n_shapes),gen_color);
  if(!config->draw_mode && proto->indices){
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, proto->indices->index_buffer);
    enable_primitive_restart(proto->indices->index_size);
  }
  if(!config->unshared_buffers){
    if(config->draw_mode){
      //      unindex_gl_shape(proto);
    }
    if(config->dual_buffers){
      bind_seperate_buffers(proto);
    }
    if(config->cpu_mat_mul){
      proto->bind_shape = shape_cpu_mat_mul;
    }
    int i;
    gl_position offset;
    //put each cube in a random position
    for(i=0;i<n_shapes;i++){
      shapes[i] = copy_gl_shape(proto);
      randomize_transform(shapes[i]->transform);
    }
  } else {
    if(config->draw_mode){
      //      unindex_gl_shape(proto);
    }
    if(config->cpu_mat_mul){
      proto->bind_shape = shape_cpu_mat_mul;
    }
    gl_position offset;
    int i;
    for(i=0;i<n_shapes;i++){  
      shapes[i] = clone_gl_shape(proto);
      if(config->dual_buffers){
        bind_seperate_buffers(proto);
      }
      randomize_transform(shapes[i]->transform);
    }
  }
  return shapes;
}
gl_scene *init_scene(GLuint program, struct config_info *config){
  gl_scene *scene = make_scene_from_program(program);
  gl_shape **shapes = gen_shapes(config);
  gl_drawable *axes = make_colored_axes_drawable(gl_color_cyan,
                                                 gl_color_magenta,
                                                 gl_color_yellow);
  scene_add_drawable(scene, axes);
  scene_add_drawables(scene, (gl_drawable**)shapes, n_shapes);
  return scene;
}
void update_scene(gl_scene *s, void *data){
  interactive_transform_update(data);
}
int main(int argc, char **argv){
  if(argc > 1){
    n_shapes = strtoul(argv[1], NULL, 0);
  }
  if(argc > 2){
    config_code = argv[2];
  }
  if(argc > 3){
    log_file = fopen(argv[3], "w");
    if(!log_file){      
      perror("open");
      exit(1);
    }
  } else {
    log_file = stdout;
  }
  global_context *ctx;
  ctx = make_global_context(800, 800, "simple test");
  context_allocate_scenes(ctx, 1);
  struct config_info config = parse_options(config_code);
  print_config(&config);
  GLuint prog = create_shader_program(basic_vertex, basic_fragment);
  gl_scene *scene = init_scene(prog, &config);
  glClearColor(0,0.5,0.5,1);
  window_set_keymap(ctx->window, default_keymap);
  interactive_transform *trans = 
    make_interactive_transform(0.05, M_PI/60, 0.05, 0.05, 
                               transform_keybindings, default_keymap);
  ctx->userdata = trans;
  ctx->scenes[0] = scene;  
  scene->update_scene = (void*)update_scene;
  transform_set_scene(trans, scene);

  pthread_t tid;
  sem_init(timer_sem, 0, 0);
  pthread_create(&tid, NULL, (void*)timer_main, ctx);
  pthread_create(&tid, NULL, (void*)render_main, ctx);

  gl_main_loop_event(ctx);
}
