/*
  General header which includes headers needed by most other files,
  specifically stdlib headers, headers which define types and
  headers with general utility functions
*/
#ifndef __MY_GL_COMMON_H__
#define __MY_GL_COMMON_H__

#ifdef __cplusplus
#include <cmath>//tgmath on C++ fails for some reason
//C++ for some reason doesn't offically support restrict
#define restrict __restrict__
extern "C" {
#else
#include <tgmath.h>
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)  /* C11 */
#define HAVE_C11
#endif

#if !(defined X_DISPLAY_MISSING)
#ifndef HAVE_X11
#define HAVE_X11
#endif
#endif


#if (defined DEBUG) && !(defined NDEBUG)
#define DEBUG_PRINTF(msg,args...) fprintf(stderr, msg, ##args)
#define IF_DEBUG(code) code
#else
#define DEBUG_PRINTF(msg,args...)
#define IF_DEBUG(code)
#endif
//standard library includes
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <errno.h>

//#include <GL/glew.h>
/*
  I don't know where glew.h is on osx/windows, I should look for it when configuring
  but for now I just include a copy in my source tree
*/
#include "glew.h" //this should be all we need, but I don't know for sure
#include "my_sdl.h"
//#include "my_glfw.h"
//other library headers
#include "gl_matrix/gl_matrix.h"
#include "C_util.h"
#include "gl_types.h"
#include "to_string.h"
#include "gl_util.h"

//threads (now we use sdl threads)
//#include "thread_util.h"
//#include "semaphore.h"
//shaders (as string constants)
#include "shaders.h"

//memory allocation
#ifndef OOM_FUN
static void oom_fun(void){
  fputs("Out of memory\n",stderr);
  raise(SIGABRT);
}
#endif
#define XMALLOC
#define ZMALLOC
static inline void* xmalloc(size_t sz){
  void *mem = malloc(sz);
  if(!mem && sz != 0){
    oom_fun();
  }
  return mem;
}
static inline void* zmalloc(size_t sz){
  void *mem = calloc(sz, 1);
  if(!mem && sz != 0){
    oom_fun();
  }
  return mem;
}
/*
  These get called on program startup to initialize global variables
*/
void init_shapes(void);
#ifdef __cplusplus
}
#endif
#endif /* __MY_GL_COMMON_H__ */
