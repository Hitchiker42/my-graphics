#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
#include "interaction.h"
/*
  This should remain fairly uncomplicated, it draws a single shape,
  and has the ability to rotate the scene, and move the shape.

  It also draws axes and a 3d grid to help show depth
*/
thread_local global_context *thread_context;
static gl_keystate transform_keybindings[25] =
  {{.keycode = GLFW_KEY_RIGHT},{.keycode = GLFW_KEY_LEFT},//translate x
   {.keycode = GLFW_KEY_UP},{.keycode = GLFW_KEY_DOWN},//translate y
   {.keycode = GLFW_KEY_UP, .modmask = GLFW_MOD_SHIFT},
   {.keycode = GLFW_KEY_DOWN, .modmask = GLFW_MOD_SHIFT}, //translate z
   //rotate
   {.keycode = 'x'}, {.keycode = 'x', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'c'}, {.keycode = 'y', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'z'}, {.keycode = 'z', .modmask = GLFW_MOD_CONTROL},
   //scale width, height, depth
   {.keycode = 'w'}, {.keycode = 'w', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'h'}, {.keycode = 'h', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'd'}, {.keycode = 'd', .modmask = GLFW_MOD_CONTROL},
   //shear ',', '.', '/' because why not
   {.keycode = ','}, {.keycode = ',', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '.'}, {.keycode = '.', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '/'}, {.keycode = '/', .modmask = GLFW_MOD_CONTROL},
   //reset
   {.keycode = 'r'}};

static gl_drawable *init_drawable(){
  gl_drawable *x = make_colored_default_convex_polygon(4, 0.5, gl_color_white);
  gl_vertex *vertices = SHAPE_VERTICES(x);
  int i;
  vertices[0].tex_coord = make_gl_tex_coord(1,1);
  vertices[1].tex_coord = make_gl_tex_coord(1,0);
  vertices[2].tex_coord = make_gl_tex_coord(0,0);
  vertices[3].tex_coord = make_gl_tex_coord(0,1);
  return x;
}
void update_scene(gl_scene *s, void *data){
  interactive_transform_update(data);
}
#define WM_KEY(key) CAT(GLFW_KEY_,key)
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "tex test");
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;
  gl_scene *scene = make_simple_scene(texture_vertex, texture_fragment);
  window_set_keymap(ctx->window, default_keymap);
  interactive_transform *trans =
    make_interactive_transform(0.05, M_PI/60, 0.05, 0.05,
                               transform_keybindings, default_keymap);
  keymap_set_key(default_keymap, WM_KEY(ESCAPE), 0,
                 keypress_quit_program, NULL);
  ctx->userdata = trans;
  scene->update_scene = (void*)update_scene;
  ctx->scenes[0] = scene;
  gl_drawable *drawable;
  glClearColor(0,1,0,1);
  drawable = init_drawable();
  transform_set_drawable(trans, drawable);
  glEnable(GL_LINE_SMOOTH);
//  glLineWidth(2.0);
  gl_drawable *axes = make_colored_axes_drawable(gl_color_cyan,
                                                 gl_color_magenta,
                                                 gl_color_yellow);
  //assume DATA_DIR = the full path to the data directory, including the
  //trailing slash (this allows this to work even on windows)
  char *tex_filename = DATA_DIR "checkerboard.png";
  gl_texture *tex = gen_texture_from_file(tex_filename, 4);
  tex->name = "tex";
  drawable->texture = tex;
  gl_check_error();
  SCENE_ADD_DRAWABLES(scene, drawable, axes);
  gl_main_loop(ctx);
}
