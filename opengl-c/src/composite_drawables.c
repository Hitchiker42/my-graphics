/*
  Composite drawables
*/
gl_drawable *make_composite_drawable(int num_drawables){
  gl_composite_drawable *composite = zmalloc(sizeof(gl_composite_drawable));
  composite->num_drawables = num_drawables;
  composite->drawables = zmalloc(num_drawables*sizeof(gl_drawable*));
  composite->refcount = 1;
  mat4_identity(composite->transform);
  composite->type = GL_drawable_composite;
  composite->vbo_offset = zmalloc(sizeof(uint32_t));
  composite->bind_drawable = bind_drawable_default;
  return (gl_drawable*)composite;
}
static gl_drawable *identity(gl_drawable* x){
  return x;
}
//This may be a bit wasteful performance wise
gl_drawable *init_composite_drawable(gl_drawable **drawables,
                                     int num_drawables, int action){
  int i;
  gl_drawable* (*act)(gl_drawable*);
  gl_drawable *composite = make_composite_drawable(num_drawables);
  switch(action){    
    case(COMPOSITE_INIT_REF):
      act = ref_gl_drawable; break;
    case(COMPOSITE_INIT_COPY):
      act = copy_gl_drawable; break;
    case(COMPOSITE_INIT_DUP):
      act = duplicate_gl_drawable; break;
      return init_composite_drawable_dup(drawables, num_drawables);
    case(COMPOSITE_INIT_CLONE):
      act = clone_gl_drawable; break;
    case(COMPOSITE_INIT_NONE):
      act = identity; break;
    default:
      return NULL; //FIX THIS, change action argument to an enum
  }
  for(i=0;i<num_drawables;i++){
    composite->drawables[i] = act(drawables[i]);
  }
  return composite;
}
void gl_draw_composite_drawable(gl_composite_drawable *composite){
  MATRIX_STACK_INIT(composite);
  gl_drawable **drawables = composite->drawables;
  int i;
  for(i=0;i<composite->num_drawables;i++){

    gl_draw_drawable(drawables[i]);

  }
  MATRIX_STACK_DEINIT(composite);
};
static void sync_composite_drawable(gl_composite_drawable *composite,
                                    void *dest){
  int i;
  for(i=0;i<composite->num_drawables;i++){
    gl_drawable *drawable = composite->drawables[i];
    if(drawable->sync){
      gl_sync_drawable(drawable);
      drawable->sync = 0;
    }
  }
  composite->sync = 0;
}
void destroy_composite_drawable(gl_composite_drawable *composite){
  int i;
  for(i=0;i<composite->num_drawables;i++){
    free_gl_drawable(composite->drawables[i]);
  }
  vbo_remove_drawable(composite->vbo, (gl_drawable*)composite);
  //this is an array of pointers, not the memory for the drawables themselves
  free(composite->drawables);
  free(composite);
  return;
}
static gl_drawable *
copy_gl_composite_drawable(gl_composite_drawable *composite){
  int i;
  gl_composite_drawable *ret = xmalloc(sizeof(gl_composite_drawable));
  memcpy(ret, composite, sizeof(gl_composite_drawable));
  ret->refcount = 1;
  for(i=0;i<ret->num_drawables;i++){
    ret->drawables[i]->refcount++;
  }
  return (gl_drawable*)ret;
}
static gl_drawable *
copy_gl_composite_drawable_rec(gl_composite_drawable *composite){
  int i;
  gl_composite_drawable *ret = xmalloc(sizeof(gl_composite_drawable));
  memcpy(ret, composite, sizeof(gl_composite_drawable));
  ret->drawables = xmalloc(composite->num_drawables * sizeof(gl_drawable*));
  ret->refcount = 1;
  for(i=0;i<ret->num_drawables;i++){
    ret->drawables[i] = copy_gl_drawable_rec(composite->drawables[i]);
  }
  return (gl_drawable*)ret;
}
static gl_composite_drawable *
duplicate_gl_composite_drawable(gl_composite_drawable *composite){
  int i;
  gl_composite_drawable *ret = xmalloc(sizeof(gl_composite_drawable));
  ret->refcount = 1;
  free_gl_drawable((gl_drawable*)composite);
  return ret;
}

