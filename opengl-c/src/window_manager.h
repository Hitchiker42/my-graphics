#ifndef __WINDOW_MANAGER_H__
#define __WINDOW_MANAGER_H__
#ifdef __cplusplus
extern "C" {
#endif
/*
*/
#include "common.h"
#include "interaction.h"
struct window_data {
  gl_keymap keymap;
#ifdef HAVE_X11
  Display *X11_display;
#endif
};
/*
  There are two ways to deal with keypresses/mouse clicks. The default method
  uses a dispatch table which assoicates a callback, and optional userdata 
  pointer, with indivual keys/buttons. Alternativly a single callback can
  be given which will be passed all key/button events.
*/
typedef void(*keypress_callback)(gl_window win, int keycode, int scancode,
                                 int action, int modmask, void *data);
typedef void(*primitive_keypress_callback)(gl_window win, int keycode, int scancode,
                                           int action, int modmask);

typedef void(*mouse_button_callback)(gl_window win, int button,
                                    int action, int modmask, void *data);
typedef void(*primitive_mouse_button_callback)(gl_window win, int button,
                                               int action, int modmask);

typedef void(*utf8_text_callback)(gl_window *win, char *str);
typedef void(*utf32_text_callback)(gl_window *win, uint32_t c);

gl_window init_gl_context(int w, int h, const char* name);

void get_mouse_position(gl_window win, int *x, int *y);
void set_window_data(gl_window win, void *userdata);
void *get_window_data(gl_window win);
//Override the default method of handling key events (using a keymap)
//and instead pass all key events to callback.
void set_primitive_key_callback(gl_window win,
                                keypress_callback callback, void *data);
//same as above but for mouse clicks.
void set_primitive_mouse_button_callback(gl_window win,
                                         mouse_button_callback callback, void *data);
                                
#if (defined USE_SDL)
#define WM_KEY(key) SDLK_##key
#elif (defined USE_GLUT)
#error "freeglut not yet supported"
#else
#define WM_KEY(key) GLFW_KEY_##key
#endif
#ifdef __cplusplus
}
#endif
#endif /* __WINDOW_MANAGER_H__ */
