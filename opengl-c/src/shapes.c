/*
  Primitive Shapes
*/
/*
  Simple function to take a set of vertices and indices and make a
  primitive shape object
*/
/* gl_polygon *init_polygon(gl_vertex *verticies, int nverts, */
/*                                          gl_index *indices, int num_indices){ */

  /*
    Compute indices.
    The indices needed to draw a 2d polygon (using GL_TRINGLE_STRIP) are:
    1,2,n-1,3,n-2,4,n-3,5,n-4,...,n/2,[n/2+1] (depending if n is even or odd)
  */
#define COMPUTE_INDICES(type, count)            \
  ({type *indices = alloca(sizeof(type)*count); \
    int i = 1, n = count-1, m = 1;              \
    /*The first index is a special case*/       \
    indices[0] = 0;                             \
    while(m <= n){                              \
      indices[i++] = m++;                       \
      if(m > n){break;}                         \
      indices[i++] = n--;                       \
    }                                           \
    (gl_index_t*)indices;})

gl_indices* compute_indices(int num){
  gl_indices *indices;
  if(num < 0xff){
    indices = allocate_gl_indices(num, 1);
    indices->indices = COMPUTE_INDICES(uint8_t, num);
  } else if(num < 0xffff){
    indices = allocate_gl_indices(num, 2);
    indices->indices = COMPUTE_INDICES(uint16_t, num);
  } else {
    indices = allocate_gl_indices(num, 4);
    indices->indices = COMPUTE_INDICES(uint32_t, num);
  }
  init_index_buffer(indices);
  return indices;
}
/*
  vertex positions for some default shapes
*/
/*
static const gl_position default_triangle[3] = 
  {{0,0.5,0,1}, {0.4330127, -0.25, 0,1}, {-0.4330127, -0.25,0,1}};
static const gl_position default_square[4] =
  {{-0.5, 0.5, 0,1.0}, {0.5, 0.5, 0,1.0},
   {-0.5, -0.5, 0,1.0}, {0.5, -0.5, 0,1.0}};
static const gl_position default_tetrahedron[4] =
  {{0,-0.25,0.5,1}, {0.4330127, -0.25,-0.25,1},
   {-0.4330127, -0.25, -0.25,1},{0,0.8660254-0.25,0,1}};
static const gl_position default_octahedron[6] =
  {{0,0.5,0,1},{0,0,-0.5,1},{0.5,0,0,1},
   {0,0,0.5,1},{-0.5,0,0,1},{0,-0.5,0,1}};
*/
/*
  Ideally these would go from -inf - inf, but we'll have
  to settle for some large value, we could use 3.40282347e+38F
  which is the maximum single precision floating point value,
  but that seems a bit excessive
*/
static const gl_position axes_position[] =
  {POSITION(-10,0,0), POSITION(10,0,0), POSITION(0,-10,0),
   POSITION(0,10,0), POSITION(-10,-10,-10), POSITION(10,10,10)};
gl_drawable *make_axes_drawable(gl_color color){
  int i;
  gl_shape *axes = (gl_shape*)make_gl_shape(6);
  gl_vertex *vertices =  SHAPE_VERTICES(axes);
  axes->draw_mode = GL_LINES;
  for(i=0;i<6;i++){
    vertices[i].pos = axes_position[i];
    vertices[i].color = color;
    vertices[i].tag = 1;
  }
  return AS_DRAWABLE(axes);
}
gl_drawable *make_colored_axes_drawable(gl_color x, gl_color y, gl_color z){
  int i;
  gl_shape *axes = (gl_shape*)make_gl_shape(6);
  gl_vertex *vertices =  SHAPE_VERTICES(axes);
  axes->draw_mode = GL_LINES;
  //I'm too lazy to unroll the loop
  gl_color colors[6] = {x,x,y,y,z,z};
  for(i=0;i<6;i++){
    vertices[i].pos = axes_position[i];
    vertices[i].color = colors[i];
    vertices[i].tag = 1;
  }
  return AS_DRAWABLE(axes);
}
#if 0
gl_shape *translate_shape(gl_shape *shape, gl_position *offset){
  float mat[16];
  mat4_identity(mat);
  mat4_translate(mat, (float*)offset, NULL);
  //this applies to all shapes using these vertices, this
  //may changes at some point
  gl_vertex *vertices = SHAPE_VERTICES(shape);
  int i;
  for(i=0;i<shape->num_vertices;i++){
    mat4_multiply_vec3(mat, vertices[i].pos.vec, NULL);
  }
  return shape;
}
gl_shape *rotate_shape(gl_shape *shape, float theta, 
                       gl_position *axis){
  float mat[16];
  mat4_identity(mat);
  mat4_rotate(mat, theta, (float*)axis, NULL);
  gl_vertex *vertices = SHAPE_VERTICES(shape);
  int i;
  for(i=0;i<shape->num_vertices;i++){
    mat4_multiply_vec3(mat, vertices[i].pos.vec, NULL);
  }
  return shape;
}
gl_shape *shape_set_uniform_color(gl_shape *shape, gl_color c){
  gl_vertex *vertices = SHAPE_VERTICES(shape);
  int i;
  for(i=0;i<shape->num_vertices;i++){
    vertices[i].color = c;
  }
  return shape;
}
#endif   
/*
   PREFIX: All functions in this file are of the form make_PREFIX_thing,
   (the second _ needs to be part of prefix). Thus PREFIX is used to
   identify the different versions of the functions.

   COLOR_ARG: The formal argument used to specify how to set vertex color
   GEN_COLOR(i): Generates a vertex color for the vertex at index i

   They are undefed at the end of shapes_templ.c
*/
#define PREFIX _
#define COLOR_ARG gl_color(*fc)(int)
#define COLOR_ARG_NAME fc
#define GEN_COLOR(i,...) fc(i)
#include "shapes_templ.c"

#define PREFIX _colored_
#define COLOR_ARG gl_color color
#define COLOR_ARG_NAME color
#define GEN_COLOR(...) color
#include "shapes_templ.c"

#define PREFIX _custom_
#define COLOR_ARG void(*fv)(gl_vertex *)
#define COLOR_ARG_NAME fv
#define GEN_COLOR(i, vert) (fv(vert),vert->color)
#include "shapes_templ.c"
/*
  Generates a surface on the x-z plane with height (aka y) determined
  by a function f(x,z)->y

  This is another version which takes an argument that determines the color
  based on the position.
*/
gl_drawable* make_surface_pos_color(double x_0, double x_n, int n,
                                    double y_0, double y_m, int m,
                                    double(*fxy)(double,double),
                                    gl_color(*fc)(gl_position)){
  int i=0,j=0,k=0;
  double dx = (x_n-x_0)/n, dy = (y_m-y_0)/m;
  double x,z,y, zx[2], zy[2];
  gl_vertex *vertices = zmalloc(m*n*sizeof(gl_vertex));
  int num_indices = m*(2*n+1);
  uint16_t *indices = xmalloc(num_indices*sizeof(uint16_t));
  zy[0] = fxy(x_0,y_0-dy);
  zy[1] = fxy(x_0,y_0+dy);
  zx[0] = fxy(x_0-dx,y_0);
  zx[1] = fxy(x_0+dx,y_0);
  for(j=0,y=y_0;j<m;j++,y+=dy){
    for(i=0,x=x_0;i<n;i++,x+=dx){
      z = fxy(x,y);
      double dz_y = (z-zy[0] + zy[1]-y + 2*(zy[0]-zy[1]))/4*dy;
      double dz_x = (y-zx[0] + zx[1]-y + 2*(zx[0]-zx[1]))/4*dx;
      gl_position grad = POSITION(dx, (dz_x+dz_y)/2,dy);
      if(y<0){
        DEBUG_PRINTF("z = %f\n",y);
      }
      vertices[j*n + i].pos = POSITION_CAST(x,z,y);
      vertices[j*n + i].color = fc(POSITION_CAST(x,y,z));
      vertices[j*n + i].normal = gl_position_to_normal(grad);

      indices[k++] = (j*n)+i;
      indices[k++] = ((j+1)*n)+i;
    }
    indices[k++] = (uint16_t)-1;
  }
  return (void*)init_gl_shape_indexed(vertices, m*n,
                               (gl_index_t*)indices, m*(2*n+1), 2);
}
