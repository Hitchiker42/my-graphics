#ifndef __SHAPES_H__
#define __SHAPES_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
/*
  TODO: Use material propertices to color a shape one color to avoid
  having to set color for each vertex
*/
#define MAKE_NAME_DECLARATIONS(name, args...)                   \
  gl_drawable *CAT(make_, name)(args, gl_color(*fc)(int));      \
  gl_drawable *CAT(make_colored_, name)(args, gl_color c);      \
  gl_drawable *CAT(make_custom_, name)(args, void(*fv)(gl_vertex*));

/*
  Construct a n sided polygon on the plane given by the point (0,0,0) and
  the normal vector norm and centered at the origin.
  The location of the first vertex is given by 'start', this point also
  implicitly determines the size of the shape. The color of each vertex
  is determined by calling f with the index of the vertex (vertices are
  counted clockwise from zero, starting with 'start'). If norm is NULL
  default to the x-y plane.
*/
MAKE_NAME_DECLARATIONS(convex_polygon, gl_position *start, int n, 
                       gl_position *norm);
/*
gl_drawable *make_convex_polygon(gl_position *start, int n, 
                                 gl_position *norm, gl_color(*f)(int));
gl_drawable *make_colored_convex_polygon(gl_position *start, int n, 
                                         gl_position *norm, gl_color color);
*/
MAKE_NAME_DECLARATIONS(cube, gl_position *square, gl_position shift);
/*
gl_drawable *make_cube(gl_position *square, gl_position shift,
                       gl_color(*f)(int));
gl_drawable *make_colored_cube(gl_position *square, gl_position shift,
                               gl_color c);
*/
MAKE_NAME_DECLARATIONS(line, float x_0, float x_n, int n,
                       gl_position(*fx)(float));
/*
gl_drawable *make_line(float x_0, float x_n, int n,
                       gl_position(*fx)(float),gl_color(*fc)(int));
gl_drawable *make_colored_line(float x_0, float x_n, int n,
                               gl_position(*fx)(float),gl_color);
*/
MAKE_NAME_DECLARATIONS(tetrahedron, gl_position *base, gl_position apex);
/*
gl_drawable *make_colored_tetrahedron(gl_position *base, 
                                      gl_position apex,gl_color);
gl_drawable *make_tetrahedron(gl_position *base, 
                              gl_position apex,gl_color(*fc)(int));
*/
MAKE_NAME_DECLARATIONS(sphere, float r);
MAKE_NAME_DECLARATIONS(sphere_indexed, float r);
MAKE_NAME_DECLARATIONS(cylinder, float r, float h);
MAKE_NAME_DECLARATIONS(grid, float x_0, float x_n, float y_0, float y_m,
                       float z_0, float z_l, gl_position(*f)(float,float,float),
                       int n, int m, int l);
MAKE_NAME_DECLARATIONS(surface, double x_0, double x_n, int n,
                       double z_0, double z_m, int m,
                       double(*fxz)(double,double));
gl_drawable* make_surface_pos_color(double x_0, double x_n, int n,
                                    double y_0, double y_m, int m,
                                    double(*fxy)(double,double),
                                    gl_color(*fc)(gl_position));
/*
gl_drawable *make_sphere(float r, gl_color(*fc)(int));
gl_drawable *make_colored_sphere(float r, gl_color);
*/
/*
  Conveince functions, these make shapes regular shapes centered
  at the origin, they have fewer arguments and are generally
  faster than the more general functions above 
*/
MAKE_NAME_DECLARATIONS(default_convex_polygon, int n, float r);
MAKE_NAME_DECLARATIONS(default_cube, float side_len);
MAKE_NAME_DECLARATIONS(default_tetrahedron, float side_len);
MAKE_NAME_DECLARATIONS(default_octahedron, float side_len);
gl_drawable *make_axes_drawable(gl_color color);
gl_drawable *make_colored_axes_drawable(gl_color x, gl_color y, gl_color z);
/*
gl_drawable *make_default_cube(gl_color(*fc)(int));
gl_drawable *make_default_tetrahedron(gl_color(*fc)(int));
gl_drawable *make_default_octahedron(gl_color(*fc)(int));

gl_drawable *make_colored_default_cube(gl_color);
gl_drawable *make_colored_default_tetrahedron(gl_color);
gl_drawable *make_colored_default_octahedron(gl_color);
*/
/*
  These functions transform vertices directly rather than
  modifing the transformation matrix. 
*/
gl_shape *translate_shape(gl_shape *shape, gl_position *offset);
gl_shape *rotate_shape(gl_shape *shape, float angle, gl_position *axis);
gl_shape *shape_set_uniform_color(gl_shape *shape, gl_color c);
/*
  Macros
*/
#define SHAPE_VERTICES(shape)  (((gl_shape*)shape)->vertices->vertices)
#ifdef __cplusplus
}
#endif
#endif /* __SHAPES_H__ */
