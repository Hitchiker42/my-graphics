/*
  NOTE TO SELF: The reason composite objects fail is probably because i recursively
  add each component to the vbo, which is unecessary if the vertices of the component
  are already in the vbo (eg. because it's a copy of another drawable)
*/
#include "common.h"
#include "context.h"
#include "drawable.h"
#include "shapes.h"
#include "matrix_stack.h"
/*
  TODO: Split into multiple files
*/
/*
  To get the best performance out of opengl we want to avoid unecessary
  synchronization (aka. flushing the gpub instruction pipeline), and
  minimize changes to vertex attribute state. This means we want
  buffers to hold as much as possible (up to a limit of 1~4 MB), but
  also avoid calls to glSubBufferData, since that causes syncronization.

  The solution for this is somewhat complex. I'll explain it later.
*/
//Note the fact this is static and has no gl_prefix;
//copy the components (vertices, or other drawables) sequentially into dest
//This is used to copy drawable to the vbo, where dest is a pointer to
//mapped buffer memory. There is no check of any kind for size in
//this function, it writes drawable->size bytes into dest, it is
//upto the caller to ensure this is possible.
#define drawable_format_string                          \
  "Printing Drawable:\nscene = %p\nvbo = %p\ntransform =\n%s"\
  "\nnum %s = %d\nvbo_offset = %u\nsize = %u\n type = %s\n"
static void sync_drawable(gl_drawable *drawable, void *dest);
/*static inline void vbo_push_drawable(gl_VBO *vbo, gl_drawable *drawable){
  if(vbo->drawables_len+1 >= vbo->drawables_size){
    vbo->drawables_size *= 2;
    vbo->drawables = realloc(vbo->drawables, vbo->drawables_size);
  }
  vbo->drawables[vbo->drawables_len++] = drawable;
}*/
static inline void vbo_check_size(gl_VBO *vbo, int size){
  int i;
  if(vbo->offset + size >= vbo->size){
    int new_size = 0;
    //delay actually updating the buffer data untill we have to
    //for now just mark each drawable as needing to be reallocated
    for(i=0;i<vbo->drawables.len;i++){
      gl_drawable *x = svector_ref(&vbo->drawables, i);
      x->reallocate = 1;
      x->sync = 1;
      x->used = 0;
      new_size += x->size;
    }
    if(new_size > vbo->size){
      DEBUG_PRINTF("Error, vbo overflow\n");
      abort();
    }
    //orphan the buffer, we get a new buffer and don't force synchronization
    glBufferData(GL_ARRAY_BUFFER, vbo->size, NULL, GL_DYNAMIC_DRAW);
    vbo->offset = 0; vbo->used = 0;
  }
}
void vbo_add_drawable_rec(gl_VBO *vbo, gl_drawable *drawable){
  if(drawable->type == GL_drawable_composite){
    int i;
    for(i=0;i<drawable->num_drawables;i++){
      vbo_add_drawable_rec(vbo, drawable->drawables[i]);
    }
    drawable->vbo = vbo;
    drawable_set_scene(drawable, vbo->scene);
  } else {
    if(drawable->vbo != NULL){return;}
    vbo_check_size(vbo, drawable->size);
    drawable->vbo = vbo;
    drawable_set_scene(drawable, vbo->scene);
    *drawable->vbo_offset = vbo->offset;
    vbo->used += drawable->size;
    vbo->offset += drawable->size;
    gl_sync_drawable(drawable);
  }
}
void vbo_add_drawable(gl_VBO *vbo, gl_drawable *drawable){
  vbo_add_drawable_rec(vbo, drawable);
  svector_push(drawable, &vbo->drawables);
}
/*
  Some convience functions to add multiple drawables
*/
void vbo_add_drawables(gl_VBO *vbo, gl_drawable **drawables, int n){
  /*
    TODO: if sum(i =0...n,drawables[i]->size) > vbo->size we
    need to allocate a new vbo.
  */
  int i;
  for(i=0;i<n;i++){
    vbo_add_drawable(vbo, drawables[i]);
  }
}
void vbo_add_drawable_va(gl_VBO *vbo, ...){
  va_list ap;
  va_start(ap, vbo);
  gl_drawable *drawable;
  while((drawable = va_arg(ap, gl_drawable*)) != NULL){
    vbo_add_drawable(vbo, drawable);
  }
  va_end(ap);
}
void vbo_add_drawables_va(gl_VBO *vbo, int n, ...){
  va_list ap;
  int i;
  gl_drawable *drawables[n];
  va_start(ap, n);
  for(i=0;i<n;i++){
    drawables[i] = va_arg(ap, gl_drawable*);
  }
  va_end(ap);
  vbo_add_drawables(vbo, drawables, n);
}

int vbo_remove_drawable(gl_VBO *vbo, gl_drawable *drawable){
  //currently just use a linear search
  svector drawables = vbo->drawables;
  int num_drawables = drawables.len;
  int i;
  for(i=0;i<num_drawables;i++){
    if(drawable == svector_ref(&drawables,i)){
//      svector_set(drawables,i,NULL);
      //this may not work, since the order things are drawn seems to matter
      svector_set(&drawables, i, svector_pop(&drawables));
      vbo->used -= drawable->size;
      return i;
    }
  }
  return 0;
}
void vbo_reallocate_drawable(gl_VBO *vbo, gl_drawable *drawable){
  vbo_check_size(vbo, drawable->size);
  *drawable->vbo_offset = vbo->offset;
  if(drawable->used){
    vbo->used += drawable->reallocate-1;
  } else {
    vbo->used += drawable->size;
  }
  drawable->sync = drawable->reallocate = 0;
  vbo->offset += drawable->size;
  gl_sync_drawable(drawable);
}

//this is the external version of sync_drawable
void gl_sync_drawable(gl_drawable *drawable){
  //The unsynchronized bit prevents syncronization, and invalidate_range
  //indicates we don't care about what was in the buffer previously
  void *data;
  if(GL_IS_SHAPE(drawable)){
    gl_shape *shape = AS_SHAPE(drawable);
//    glBufferSubData(GL_ARRAY_BUFFER, *shape->vbo_offset,
//                    shape->size, shape->vertices->vertices);
/*
//    int flag;
//    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_MAPPED, &flag);*/
    //this can probably be optimized more, but this is always correct
    //and faster than doing nothing
//    if(shape->vertices->refcount == 1){
      //no one else is using these vertices, we can use map asynchronously
//      data = glMapBufferRange(GL_ARRAY_BUFFER, *shape->vbo_offset,
//                              shape->size, GL_MAP_WRITE_BIT |
//                              GL_MAP_UNSYNCHRONIZED_BIT |
//                              GL_MAP_INVALIDATE_RANGE_BIT);
//    } else {
    gl_check_error();
    data = glMapBufferRange(GL_ARRAY_BUFFER, *drawable->vbo_offset,
                            drawable->size, GL_MAP_WRITE_BIT);
//    }
/*    int sz;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &sz);
    DEBUG_PRINTF("offset = %d, length = %d, buf_sz  = %d\n",
    *drawable->vbo_offset, drawable->size, sz);*/
    gl_check_error();
    assert(data != NULL);
    //This call could (in theory) be done in a seperate thread
    sync_drawable(drawable, data);
    glUnmapBuffer(GL_ARRAY_BUFFER);
  } else {
    sync_drawable(drawable, NULL);
  }
}
/*
  Basic Shapes
 */
#include "shapes.c"
/*
  Composite drawables
 */
#include "composite_drawables.c"
/*
  Dispatch/Generic functions
*/

/*
  Using explicit dispatch tables might be preferable if I was
  going to add lots of new drawables, or
  add new drawables at runtime (which I could do if I really wanted), but
  conidering the number of drawables should remain small using
  a switch is faster and eaiser.
*/
#define gen_dispatch_func(prefix, suffix, rettype)                      \
  rettype prefix##drawable##suffix(gl_drawable *drawable){              \
    switch(drawable->type){                                             \
      case(GL_drawable_shape):                                           \
      case(GL_drawable_points):                                         \
      case(GL_drawable_line):                                           \
      case(GL_drawable_2d_polygon):                                     \
      case(GL_drawable_3d_polygon):                                     \
        return prefix##polygon##suffix(                                 \
          (gl_polygon*)drawable);                                       \
      case(GL_drawable_composite):                                      \
        return prefix##composite_drawable##suffix(                      \
          (gl_composite_drawable*)drawable);                            \
    }                                                                   \
    __builtin_unreachable();                                            \
  }
//enough functions follow this pattern (prefix, no suffix, return void)
//that it's worth makeing into a macro
#define gen_dispatch_func_prefix(prefix)        \
  gen_dispatch_func(prefix##_,,void)
void bind_drawable_default(gl_drawable *drawable){
  switch(drawable->type){
    case(GL_drawable_shape):
    case(GL_drawable_points):
    case(GL_drawable_line):
      return bind_non_polygon_default(drawable);
    case(GL_drawable_2d_polygon):
    case(GL_drawable_3d_polygon):
      return bind_polygon_default(drawable);
    case(GL_drawable_composite):
      return;
  }
}
void gl_draw_drawable(gl_drawable *drawable){
  if(drawable->sync){
    if(drawable->reallocate){//only needed if reallocated to a bigger size
      vbo_reallocate_drawable(drawable->vbo, drawable);
    } else {
      gl_sync_drawable(drawable);
    }
  }
  switch(drawable->type){
    case(GL_drawable_shape):
    case(GL_drawable_points):
    case(GL_drawable_line):
      return gl_draw_non_polygon((gl_shape*)drawable);
    case(GL_drawable_2d_polygon):
    case(GL_drawable_3d_polygon):
      return gl_draw_polygon((gl_polygon*)drawable);
    case(GL_drawable_composite):
      return gl_draw_composite_drawable((gl_composite_drawable*)drawable);
  }
}
void sync_drawable(gl_drawable *drawable, void *dest){
  switch(drawable->type){
    case(GL_drawable_shape):
    case(GL_drawable_points):
    case(GL_drawable_line):
    case(GL_drawable_2d_polygon):
    case(GL_drawable_3d_polygon):
      return sync_shape((gl_shape*)drawable, dest);
    case(GL_drawable_composite):
      return sync_composite_drawable((gl_composite_drawable*)drawable, dest);
  }
}
gen_dispatch_func_prefix(destroy)
gen_dispatch_func(copy_gl_,,gl_drawable*)
//gen_dispatch_func(duplicate_gl_,,gl_drawable*)
gl_drawable *copy_gl_drawable_rec(gl_drawable *drawable){
  //might need to copy shape_multi drawables recursively
  if(drawable->type == GL_drawable_composite){
    return copy_gl_composite_drawable_rec((gl_composite_drawable*)drawable);
  } else {
    return copy_gl_drawable(drawable);
  }
}
void free_gl_drawable(gl_drawable *drawable){
  drawable->refcount--;
  if(drawable->refcount<0){
    destroy_drawable(drawable);
  }
}
gl_drawable* ref_gl_drawable(gl_drawable *drawable){
  drawable->refcount++;
  return drawable;
}
gl_drawable* duplicate_gl_drawable(gl_drawable *drawable){
  /*
    As it is now this is pretty wasteful, since it ups the refcount
    of the components (in copy_gl_drawable)  and then imediately lowers
    them (in free_gl_drawable), but I'll optimize it later.
  */
  gl_drawable *ret = copy_gl_drawable(drawable);
  free_gl_drawable(drawable);
  return ret;
}
#define allocate_shape(obj, obj_type)                   \
  ({obj = zmalloc(sizeof(gl_polygon));                  \
    obj->vertices = zmalloc(sizeof(gl_vertices));       \
    obj->type = obj_type;                               \
    obj;})

void *allocate_gl_drawable(enum GL_drawable_type type){
  gl_drawable *ret;
  switch(type){
    case(GL_drawable_shape):
      return allocate_shape(ret, GL_drawable_shape);
    case(GL_drawable_points):
      return allocate_shape(ret, GL_drawable_points);
    case(GL_drawable_line):
      return allocate_shape(ret, GL_drawable_line);
    case(GL_drawable_2d_polygon):
      return allocate_shape(ret, GL_drawable_2d_polygon);
    case(GL_drawable_3d_polygon):
      return allocate_shape(ret, GL_drawable_3d_polygon);
    case(GL_drawable_composite):
      ret = zmalloc(sizeof(gl_composite_drawable));
      ret->type = GL_drawable_composite;
      return ret;
  }
  __builtin_unreachable();
}
void drawable_set_scene(gl_drawable *drawable, gl_scene *scene){
  drawable->scene = scene;
  if(drawable->type == GL_drawable_composite){
    int i;
    for(i=0;i<drawable->num_drawables;i++){
      drawable_set_scene(drawable->drawables[i], scene);
    }
  }
}
//FIXME: This is broken, it's just a temporary fix to get things to compile
gl_drawable *clone_gl_drawable(gl_drawable *drawable){
  return copy_gl_drawable(drawable);
}
#define enum_str_case(name)                     \
  case name:                                    \
  return #name
char *drawable_type_to_string(enum GL_drawable_type type){
  switch(type){
    enum_str_case(GL_drawable_shape);
    enum_str_case(GL_drawable_points);
    enum_str_case(GL_drawable_line);
    enum_str_case(GL_drawable_2d_polygon);
    enum_str_case(GL_drawable_3d_polygon);
    enum_str_case(GL_drawable_composite);
  }
  __builtin_unreachable();
}
//format the common fields of all drawables to a string
/*
  Format String;
  "scene = %p\nvbo = %p\ntransform = %s\nnum %s = %d\n"
  "vbo_offset = %u\nsize = %u\n type = %s\n";
*/
char *generic_drawable_to_string(gl_drawable *drawable){
  char *buf;
  char *transform_buf = mat4_str(drawable->transform);  
  int sz = asprintf(&buf, drawable_format_string,
                    drawable->scene, drawable->vbo, transform_buf,
                    "components", drawable->num_components,
                    *drawable->vbo_offset, drawable->size,
                    drawable_type_to_string(drawable->type));
  free(transform_buf);
  return buf;
}
char *drawable_to_string(gl_drawable *drawable){
  switch(drawable->type){
    case(GL_drawable_shape):
    case(GL_drawable_points):
    case(GL_drawable_line):
    case(GL_drawable_2d_polygon):
    case(GL_drawable_3d_polygon):
      return non_polygon_to_string((gl_shape*)drawable);
    default:
      return generic_drawable_to_string(drawable);
  }
}

