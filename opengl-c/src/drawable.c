#include "common.h"
#include "drawable.h"
#include "context.h"
#include "matrix_stack.h"
/*
  generic functions
*/
//mask off the least significant bit, to ensure we have a valid pointer
#define XPTR(ptr) ((void*)(((uint64_t)ptr) & (~0x1ul)))
gl_drawable* ref_drawable(gl_drawable *drawable){
  gl_drawable_header *d = XPTR(drawable);
  d->refcount++;
  return drawable;
}
gl_shape *copy_gl_shape(gl_shape *shape);
gl_shape *clone_gl_shape(gl_shape *shape);
gl_composite_drawable *
copy_gl_composite_drawable(gl_composite_drawable *drawable, int action);
gl_composite_drawable *
clone_gl_composite_drawable(gl_composite_drawable *drawable);
gl_drawable* copy_drawable(gl_drawable *drawable){
  gl_drawable *ret;
  if(IS_COMPOSITE(drawable)){
    return (void*)copy_gl_composite_drawable(XPTR(drawable), COMPOSITE_COPY);
  } else {
    return (void*)copy_gl_shape(AS_SHAPE(drawable));
  }
}
/*gl_drawable* clone_drawable(gl_drawable *drawable){
  if(IS_COMPOSITE(drawable)){
    return (void*)clone_gl_composite_drawable(XPTR(drawable));
  } else {
    return (void*)clone_gl_shape(AS_SHAPE(drawable));
  }
  }*/
/*
  Allocation/initialization/copying of shapes
*/
gl_shape *allocate_gl_shape(){
  gl_shape *shape = zmalloc(sizeof(gl_shape));
  shape->refcount = 1;
  float *temp = shape->transform;
  set_identity(temp);
  shape->draw_mode = GL_TRIANGLE_STRIP;
  return shape;
}
gl_vertices *allocate_gl_vertices(int num_vertices){
  gl_vertices *vertices = zmalloc(sizeof(gl_vertices));
  vertices->num_vertices = num_vertices;
  vertices->refcount = 1;
  return vertices;
}
gl_vertices *init_gl_vertices(gl_vertex *verts, int num_vertices){
  gl_vertices *vertices = allocate_gl_vertices(num_vertices);
  vertices->vertices = verts;
  return vertices;
}
gl_vertices *copy_gl_vertices(gl_vertices *verts){
  gl_vertices *vertices = allocate_gl_vertices(verts->num_vertices);
  vertices->vertices = xmalloc(verts->num_vertices*sizeof(gl_vertex)); 
  memcpy(vertices->vertices, verts->vertices, verts->num_vertices*sizeof(gl_vertex));
  vertices->refcount = 1;
  return vertices;
}
/*
  It is conceivable that num_indices could be less than num_vertices,
  so we a seperate argument to determine index size.
*/
gl_indices *allocate_gl_indices(int num_indices, int index_size){
  gl_indices *indices = zmalloc(sizeof(gl_indices));
  indices->num_indices = num_indices;
  if(index_size == 1){
    indices->index_size = 1;
    indices->index_packing = 4;
    indices->index_type = GL_UNSIGNED_BYTE;
  } else if(index_size == 2){
    indices->index_size = 2;
    indices->index_packing = 2;
    indices->index_type = GL_UNSIGNED_SHORT;
  } else {
    indices->index_size = 4;
    indices->index_packing = 1;
    indices->index_type = GL_UNSIGNED_INT;
  }
  return indices;
}
gl_indices *init_gl_indices(gl_index_t *indices,
                            int num_indices, int index_size){
  gl_indices *inds = allocate_gl_indices(num_indices, index_size);
  inds->indices = indices;
  return inds;
}
gl_indices *copy_gl_indices(gl_indices *indices){
  if(!indices){return NULL;}
  gl_indices *inds = xmalloc(sizeof(gl_indices));
  memcpy(inds, indices, sizeof(gl_indices));
  inds->indices = xmalloc(inds->num_indices * inds->index_size);
  memcpy(inds->indices, indices->indices, inds->num_indices *inds->index_size);
  inds->refcount = 1;
  return inds;
}
void init_index_buffer(gl_indices *indices){
  gl_check_error();
  glGenBuffers(1, &indices->index_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices->index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               indices->num_indices * indices->index_size,
               indices->indices, GL_STATIC_DRAW);
  gl_check_error();
  //  indices->indices = NULL;
}
inline gl_shape *make_gl_shape_indexed(int num_vertices, int num_indices){
  gl_shape *shape = allocate_gl_shape();
  shape->vertices = allocate_gl_vertices(num_vertices);
  shape->vertices->vertices = zmalloc(num_vertices*sizeof(gl_vertex));
  if(num_indices > 0){
    int index_size = (num_vertices < 0xff ? 1 :
                      (num_vertices < 0xffff ? 2 : 4));
    shape->indices = allocate_gl_indices(num_vertices, index_size);
    shape->indices->indices = zmalloc(index_size*num_indices);
  }
  return shape;
}
gl_shape *make_gl_shape(int num_vertices){
  return make_gl_shape_indexed(num_vertices, 0);
}
inline gl_shape *init_gl_shape_indexed(gl_vertex *vertices, int num_vertices,
                                       gl_index_t *indices, int num_indices,
                                       int index_size){
  gl_shape *shape = allocate_gl_shape();
  shape->vertices = allocate_gl_vertices(num_vertices);
  shape->vertices->vertices = vertices;
  if(indices != NULL){
    index_size = (index_size ? index_size :
                  (num_vertices < 0xff ? 1 :
                   (num_vertices < 0xffff ? 2 : 4)));
    shape->indices = allocate_gl_indices(num_indices, index_size);
    shape->indices->indices = indices;
    init_index_buffer(shape->indices);
  }
  return shape;
}
gl_shape *create_gl_shape(gl_vertices *verts, gl_indices *inds){
  gl_shape *shape = allocate_gl_shape();
  shape->vertices = verts;
  shape->indices = inds;
  return shape;
}
gl_shape *init_gl_shape(gl_vertex *vertices, int num_vertices){
  return init_gl_shape_indexed(vertices, num_vertices, NULL,0,0);
}
gl_shape *copy_gl_shape(gl_shape *shape){
  gl_shape *copy = xmalloc(sizeof(gl_shape));
  memcpy(copy, shape, sizeof(gl_shape));
  memset(copy->transform, '\0', 16*sizeof(float));
  set_identity(copy->transform);
  copy->refcount = 1;
  copy->vertices->refcount++;
  if(copy->indices){
    copy->indices->refcount++;
  }
  return copy;
}
//maybe make seperate copy/clone vertices/indices
gl_shape *clone_gl_shape(gl_shape *shape){
  gl_shape *clone = xmalloc(sizeof(gl_shape));
  memcpy(clone, shape, sizeof(gl_shape));
  memset(clone->transform, '\0', 16*sizeof(float));
  clone->vertices = copy_gl_vertices(shape->vertices);
  //  clone->indices = copy_gl_indices(shape->indices);
  clone->refcount = 1;
  return clone;
}
void drawable_set_scene(gl_drawable *drawable, gl_scene *scene){
  struct gl_drawable_header *d = XPTR(drawable);
  d->scene = scene;
  d->vbo = &(scene->VBO);
  return;
}
void drawable_set_vbo(gl_drawable *drawable, gl_VBO *vbo){
  struct gl_drawable_header *d = XPTR(drawable);
  d->vbo = vbo;
  d->scene = vbo->scene;
  return;
}
/*
  composite drawables
*/
static gl_composite_drawable *allocate_composite_drawable(int num_drawables){
  gl_composite_drawable *comp = zmalloc(sizeof(gl_composite_drawable));
  comp->refcount = 1;
  comp->transform[0] = comp->transform[5] =
    comp->transform[10] = comp->transform[15];
  comp->num_drawables = num_drawables;
  return comp;
}
static void init_composite_components(gl_composite_drawable *comp,
                                      gl_drawable **drawables,
                                      int count, int action){
  int i;
  if(action == COMPOSITE_COPY){
    for(i=0;i<count;i++){
      comp->drawables[i] = copy_drawable(drawables[i]);
    }
  } else if(action == COMPOSITE_REF){
    for(i=0;i<count;i++){
      comp->drawables[i] = ref_drawable(drawables[i]);
    }
  } else {
    memcpy(comp->drawables, drawables, count *sizeof(gl_drawable*));
  }
}
gl_composite_drawable *make_composite_drawable(gl_drawable **drawables,
                                               int count,
                                               int action){
  gl_composite_drawable *comp = allocate_composite_drawable(count);
  comp->drawables = zmalloc(sizeof(gl_drawable*)*count);
  int i;
  init_composite_components(comp, drawables, count, action);
  return AS_COMPOSITE(comp);
}
gl_composite_drawable *init_composite_drawable(gl_drawable **drawables,
                                               int count){
  gl_composite_drawable *comp = allocate_composite_drawable(count);
  comp->drawables = drawables;
  return AS_COMPOSITE(comp);
}

gl_composite_drawable *gen_composite_drawable(gl_shape *proto,
                                              void(*init)(gl_shape*,int),
                                              int count){
  int i;
  gl_composite_drawable *comp = allocate_composite_drawable(count);
  comp->drawables = zmalloc(sizeof(gl_drawable*)*count);
  for(i=0;i<count;i++){
    gl_shape *instance = copy_gl_shape(proto);
    init(instance, i);
    comp->drawables[i] = AS_DRAWABLE(instance);
  }
  return AS_COMPOSITE(comp);
}
gl_composite_drawable *
copy_gl_composite_drawable(gl_composite_drawable *drawable, int action){
  return make_composite_drawable(drawable->drawables,
                                 drawable->num_drawables,
                                 action);
  drawable = XPTR(drawable);
  gl_composite_drawable *comp = xmalloc(sizeof(gl_composite_drawable));
  memcpy(comp, drawable, sizeof(gl_composite_drawable));
  comp->refcount = 1;
  set_identity(comp->transform);
  if(action == COMPOSITE_COPY){
  }
}
/*
  Flatten a composite drawable into a gl_shape, the resultant
  shape should be identical to the composite drawable, but
  will render faster
*/
//maybe take a pointer to the matrix stack being used
#if 0
static void cache_composite_drawable_component(gl_drawable *drawable,
                                               gl_vertex  **vertex_ptr, int *nverts,
                                               gl_index_t **index_ptr, int *nindices){
  gl_shape *shape;
  if(IS_COMPOSITE(drawable)){
    shape = cache_composite_drawable(drawable);
  } else {
    shape = drawable;
  }
  gl_vertices *verts = shape->vertices;
  gl_indices *inds = shape->indices;
  /*
    If someone else is using the same vertices/indices we need to copy them,
    if not we can just modify them directly
   */
  if(verts->refcount > 1){
    verts = copy_gl_vertices(verts);
    verts->refcount--;
  }
  if(inds && inds->refcount > 1){
    inds = copy_gl_indices(inds);
    inds->refcount--;
  }
  MATRIX_STACK_PUSH_MUL(shape);
  float *transform = MATRIX_STACK_POP(shape);
  float normal_transform[16];
  mat4_to_inverse_mat3(transform, normal_transform);
  mat3_transpose(normal_transform, NULL);
  mat3_to_mat4(normal_transform, normal_transform);
  int i;
  for(i=0;i<shape->num_vertices;i++){
    mat4_multiply_vec3(transfrom, verts->vertices[i].pos.vec, NULL);
    gl_position normal = gl_normal_to_position(verts->vertices[i].normal);
    mat4_multiply_vec3(normal_transform, normal, NULL);
    verts->vertices[i].normal = gl_position_to_normal(normal);
  }
  if(indices == NULL){
  }
  *vertex_ptr = temp->vertices;
  *index_ptr = temp->indices;
  deref_gl_drawable(shape);
  return;
}
gl_shape *cache_composite_drawable(gl_composite_drawable *comp){
}
#endif
/*
  Functions related to drawing
*/
#define shape_update_uniform(obj) gl_shape_update_uniform(AS_SHAPE(obj))
static void gl_shape_update_uniform(gl_drawable *drawable){
  gl_shape *shape = AS_SHAPE(drawable);
  gl_scene *scene = shape->scene;
  if(scene->use_stack){
    MATRIX_STACK_PUSH_MUL(shape);
    MATRIX_STACK_UPDATE_UNIFORM(shape);
    MATRIX_STACK_POP(shape);
  } else {
    update_uniform_block(&shape->scene->transform, shape->transform,
                         0, 16*sizeof(float));
  }
}
void bind_shape_default(gl_shape *shape){
  shape_update_uniform(shape);
  //this needs to be more efficent
  if(shape->texture){
    bind_texture(shape->texture);
  }
}
static int count = 0;
void gl_draw_shape(gl_shape *shape){
  if(shape->bind_shape){
    shape->bind_shape(shape);
  } else {
    bind_shape_default(shape);
  }
  gl_vertices *verts = shape->vertices;
  gl_indices *inds = shape->indices;
  /*
    Add instanced rendering if/when I need it
   */

  if(inds){
    glDrawElementsBaseVertex(shape->draw_mode, inds->num_indices,
                             inds->index_type, NULL,
                             verts->vbo_offset/sizeof(gl_vertex));
    //    disable_primitive_restart();
  } else {
    //    glDisable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
    glDrawArrays(shape->draw_mode, verts->vbo_offset/sizeof(gl_vertex),
                 verts->num_vertices);
    //    glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  }
}
void gl_draw_composite(gl_composite *comp){
  MATRIX_STACK_INIT(comp);
  int i;
  for(i=0;i<comp->num_drawables;i++){
    gl_draw_drawable(comp->drawables[i]);
  }
  MATRIX_STACK_DEINIT(comp);
}
void gl_draw_drawable(gl_drawable *d){
  if(IS_COMPOSITE(d)){
    gl_draw_composite(XPTR(d));
  } else {
    gl_draw_shape((void*)d);
  }
}
/*
  vbo stuff
*/
void gl_sync_vertices(gl_vertices *verts);
static inline void vbo_check_size(gl_VBO *vbo, int size){
  int i;
  if(vbo->offset + size >= vbo->size){
    int new_size = size;
    for(i=0;i<vbo->drawables.len;i++){
      gl_shape *x = (void*)svector_ref(&vbo->drawables, i);
      if(!IS_COMPOSITE(x)){
        new_size += x->vertices->num_vertices *sizeof(gl_vertex);
      }
    }
    while(new_size >= 0.8*vbo->size){
      vbo->size *= 2;
    }
    //reallocate/orphan the buffer
    glBufferData(GL_ARRAY_BUFFER, vbo->size, NULL, GL_DYNAMIC_DRAW);
    //put the drawables back in the vbo
    vbo->offset = 0; vbo->used = 0;
    for(i=0;i<vbo->drawables.len;i++){
      gl_shape *shape = AS_SHAPE(svector_ref(&vbo->drawables, i));
      if(!IS_COMPOSITE(shape)){
        gl_vertices *verts = shape->vertices;
        int size = verts->num_vertices * sizeof(gl_vertex);
        verts->vbo_offset = vbo->offset;
        vbo->offset += size;
        vbo->used += size;
        gl_sync_vertices(verts);
      }
    }
  }
}
void vbo_add_drawable_rec(gl_VBO *vbo, gl_drawable *drawable){
  if(IS_COMPOSITE(drawable)){
    gl_composite_drawable *comp = XPTR(drawable);
    drawable_set_scene(comp, vbo->scene);
    int i;
    for(i=0;i<comp->num_drawables;i++){
      vbo_add_drawable_rec(vbo, comp->drawables[i]);
    }
  } else {
    gl_shape *shape = AS_SHAPE(drawable);
    gl_vertices *verts = shape->vertices;
    drawable_set_scene(drawable, vbo->scene);
    //if the vertices are already in the vbo, don't add them again
    
    if(verts->vertex_buffer != 0){
      return;}
    int size = verts->num_vertices * sizeof(gl_vertex);

    verts->vertex_buffer = vbo->id;
    vbo_check_size(vbo, size);
    verts->vbo_offset = vbo->offset;
    vbo->offset += size;
    vbo->used += size;
    gl_sync_vertices(verts);
    gl_check_error();
  }
}
void gl_sync_vertices(gl_vertices *verts){
  gl_check_error();
  void *data = glMapBufferRange(GL_ARRAY_BUFFER, verts->vbo_offset,
                                verts->num_vertices*sizeof(gl_vertex),
                                GL_MAP_WRITE_BIT);
  memcpy(data, verts->vertices, verts->num_vertices*sizeof(gl_vertex));
  gl_check_error();
  glUnmapBuffer(GL_ARRAY_BUFFER);
  verts->sync = 0;
  gl_check_error();
}
/*
  This can definately be optimized
*/
void gl_sync_drawable(gl_drawable *drawable){
  void *data;
  if(!IS_COMPOSITE(drawable)){
    gl_shape *shape = AS_SHAPE(drawable);
    if(shape->vertices->sync){
      gl_sync_vertices(shape->vertices);
    }
  } else {
    gl_composite *comp = AS_COMPOSITE(drawable);
    int i;
    for(i=0;i<comp->num_drawables;i++){
      gl_sync_drawable(comp->drawables[i]);
    }
  }
}
void vbo_add_drawable(gl_VBO *vbo, gl_drawable *drawable){
  vbo_add_drawable_rec(vbo, drawable);
  svector_push(drawable, &vbo->drawables);
}
void vbo_add_drawables(gl_VBO *vbo, gl_drawable **drawables, int n){
  int i;
  gl_check_error();
  for(i=0;i<n;i++){
    vbo_add_drawable(vbo, drawables[i]);
  }
  gl_check_error();
}
#include "shapes.c"
