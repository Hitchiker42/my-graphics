#include "interaction.h"
keypress_closures default_keymap_mem[256];
gl_keymap default_keymap = default_keymap_mem;
gl_keymap current_keymap = default_keymap_mem;
void window_set_keymap(gl_window win, gl_keymap map){
  struct window_data *data = glfwGetWindowUserPointer(win);
  data->keymap = map;
}
#ifdef HAVE_X11
/*
  We'll probably have to define our own platform specific key macros
  to avoid having to translate back and forth between X11 and GLFW.

  I'll figure out something eventually
*/
static int glfw_modmask_to_X11(int mods){
  int state = 0;
  if(mods & GLFW_MOD_SHIFT){
    state |= ShiftMask;
  }
  if(mods & GLFW_MOD_CONTROL){
    state |= ControlMask;
  }
  if(mods & GLFW_MOD_ALT){
    state |= Mod1Mask;
  }
  if(mods & GLFW_MOD_SUPER){
    state |= Mod4Mask;
  }
  return state;
}
static int X11_state_to_glfw(int state){
  int mods = 0;
  if(state & ShiftMask){
    mods |= GLFW_MOD_SHIFT;
  }
  if(state & ControlMask){
    mods |= GLFW_MOD_CONTROL;
  }
  if(state & Mod1Mask){
    mods |= GLFW_MOD_ALT;
  }
  if(state & Mod4Mask){
    mods |= GLFW_MOD_SUPER;
  }
  return mods;
}
void lookup_key(struct window_data *data, int keycode, int state,
                unsigned long *sym, int *mods){

  int err = XkbLookupKeySym(data->X11_display, keycode,
                            glfw_modmask_to_X11(state), mods, sym);
  mods = X11_state_to_glfw(*mods);
  return;
}
#else
#define lookup_key(data, keycode, state, sym, mods)                     \
  *sym = keycode;                                                       \
  *mods = state
#endif
void keymap_set_key(gl_keymap map, int key, int mod,
                    keypress_callback callback, void *data){
  keypress_closures *actions = KEYMAP_GET_KEYPTR(map, key);
  keypress_closure *act = actions->closures + MODIFIER_TO_INDEX(mod);
  act->callback = callback;
  act->data = data;
}
/*
  This only works with SDL, glfw doesn't give enough control
  over events to allow this.
*/
/*
  Interactive Transformation
*/

//this needs to run once a frame
void interactive_transform_update(interactive_transform *trans){
  float *transforms = trans->transforms;
  int i,j;
  /*
    Update the transformation values.

    This is rather hard to follow, but basically it loops over each
    axis of each transform and increments/decrements that value
    by it's coorsponding delta if the inc/dec bit is set in the
    coorsponding bit mask.

    If I actually wrote it out (it a easy to understand way) it would
    entail copying the code in the inner loop 12 times.
   */
  for(i=0;i<4;i++){
    for(j=0;j<3;j++){
      if(trans->masks[i] & (1 << (2*j))){
        transforms[i*3 + j] -= trans->deltas[i];
      } else if (trans->masks[i] & (1 << ((2*j)+1))){
        transforms[i*3 + j] += trans->deltas[i];
      }
    }
  }
  /*
    Generate the transformation matrix.
    It might be possible to optimize this by doing some of the translations
    inline, but that would likely lead to errors, and it probably unecessary.
  */
  float *matrix = trans->matrix;
  matrix = mat4_identity(matrix);
  if(trans->shear_x){
    matrix = mat4_shearX(matrix, trans->shear_x, trans->shear_x, NULL);
  }
  if(trans->shear_y){
    matrix = mat4_shearY(matrix, trans->shear_y, trans->shear_y, NULL);
  }
  if(trans->shear_z){
    matrix = mat4_shearZ(matrix, trans->shear_z, trans->shear_z, NULL);
  }
  if(trans->scale[0] != 1 || trans->scale[1] != 1 || trans->scale[2] != 1){
    matrix = mat4_scale(matrix, trans->scale, NULL);
  }
  if(trans->translate[0] || trans->translate[1] || trans->translate[2]){
    matrix = mat4_translate(matrix, trans->translate, NULL);
  }
  if(trans->rotate_x){
    matrix = mat4_rotateX(matrix, trans->rotate_x, NULL);
  }
  if(trans->rotate_y){
    matrix = mat4_rotateY(matrix, trans->rotate_y, NULL);
  }
  if(trans->rotate_z){
    matrix = mat4_rotateZ(matrix, trans->rotate_z, NULL);
  }
  return;
}

interactive_transform*
make_interactive_transform(float d_translate, float d_rotate,
                           float d_scale, float d_shear,
                           gl_keystate *keys, gl_keymap map){
  interactive_transform *trans = zmalloc(sizeof(interactive_transform));
  int i;
  float deltas[4] = {d_translate, d_rotate, d_scale, d_shear};
  for(i=0;i<4;i++){
    trans->deltas[i] = deltas[i];
  }
  trans->scale_x = trans->scale_y = trans->scale_z = 1;
  interactive_transform_bind_keys(trans, keys, map);
  return trans;
}


/*
  I should probably do something for keys with modifiers so that if the modifier
  is released the transformation is stopped
*/
#define gen_update_function(what, what_lc, direction)                   \
  void CAT4(transform_,what,_,direction)(gl_window win, int keycode,    \
                                         int scancode, int action,      \
                                         int modmask, void *data){      \
    interactive_transform *trans = (interactive_transform*)data;        \
    if(action == KEY_PRESS){                                            \
      CAT(transform_add_,what_lc)(trans, CAT3(TRANSFORM_, direction, _BIT)); \
    } else if(action == KEY_RELEASE){                                   \
      CAT(transform_remove_,what_lc)(trans, CAT3(TRANSFORM_, direction, _BIT)); \
    }                                                                   \
    return;                                                             \
  }
#define gen_update_functions(what, what_lc)     \
  gen_update_function(what, what_lc, X_DEC);    \
  gen_update_function(what, what_lc, X_INC);    \
  gen_update_function(what, what_lc, Y_DEC);    \
  gen_update_function(what, what_lc, Y_INC);    \
  gen_update_function(what, what_lc, Z_DEC);    \
  gen_update_function(what, what_lc, Z_INC);

gen_update_functions(TRANSLATE, translate);
gen_update_functions(ROTATE, rotate);
gen_update_functions(SCALE, scale);
gen_update_functions(SHEAR, shear);
#undef gen_update_function
#undef gen_update_functions
static keypress_callback transform_callbacks [] =
  {transform_TRANSLATE_X_INC, transform_TRANSLATE_X_DEC,
   transform_TRANSLATE_Y_INC, transform_TRANSLATE_Y_DEC,
   transform_TRANSLATE_Z_INC, transform_TRANSLATE_Z_DEC,
   transform_ROTATE_X_INC, transform_ROTATE_X_DEC,
   transform_ROTATE_Y_INC, transform_ROTATE_Y_DEC,
   transform_ROTATE_Z_INC, transform_ROTATE_Z_DEC,
   transform_SCALE_X_INC, transform_SCALE_X_DEC,
   transform_SCALE_Y_INC, transform_SCALE_Y_DEC,
   transform_SCALE_Z_INC, transform_SCALE_Z_DEC,
   transform_SHEAR_X_INC, transform_SHEAR_X_DEC,
   transform_SHEAR_Y_INC, transform_SHEAR_Y_DEC,
   transform_SHEAR_Z_INC, transform_SHEAR_Z_DEC};
void transform_reset_all_callback(gl_window win, int key, void *data,
                                  int action,int modmask){
  if(action == KEY_RELEASE){
    transform_reset_all(data);
  }
}
void interactive_transform_bind_keys(interactive_transform *trans,
                                     gl_keystate keys[25], gl_keymap map){
  if(trans->map == NULL){
    trans->map = (map ? map : default_keymap);
  }
  int i = 0, j = 0, k = 0;
  for(i=0;i<24;i++){
    j = KEY_TO_KEYMAP_INDEX(keys[i].keycode);
    k = MODIFIER_TO_INDEX(keys[i].modmask);
    //    DEBUG_PRINTF("Setting keystate: %sto callback %p\n",
    //                 keystate_to_string(keys[i]), transform_callbacks[i]);
    trans->map[j].closures[k].callback = transform_callbacks[i];
    trans->map[j].closures[k].data = trans;
  }
  j = KEY_TO_KEYMAP_INDEX(keys[i].keycode);
  k = MODIFIER_TO_INDEX(keys[i].modmask);
  //  DEBUG_PRINTF("Setting keystate: %sto callback %p\n",
  //               keystate_to_string(keys[i]), transform_reset_all_callback);
  trans->map[j].closures[k].callback = transform_reset_all_callback;
  trans->map[j].closures[k].data = trans;
}
void interactive_transform_update_key(interactive_transform *trans,
                                      gl_keystate key, int what, int bit){
  //ctz gets the 0 based index of the first set bit,
  //since both 'what' and 'bit' can't be 0 we can use this
  int i = MODIFIER_TO_INDEX(key.modmask);
  int j = KEY_TO_KEYMAP_INDEX(key.keycode);
  int k = __builtin_ctz(what)*6 + __builtin_ctz(bit);
  trans->map[i].closures[j].callback = transform_callbacks[k];
  //presumably this doesn't need to be changed, but it doesn't hurt
  trans->map[i].closures[j].data = trans;
}

/*
  Interactive Camera
*/
/*
  Needs to run once a scene
*/

static void camera_gen_view(interactive_camera *cam, gl_position view[3]){
  gl_scene *scene = cam->scene;
  gl_camera *camera = cam->camera;
  /*view = {eye, center, up}*/
  view[0] = camera->eye;
  view[1] = camera->center;
  view[2] = camera->up;
  int i;
  for(i=0;i<3;i++){
    view[1].vec[i] += cam->pan[i];
    view[1].vec[i] += cam->move[i];
    view[0].vec[i] += cam->move[i];
  }
  DEBUG_PRINTF("eye moved = %s\n", format_gl_position(view[0]));
  camera_rotate_horizontal(&view[0], &view[1], &view[2], cam->rotate_xz);
  camera_rotate_vertical(&view[0], &view[1], &view[2], cam->rotate_yz);
  view[0].z += cam->zoom;
  DEBUG_PRINTF("eye rotated = %s\n", format_gl_position(view[0]));
  view[0].z = CLAMP(view[1].z, view[1].z+ZOOM_MIN, view[1].z+ZOOM_MAX);
  DEBUG_PRINTF("eye clamped = %s\n", format_gl_position(view[0]));
}
char* interactive_camera_to_string(interactive_camera *cam){
  //allocate 10 bytes per float, which should be more than enough
  //For now just format the transformations and the deltas
  const char *fmt = "pan    = (%f, %f, %f), panΔ    = %f\n"
                    "move   = (%f, %f, %f), moveΔ   = %f\n"
                    "rotate = (%f, %f), rotateΔ = %f\n"
                    "zoom   = %f, zoomΔ = %f\n"
                    "eye    = (%f, %f, %f)\n"
                    "center = (%f, %f, %f)\n"
                    "up     = (%f, %f, %f)\n";
  gl_position view[3];
  camera_gen_view(cam, view);
  char *buf;
  asprintf(&buf, fmt,
           cam->pan_x, cam->pan_y, cam->pan_z, cam->pan_delta,
           cam->move_x, cam->move_y, cam->move_z, cam->move_delta,
           cam->rotate_xz, cam->rotate_yz, cam->rotate_delta,
           cam->zoom, cam->zoom_delta,
           view[0].x, view[0].y, view[0].z,
           view[1].x, view[1].y, view[1].z,
           view[2].x, view[2].y, view[2].z);
  return buf;
}
void interactive_camera_update(interactive_camera *cam){
  int i,j;
  //This updates move and pan
  for(i=0;i<2;i++){
    for(j=0;j<3;j++){
      if(cam->masks[i] & (1 << (2*j))){
        cam->transforms[i*3 + j] -= cam->deltas[i];
      } else if (cam->masks[i] & (1 << ((2*j)+1))){
        cam->transforms[i*3 + j] += cam->deltas[i];
      }
    }
  }

  if(cam->rotate_mask & CAMERA_X_INC_BIT){
    cam->rotate_xz += cam->rotate_delta;
  } else if(cam->rotate_mask & CAMERA_X_DEC_BIT){
    cam->rotate_xz -= cam->rotate_delta;
  }

  if(cam->rotate_mask & CAMERA_Y_INC_BIT){
    cam->rotate_yz += cam->rotate_delta;
  } else if(cam->rotate_mask & CAMERA_Y_DEC_BIT){
    cam->rotate_yz -= cam->rotate_delta;
  }

  if(cam->zoom_mask & CAMERA_INC_BIT){
    cam->zoom += cam->zoom_delta;
  } else if(cam->zoom_mask & CAMERA_DEC_BIT){
    cam->zoom -= cam->zoom_delta;
  }


  gl_scene *scene = cam->scene;
  gl_camera *camera = cam->camera;
  gl_position eye = camera->eye;
  gl_position center = camera->center;
  gl_position up = camera->up;
  DEBUG_PRINTF("eye = %s\n",format_gl_position(eye));
  for(i=0;i<3;i++){
    center.vec[i] += cam->pan[i];
    center.vec[i] += cam->move[i];
    eye.vec[i] += cam->move[i];
  }
  DEBUG_PRINTF("eye moved = %s\n",format_gl_position(eye));
  camera_rotate_horizontal(&center, &eye, &up, cam->rotate_xz);
  camera_rotate_vertical(&center, &eye, &up, cam->rotate_yz);
  DEBUG_PRINTF("eye rotated = %s\n",format_gl_position(eye));
  eye.z += cam->zoom;
  DEBUG_PRINTF("eye zoomed = %s\n",format_gl_position(eye));
  eye.z = CLAMP(eye.z, center.z + ZOOM_MIN, center.z + ZOOM_MAX);
  DEBUG_PRINTF("eye clamped = %s\n",format_gl_position(eye));
  mat4_identity(scene->view);
  mat4_look_at(eye.vec, center.vec, up.vec, scene->view);
  //update projection, when/if I add it
}
interactive_camera *make_interactive_camera(float d_pan, float d_move,
                                            float d_rotate, float d_zoom,
                                            gl_camera *cam, gl_keystate *keys,
                                            gl_keymap map){
  interactive_camera *camera = zmalloc(sizeof(interactive_camera));
  camera->camera = cam;
  camera->pan_delta = d_pan;
  camera->move_delta = d_move;
  camera->rotate_delta = d_rotate;
  camera->zoom_delta = d_zoom;
  //  camera->zoom = 1;
  interactive_camera_bind_keys(camera, keys, map);
  return camera;
}
#define gen_camera_function(name, name_lc, dir)                         \
  void CAT4(interactive_camera_,name,_,dir)(gl_window win, int keycode, \
                                            int scancode, int action,   \
                                            int modmask,void *data){    \
    interactive_camera *camera = data;                                  \
    if(action == GLFW_PRESS){                                           \
      CAT(interactive_camera_add_, name_lc)(camera, CAT3(CAMERA_, dir, _BIT)); \
    } else if(action == GLFW_RELEASE){                                  \
      CAT(interactive_camera_remove_, name_lc)(camera, CAT3(CAMERA_, dir, _BIT)); \
    }                                                                   \
  }
#define gen_camera_functions_xyz(name, name_lc) \
  gen_camera_function(name, name_lc, X_DEC);    \
  gen_camera_function(name, name_lc, X_INC);    \
  gen_camera_function(name, name_lc, Y_DEC);    \
  gen_camera_function(name, name_lc, Y_INC);    \
  gen_camera_function(name, name_lc, Z_DEC);    \
  gen_camera_function(name, name_lc, Z_INC);

gen_camera_functions_xyz(PAN, pan)
gen_camera_functions_xyz(MOVE, move)

gen_camera_function(ROTATE, rotate, X_DEC);
gen_camera_function(ROTATE, rotate, X_INC);
gen_camera_function(ROTATE, rotate, Y_DEC);
gen_camera_function(ROTATE, rotate, Y_INC);
gen_camera_function(ZOOM, zoom, DEC);
gen_camera_function(ZOOM, zoom, INC);
static keypress_callback camera_callbacks[] =
  {interactive_camera_PAN_X_INC, interactive_camera_PAN_X_DEC,
   interactive_camera_PAN_Y_INC, interactive_camera_PAN_Y_DEC,
   interactive_camera_PAN_Z_INC, interactive_camera_PAN_Z_DEC,
   interactive_camera_MOVE_X_INC, interactive_camera_MOVE_X_DEC,
   interactive_camera_MOVE_Y_INC, interactive_camera_MOVE_Y_DEC,
   interactive_camera_MOVE_Z_INC, interactive_camera_MOVE_Z_DEC,
   interactive_camera_ROTATE_X_INC, interactive_camera_ROTATE_X_DEC,
   interactive_camera_ROTATE_Y_INC, interactive_camera_ROTATE_Y_DEC,
   interactive_camera_ZOOM_INC, interactive_camera_ZOOM_DEC
  };
void camera_reset_all_callback(gl_window win, int key, void *data,
                               int action,int modmask){
  if(action == KEY_RELEASE){
    interactive_camera *cam = data;
    /*    DEBUG_PRINTF("Camera state:\n");
    DEBUG_PRINTF(gl_camera_to_string(cam->camera));
    DEBUG_PRINTF("Reseting camera, state before reset:\n");
    DEBUG_PRINTF(interactive_camera_to_string(data));*/
    camera_reset_all(data);
    /*    DEBUG_PRINTF("Reseting camera, state after reset:\n");
          DEBUG_PRINTF(interactive_camera_to_string(data));*/
  }
}
void interactive_camera_bind_keys(interactive_camera *cam,
                                  gl_keystate keys[19], gl_keymap map){
  if(cam->map == NULL){
    cam->map = (map ? map : default_keymap);
  }
  int i = 0, j = 0, k = 0;
  for(i=0;i<18;i++){
    j = KEY_TO_KEYMAP_INDEX(keys[i].keycode);
    k = MODIFIER_TO_INDEX(keys[i].modmask);
    //    DEBUG_PRINTF("Setting keystate: %sto callback %p\n",
    //                 keystate_to_string(keys[i]), camera_callbacks[i]);
    cam->map[j].closures[k].callback = camera_callbacks[i];
    cam->map[j].closures[k].data = cam;
  }
  j = KEY_TO_KEYMAP_INDEX(keys[i].keycode);
  k = MODIFIER_TO_INDEX(keys[i].modmask);
  //  DEBUG_PRINTF("Setting keystate: %sto callback %p\n",
  //               keystate_to_string(keys[i]), camera_reset_all_callback);
  cam->map[j].closures[k].callback = camera_reset_all_callback;
  cam->map[j].closures[k].data = cam;
}
static void keypress_quit(gl_window win, int keycode, int scancode,
                          int action, int modmask, void *data){
  exit(0);
}
keypress_callback keypress_quit_program = keypress_quit;
