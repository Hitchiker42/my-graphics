#ifndef __INTERACTION_H__
#error "Do not include interactive_transform.h directly, use interaction.h"
#endif
    
#define TRANSFORM_MIN_BIT 0x1

#define TRANSFORM_X_DEC_BIT 0x1
#define TRANSFORM_X_INC_BIT 0x2
#define TRANSFORM_Y_DEC_BIT 0x4
#define TRANSFORM_Y_INC_BIT 0x8
#define TRANSFORM_Z_DEC_BIT 0x10
#define TRANSFORM_Z_INC_BIT 0x20

#define TRANSFORM_MAX_BIT 0x20

#define TRANSFORM_TRANSLATE 0x1
#define TRANSFORM_ROTATE 0x2
#define TRANSFORM_SCALE 0x4
#define TRANSFORM_SHEAR 0x8
#define TRANSFORM_ALL                                           \
  (TRANSFORM_TRANSLATE | TRANSFORM_ROTATE | TRANSFORM_SCALE | TRANSFORM_SHEAR)

/*
  This structure contains information requried for interactively
  rotating, translating, scaling, and shearing an object or scene.
*/
typedef struct interactive_transform interactive_transform;
struct interactive_transform {
  float *matrix;
  gl_keymap map;
  union {
    float transforms[12];
    struct {
      union {
        float translate[3];
        struct {
          float translate_x;
          float translate_y;
          float translate_z;
        };
      };
      union {
        float rotate[3];
        struct {
          float rotate_x;
          float rotate_y;
          float rotate_z;
        };
      };

      union {
        float scale[3];
        struct {
          float scale_x;
          float scale_y;
          float scale_z;
        };
      };
      union {
        float shear[3];
        struct {
          float shear_x;
          float shear_y;
          float shear_z;
        };
      };
    };
  };
  //How much to move/rotate/scale by per frame
  union {
    float deltas[4];
    struct {
      float translate_delta;
      float rotate_delta;
      //only alow uniform scaling
      float scale_delta;
      float shear_delta;
    };
  };
  /*
    These are in the same order as the structs above, with the negitive key
    first then the positive key.
    i.e translate_x_neg, translate_x_pos,...shear_z_neg, shear_z_pos
    + 1 key to reset everything
   */
  gl_keystate keybindings[25];
  //which axes and directions to move/rotate/scale
  union {
    uint8_t masks[4];
    struct {
      uint8_t translate_mask;
      uint8_t rotate_mask;
      uint8_t scale_mask;
      uint8_t shear_mask;
    };
  };
};
#define gen_transform_add_fun(op)                                      \
  static inline void                                                    \
  CAT(transform_add_,op)(interactive_transform *trans, uint8_t bit){    \
    trans->CAT(op,_mask) |= bit;                                        \
  }
#define gen_transform_remove_fun(op)                                      \
  static inline void                                                    \
  CAT(transform_remove_,op)(interactive_transform *trans, uint8_t bit){    \
    trans->CAT(op,_mask) &= ~(bit);                                     \
  }
#define gen_transform_funs(op)                                  \
  gen_transform_add_fun(op)                                     \
  gen_transform_remove_fun(op)

gen_transform_funs(translate);
gen_transform_funs(rotate);
gen_transform_funs(scale);
gen_transform_funs(shear);
static inline
void transform_reset(interactive_transform *trans, int what){
  int i;
  if(what & TRANSFORM_TRANSLATE){
    trans->translate_mask = 0;
    for(i=0;i<3;i++){
      trans->translate[i] = 0;
    }
  }
  if(what & TRANSFORM_ROTATE){
    trans->rotate_mask = 0;
    for(i=0;i<3;i++){
      trans->rotate[i] = 0;
    }
  }
  if(what & TRANSFORM_SCALE){
    trans->scale_mask = 0;
    for(i=0;i<3;i++){
      trans->scale[i] = 1;
    }
  }
  if(what & TRANSFORM_SHEAR){
    trans->shear_mask = 0;
    for(i=0;i<3;i++){
      trans->shear[i] = 0;
    }
  }
}
static inline
void transform_reset_all(interactive_transform *trans){
  int i;
  for(i=0;i<4;i++){
    trans->masks[i] = 0;
  }
  for(i=0;i<3;i++){
    trans->translate[i] = 0;
    trans->rotate[i] = 0;
    trans->scale[i] = 1;
    trans->shear[i] = 0;
  }
}
static inline
void transform_set_delta(interactive_transform *trans, float val, int what){
  //We could do this like the function above, but it doesn't really
  //make sense to set the delta for multiple transforms to the same thing
  switch(what){
    case TRANSFORM_TRANSLATE:
      trans->translate_delta = val; return;
    case TRANSFORM_ROTATE:
      trans->rotate_delta = val; return;
    case TRANSFORM_SCALE:
      trans->scale_delta = val; return;
    case TRANSFORM_SHEAR:
      trans->shear_delta = val; return;
    default:
      fprintf(stderr,
              "Unknown transformation %d passed to transform set delta", what);
      return;
  }
  __builtin_unreachable();
}

//these 3 all set the matrix field of the struct
static inline
void transform_set_matrix(interactive_transform *trans, float *matrix){
  trans->matrix = matrix;
}
static inline
void transform_set_drawable(interactive_transform *trans,
                            gl_drawable *drawable){
  trans->matrix = drawable->transform;
}
static inline
void transform_set_scene(interactive_transform *trans,
                         gl_scene *scene){
  trans->matrix = scene->scene;
}
interactive_transform*
make_interactive_transform(float d_move, float d_rotate, 
                           float d_scale, float d_shear,
                           gl_keystate *keys, gl_keymap map);
//needs to run once a frame
void interactive_transform_update(interactive_transform *trans);
/*
  we need num_transforms * num_axes * num_directions or 4 * 3 * 2 = 24 keys
  It's easiest for the function to just take an array, but I'll define
  some macros to make calling this eaiser
*/
/*
  I could be clever and store a key and modifer mask in one integer
  via a struct like {int16_t key; int16_t mod}, or via some bitshifting
  but it's just eaiser to specifiy them seperately
*/
void interactive_transform_bind_keys(interactive_transform *trans,
                                     gl_keystate keys[25], gl_keymap map);
static inline
void interactive_transform_set_keys(interactive_transform *trans){
  interactive_transform_bind_keys(trans, trans->keybindings, trans->map);
}
#define interactive_transform_bind_keys_arrays(trans, keys, mods)       \
  ({gl_keystate keystates[24];                                          \
    int i;                                                              \
    for(i=0;i<24;i++){                                                  \
      keystats[i] = (gl_keystate){.key = keys[i], .modmask = mods[i]};  \
    }                                                                   \
    interactive_transform_bind_keys(trans, keystates);                  \
  })
#define interactive_transform_bind_keys_va(trans, ...)                  \
  ({gl_keystate keystates[] = {__VA_ARGS__};                            \
    interactive_transform_bind_keys(trans, keystates);                  \
  })
void interactive_transform_update_key(interactive_transform *trans,
                                      gl_keystate key, int what, int bit);
