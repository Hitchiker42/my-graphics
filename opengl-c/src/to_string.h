#ifndef __TO_STRING_H__
#define __TO_STRING_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
typedef char *(gl_enum_to_string_func)(GLenum);
char * __attribute__((const)) gl_primitive_type_to_string(GLenum val);
char * __attribute__((const)) gl_shader_type_to_string(GLenum val);
char * __attribute__((const)) gl_error_code_to_string(GLenum val);
//for compatability with old code
static gl_enum_to_string_func *gl_strerror = gl_error_code_to_string;
static gl_enum_to_string_func *draw_mode_to_string = gl_primitive_type_to_string;
//Buffer size large enough to hold any of the components of a vertx as a string
static const int gl_component_str_buf_sz = 32;
/*
  Print a position as a tuple, don't print the w coordinate (ex (x, y, z)).
  Positions are assumed to be in the range [-1.0,1.0].
*/
char *gl_position_to_string(gl_position pos, char *buf);
//Normals are formatted identically to positions, but need to be processed
//differently due to the way they are stored
char *gl_normal_to_string(gl_normal norm, char *buf);
//Similar to position, but with only 2 components, in the range [0.0,1.0]
char *gl_tex_coord_to_string(gl_tex_coord coord, char *buf);
/*
  There are a few options for color, for now we print a tuple of hex byte values.
  We could print the normalized floating point values, or just a single hex value.
  The order of the tuple is RGBA, Even though the value stored in memory is ARGB.

  For the size we need 16 bytes for numbers + '('+')'+3','+3' '
*/
char *gl_color_to_string(gl_color pos, char *buf);
char* gl_camera_to_string(gl_camera *cam);
char *gl_shape_to_string(gl_shape *shape, char *buf);
//for compatability
char *drawable_to_string(gl_drawable *drawable);
char *keypress_closures_to_string(keypress_closures *kc);
char *keystate_to_string(gl_keystate k);
char* interactive_camera_to_string(interactive_camera *cam);

//return a static/stack allocated (i.e don't free) string containing
//a textual representation of key, mod is necessary to differentiate
//upper and lower case characters
//#define key_to_string(key, mod)
//  ({char str[2];
/*
  Print 'num_components' compontexts of the given vertex.
  The order in which components are chosen is:
    position, color, normals, texure coordinates

  The format is a tuple with named values, 2 per line, ex:
  (position = (x, y, z), color = (r, g, b, a),
   normals = (x, y, z), texture coordinates = (u, v))
*/
//this is the size needed for 4 components, 128 is enough for position+color
static const int gl_vertex_str_buf_sz = 256;
char *gl_vertex_to_string(gl_vertex vert, char *buf, int num_components);
//Some macros to print objects on stack allocated memory
#define format_gl_position(pos)                 \
  ({char *buf = alloca(gl_component_str_buf_sz);      \
    buf = gl_position_to_string(pos,buf);           \
    buf;})
#define format_gl_color(color)                  \
  ({char *buf = alloca(gl_component_str_buf_sz);    \
    buf = gl_color_to_string(color,buf);              \
    buf;})
#define format_gl_normal(norm)                  \
  ({char *buf = alloca(gl_component_str_buf_sz);      \
    buf = gl_normal_to_string(norm,buf);             \
    buf;})
#define format_gl_tex_coord(coords)            \
  ({char *buf = alloca(gl_component_str_buf_sz);      \
    buf = gl_tex_coord_to_string(coords,buf);         \
    buf;})
#define format_gl_vertex(vert, n)               \
  ({char *buf = alloca(gl_vertex_str_buf_sz);   \
    buf = gl_vertex_to_string(vert, buf, n);    \
    buf;})
#define format_gl_vertex_1(vert) format_gl_vertex(vert, 1)
#define format_gl_vertex_2(vert) format_gl_vertex(vert, 2)
#define format_gl_vertex_simple(vert) format_gl_vertex_2(vert)
#define format_gl_vertex_3(vert) format_gl_vertex(vert, 3)
#define format_gl_vertex_4(vert) format_gl_vertex(vert, 4)
//pretty slow since it does heap allocation, oh well
#define format_keystate(k)                      \
  ({char *tmp_buf = keystate_to_string(k);      \
    char *buf = alloca(strlen(tmp_buf)+1);      \
    memcpy(buf, tmp_buf, strlen(tmp_buf)+1);    \
    free(tmp_buf);                              \
    buf;})

#define gl_print_mat4(mat)                      \
  ({char* buf=mat4_str(mat);                    \
    DEBUG_PRINTF("%s\n",buf);                   \
    free(buf);})
/*
  Get a printable string representing key, the string is either
  statically allocated or allocated on the stack and thus doesn't
  need to be freed, thats why it's a macro, so it can return stack
  allocated memory.
*/
#define gl_key_to_string(key)                   \
  ({char c, *s;                                 \
    int type = gl_get_keyname(key, &c, &s);     \
    if(type == 1){                              \
      char tmp[8];                              \
      snprintf(tmp, 8, "%c", key);              \
      s = tmp;                                  \
    } else if(type == 3){                       \
      char tmp[24];                             \
      snprintf(tmp, 24, "%#0x", key);           \
      s = tmp;                                  \
    }                                           \
    s;})

#define DEBUG_PRINT_SHAPE(shape_obj)            \
  ({char *buf;                                  \
    buf = gl_shape_to_string(shape_obj, buf);   \
    DEBUG_PRINTF("%s",buf);                     \
    free(buf);})
#ifdef __cplusplus
}
#endif
#endif /* __TO_STRING_H__ */
