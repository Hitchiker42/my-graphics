#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
#include "interaction.h"
/*
  This should remain fairly uncomplicated, it draws a single shape,
  and has the ability to rotate the scene, and move the shape.  
  
  It also draws axes and a 3d grid to help show depth
*/
thread_local global_context *thread_context;
static gl_keystate transform_keybindings[25] =
  {{.keycode = GLFW_KEY_RIGHT},{.keycode = GLFW_KEY_LEFT},//translate x
   {.keycode = GLFW_KEY_UP},{.keycode = GLFW_KEY_DOWN},//translate y
   {.keycode = GLFW_KEY_UP, .modmask = GLFW_MOD_SHIFT},
   {.keycode = GLFW_KEY_DOWN, .modmask = GLFW_MOD_SHIFT}, //translate z
   //rotate
   {.keycode = 'x'}, {.keycode = 'x', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'c'}, {.keycode = 'c', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'z'}, {.keycode = 'z', .modmask = GLFW_MOD_CONTROL},
   //scale width, height, depth
   {.keycode = 'w'}, {.keycode = 'w', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'h'}, {.keycode = 'h', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'd'}, {.keycode = 'd', .modmask = GLFW_MOD_CONTROL},
   //shear ',', '.', '/' because why not
   {.keycode = ','}, {.keycode = ',', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '.'}, {.keycode = '.', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '/'}, {.keycode = '/', .modmask = GLFW_MOD_CONTROL},
   //reset
   {.keycode = 'r'}};
static const int grid_step = 20;
gl_position grid_gen(float x, float y, float z){
  float d = z/20.0;
  gl_position ret = POSITION(x+d,y+d,z);
  return ret;
}
gl_color grid_color(int i){
  int z = i/(grid_step*grid_step);
  int y = (i%(grid_step*grid_step))/grid_step;
  int x = (i%(grid_step*grid_step))%grid_step;
  gl_color ret = {.r = z*(0xff/grid_step),
                  .g = y*(0xff/grid_step),
                  .b = x*(0xff/grid_step), .a = 0xff};

  return ret;
}
void update_scene(gl_scene *s, void *data){
  interactive_transform_update(data);
}
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "simple test");
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;
  gl_scene *scene = make_simple_scene(basic_vertex, basic_fragment);
  //window_set_keymap(ctx->window, default_keymap_ptr);
  interactive_transform *trans = 
    make_interactive_transform(0.05, M_PI/60, 0.05, 0.05, 
                               transform_keybindings, default_keymap);
  ctx->userdata = trans;
  //These 2 transformations are the default
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  //  mat4_ortho(-1,1,-1,1,-1,1,scene->projection);
  
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  //mat4_perspective(135.0, 1, 0.25, 100, scene->projection);
  
  scene->update_scene = (void*)update_scene;
  float scale[3] = {0.5,0.5,0.5};
  mat4_scale(scene->projection, scale , NULL);
  ctx->scenes[0] = scene;

  /*
   */
  //  glClearColor(1,0,0,1);
  //  drawable = init_drawable();
  int i, j, k;
  int i_max = 30, j_max = 30, k_max = 30;
#define num_colors 10
  gl_drawable **proto = xmalloc(num_colors*sizeof(gl_drawable*));
  gl_color colors[num_colors] =
    {gl_color_blue, gl_color_cyan, gl_color_green, gl_color_magenta,
     gl_color_red, gl_color_yellow, gl_color_white, GL_COLOR_GREY(7f),
     GL_COLOR_GREY(c0), gl_color_black};
  for(i=0;i<num_colors;i++){
    proto[i] = make_colored_default_cube(0.01, colors[i]);
  }
  gl_drawable **drawables = xmalloc((i_max*j_max*k_max)*
                                    sizeof(gl_drawable*));
  gl_position offset = POSITION(0,0,0);
  for(i=0;i<i_max;i++){
    gl_drawable *orig = proto[i%num_colors];
    offset.x = (i-(i_max/2))*0.1;
    for(j=0;j<j_max;j++){
      offset.y = (j-(j_max/2))*0.1;
      for(k=0;k<k_max;k++){
        offset.z = (k-(k_max/2))*0.01;
        drawables[(i*j_max*k_max) + (j*k_max) + k] = copy_drawable(orig);
        mat4_translate(drawables[(i*j_max*k_max) + (j*k_max) + k]->transform,
                       offset.vec, NULL);
      }
    }
  }
  gl_check_error();
  transform_set_scene(trans, scene);
  glEnable(GL_LINE_SMOOTH);
  glDisable(GL_MULTISAMPLE);
//  glLineWidth(2.0);
  gl_drawable *axes = make_colored_axes_drawable(gl_color_cyan,
                                                 gl_color_magenta,
                                                 gl_color_yellow);
  gl_check_error();
  gl_drawable *grid = make_grid(-1.0, 1.0, -1.0, 1.0,
                                        -1.0, 1.0, grid_gen,
                                        grid_step, grid_step,
                                        grid_step, grid_color);
  gl_check_error();
  glPointSize(2.0);  
  //  SCENE_ADD_DRAWABLES(scene, axes, grid);
  scene_add_drawables(scene, drawables, i_max*j_max*k_max);
  gl_check_error();
  //  DEBUG_PRINTF(drawable_to_string(drawable));
  //  pthread_create(&ctx->thread_id, NULL, (void*)gl_main_loop_render, ctx);
  gl_main_loop(ctx);
}
