#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
thread_local global_context *thread_context;
static const char *vertex_shader =
  "#version 330 core\n"
  "layout(location = 0) in vec4 position;\n"
  "layout(location = 1) in vec4 color;\n"
  //unused but part of the default vertex attribute layout
  "layout(location = 2) in vec3 normals;\n"
  "layout(location = 3) in vec2 tex_coords;\n"
  "layout(row_major) uniform;\n"
  "layout(std140) uniform;\n"
  "out vec4 v_color;\n"
  "uniform transform {\n"
  "  mat4 model;\n"
  "  mat4 proj_view;\n"
  "};"
  "uniform vec4 u_color;\n"
  "void main(){\n"
  "  gl_Position = position * model * proj_view;\n"
  "  if(color == vec4(0.0)){;\n"
  "    v_color = u_color;\n"
  "  } else {\n"
  "    v_color = color;\n"
  "  }\n"
  "}\n";
static const char *fragment_shader =
  "#version 330 core\n"
  "in vec4 v_color;\n"
  "out vec4 f_color;\n"
  "void main(){\n"
  "  f_color = v_color;\n"
  "}\n";
struct userdata {
  float color[4];
  float alpha_x;
  float alpha_y;
  float alpha_z;
  int count;
  int rotate;
  unsigned int scene_index;
};
void update_userdata(struct userdata *data){
  if(!(++data->count % 10)){
    int i;
    for(i=0;i<4;i++){
      data->color[i] = drand48();
    }
  }
}
void update_scene(gl_scene *scene, struct userdata *data){
  if(!(data->count % 10)){
    //this is kinda lazy but oh well
    int loc = glGetUniformLocation(scene->program, "u_color");
    glUniform4fv(loc, 1, data->color);
  }
  if(!(data->count % 10)){
    int i,j;
    gl_drawable *diamond = svector_ref(&scene->VBO.drawables,1);
    diamond->sync = 1;
    for(i=0;i<diamond->num_vertices;i++){
      diamond->vertices->vertices[i].color.argb = lrand48();
    }
  }

  if(data->rotate){
    /*
      This really ought to be done by rotating around some vector, rather
      than rotating 3 times.
     */
    if(data->rotate & 1){
      data->alpha_x = fmod(data->alpha_x + (2*M_PI)/60, 2*M_PI);
    }
    if(data->rotate & 2){
      data->alpha_y = fmod(data->alpha_y + (2*M_PI)/60, 2*M_PI);
    }
    if(data->rotate & 4){
      data->alpha_z = fmod(data->alpha_z + (2*M_PI)/60, 2*M_PI);
    }
    mat4_identity(scene->projection);
    mat4_rotateX(scene->projection,
                 data->alpha_x, NULL);
    mat4_rotateY(scene->projection,
                 data->alpha_y, NULL);
    mat4_rotateZ(scene->projection,
                 data->alpha_z, NULL);
  }
/*  update_uniform_block_data(&scene->uniforms, data->transform,
                            16*sizeof(float), 16*sizeof(float));*/
}
/*
  Some hardcoded shapes to test drawing, adding shapes to scenes, etc
  independent of the drawable functions.
*/
static struct vertex shapes[] = //triangles + a Square
  {{.pos = POSITION(-0.7, -0.5,-0.3,1.0), .color = {0x7fffff00}},
   {.pos = POSITION(0.5, -0.5,-0.3,1.0), .color = {0x7f00ffff}},
   {.pos = POSITION(-0.7, 0.5,-0.3,1.0), .color = {0x7fff00ff}},
   //second triangle
   {.pos = POSITION(0.5, 0.0,0.5,1.0), .color = {0x7f00ffff}},
   {.pos = POSITION(-0.7, 0.0,0.5,1.0), .color = {0x7fff00ff}},
   {.pos = POSITION(0.5, 1.0,0.5,1.0), .color = {0x7fffff00}}};
   //square (more like a diamond
/*   {.pos = {-0.75,  -0.25, -0.1,1.0}, .color = {0xff7f7f7f}},
   {.pos = {-0.25, 0.25, -0.1,1.0}, .color = {0xffffffff}},
   {.pos = {-0.25, -0.75, -0.1,1.0}, .color = {0xf7000000}},
   {.pos = {0.25,  -0.25, -0.1,1.0}, .color = {0xff7f7f7f}}};*/
#define cube_color_1 {.argb = 0x00000000}
#define cube_color_2 {.argb = 0x00000000}
struct vertex cube[] =
  {{.pos = POSITION(-0.25, 0.25, 0.5,1.0), .color = cube_color_1},
   {.pos = POSITION(0.25, 0.25, 0.5,1.0), .color = cube_color_1},
   {.pos = POSITION(-0.25, -0.25, 0.5,1.0), .color = cube_color_1},
   {.pos = POSITION(0.25, -0.25, 0.5,1.0), .color = cube_color_1},
   {.pos = POSITION(-0.25, 0.25, -0.75,1.0), .color = cube_color_2},
   {.pos = POSITION(0.25, 0.25, -0.75,1.0), .color = cube_color_2},
   {.pos = POSITION(-0.25, -0.25, -0.75,1.0), .color = cube_color_2},
   {.pos = POSITION(0.25, -0.25, -0.75,1.0), .color = cube_color_2}};
struct vertex axes_vertices[] =
  {{.pos = POSITION(-1,0,0,1), .color = GL_COLOR_GREEN},
   {.pos = POSITION(1,0,0,1), .color = GL_COLOR_GREEN},
   {.pos = POSITION(0,-1,0,1), .color = GL_COLOR_GREEN},
   {.pos = POSITION(0,1,0,1), .color = GL_COLOR_GREEN},
   {.pos = POSITION(-1,-1,-1,1), .color = GL_COLOR_GREEN},
   {.pos = POSITION(1,1,1,1), .color = GL_COLOR_GREEN}};
/*
  Need to make sure index array length (for 8 bit indices) is a multiple of 4.
*/
static uint8_t cube_indices[18] =
  {0, 1, 2, 3, 6, 7, 4, 5, 0xff, 7, 3, 5, 1, 4, 0, 6, 2, 2};
static uint8_t shape_indices[] =
  {0, 1, 2, 0xff, 3, 4, 5};//, 0xff, 6, 7, 8, 9};
/*
  Basic input handlers, to test that input handling actually works.
  They just print the type of input recieved to stdout.
*/

void key_callback(gl_window win, int key, int scancode,
                  int action, int modmask){
  double x,y;
  glfwGetCursorPos(win, &x, &y);
  if(action == GLFW_PRESS){
    printf("key %s pressed at location (%f,%f)\n",
           gl_key_to_string(key),x,y);
  } else if(action == GLFW_RELEASE){
    printf("key %s released at location (%f,%f)\n",
           gl_key_to_string(key),x,y);
  }
  if(key == GLFW_KEY_SPACE && action == GLFW_RELEASE){
    if(thread_context->update_userdata){
      thread_context->update_userdata = NULL;
    } else {
      thread_context->update_userdata = (void*)update_userdata;
    }
  }
  struct userdata *data = thread_context->userdata;
  if(key == 'x' || key == 'X'){
    if(action == GLFW_PRESS){
      data->rotate |= 1;
    } else if(action == GLFW_RELEASE){
      data->rotate &= (~1);
    }
  }
  if(key == 'y' || key == 'Y'){
    if(action == GLFW_PRESS){
      data->rotate |= 2;
    } else if(action == GLFW_RELEASE){
      data->rotate &= (~2);
    }
  }
  if(key == 'z' || key == 'Z'){
    if(action == GLFW_PRESS){
      data->rotate |= 4;
    } else if(action == GLFW_RELEASE){
      data->rotate &= (~4);
    }
  }
  if(action == GLFW_RELEASE){
    if(key == 'r' || key == 'R'){
      mat4_identity(thread_context->scenes[data->scene_index]->projection);
      data->rotate = data->alpha_x = data->alpha_y = data->alpha_z = 0;
    }
    if(key == GLFW_KEY_RIGHT){
      thread_context->scenes[data->scene_index]->is_active = 0;
      data->scene_index = (data->scene_index + 1) % thread_context->num_scenes;
      thread_context->scenes[data->scene_index]->is_active = 1;
    }
    if(key == GLFW_KEY_LEFT){
      thread_context->scenes[data->scene_index]->is_active = 0;
      data->scene_index = (data->scene_index - 1) % thread_context->num_scenes;
      thread_context->scenes[data->scene_index]->is_active = 1;
    }
  }
}
void button_callback(gl_window win, int button, int action, int modmask,
                     double x, double y){
  if(action == GLFW_PRESS){
    printf("mouse button %d pressed at location (%f,%f)\n",
           button,x,y);
  } else if(action == GLFW_RELEASE){
    printf("mouse button %d released at location (%f,%f)\n",
           button,x,y);
  }
}
void pos_callback(gl_window win, double x, double y){
  printf("mouse moved to position (%f,%f)\n", x, y);
}
gl_color square_color(int i){
  gl_color ret = {0};
  switch(i){
    case 0:
      ret.argb=0x7fffffff;
      return ret;
    case 1:
    case 3:
      ret.argb=0x7f7f7f7f;
      return ret;
    case 2:
      ret.argb=0xf7000000;
      return ret;
    default:
      return ret;
  }
}
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "basic test");
  context_allocate_scenes(ctx, 1);
  context_allocate_userdata(ctx, sizeof(struct userdata), update_userdata);
  struct userdata *data = (struct userdata*)ctx->userdata;

  glfwSetKeyCallback(ctx->window, key_callback);
  thread_context = ctx;
  DEBUG_PRINTF("Initialized context\n");

  gl_scene *scene_1 = make_simple_scene(vertex_shader, fragment_shader);
//  gl_scene *scene_2 = make_simple_scene(vertex_shader, fragment_shader);
//  scene_2->is_active = 0;
  scene_1->update_scene = (void*)update_scene;
  ctx->scenes[0] = scene_1;
//  ctx->scenes[1] = scene_2;
//  init_uniform_block(&scene_2->uniforms, scene_2->program,
//                     "transform", sizeof(float)*36);

  DEBUG_PRINTF("Initialized uniforms\n");
  gl_drawable *shapes_obj = INIT_POLYGON(shapes, shape_indices);
  DEBUG_PRINTF("Initialized shapes\n");
  gl_drawable *cube_obj = INIT_POLYGON(cube, cube_indices);
  DEBUG_PRINTF("Initialized cube\n");
  shapes_obj->type = GL_drawable_2d_polygon;
  cube_obj->type = GL_drawable_3d_polygon;

  gl_position diamond_start = POSITION(0.0,0.5,-0.1,1);
  gl_position diamond_offset = POSITION(-0.25, -0.25,0,0);
  gl_position oct_start = POSITION(0.25,0.25,0.25,1);
  gl_drawable *oct = make_colored_convex_polygon(&oct_start, 8,
                                                 NULL, gl_color_red);
  gl_drawable *diamond = make_convex_polygon(&diamond_start, 4,
                                             NULL, square_color);
  mat4_translate(diamond->transform, (float*)&diamond_offset,NULL);

  gl_position square[4] = {POSITION(-0.3,0.3,-.2,1),
                           POSITION(0.3,0.3,-.2,1),
                           POSITION(-0.3,-0.3,-.2,1),
                           POSITION(0.3,-0.3,-.2,1)};
  gl_position triangle[3] = {POSITION(0,-0.25,0.5,1), 
                             POSITION(0.4330127, -0.25,-0.25,1),
                             POSITION(-0.4330127, -0.25, -0.25,1)};
  gl_position apex = POSITION(0,0.8660254-0.25,0,1);
  gl_position offset = POSITION(-0.1,-0.1,-0.6,1);
  gl_drawable *cube2 = make_colored_cube(square, offset,
                                         (gl_color){0x7fff00ff});
  gl_drawable *tetra = make_colored_tetrahedron(triangle, apex,
                                                gl_color_white);
  offset = POSITION_CAST(.5,.5,0);
  mat4_translate(cube2->transform, (float*)&offset, NULL);
  float trans[3] = {0.5,0.5,0.2};
  mat4_translate(cube_obj->transform, trans, NULL);

  gl_drawable *components[3] = {oct, tetra, cube2};
  gl_drawable *composite = init_composite_drawable(components, 3,
                                                   COMPOSITE_INIT_COPY);
  gl_drawable *components2[2] = {composite, diamond};
  gl_drawable *composite2 = init_composite_drawable(components2, 2,
                                                    COMPOSITE_INIT_COPY);
  
  float translate[3] = {-0.5, 0, 0};
  float translate2[3] = {0, .5, -0.25};
  float scale[3] = {1,1,2};
  mat4_rotateZ(composite->transform,
                                       M_PI/4, NULL);
  mat4_translate(composite->transform, 
                                         translate, NULL);
  mat4_scale(composite2->transform, scale, NULL);
  mat4_translate(composite2->transform,
                                         translate2, NULL);
  gl_drawable *axes = init_line_obj(axes_vertices, 6);
  set_draw_mode(axes, GL_LINES);
  glEnable(GL_LINE_SMOOTH);
  gl_check_error();
  DEBUG_PRINTF("Initialized shape\n");
//  oct->sync = tetra->sync = 1;
  gl_drawable *drawables[] = {oct, diamond, shapes_obj, cube_obj,
                              tetra, cube2, composite, composite2, axes};
  //  gl_drawable *drawables_2[] = {copy_gl_drawable(cube_obj),
  //                                copy_gl_drawable(cube2),
  //                                copy_gl_drawable(tetra)};
  scene_add_drawables(scene_1, drawables, 9);
  //  scene_add_drawables(scene_1, axis, 3);
  //  scene_add_drawables(scene_2, drawables_2, 3);
  DEBUG_PRINTF("Starting main loop\n");
  gl_check_error();
  gl_main_loop(ctx);
  return 0;
}
