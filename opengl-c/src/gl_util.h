#ifndef __GL_UTIL_H__
#define __GL_UTIL_H__
#ifndef __MY_GL_TYPES_H__
#error "gl_types.h must be included before gl_util.h\n" \
  "You should probably just include common.h instead\n"
#endif
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
#include "inline_position_functs.h"
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include "stb_image.h"
/*
  Macros
*/
//needed to make the GL_GET macro work
typedef GLint GLInteger;
typedef GLint64 GLInteger64;
typedef GLfloat GLFloat;
typedef GLboolean GLBoolean;
typedef GLdouble GLDouble;

//Values for some open gl context attributes
//I might move these someplace else eventually
#define GL_UTIL_CONTEXT_VERSION_MAJOR 3
#define GL_UTIL_CONTEXT_VERSION_MINOR 3
#define GL_UTIL_MULTISAMPLE 4

#define GL_GET1(what)                            \
  GL_GET2(what, Integer)
#define GL_GET2(what, type)                      \
  ({CAT(GL,type) tmp;                           \
    CAT3(glGet, type,v)(what, &tmp);            \
    tmp;})
#define GL_GET_I1(what, idx)                     \
  GL_GET_I2(what, idx, Integer)
#define GL_GET_I2(what, idx, type)               \
  ({CAT(GL,type) tmp;                           \
    CAT3(glGet, type,i_v)(what, idx,&tmp);      \
    tmp;})
#define GL_GET(...) VFUNC(GL_GET, __VA_ARGS__)
#define GL_GET_I(...) VFUNC(GL_GET_I, __VA_ARGS__)
static const float const_id_matrix[16] = {[0] = 1, [5] = 1, [10] = 1, [15] = 1};
//make programs bind the same as every other kind of object
#define glBindProgram(p) glUseProgram(p)
#define rand_pos()                              \
  ({gl_position pos;                            \
    pos.x = (2*drand48()) - 1;                  \
    pos.y = (2*drand48()) - 1;                  \
    pos.z = (2*drand48()) - 1;                  \
    pos.w = 1;                                  \
    pos;})
//this name makes more sense (maybe change to GL_ARRAY_INDEX_BUFFER)
#define GL_INDEX_BUFER GL_ELEMENT_ARRAY_BUFFER
#ifdef HAVE_C11
//not in tgmath.h
#define sincos(x,s,c)                           \
  _Generic((x),                                 \
           float   : sincosf,                   \
           default : sincos)(x,s,c)
//position for shapes to possibly be added later
#define distance(x,y)                           \
  _Generic((x),                                 \
           float : hypotf,                      \
           double : hypot,                      \
           gl_position : position_distance)(x,y)
#endif
//takes 2 variables, not pointers
#define get_cursor_pos(x,y)                     \
  glfwGetCursorPos(thread_context->window, &x, &y);
//returns a gl_position
#define get_cursor_pos_world()                          \
  ({double x,y;                                         \
    glfwGetCursorPos(thread_context->window, &x, &y);   \
    screen_coords_to_world(x,y);})
#define screen_coords_to_world(sx,sy)                   \
  ({int w, h;                                           \
    gl_position ret;                                    \
    glfwGetWindowSize(thread_context->window, &w, &h);  \
    ret.x = (2*(sx/w))-1;                                       \
    ret.y = 1-(2*(sy/h));                                       \
    ret.z = 0, ret.w = 1;                                       \
    ret;})
#define set_identity(arr)                       \
  arr[0] = arr[5] = arr[10] = arr[15] = 1.0f;
/*
  These next two macros allow writing code using the variable sized index
  type eaiser, but at the cost of speed.
*/
#define get_index(inds, ind)                    \
  ({uint32_t val;                               \
    if(inds->index_size == 1){                  \
      val = ((uint8_t*)inds->indices)[ind];     \
    } else if(inds->index_size == 2){           \
      val = ((uint16_t*)inds->indices)[ind];    \
    } else {                                    \
      val = ((uint32_t*)inds->indices)[ind];    \
    }                                           \
    val;})
#define indices_to_uint32(inds)                              \
  ({uint32_t *ret = (uint32_t*)inds->indices;                 \
    int i;                                                      \
    if(inds->index_size < 4){                                \
      ret = alloca(inds->num_indices*sizeof(uint32_t));      \
      if(inds->index_size == 2){                             \
        uint16_t *temp = (uint16_t*)inds->indices;           \
        for(i=0;i<inds->num_indices;i++){                    \
          ret[i] = temp[i];                                     \
        }                                                       \
      } else {                                                  \
        uint8_t *temp = (uint8_t*)inds->indices;             \
        for(i=0;i<inds->num_indices;i++){                    \
          ret[i] = temp[i];                                     \
        }                                                       \
      }                                                         \
    }                                                           \
    ret;})
/*
  Basic idea of this is to create a program struct, attach an arbitary
  number of shaders of various types, then compile/link the program, which
  destroys the program struct and returns a program id for the linked program.
*/
typedef struct gl_program gl_program;
struct gl_texture {
  void *data;//can reasonably be null
  char *name;
  GLuint id;//value from glGenTextures
  GLenum target;//GL_TEXTURE_...
  int idx;//texture unit index (usually just 0)
  int w;//width
  int h;//height
  int d;//depth
  int levels;//mipmap levels
  int comp;//number of components in each pixel
  int filter_min;
  int filter_max;
  int wrap_s;
  int wrap_t;
  int wrap_r;
  int refcount;
};
/*
  get the format of a texture based on the number of components per pixel, only
  r,rg,rgb and rgba formats with 8 bits per component are supported.
*/
#define TEXTURE_FORMAT_SIZED(tex) sized_format_by_components[tex->comp-1]
#define TEXTURE_FORMAT(tex) format_by_components[tex->comp-1]
static GLenum sized_format_by_components[4] = {GL_R8, GL_RG8, GL_RGB8, GL_RGBA8};
static GLenum format_by_components[4] = {GL_RED, GL_RG, GL_RGB, GL_RGBA};


int __attribute__((const)) gl_type_size(GLenum type);
/*
  functions for creating glsl programs, code in gl_program.c
*/
/*
  gl_program is an opaque type, shaders can be added to it via 
  program_attach_shader, and linked into a program via link_program. Calling
  link program, will also destroy the program object.
*/
gl_program *make_gl_program(void);
void program_attach_shader(gl_program *prog, const char *shader, GLenum type);
GLuint link_program(gl_program *prog);
/*
  Arguments need to be null terminated strings.
  Could be eaisly modified to take more arguments.
*/
GLuint create_shader_program(const char *vertex_shader_source,
                             const char *fragment_shader_source);
/*
  Clear the screen, call draw, swap buffers and then poll events
*/
//create a buffer on the gpu and bind the given data to it
GLuint make_data_buffer(GLenum buffer_type, void *data,
                        size_t size, int usage);
//in glfw.c
gl_window init_gl_context(int w, int h, const char* name);

mat4_t randomize_transform(mat4_t trans);
/*
  If you need more than one VAO it's probably better to explicitly
  call glGenVertexArray.
*/
static inline GLuint make_VAO(void){
  GLuint VAO;
  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);
  return VAO;
}
#define make_array_buffer(data,size,usage)              \
  make_data_buffer(GL_ARRAY_BUFFER, data, size, usage)
#define make_index_buffer(data,size,usage)                      \
  make_data_buffer(GL_ELEMENT_ARRAY_BUFFER, data, size, usage)
/*
  Binds the data/vertex layout for vertex attribute loc to the
  current VAO
*/
void enable_vertex_attribs(GLuint buf, gl_vertex_attrib *attribs, int n);
/*
  Enables a single vertex attribute on for the currently bound VAO/VBO
*/
static inline void enable_vertex_attrib(gl_vertex_attrib attrib){
  glEnableVertexAttribArray(attrib.location);
  glVertexAttribPointer(attrib.location, attrib.size, attrib.type,
                        attrib.normalized, attrib.stride,
                        (void*)attrib.offset);
}
/*
  Generate a texture from a raw bitmap, image file in memory,
  or image file on disk, respectively.
*/
gl_texture *gen_texture_from_bitmap(uint8_t *data, int w, int h,
                                    int l, int comp);
gl_texture *gen_texture_from_image(uint8_t *data, int size, int level);
gl_texture *gen_texture_from_file(const char *file, int level);
/*
  Move to texture file, if/when that gets created

  Creates a wxh 2d image contaning a black and white checkerboard pattern.
  most boxes in the checkerboard are box_w x box_h, with the exception of
  those on the bottom and right edges, where they may be smaller.
*/
unsigned int *gen_checkerboard_image(int w, int h, int box_w, int box_h);
//Rather than calling exit it's more useful to raise a sigtrap since
//that will be caught by a debugger
#define gl_error_exit(code) (code == GL_NO_ERROR ? (void)0 :    \
                             raise(SIGTRAP))
#define gl_check_error()                                        \
  ({GLenum  error = glGetError();                               \
    GLenum  err_code = error;                                   \
    if(error != GL_NO_ERROR){                                   \
      fprintf(stderr, "OpenGL error(s) in %s at %s:%d:\n",      \
              __func__,__FILE__,__LINE__);                      \
      do {                                                      \
        fprintf(stderr, "\t%s\n", gl_strerror(error));          \
      } while((error = glGetError()) != GL_NO_ERROR);           \
      print_backtrace();                                        \
    }                                                           \
    gl_error_exit(err_code);})                                  \
  //these implicitly call gl_check_error
#define gl_print_msg(msg, args...)              \
  ({fprintf(stderr, msg, ##args);               \
    gl_check_error();})
#define gl_print_location()                     \
  ({fprintf(stderr, "%s [%s:%d]\n",             \
            __func__,__FILE__,__LINE__);        \
    gl_check_error();})
/*
  Keyboard input utility functions and macros
*/
#define GLFW_KEY_SHIFT_L GLFW_KEY_LEFT_SHIFT
#define GLFW_KEY_CTRL_L GLFW_KEY_LEFT_CONTROL
#define GLFW_KEY_ALT_L GLFW_KEY_LEFT_ALT
#define GLFW_KEY_SUPER_L GLFW_KEY_LEFT_SUPER
#define GLFW_KEY_SHIFT_R GLFW_KEY_RIGHT_SHIFT
#define GLFW_KEY_CTRL_R GLFW_KEY_RIGHT_CONTROL
#define GLFW_KEY_ALT_R GLFW_KEY_RIGHT_ALT
#define GLFW_KEY_SUPER_R GLFW_KEY_RIGHT_SUPER
//this could do (key >= GLFW_KEY_RIGHT && key<= GLFW_KEY_UP)
//but that's dependent on the ordering of the key values
//which admittedly shouldn't change
#define IS_ARROW_KEY(key)                       \
  (key == GL_KEY_RIGHT || key == GL_KEY_LEFT || \
   key == GL_KEY_UP || key == GL_KEY_DOWN)
/*
  Get a printable name for a keyboard key, returns one of 3 values,
  1 means the key can be represented by a single character stored in c,
  2 means the key can be represented by a string stored in s,
  3 means the key should be printed numerically.
*/
int gl_get_keyname(int key, char *c, char **s);
gl_color gl_rand_color(int __attribute__((unused)) dummy);
/*
  this could just be: return f * (1<<bitdepth),
  but there's no harm in optimizing common cases,
  even if it's probably unecessary
*/
static inline int float_to_normalized_int(float f, int bitdepth){
  switch(bitdepth){
    case 7: return f*0x7f; //signed 8 bit integer
    case 8: return f*0xff;
    //Normals use 10 bit integers
    case 9: return f*0x1ff;//signed 10 bit integer
    case 10: return f*0x3ff;
    case 15: return f*0x7fff;
    case 16: return f*0xffff;
    case 31: return f*0x7fffffff;//signed 32 bit integer
    case 32: return f*0xffffffff;
    default:
      return f * ((1<<bitdepth)-1);
  }
}
/*
  Bitdepth is the number of bits used in the integer, including the sign
*/
static inline float normalized_int_to_float(unsigned int i,
                                            int bitdepth, int is_signed){
  if(is_signed){
    //this ensures -(2^(bitdepth-1)) correctly resolves to -1
    return MAX((float)i/((1<<(bitdepth-1))-1),-1.0f);
  } else {
    return ((unsigned int)i)/((1<<bitdepth)-1);
  }
}
static inline gl_tex_coord make_gl_tex_coord(float s, float t){
  gl_tex_coord st;
  st.s = float_to_normalized_int(s, 16);
  st.t = float_to_normalized_int(t, 16);
  return st;
}
/*
  Attempts to convert position 'pos' into a relatively equilvent texture 
  coordinate.
  to get s,t from x,y add 1, divide by 2 and convert to normalized integer
*/
static inline gl_tex_coord gl_position_to_tex_coord(gl_position pos){
  gl_tex_coord st;
  st.s = ((pos.x + 1)/2.0)*0xff;
  st.t = ((pos.y + 1)/2.0)*0xff;
  return st;
}
  
static inline gl_normal gl_position_to_normal(gl_position pos){
  gl_normal ret = {0};
  //assume that the position itself needs to be normalized
  gl_position n_pos = position_normalize(pos);
  //this is just converting a float into a normalized signed 10 bit integer
  ret.nx = n_pos.x * 0x1ff;
  ret.ny = n_pos.y * 0x1ff;
  ret.nz = n_pos.z * 0x1ff;
  ret.tag = 0;
  return ret;
}
static inline gl_position gl_normal_to_position(gl_normal norm){
  gl_position ret = {0};
  //assume that the normals are...normalized
  ret.x = MAX(((float)norm.nx/0x1ff),-1.0f);
  ret.y = MAX(((float)norm.ny/0x1ff),-1.0f);
  ret.z = MAX(((float)norm.nz/0x1ff),-1.0f);
  ret.w = 1;
  return ret;
}
static inline gl_color gl_colorf_to_color(gl_colorf cf){
  gl_color c;
  //f * 0xff converts f into a normalized 8 bit integer
  c.b = cf.b*0xff;
  c.g = cf.b*0xff;
  c.r = cf.r*0xff;
  c.a = cf.a*0xff;
  return c;
}
static inline gl_colorf gl_color_to_colorf(gl_color c){
  gl_colorf cf;
  //f * 0xff converts f into a normalized 8 bit integer
  cf.b = (float)c.b/0xff;
  cf.g = (float)c.g/0xff;
  cf.r = (float)c.g/0xff;
  cf.a = (float)c.a/0xff;
  return cf;
}
static inline void enable_primitive_restart(int index_size){
  if(GLEW_ARB_ES3_compatibility){
      glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  } else {
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex((1<<(index_size*CHAR_BIT))-1);
  }
}
static inline void disable_primitive_restart(){
  if(GLEW_ARB_ES3_compatibility){
    glDisable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  } else {
    glDisable(GL_PRIMITIVE_RESTART);
  }
}
/*
  Info about drawing modes (1st argument to glDrawArrays)
  GL_LINES, every set of 2 vertices specifies a line
  GL_LINE_STRIP, a line is drawn between each adjecent vertex,
  i.e 1->2->3->4, vs 1->2,3->4 for GL_LINE
  GL_LINE_LOOP, same as GL_LINE_STRIP except an extra line is
  drawn between the 1st and last point (i.e 4->1->2->3->4)

  GL_TRIANGLES, every set of 3 vertices specifies a triangle
  GL_TRIANGLE_STRIP, every set of 3 adjecent vertices specifies a triangle,
  i.e ((1,2,3),(2,3,4),(3,4,5),(4,5,6)) vs ((1,2,3),(4,5,6)) for TRIANGLES
  GL_TRIANGLE_FAN, the first vertex is fixed and a triangle is drawn
  with the first vertex and each set of 2 adjecent vertices,
  i.e ((1,2,3),(1,3,4),(1,4,5),(1,5,6))

  GL_PATCH, uses user specified tessalation shader

  A notable function is glPrimitiveRestartIndex, which works with
  indexed drawing functions, it sets a sentinel value which when
  encounteded in the index array restarts the set of vertices being
  used with the next vertex.

  It's probably best to use glDrawELementsxs
*/
#ifdef __cplusplus
}
#endif
#endif
