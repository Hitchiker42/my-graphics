#include "common.h"
#include "interaction.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
thread_local global_context *thread_context;
static gl_keystate transform_keybindings[25] =
  {{.keycode = GLFW_KEY_RIGHT},{.keycode = GLFW_KEY_LEFT},//translate x
   {.keycode = GLFW_KEY_UP},{.keycode = GLFW_KEY_DOWN},//translate y
   {.keycode = GLFW_KEY_UP, .modmask = GLFW_MOD_SHIFT},
   {.keycode = GLFW_KEY_DOWN, .modmask = GLFW_MOD_SHIFT}, //translate z
   //rotate
   {.keycode = 'x'}, {.keycode = 'x', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'y'}, {.keycode = 'y', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'z'}, {.keycode = 'z', .modmask = GLFW_MOD_CONTROL},
   //scale width, height, depth
   {.keycode = 'w'}, {.keycode = 'w', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'h'}, {.keycode = 'h', .modmask = GLFW_MOD_CONTROL},
   {.keycode = 'd'}, {.keycode = 'd', .modmask = GLFW_MOD_CONTROL},
   //shear ',', '.', '/' because why not
   {.keycode = ','}, {.keycode = ',', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '.'}, {.keycode = '.', .modmask = GLFW_MOD_CONTROL},
   {.keycode = '/'}, {.keycode = '/', .modmask = GLFW_MOD_CONTROL},
   //reset
   {.keycode = 'r'}};
/*
  Numpad: (n=numlock, r=return)
  n/*-
  789+
  456+
  123r
  00.r
*/
/* static gl_keystate camera_keybindings[19] = */
/*   {//no keys bound for panning */
/*     {0},{0},{0},{0},{0},{0}, */
/*     {.keycode = GLFW_KEY_KP_6}, {.keycode = GLFW_KEY_KP_4}, */
/*     {.keycode = GLFW_KEY_KP_8}, {.keycode = GLFW_KEY_KP_2}, */
/*     {.keycode = GLFW_KEY_KP_8, .modmask = GLFW_MOD_SHIFT}, */
/*     {.keycode = GLFW_KEY_KP_2, .modmask = GLFW_MOD_SHIFT}, */
/*     //rotate */
/*     {.keycode = GLFW_KEY_KP_9}, {.keycode = GLFW_KEY_KP_7}, */
/*     {.keycode = GLFW_KEY_KP_1}, {.keycode = GLFW_KEY_KP_3}, */
/*     //zoom */
/*     {.keycode = GLFW_KEY_KP_ADD}, {.keycode = GLFW_KEY_KP_SUBTRACT}, */
/*     //reset */
/*     {.keycode = GLFW_KEY_KP_0}}; */
static gl_keystate camera_keybindings[19] =
  {//no keys bound for panning
    {0},{0},{0},{0},{0},{0},
    {.keycode = GLFW_KEY_6}, {.keycode = GLFW_KEY_4},
    {.keycode = GLFW_KEY_8}, {.keycode = GLFW_KEY_2},
    {.keycode = GLFW_KEY_8, .modmask = GLFW_MOD_CONTROL},
    {.keycode = GLFW_KEY_2, .modmask = GLFW_MOD_CONTROL},
    //rotate
    {.keycode = GLFW_KEY_9}, {.keycode = GLFW_KEY_7},
    {.keycode = GLFW_KEY_1}, {.keycode = GLFW_KEY_3},
    //zoom
    {.keycode = '+'}, {.keycode = '-'},
    //reset
    {.keycode = GLFW_KEY_0}};
gl_color gradient(int i){
  //this is cheating a bit, since it's using info from make_sphere
  const int step = 10*M_PI;
  double max = 5*step;
  int intensity = (i/max) * 255;
  gl_color ret;
  ret.g = ret.r = ret.b = intensity;
  ret.a = 0xff;
  return ret;
}
static const int grid_step = 20;
gl_position grid_gen(float x, float y, float z){
  float d = z/20.0;
  gl_position ret = POSITION(x+d,y+d,z);
  return ret;
}
gl_color grid_color(int i){
  int z = i/(grid_step*grid_step);
  int y = (i%(grid_step*grid_step))/grid_step;
  int x = (i%(grid_step*grid_step))%grid_step;
  gl_color ret = {.r = z*(0xff/grid_step),
                  .g = y*(0xff/grid_step),
                  .b = x*(0xff/grid_step), .a = 0xff};

  return ret;
}
static gl_drawable *init_drawable(){
  gl_shape *x = AS_SHAPE(make_sphere(0.5, gradient));
//  gl_position trans = POSITION(0,0,-.75);
  return AS_DRAWABLE(x);
}
void update_scene(gl_scene *scene, void *arg){
  void **data = arg;
  interactive_transform *trans = data[0];
  interactive_camera *cam = data[1];
  interactive_transform_update(trans);
  interactive_camera_update(cam);
}
int main(){
  global_context *ctx;
  ctx = make_global_context(800, 800, "simple test");
  gl_scene *scene = make_simple_scene(basic_vertex, basic_fragment);
  glfwSetKeyCallback(ctx->window, default_keypress_callback);
  window_set_keymap(ctx->window, default_keymap_ptr);
  interactive_transform *trans = 
    make_interactive_transform(0.05, M_PI/60, 0.05, 0.05, 
                               transform_keybindings, default_keymap_ptr);
  gl_camera *cam = make_gl_camera_perspective(POSITION_CAST(0,0,1),
                                              POSITION_CAST(0,0,0),
                                              POSITION_CAST(0,1,0),
                                              135, 1, 0.2, 10);
  scene_bind_camera(scene, cam);
  //scene_bind_camera(scene, &default_camera);
  interactive_camera *icam = make_interactive_camera(0.05,0.05, M_PI/60, 0.05,
                                                     //&default_camera,
                                                     cam,
                                                     camera_keybindings,
                                                     default_keymap_ptr);
  interactive_camera_set_scene(icam, scene);
  int i;
  for(i=0;i<25;i++){    
    gl_keystate k = transform_keybindings[i];
    int code = KEY_TO_KEYMAP_INDEX(k.keycode);
    int idx = MODIFIER_TO_INDEX(k.modmask);
    //    DEBUG_PRINTF("Verifying keystate: %s", keystate_to_string(k));
    assert(trans->map[code].closures[idx].callback);
  }
  ctx->userdata = xmalloc(2*sizeof(void*));
  ((void**)ctx->userdata)[0] = trans;
  ((void**)ctx->userdata)[1] = icam;
  context_allocate_scenes(ctx, 1);
  thread_context = ctx;

  //These 2 transformations are the default
  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
  //  mat4_ortho(-1,1,-1,1,-1,1,scene->projection);

  //  mat4_look_at(pos_origin.vec, pos_z_min.vec, pos_y_max.vec, scene->view);
//  mat4_perspective(135.0, 1, 0.25, 100, scene->projection);

  scene->update_scene = (void*)update_scene;
  ctx->scenes[0] = scene;;
  gl_drawable *drawable;
  drawable = init_drawable();
  transform_set_drawable(trans, drawable);
  //  interactive_transform_bind_keys(trans, transform_keybindings, NULL);
  glClearColor(1,0,0,1);
  glEnable(GL_LINE_SMOOTH);
//  glLineWidth(2.0);
  gl_drawable *axes = make_colored_axes_drawable(gl_color_cyan,
                                                 gl_color_magenta,
                                                 gl_color_yellow);
  gl_drawable *grid = make_grid(-1.0, 1.0, -1.0, 1.0,
                                        -1.0, 1.0, grid_gen,
                                        grid_step, grid_step,
                                        grid_step, grid_color);
  glPointSize(2.0);
  SCENE_ADD_DRAWABLES(scene, drawable, axes, grid);
//  DEBUG_PRINTF(drawable_to_string(drawable));
/*  for(i=0;i<25;i++){    
    gl_keystate k = transform_keybindings[i];
    int code = KEY_TO_KEYMAP_INDEX(k.keycode);
    int idx = MODIFIER_TO_INDEX(k.modmask);
    //    DEBUG_PRINTF("Verifying keystate: %s", keystate_to_string(k));
    assert(trans->map[code].closures[idx].callback);
    }*/
  gl_main_loop(ctx);
}
