#ifndef __WM_GLFW_H__
#define __WM_GLFW_H__
#ifdef __cplusplus
extern "C" {
#endif
static inline void gl_poll_events(void *arg){
  glfwPollEvents();
}
static inline void gl_wait_events(void *arg){
  glfwWaitEvents();
}
static inline void gl_swap_buffers(gl_window window){
  glfwSwapBuffers(window);
}
#ifdef __cplusplus
}
#endif
#endif /* __WM_GLFW_H__ */
