typedef struct queue queue;
typedef struct queue_entry qentry;
struct queue {
  qentry *head;
};
struct queue_entry {
  void *data;
  size_t sz;
  qentry *next;
};
void queue_push(queue *q, void *data, size_t sz){
  qentry *entry = make_queue_entry(data, sz, q->head);
 retry:
  if(atomic_compare_exchange(&(q->head), &(entry->next), entry)){
    return;
  } else {
    goto retry;
  }
}  
