/* Possible groups to define:

__GL_VERTEX_HINTS_MASK_PGI_TO_STRING__ (defines gl_vertex_hints_mask_PGI_to_string)
__GL_MATRIX_MODE_TO_STRING__ (defines gl_matrix_mode_to_string)
__GL_TEXTURE_PARAMETER_NAME_TO_STRING__ (defines gl_texture_parameter_name_to_string)
__GL_TEXTURE_MAG_FILTER_TO_STRING__ (defines gl_texture_mag_filter_to_string)
__GL_GET_PNAME_TO_STRING__ (defines gl_get_PName_to_string)
__GL_PIXEL_MAP_TO_STRING__ (defines gl_pixel_map_to_string)
__GL_MAP_TARGET_TO_STRING__ (defines gl_map_target_to_string)
__GL_LIGHT_ENV_MODE_SGIX_TO_STRING__ (defines gl_light_env_mode_SGIX_to_string)
__GL_COLOR_MATERIAL_FACE_TO_STRING__ (defines gl_color_material_face_to_string)
__GL_CONVOLUTION_PARAMETER_EXT_TO_STRING__ (defines gl_convolution_parameter_EXT_to_string)
__GL_OCCLUSION_QUERY_EVENT_MASK_AMD_TO_STRING__ (defines gl_occlusion_query_event_mask_AMD_to_string)
__GL_GET_PIXEL_MAP_TO_STRING__ (defines gl_get_pixel_map_to_string)
__GL_RENDERING_MODE_TO_STRING__ (defines gl_rendering_mode_to_string)
__GL_PATH_RENDERING_MASK_NV_TO_STRING__ (defines gl_path_rendering_mask_NV_to_string)
__GL_CONTAINER_TYPE_TO_STRING__ (defines gl_container_type_to_string)
__GL_INTERLEAVED_ARRAY_FORMAT_TO_STRING__ (defines gl_interleaved_array_format_to_string)
__GL_LIST_MODE_TO_STRING__ (defines gl_list_mode_to_string)
__GL_TEX_COORD_POINTER_TYPE_TO_STRING__ (defines gl_tex_coord_pointer_type_to_string)
__GL_STRING_NAME_TO_STRING__ (defines gl_string_name_to_string)
__GL_COMMAND_OPCODES_NV_TO_STRING__ (defines gl_command_opcodes_NV_to_string)
__GL_PIXEL_STORE_SUBSAMPLE_RATE_TO_STRING__ (defines gl_pixel_store_subsample_rate_to_string)
__GL_GET_TEXTURE_PARAMETER_TO_STRING__ (defines gl_get_texture_parameter_to_string)
__GL_CLIENT_ATTRIB_MASK_TO_STRING__ (defines gl_client_attrib_mask_to_string)
__GL_BLENDING_FACTOR_SRC_TO_STRING__ (defines gl_blending_factor_src_to_string)
__GL_SHADER_TYPE_TO_STRING__ (defines gl_shader_type_to_string)
__GL_GET_POINTERV_PNAME_TO_STRING__ (defines gl_get_pointerv_PName_to_string)
__GL_POLYGON_MODE_TO_STRING__ (defines gl_polygon_mode_to_string)
__GL_GET_MINMAX_PARAMETER_PNAME_EXT_TO_STRING__ (defines gl_get_minmax_parameter_PName_EXT_to_string)
__GL_BUFFER_BIT_QCOM_TO_STRING__ (defines gl_buffer_bit_QCOM_to_string)
__GL_MAP_BUFFER_USAGE_MASK_TO_STRING__ (defines gl_map_buffer_usage_mask_to_string)
__GL_TRIANGLE_LIST_SUN_TO_STRING__ (defines gl_triangle_list_SUN_to_string)
__GL_NORMAL_POINTER_TYPE_TO_STRING__ (defines gl_normal_pointer_type_to_string)
__GL_TEXTURE_ENV_MODE_TO_STRING__ (defines gl_texture_env_mode_to_string)
__GL_PIXEL_STORE_RESAMPLE_MODE_TO_STRING__ (defines gl_pixel_store_resample_mode_to_string)
__GL_PATH_RENDERING_TOKEN_NV_TO_STRING__ (defines gl_path_rendering_token_NV_to_string)
__GL_MESH_MODE2_TO_STRING__ (defines gl_mesh_mode2_to_string)
__GL_GET_MAP_QUERY_TO_STRING__ (defines gl_get_map_query_to_string)
__GL_BLENDING_FACTOR_DEST_TO_STRING__ (defines gl_blending_factor_dest_to_string)
__GL_GET_HISTOGRAM_PARAMETER_PNAME_EXT_TO_STRING__ (defines gl_get_histogram_parameter_PName_EXT_to_string)
__GL_MATERIAL_FACE_TO_STRING__ (defines gl_material_face_to_string)
__GL_FRAGMENT_SHADER_DEST_MOD_MASK_ATI_TO_STRING__ (defines gl_fragment_shader_dest_mod_mask_ATI_to_string)
__GL_INTERNAL_FORMAT_TO_STRING__ (defines gl_internal_format_to_string)
__GL_DEPTH_FUNCTION_TO_STRING__ (defines gl_depth_function_to_string)
__GL_FFD_TARGET_SGIX_TO_STRING__ (defines gl_ffd_target_SGIX_to_string)
__GL_PIXEL_TEX_GEN_MODE_TO_STRING__ (defines gl_pixel_tex_gen_mode_to_string)
__GL_BLEND_EQUATION_MODE_EXT_TO_STRING__ (defines gl_blend_equation_mode_EXT_to_string)
__GL_TEXTURE_STORAGE_MASK_AMD_TO_STRING__ (defines gl_texture_storage_mask_AMD_to_string)
__GL_COLOR_MATERIAL_PARAMETER_TO_STRING__ (defines gl_color_material_parameter_to_string)
__GL_CONTEXT_FLAG_MASK_TO_STRING__ (defines gl_context_flag_mask_to_string)
__GL_LIST_NAME_TYPE_TO_STRING__ (defines gl_list_name_type_to_string)
__GL_PIXEL_FORMAT_TO_STRING__ (defines gl_pixel_format_to_string)
__GL_PRIMITIVE_TYPE_TO_STRING__ (defines gl_primitive_type_to_string)
__GL_LIGHT_NAME_TO_STRING__ (defines gl_light_name_to_string)
__GL_PIXEL_COPY_TYPE_TO_STRING__ (defines gl_pixel_copy_type_to_string)
__GL_FOG_POINTER_TYPE_EXT_TO_STRING__ (defines gl_fog_pointer_type_EXT_to_string)
__GL_GET_CONVOLUTION_PARAMETER_TO_STRING__ (defines gl_get_convolution_parameter_to_string)
__GL_MINMAX_TARGET_EXT_TO_STRING__ (defines gl_minmax_target_EXT_to_string)
__GL_FRAGMENT_SHADER_DEST_MASK_ATI_TO_STRING__ (defines gl_fragment_shader_dest_mask_ATI_to_string)
__GL_FFD_MASK_SGIX_TO_STRING__ (defines gl_ffd_mask_SGIX_to_string)
__GL_DATA_TYPE_TO_STRING__ (defines gl_data_type_to_string)
__GL_ACCUM_OP_TO_STRING__ (defines gl_accum_op_to_string)
__GL_READ_BUFFER_MODE_TO_STRING__ (defines gl_read_buffer_mode_to_string)
__GL_MESH_MODE1_TO_STRING__ (defines gl_mesh_mode1_to_string)
__GL_HINT_MODE_TO_STRING__ (defines gl_hint_mode_to_string)
__GL_LIGHT_ENV_PARAMETER_SGIX_TO_STRING__ (defines gl_light_env_parameter_SGIX_to_string)
__GL_TRACE_MASK_MESA_TO_STRING__ (defines gl_trace_mask_MESA_to_string)
__GL_CLEAR_BUFFER_MASK_TO_STRING__ (defines gl_clear_buffer_mask_to_string)
__GL_BOOLEAN_TO_STRING__ (defines gl_boolean_to_string)
__GL_TEXTURE_COORD_NAME_TO_STRING__ (defines gl_texture_coord_name_to_string)
__GL_STENCIL_OP_TO_STRING__ (defines gl_stencil_op_to_string)
__GL_TEXTURE_MIN_FILTER_TO_STRING__ (defines gl_texture_min_filter_to_string)
__GL_SYNC_OBJECT_MASK_TO_STRING__ (defines gl_sync_object_mask_to_string)
__GL_COLOR_TABLE_TARGET_SGI_TO_STRING__ (defines gl_color_table_target_SGI_to_string)
__GL_LOGIC_OP_TO_STRING__ (defines gl_logic_op_to_string)
__GL_INDEX_POINTER_TYPE_TO_STRING__ (defines gl_index_pointer_type_to_string)
__GL_PIXEL_TRANSFER_PARAMETER_TO_STRING__ (defines gl_pixel_transfer_parameter_to_string)
__GL_SPECIAL_NUMBERS_TO_STRING__ (defines gl_special_numbers_to_string)
__GL_CONVOLUTION_TARGET_EXT_TO_STRING__ (defines gl_convolution_target_EXT_to_string)
__GL_ATTRIB_MASK_TO_STRING__ (defines gl_attrib_mask_to_string)
__GL_TEXTURE_TARGET_TO_STRING__ (defines gl_texture_target_to_string)
__GL_LIGHT_PARAMETER_TO_STRING__ (defines gl_light_parameter_to_string)
__GL_LIGHT_MODEL_COLOR_CONTROL_TO_STRING__ (defines gl_light_model_color_control_to_string)
__GL_SAMPLE_PATTERN_SGIS_TO_STRING__ (defines gl_sample_pattern_SGIS_to_string)
__GL_CULL_FACE_MODE_TO_STRING__ (defines gl_cull_face_mode_to_string)
__GL_TRANSFORM_FEEDBACK_TOKEN_NV_TO_STRING__ (defines gl_transform_feedback_token_NV_to_string)
__GL_DRAW_BUFFER_MODE_TO_STRING__ (defines gl_draw_buffer_mode_to_string)
__GL_POINT_PARAMETER_NAME_SGIS_TO_STRING__ (defines gl_point_parameter_name_SGIS_to_string)
__GL_TEXTURE_ENV_TARGET_TO_STRING__ (defines gl_texture_env_target_to_string)
__GL_CLIP_PLANE_NAME_TO_STRING__ (defines gl_clip_plane_name_to_string)
__GL_FOG_MODE_TO_STRING__ (defines gl_fog_mode_to_string)
__GL_USE_PROGRAM_STAGE_MASK_TO_STRING__ (defines gl_use_program_stage_mask_to_string)
__GL_FOG_POINTER_TYPE_IBM_TO_STRING__ (defines gl_fog_pointer_type_IBM_to_string)
__GL_FOG_PARAMETER_TO_STRING__ (defines gl_fog_parameter_to_string)
__GL_COLOR_POINTER_TYPE_TO_STRING__ (defines gl_color_pointer_type_to_string)
__GL_SEPARABLE_TARGET_EXT_TO_STRING__ (defines gl_separable_target_EXT_to_string)
__GL_PIXEL_TYPE_TO_STRING__ (defines gl_pixel_type_to_string)
__GL_PIXEL_STORE_PARAMETER_TO_STRING__ (defines gl_pixel_store_parameter_to_string)
__GL_TEXTURE_ENV_PARAMETER_TO_STRING__ (defines gl_texture_env_parameter_to_string)
__GL_ATTRIBUTE_TYPE_TO_STRING__ (defines gl_attribute_type_to_string)
__GL_CONVOLUTION_BORDER_MODE_EXT_TO_STRING__ (defines gl_convolution_border_mode_EXT_to_string)
__GL_FOG_COORDINATE_POINTER_TYPE_TO_STRING__ (defines gl_fog_coordinate_pointer_type_to_string)
__GL_REGISTER_COMBINER_PNAME_TO_STRING__ (defines gl_register_combiner_pname_to_string)
__GL_TEXTURE_GEN_PARAMETER_TO_STRING__ (defines gl_texture_gen_parameter_to_string)
__GL_LIGHT_MODEL_PARAMETER_TO_STRING__ (defines gl_light_model_parameter_to_string)
__GL_ERROR_CODE_TO_STRING__ (defines gl_error_code_to_string)
__GL_MAP_TEXTURE_FORMAT_INTEL_TO_STRING__ (defines gl_map_texture_format_INTEL_to_string)
__GL_HISTOGRAM_TARGET_EXT_TO_STRING__ (defines gl_histogram_target_EXT_to_string)
__GL_TEXTURE_FILTER_FUNC_SGIS_TO_STRING__ (defines gl_texture_filter_func_SGIS_to_string)
__GL_FEED_BACK_TOKEN_TO_STRING__ (defines gl_feed_back_token_to_string)
__GL_LIST_PARAMETER_NAME_TO_STRING__ (defines gl_list_parameter_name_to_string)
__GL_TEXTURE_WRAP_MODE_TO_STRING__ (defines gl_texture_wrap_mode_to_string)
__GL_ENABLE_CAP_TO_STRING__ (defines gl_enable_cap_to_string)
__GL_SHADING_MODEL_TO_STRING__ (defines gl_shading_model_to_string)
__GL_MATERIAL_PARAMETER_TO_STRING__ (defines gl_material_parameter_to_string)
__GL_HINT_TARGET_TO_STRING__ (defines gl_hint_target_to_string)
__GL_PERFORMANCE_QUERY_CAPS_MASK_INTEL_TO_STRING__ (defines gl_performance_query_caps_mask_INTEL_to_string)
__GL_STENCIL_FUNCTION_TO_STRING__ (defines gl_stencil_function_to_string)
__GL_VERTEX_POINTER_TYPE_TO_STRING__ (defines gl_vertex_pointer_type_to_string)
__GL_ALPHA_FUNCTION_TO_STRING__ (defines gl_alpha_function_to_string)
__GL_COLOR_TABLE_PARAMETER_PNAME_SGI_TO_STRING__ (defines gl_color_table_parameter_PName_SGI_to_string)
__GL_GET_COLOR_TABLE_PARAMETER_PNAME_SGI_TO_STRING__ (defines gl_get_color_table_parameter_PName_SGI_to_string)
__GL_TEXTURE_GEN_MODE_TO_STRING__ (defines gl_texture_gen_mode_to_string)
__GL_FRAGMENT_LIGHT_MODEL_PARAMETER_SGIX_TO_STRING__ (defines gl_fragment_light_model_parameter_SGIX_to_string)
__GL_PIXEL_TEX_GEN_PARAMETER_NAME_SGIS_TO_STRING__ (defines gl_pixel_tex_gen_parameter_name_SGIS_to_string)
__GL_FRONT_FACE_DIRECTION_TO_STRING__ (defines gl_front_face_direction_to_string)
__GL_FRAGMENT_SHADER_COLOR_MOD_MASK_ATI_TO_STRING__ (defines gl_fragment_shader_color_mod_mask_ATI_to_string)
__GL_CONTEXT_PROFILE_MASK_TO_STRING__ (defines gl_context_profile_mask_to_string)
__GL_MEMORY_BARRIER_MASK_TO_STRING__ (defines gl_memory_barrier_mask_to_string)
__GL_FEEDBACK_TYPE_TO_STRING__ (defines gl_feedback_type_to_string)
*/
#ifdef __GL_VERTEX_HINTS_MASK_PGI_TO_STRING__
char * __attribute__((const)) gl_vertex_hints_mask_PGI_to_string(GLenum val){
  switch(val){
    case(GL_VERTEX23_BIT_PGI):{
      return("GL_VERTEX23_BIT_PGI");
    }
    case(GL_VERTEX4_BIT_PGI):{
      return("GL_VERTEX4_BIT_PGI");
    }
    case(GL_COLOR3_BIT_PGI):{
      return("GL_COLOR3_BIT_PGI");
    }
    case(GL_COLOR4_BIT_PGI):{
      return("GL_COLOR4_BIT_PGI");
    }
    case(GL_EDGEFLAG_BIT_PGI):{
      return("GL_EDGEFLAG_BIT_PGI");
    }
    case(GL_INDEX_BIT_PGI):{
      return("GL_INDEX_BIT_PGI");
    }
    case(GL_MAT_AMBIENT_BIT_PGI):{
      return("GL_MAT_AMBIENT_BIT_PGI");
    }
    case(GL_MAT_AMBIENT_AND_DIFFUSE_BIT_PGI):{
      return("GL_MAT_AMBIENT_AND_DIFFUSE_BIT_PGI");
    }
    case(GL_MAT_DIFFUSE_BIT_PGI):{
      return("GL_MAT_DIFFUSE_BIT_PGI");
    }
    case(GL_MAT_EMISSION_BIT_PGI):{
      return("GL_MAT_EMISSION_BIT_PGI");
    }
    case(GL_MAT_COLOR_INDEXES_BIT_PGI):{
      return("GL_MAT_COLOR_INDEXES_BIT_PGI");
    }
    case(GL_MAT_SHININESS_BIT_PGI):{
      return("GL_MAT_SHININESS_BIT_PGI");
    }
    case(GL_MAT_SPECULAR_BIT_PGI):{
      return("GL_MAT_SPECULAR_BIT_PGI");
    }
    case(GL_NORMAL_BIT_PGI):{
      return("GL_NORMAL_BIT_PGI");
    }
    case(GL_TEXCOORD1_BIT_PGI):{
      return("GL_TEXCOORD1_BIT_PGI");
    }
    case(GL_TEXCOORD2_BIT_PGI):{
      return("GL_TEXCOORD2_BIT_PGI");
    }
    case(GL_TEXCOORD3_BIT_PGI):{
      return("GL_TEXCOORD3_BIT_PGI");
    }
    case(GL_TEXCOORD4_BIT_PGI):{
      return("GL_TEXCOORD4_BIT_PGI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_VERTEX_HINTS_MASK_PGI_TO_STRING__ */

#ifdef __GL_MATRIX_MODE_TO_STRING__
char * __attribute__((const)) gl_matrix_mode_to_string(GLenum val){
  switch(val){
    case(GL_MODELVIEW):{
      return("GL_MODELVIEW");
    }
    case(GL_PROJECTION):{
      return("GL_PROJECTION");
    }
    case(GL_TEXTURE):{
      return("GL_TEXTURE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MATRIX_MODE_TO_STRING__ */

#ifdef __GL_TEXTURE_PARAMETER_NAME_TO_STRING__
char * __attribute__((const)) gl_texture_parameter_name_to_string(GLenum val){
  switch(val){
    case(GL_DETAIL_TEXTURE_LEVEL_SGIS):{
      return("GL_DETAIL_TEXTURE_LEVEL_SGIS");
    }
    case(GL_DETAIL_TEXTURE_MODE_SGIS):{
      return("GL_DETAIL_TEXTURE_MODE_SGIS");
    }
    case(GL_DUAL_TEXTURE_SELECT_SGIS):{
      return("GL_DUAL_TEXTURE_SELECT_SGIS");
    }
    case(GL_GENERATE_MIPMAP):{
      return("GL_GENERATE_MIPMAP");
    }
    case(GL_GENERATE_MIPMAP_SGIS):{
      return("GL_GENERATE_MIPMAP_SGIS");
    }
    case(GL_POST_TEXTURE_FILTER_BIAS_SGIX):{
      return("GL_POST_TEXTURE_FILTER_BIAS_SGIX");
    }
    case(GL_POST_TEXTURE_FILTER_SCALE_SGIX):{
      return("GL_POST_TEXTURE_FILTER_SCALE_SGIX");
    }
    case(GL_QUAD_TEXTURE_SELECT_SGIS):{
      return("GL_QUAD_TEXTURE_SELECT_SGIS");
    }
    case(GL_SHADOW_AMBIENT_SGIX):{
      return("GL_SHADOW_AMBIENT_SGIX");
    }
    case(GL_TEXTURE_BORDER_COLOR):{
      return("GL_TEXTURE_BORDER_COLOR");
    }
    case(GL_TEXTURE_CLIPMAP_CENTER_SGIX):{
      return("GL_TEXTURE_CLIPMAP_CENTER_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_DEPTH_SGIX):{
      return("GL_TEXTURE_CLIPMAP_DEPTH_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_FRAME_SGIX):{
      return("GL_TEXTURE_CLIPMAP_FRAME_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_LOD_OFFSET_SGIX):{
      return("GL_TEXTURE_CLIPMAP_LOD_OFFSET_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_OFFSET_SGIX):{
      return("GL_TEXTURE_CLIPMAP_OFFSET_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_VIRTUAL_DEPTH_SGIX):{
      return("GL_TEXTURE_CLIPMAP_VIRTUAL_DEPTH_SGIX");
    }
    case(GL_TEXTURE_COMPARE_SGIX):{
      return("GL_TEXTURE_COMPARE_SGIX");
    }
    case(GL_TEXTURE_LOD_BIAS_R_SGIX):{
      return("GL_TEXTURE_LOD_BIAS_R_SGIX");
    }
    case(GL_TEXTURE_LOD_BIAS_S_SGIX):{
      return("GL_TEXTURE_LOD_BIAS_S_SGIX");
    }
    case(GL_TEXTURE_LOD_BIAS_T_SGIX):{
      return("GL_TEXTURE_LOD_BIAS_T_SGIX");
    }
    case(GL_TEXTURE_MAG_FILTER):{
      return("GL_TEXTURE_MAG_FILTER");
    }
    case(GL_TEXTURE_MAX_CLAMP_R_SGIX):{
      return("GL_TEXTURE_MAX_CLAMP_R_SGIX");
    }
    case(GL_TEXTURE_MAX_CLAMP_S_SGIX):{
      return("GL_TEXTURE_MAX_CLAMP_S_SGIX");
    }
    case(GL_TEXTURE_MAX_CLAMP_T_SGIX):{
      return("GL_TEXTURE_MAX_CLAMP_T_SGIX");
    }
    case(GL_TEXTURE_MIN_FILTER):{
      return("GL_TEXTURE_MIN_FILTER");
    }
    case(GL_TEXTURE_PRIORITY):{
      return("GL_TEXTURE_PRIORITY");
    }
    case(GL_TEXTURE_WRAP_Q_SGIS):{
      return("GL_TEXTURE_WRAP_Q_SGIS");
    }
    case(GL_TEXTURE_WRAP_R):{
      return("GL_TEXTURE_WRAP_R");
    }
    case(GL_TEXTURE_WRAP_S):{
      return("GL_TEXTURE_WRAP_S");
    }
    case(GL_TEXTURE_WRAP_T):{
      return("GL_TEXTURE_WRAP_T");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_PARAMETER_NAME_TO_STRING__ */

#ifdef __GL_TEXTURE_MAG_FILTER_TO_STRING__
char * __attribute__((const)) gl_texture_mag_filter_to_string(GLenum val){
  switch(val){
    case(GL_FILTER4_SGIS):{
      return("GL_FILTER4_SGIS");
    }
    case(GL_LINEAR):{
      return("GL_LINEAR");
    }
    case(GL_LINEAR_DETAIL_ALPHA_SGIS):{
      return("GL_LINEAR_DETAIL_ALPHA_SGIS");
    }
    case(GL_LINEAR_DETAIL_COLOR_SGIS):{
      return("GL_LINEAR_DETAIL_COLOR_SGIS");
    }
    case(GL_LINEAR_DETAIL_SGIS):{
      return("GL_LINEAR_DETAIL_SGIS");
    }
    case(GL_LINEAR_SHARPEN_ALPHA_SGIS):{
      return("GL_LINEAR_SHARPEN_ALPHA_SGIS");
    }
    case(GL_LINEAR_SHARPEN_COLOR_SGIS):{
      return("GL_LINEAR_SHARPEN_COLOR_SGIS");
    }
    case(GL_LINEAR_SHARPEN_SGIS):{
      return("GL_LINEAR_SHARPEN_SGIS");
    }
    case(GL_NEAREST):{
      return("GL_NEAREST");
    }
    case(GL_PIXEL_TEX_GEN_Q_CEILING_SGIX):{
      return("GL_PIXEL_TEX_GEN_Q_CEILING_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_Q_FLOOR_SGIX):{
      return("GL_PIXEL_TEX_GEN_Q_FLOOR_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_Q_ROUND_SGIX):{
      return("GL_PIXEL_TEX_GEN_Q_ROUND_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_MAG_FILTER_TO_STRING__ */

#ifdef __GL_GET_PNAME_TO_STRING__
char * __attribute__((const)) gl_get_PName_to_string(GLenum val){
  switch(val){
    case(GL_ACCUM_ALPHA_BITS):{
      return("GL_ACCUM_ALPHA_BITS");
    }
    case(GL_ACCUM_BLUE_BITS):{
      return("GL_ACCUM_BLUE_BITS");
    }
    case(GL_ACCUM_CLEAR_VALUE):{
      return("GL_ACCUM_CLEAR_VALUE");
    }
    case(GL_ACCUM_GREEN_BITS):{
      return("GL_ACCUM_GREEN_BITS");
    }
    case(GL_ACCUM_RED_BITS):{
      return("GL_ACCUM_RED_BITS");
    }
    case(GL_ALIASED_LINE_WIDTH_RANGE):{
      return("GL_ALIASED_LINE_WIDTH_RANGE");
    }
    case(GL_ALIASED_POINT_SIZE_RANGE):{
      return("GL_ALIASED_POINT_SIZE_RANGE");
    }
    case(GL_ALPHA_BIAS):{
      return("GL_ALPHA_BIAS");
    }
    case(GL_ALPHA_BITS):{
      return("GL_ALPHA_BITS");
    }
    case(GL_ALPHA_SCALE):{
      return("GL_ALPHA_SCALE");
    }
    case(GL_ALPHA_TEST):{
      return("GL_ALPHA_TEST");
    }
    case(GL_ALPHA_TEST_FUNC):{
      return("GL_ALPHA_TEST_FUNC");
    }
    case(GL_ALPHA_TEST_FUNC_QCOM):{
      return("GL_ALPHA_TEST_FUNC_QCOM");
    }
    case(GL_ALPHA_TEST_QCOM):{
      return("GL_ALPHA_TEST_QCOM");
    }
    case(GL_ALPHA_TEST_REF):{
      return("GL_ALPHA_TEST_REF");
    }
    case(GL_ALPHA_TEST_REF_QCOM):{
      return("GL_ALPHA_TEST_REF_QCOM");
    }
    case(GL_ASYNC_DRAW_PIXELS_SGIX):{
      return("GL_ASYNC_DRAW_PIXELS_SGIX");
    }
    case(GL_ASYNC_HISTOGRAM_SGIX):{
      return("GL_ASYNC_HISTOGRAM_SGIX");
    }
    case(GL_ASYNC_MARKER_SGIX):{
      return("GL_ASYNC_MARKER_SGIX");
    }
    case(GL_ASYNC_READ_PIXELS_SGIX):{
      return("GL_ASYNC_READ_PIXELS_SGIX");
    }
    case(GL_ASYNC_TEX_IMAGE_SGIX):{
      return("GL_ASYNC_TEX_IMAGE_SGIX");
    }
    case(GL_ATTRIB_STACK_DEPTH):{
      return("GL_ATTRIB_STACK_DEPTH");
    }
    case(GL_AUTO_NORMAL):{
      return("GL_AUTO_NORMAL");
    }
    case(GL_AUX_BUFFERS):{
      return("GL_AUX_BUFFERS");
    }
    case(GL_BLEND):{
      return("GL_BLEND");
    }
    case(GL_BLEND_DST):{
      return("GL_BLEND_DST");
    }
    case(GL_BLEND_SRC):{
      return("GL_BLEND_SRC");
    }
    case(GL_BLUE_BIAS):{
      return("GL_BLUE_BIAS");
    }
    case(GL_BLUE_BITS):{
      return("GL_BLUE_BITS");
    }
    case(GL_BLUE_SCALE):{
      return("GL_BLUE_SCALE");
    }
    case(GL_CALLIGRAPHIC_FRAGMENT_SGIX):{
      return("GL_CALLIGRAPHIC_FRAGMENT_SGIX");
    }
    case(GL_CLIENT_ATTRIB_STACK_DEPTH):{
      return("GL_CLIENT_ATTRIB_STACK_DEPTH");
    }
    case(GL_CLIP_PLANE0):{
      return("GL_CLIP_PLANE0");
    }
    case(GL_CLIP_PLANE1):{
      return("GL_CLIP_PLANE1");
    }
    case(GL_CLIP_PLANE2):{
      return("GL_CLIP_PLANE2");
    }
    case(GL_CLIP_PLANE3):{
      return("GL_CLIP_PLANE3");
    }
    case(GL_CLIP_PLANE4):{
      return("GL_CLIP_PLANE4");
    }
    case(GL_CLIP_PLANE5):{
      return("GL_CLIP_PLANE5");
    }
    case(GL_COLOR_ARRAY):{
      return("GL_COLOR_ARRAY");
    }
    case(GL_COLOR_ARRAY_SIZE):{
      return("GL_COLOR_ARRAY_SIZE");
    }
    case(GL_COLOR_ARRAY_STRIDE):{
      return("GL_COLOR_ARRAY_STRIDE");
    }
    case(GL_COLOR_ARRAY_TYPE):{
      return("GL_COLOR_ARRAY_TYPE");
    }
    case(GL_COLOR_CLEAR_VALUE):{
      return("GL_COLOR_CLEAR_VALUE");
    }
    case(GL_COLOR_LOGIC_OP):{
      return("GL_COLOR_LOGIC_OP");
    }
    case(GL_COLOR_MATERIAL):{
      return("GL_COLOR_MATERIAL");
    }
    case(GL_COLOR_MATERIAL_FACE):{
      return("GL_COLOR_MATERIAL_FACE");
    }
    case(GL_COLOR_MATERIAL_PARAMETER):{
      return("GL_COLOR_MATERIAL_PARAMETER");
    }
    case(GL_COLOR_MATRIX_SGI):{
      return("GL_COLOR_MATRIX_SGI");
    }
    case(GL_COLOR_MATRIX_STACK_DEPTH_SGI):{
      return("GL_COLOR_MATRIX_STACK_DEPTH_SGI");
    }
    case(GL_COLOR_TABLE_SGI):{
      return("GL_COLOR_TABLE_SGI");
    }
    case(GL_COLOR_WRITEMASK):{
      return("GL_COLOR_WRITEMASK");
    }
    case(GL_CONVOLUTION_HINT_SGIX):{
      return("GL_CONVOLUTION_HINT_SGIX");
    }
    case(GL_CULL_FACE):{
      return("GL_CULL_FACE");
    }
    case(GL_CULL_FACE_MODE):{
      return("GL_CULL_FACE_MODE");
    }
    case(GL_CURRENT_COLOR):{
      return("GL_CURRENT_COLOR");
    }
    case(GL_CURRENT_INDEX):{
      return("GL_CURRENT_INDEX");
    }
    case(GL_CURRENT_NORMAL):{
      return("GL_CURRENT_NORMAL");
    }
    case(GL_CURRENT_RASTER_COLOR):{
      return("GL_CURRENT_RASTER_COLOR");
    }
    case(GL_CURRENT_RASTER_DISTANCE):{
      return("GL_CURRENT_RASTER_DISTANCE");
    }
    case(GL_CURRENT_RASTER_INDEX):{
      return("GL_CURRENT_RASTER_INDEX");
    }
    case(GL_CURRENT_RASTER_POSITION):{
      return("GL_CURRENT_RASTER_POSITION");
    }
    case(GL_CURRENT_RASTER_POSITION_VALID):{
      return("GL_CURRENT_RASTER_POSITION_VALID");
    }
    case(GL_CURRENT_RASTER_TEXTURE_COORDS):{
      return("GL_CURRENT_RASTER_TEXTURE_COORDS");
    }
    case(GL_CURRENT_TEXTURE_COORDS):{
      return("GL_CURRENT_TEXTURE_COORDS");
    }
    case(GL_DEFORMATIONS_MASK_SGIX):{
      return("GL_DEFORMATIONS_MASK_SGIX");
    }
    case(GL_DEPTH_BIAS):{
      return("GL_DEPTH_BIAS");
    }
    case(GL_DEPTH_BITS):{
      return("GL_DEPTH_BITS");
    }
    case(GL_DEPTH_CLEAR_VALUE):{
      return("GL_DEPTH_CLEAR_VALUE");
    }
    case(GL_DEPTH_FUNC):{
      return("GL_DEPTH_FUNC");
    }
    case(GL_DEPTH_RANGE):{
      return("GL_DEPTH_RANGE");
    }
    case(GL_DEPTH_SCALE):{
      return("GL_DEPTH_SCALE");
    }
    case(GL_DEPTH_TEST):{
      return("GL_DEPTH_TEST");
    }
    case(GL_DEPTH_WRITEMASK):{
      return("GL_DEPTH_WRITEMASK");
    }
    case(GL_DETAIL_TEXTURE_2D_BINDING_SGIS):{
      return("GL_DETAIL_TEXTURE_2D_BINDING_SGIS");
    }
    case(GL_DISTANCE_ATTENUATION_SGIS):{
      return("GL_DISTANCE_ATTENUATION_SGIS");
    }
    case(GL_DITHER):{
      return("GL_DITHER");
    }
    case(GL_DOUBLEBUFFER):{
      return("GL_DOUBLEBUFFER");
    }
    case(GL_DRAW_BUFFER):{
      return("GL_DRAW_BUFFER");
    }
    case(GL_EDGE_FLAG):{
      return("GL_EDGE_FLAG");
    }
    case(GL_EDGE_FLAG_ARRAY):{
      return("GL_EDGE_FLAG_ARRAY");
    }
    case(GL_EDGE_FLAG_ARRAY_STRIDE):{
      return("GL_EDGE_FLAG_ARRAY_STRIDE");
    }
    case(GL_FEEDBACK_BUFFER_SIZE):{
      return("GL_FEEDBACK_BUFFER_SIZE");
    }
    case(GL_FEEDBACK_BUFFER_TYPE):{
      return("GL_FEEDBACK_BUFFER_TYPE");
    }
    case(GL_FOG):{
      return("GL_FOG");
    }
    case(GL_FOG_COLOR):{
      return("GL_FOG_COLOR");
    }
    case(GL_FOG_DENSITY):{
      return("GL_FOG_DENSITY");
    }
    case(GL_FOG_END):{
      return("GL_FOG_END");
    }
    case(GL_FOG_FUNC_POINTS_SGIS):{
      return("GL_FOG_FUNC_POINTS_SGIS");
    }
    case(GL_FOG_HINT):{
      return("GL_FOG_HINT");
    }
    case(GL_FOG_INDEX):{
      return("GL_FOG_INDEX");
    }
    case(GL_FOG_MODE):{
      return("GL_FOG_MODE");
    }
    case(GL_FOG_OFFSET_SGIX):{
      return("GL_FOG_OFFSET_SGIX");
    }
    case(GL_FOG_OFFSET_VALUE_SGIX):{
      return("GL_FOG_OFFSET_VALUE_SGIX");
    }
    case(GL_FOG_START):{
      return("GL_FOG_START");
    }
    case(GL_FRAGMENT_COLOR_MATERIAL_FACE_SGIX):{
      return("GL_FRAGMENT_COLOR_MATERIAL_FACE_SGIX");
    }
    case(GL_FRAGMENT_COLOR_MATERIAL_PARAMETER_SGIX):{
      return("GL_FRAGMENT_COLOR_MATERIAL_PARAMETER_SGIX");
    }
    case(GL_FRAGMENT_COLOR_MATERIAL_SGIX):{
      return("GL_FRAGMENT_COLOR_MATERIAL_SGIX");
    }
    case(GL_FRAGMENT_LIGHT0_SGIX):{
      return("GL_FRAGMENT_LIGHT0_SGIX");
    }
    case(GL_FRAGMENT_LIGHTING_SGIX):{
      return("GL_FRAGMENT_LIGHTING_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_AMBIENT_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_AMBIENT_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_LOCAL_VIEWER_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_LOCAL_VIEWER_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_NORMAL_INTERPOLATION_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_NORMAL_INTERPOLATION_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_TWO_SIDE_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_TWO_SIDE_SGIX");
    }
    case(GL_FRAMEZOOM_FACTOR_SGIX):{
      return("GL_FRAMEZOOM_FACTOR_SGIX");
    }
    case(GL_FRAMEZOOM_SGIX):{
      return("GL_FRAMEZOOM_SGIX");
    }
    case(GL_FRONT_FACE):{
      return("GL_FRONT_FACE");
    }
    case(GL_GENERATE_MIPMAP_HINT_SGIS):{
      return("GL_GENERATE_MIPMAP_HINT_SGIS");
    }
    case(GL_GREEN_BIAS):{
      return("GL_GREEN_BIAS");
    }
    case(GL_GREEN_BITS):{
      return("GL_GREEN_BITS");
    }
    case(GL_GREEN_SCALE):{
      return("GL_GREEN_SCALE");
    }
    case(GL_INDEX_ARRAY):{
      return("GL_INDEX_ARRAY");
    }
    case(GL_INDEX_ARRAY_STRIDE):{
      return("GL_INDEX_ARRAY_STRIDE");
    }
    case(GL_INDEX_ARRAY_TYPE):{
      return("GL_INDEX_ARRAY_TYPE");
    }
    case(GL_INDEX_BITS):{
      return("GL_INDEX_BITS");
    }
    case(GL_INDEX_CLEAR_VALUE):{
      return("GL_INDEX_CLEAR_VALUE");
    }
    case(GL_INDEX_LOGIC_OP):{
      return("GL_INDEX_LOGIC_OP");
    }
    case(GL_INDEX_MODE):{
      return("GL_INDEX_MODE");
    }
    case(GL_INDEX_OFFSET):{
      return("GL_INDEX_OFFSET");
    }
    case(GL_INDEX_SHIFT):{
      return("GL_INDEX_SHIFT");
    }
    case(GL_INDEX_WRITEMASK):{
      return("GL_INDEX_WRITEMASK");
    }
    case(GL_INSTRUMENT_MEASUREMENTS_SGIX):{
      return("GL_INSTRUMENT_MEASUREMENTS_SGIX");
    }
    case(GL_INTERLACE_SGIX):{
      return("GL_INTERLACE_SGIX");
    }
    case(GL_IR_INSTRUMENT1_SGIX):{
      return("GL_IR_INSTRUMENT1_SGIX");
    }
    case(GL_LIGHT0):{
      return("GL_LIGHT0");
    }
    case(GL_LIGHT1):{
      return("GL_LIGHT1");
    }
    case(GL_LIGHT2):{
      return("GL_LIGHT2");
    }
    case(GL_LIGHT3):{
      return("GL_LIGHT3");
    }
    case(GL_LIGHT4):{
      return("GL_LIGHT4");
    }
    case(GL_LIGHT5):{
      return("GL_LIGHT5");
    }
    case(GL_LIGHT6):{
      return("GL_LIGHT6");
    }
    case(GL_LIGHT7):{
      return("GL_LIGHT7");
    }
    case(GL_LIGHTING):{
      return("GL_LIGHTING");
    }
    case(GL_LIGHT_ENV_MODE_SGIX):{
      return("GL_LIGHT_ENV_MODE_SGIX");
    }
    case(GL_LIGHT_MODEL_AMBIENT):{
      return("GL_LIGHT_MODEL_AMBIENT");
    }
    case(GL_LIGHT_MODEL_COLOR_CONTROL):{
      return("GL_LIGHT_MODEL_COLOR_CONTROL");
    }
    case(GL_LIGHT_MODEL_LOCAL_VIEWER):{
      return("GL_LIGHT_MODEL_LOCAL_VIEWER");
    }
    case(GL_LIGHT_MODEL_TWO_SIDE):{
      return("GL_LIGHT_MODEL_TWO_SIDE");
    }
    case(GL_LINE_SMOOTH):{
      return("GL_LINE_SMOOTH");
    }
    case(GL_LINE_SMOOTH_HINT):{
      return("GL_LINE_SMOOTH_HINT");
    }
    case(GL_LINE_STIPPLE):{
      return("GL_LINE_STIPPLE");
    }
    case(GL_LINE_STIPPLE_PATTERN):{
      return("GL_LINE_STIPPLE_PATTERN");
    }
    case(GL_LINE_STIPPLE_REPEAT):{
      return("GL_LINE_STIPPLE_REPEAT");
    }
    case(GL_LINE_WIDTH):{
      return("GL_LINE_WIDTH");
    }
    case(GL_LINE_WIDTH_GRANULARITY):{
      return("GL_LINE_WIDTH_GRANULARITY");
    }
    case(GL_LINE_WIDTH_RANGE):{
      return("GL_LINE_WIDTH_RANGE");
    }
    case(GL_LIST_BASE):{
      return("GL_LIST_BASE");
    }
    case(GL_LIST_INDEX):{
      return("GL_LIST_INDEX");
    }
    case(GL_LIST_MODE):{
      return("GL_LIST_MODE");
    }
    case(GL_LOGIC_OP):{
      return("GL_LOGIC_OP");
    }
    case(GL_LOGIC_OP_MODE):{
      return("GL_LOGIC_OP_MODE");
    }
    case(GL_MAP1_COLOR_4):{
      return("GL_MAP1_COLOR_4");
    }
    case(GL_MAP1_GRID_DOMAIN):{
      return("GL_MAP1_GRID_DOMAIN");
    }
    case(GL_MAP1_GRID_SEGMENTS):{
      return("GL_MAP1_GRID_SEGMENTS");
    }
    case(GL_MAP1_INDEX):{
      return("GL_MAP1_INDEX");
    }
    case(GL_MAP1_NORMAL):{
      return("GL_MAP1_NORMAL");
    }
    case(GL_MAP1_TEXTURE_COORD_1):{
      return("GL_MAP1_TEXTURE_COORD_1");
    }
    case(GL_MAP1_TEXTURE_COORD_2):{
      return("GL_MAP1_TEXTURE_COORD_2");
    }
    case(GL_MAP1_TEXTURE_COORD_3):{
      return("GL_MAP1_TEXTURE_COORD_3");
    }
    case(GL_MAP1_TEXTURE_COORD_4):{
      return("GL_MAP1_TEXTURE_COORD_4");
    }
    case(GL_MAP1_VERTEX_3):{
      return("GL_MAP1_VERTEX_3");
    }
    case(GL_MAP1_VERTEX_4):{
      return("GL_MAP1_VERTEX_4");
    }
    case(GL_MAP2_COLOR_4):{
      return("GL_MAP2_COLOR_4");
    }
    case(GL_MAP2_GRID_DOMAIN):{
      return("GL_MAP2_GRID_DOMAIN");
    }
    case(GL_MAP2_GRID_SEGMENTS):{
      return("GL_MAP2_GRID_SEGMENTS");
    }
    case(GL_MAP2_INDEX):{
      return("GL_MAP2_INDEX");
    }
    case(GL_MAP2_NORMAL):{
      return("GL_MAP2_NORMAL");
    }
    case(GL_MAP2_TEXTURE_COORD_1):{
      return("GL_MAP2_TEXTURE_COORD_1");
    }
    case(GL_MAP2_TEXTURE_COORD_2):{
      return("GL_MAP2_TEXTURE_COORD_2");
    }
    case(GL_MAP2_TEXTURE_COORD_3):{
      return("GL_MAP2_TEXTURE_COORD_3");
    }
    case(GL_MAP2_TEXTURE_COORD_4):{
      return("GL_MAP2_TEXTURE_COORD_4");
    }
    case(GL_MAP2_VERTEX_3):{
      return("GL_MAP2_VERTEX_3");
    }
    case(GL_MAP2_VERTEX_4):{
      return("GL_MAP2_VERTEX_4");
    }
    case(GL_MAP_COLOR):{
      return("GL_MAP_COLOR");
    }
    case(GL_MAP_STENCIL):{
      return("GL_MAP_STENCIL");
    }
    case(GL_MATRIX_MODE):{
      return("GL_MATRIX_MODE");
    }
    case(GL_MAX_4D_TEXTURE_SIZE_SGIS):{
      return("GL_MAX_4D_TEXTURE_SIZE_SGIS");
    }
    case(GL_MAX_ACTIVE_LIGHTS_SGIX):{
      return("GL_MAX_ACTIVE_LIGHTS_SGIX");
    }
    case(GL_MAX_ASYNC_DRAW_PIXELS_SGIX):{
      return("GL_MAX_ASYNC_DRAW_PIXELS_SGIX");
    }
    case(GL_MAX_ASYNC_HISTOGRAM_SGIX):{
      return("GL_MAX_ASYNC_HISTOGRAM_SGIX");
    }
    case(GL_MAX_ASYNC_READ_PIXELS_SGIX):{
      return("GL_MAX_ASYNC_READ_PIXELS_SGIX");
    }
    case(GL_MAX_ASYNC_TEX_IMAGE_SGIX):{
      return("GL_MAX_ASYNC_TEX_IMAGE_SGIX");
    }
    case(GL_MAX_ATTRIB_STACK_DEPTH):{
      return("GL_MAX_ATTRIB_STACK_DEPTH");
    }
    case(GL_MAX_CLIENT_ATTRIB_STACK_DEPTH):{
      return("GL_MAX_CLIENT_ATTRIB_STACK_DEPTH");
    }
    case(GL_MAX_CLIPMAP_DEPTH_SGIX):{
      return("GL_MAX_CLIPMAP_DEPTH_SGIX");
    }
    case(GL_MAX_CLIPMAP_VIRTUAL_DEPTH_SGIX):{
      return("GL_MAX_CLIPMAP_VIRTUAL_DEPTH_SGIX");
    }
    case(GL_MAX_CLIP_DISTANCES):{
      return("GL_MAX_CLIP_DISTANCES");
    }
    case(GL_MAX_CLIP_PLANES):{
      return("GL_MAX_CLIP_PLANES");
    }
    case(GL_MAX_COLOR_MATRIX_STACK_DEPTH_SGI):{
      return("GL_MAX_COLOR_MATRIX_STACK_DEPTH_SGI");
    }
    case(GL_MAX_EVAL_ORDER):{
      return("GL_MAX_EVAL_ORDER");
    }
    case(GL_MAX_FOG_FUNC_POINTS_SGIS):{
      return("GL_MAX_FOG_FUNC_POINTS_SGIS");
    }
    case(GL_MAX_FRAGMENT_LIGHTS_SGIX):{
      return("GL_MAX_FRAGMENT_LIGHTS_SGIX");
    }
    case(GL_MAX_FRAMEZOOM_FACTOR_SGIX):{
      return("GL_MAX_FRAMEZOOM_FACTOR_SGIX");
    }
    case(GL_MAX_LIGHTS):{
      return("GL_MAX_LIGHTS");
    }
    case(GL_MAX_LIST_NESTING):{
      return("GL_MAX_LIST_NESTING");
    }
    case(GL_MAX_MODELVIEW_STACK_DEPTH):{
      return("GL_MAX_MODELVIEW_STACK_DEPTH");
    }
    case(GL_MAX_NAME_STACK_DEPTH):{
      return("GL_MAX_NAME_STACK_DEPTH");
    }
    case(GL_MAX_PIXEL_MAP_TABLE):{
      return("GL_MAX_PIXEL_MAP_TABLE");
    }
    case(GL_MAX_PROJECTION_STACK_DEPTH):{
      return("GL_MAX_PROJECTION_STACK_DEPTH");
    }
    case(GL_MAX_TEXTURE_SIZE):{
      return("GL_MAX_TEXTURE_SIZE");
    }
    case(GL_MAX_TEXTURE_STACK_DEPTH):{
      return("GL_MAX_TEXTURE_STACK_DEPTH");
    }
    case(GL_MAX_VIEWPORT_DIMS):{
      return("GL_MAX_VIEWPORT_DIMS");
    }
    case(GL_MODELVIEW_MATRIX):{
      return("GL_MODELVIEW_MATRIX");
    }
    case(GL_MODELVIEW_STACK_DEPTH):{
      return("GL_MODELVIEW_STACK_DEPTH");
    }
    case(GL_MULTISAMPLE_SGIS):{
      return("GL_MULTISAMPLE_SGIS");
    }
    case(GL_NAME_STACK_DEPTH):{
      return("GL_NAME_STACK_DEPTH");
    }
    case(GL_NORMALIZE):{
      return("GL_NORMALIZE");
    }
    case(GL_NORMAL_ARRAY):{
      return("GL_NORMAL_ARRAY");
    }
    case(GL_NORMAL_ARRAY_STRIDE):{
      return("GL_NORMAL_ARRAY_STRIDE");
    }
    case(GL_NORMAL_ARRAY_TYPE):{
      return("GL_NORMAL_ARRAY_TYPE");
    }
    case(GL_PACK_ALIGNMENT):{
      return("GL_PACK_ALIGNMENT");
    }
    case(GL_PACK_IMAGE_DEPTH_SGIS):{
      return("GL_PACK_IMAGE_DEPTH_SGIS");
    }
    case(GL_PACK_LSB_FIRST):{
      return("GL_PACK_LSB_FIRST");
    }
    case(GL_PACK_RESAMPLE_SGIX):{
      return("GL_PACK_RESAMPLE_SGIX");
    }
    case(GL_PACK_ROW_LENGTH):{
      return("GL_PACK_ROW_LENGTH");
    }
    case(GL_PACK_SKIP_PIXELS):{
      return("GL_PACK_SKIP_PIXELS");
    }
    case(GL_PACK_SKIP_ROWS):{
      return("GL_PACK_SKIP_ROWS");
    }
    case(GL_PACK_SKIP_VOLUMES_SGIS):{
      return("GL_PACK_SKIP_VOLUMES_SGIS");
    }
    case(GL_PACK_SUBSAMPLE_RATE_SGIX):{
      return("GL_PACK_SUBSAMPLE_RATE_SGIX");
    }
    case(GL_PACK_SWAP_BYTES):{
      return("GL_PACK_SWAP_BYTES");
    }
    case(GL_PERSPECTIVE_CORRECTION_HINT):{
      return("GL_PERSPECTIVE_CORRECTION_HINT");
    }
    case(GL_PIXEL_MAP_A_TO_A_SIZE):{
      return("GL_PIXEL_MAP_A_TO_A_SIZE");
    }
    case(GL_PIXEL_MAP_B_TO_B_SIZE):{
      return("GL_PIXEL_MAP_B_TO_B_SIZE");
    }
    case(GL_PIXEL_MAP_G_TO_G_SIZE):{
      return("GL_PIXEL_MAP_G_TO_G_SIZE");
    }
    case(GL_PIXEL_MAP_I_TO_A_SIZE):{
      return("GL_PIXEL_MAP_I_TO_A_SIZE");
    }
    case(GL_PIXEL_MAP_I_TO_B_SIZE):{
      return("GL_PIXEL_MAP_I_TO_B_SIZE");
    }
    case(GL_PIXEL_MAP_I_TO_G_SIZE):{
      return("GL_PIXEL_MAP_I_TO_G_SIZE");
    }
    case(GL_PIXEL_MAP_I_TO_I_SIZE):{
      return("GL_PIXEL_MAP_I_TO_I_SIZE");
    }
    case(GL_PIXEL_MAP_I_TO_R_SIZE):{
      return("GL_PIXEL_MAP_I_TO_R_SIZE");
    }
    case(GL_PIXEL_MAP_R_TO_R_SIZE):{
      return("GL_PIXEL_MAP_R_TO_R_SIZE");
    }
    case(GL_PIXEL_MAP_S_TO_S_SIZE):{
      return("GL_PIXEL_MAP_S_TO_S_SIZE");
    }
    case(GL_PIXEL_TEXTURE_SGIS):{
      return("GL_PIXEL_TEXTURE_SGIS");
    }
    case(GL_PIXEL_TEX_GEN_MODE_SGIX):{
      return("GL_PIXEL_TEX_GEN_MODE_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_SGIX):{
      return("GL_PIXEL_TEX_GEN_SGIX");
    }
    case(GL_PIXEL_TILE_BEST_ALIGNMENT_SGIX):{
      return("GL_PIXEL_TILE_BEST_ALIGNMENT_SGIX");
    }
    case(GL_PIXEL_TILE_CACHE_INCREMENT_SGIX):{
      return("GL_PIXEL_TILE_CACHE_INCREMENT_SGIX");
    }
    case(GL_PIXEL_TILE_CACHE_SIZE_SGIX):{
      return("GL_PIXEL_TILE_CACHE_SIZE_SGIX");
    }
    case(GL_PIXEL_TILE_GRID_DEPTH_SGIX):{
      return("GL_PIXEL_TILE_GRID_DEPTH_SGIX");
    }
    case(GL_PIXEL_TILE_GRID_HEIGHT_SGIX):{
      return("GL_PIXEL_TILE_GRID_HEIGHT_SGIX");
    }
    case(GL_PIXEL_TILE_GRID_WIDTH_SGIX):{
      return("GL_PIXEL_TILE_GRID_WIDTH_SGIX");
    }
    case(GL_PIXEL_TILE_HEIGHT_SGIX):{
      return("GL_PIXEL_TILE_HEIGHT_SGIX");
    }
    case(GL_PIXEL_TILE_WIDTH_SGIX):{
      return("GL_PIXEL_TILE_WIDTH_SGIX");
    }
    case(GL_POINT_FADE_THRESHOLD_SIZE_SGIS):{
      return("GL_POINT_FADE_THRESHOLD_SIZE_SGIS");
    }
    case(GL_POINT_SIZE):{
      return("GL_POINT_SIZE");
    }
    case(GL_POINT_SIZE_GRANULARITY):{
      return("GL_POINT_SIZE_GRANULARITY");
    }
    case(GL_POINT_SIZE_MAX_SGIS):{
      return("GL_POINT_SIZE_MAX_SGIS");
    }
    case(GL_POINT_SIZE_MIN_SGIS):{
      return("GL_POINT_SIZE_MIN_SGIS");
    }
    case(GL_POINT_SIZE_RANGE):{
      return("GL_POINT_SIZE_RANGE");
    }
    case(GL_POINT_SMOOTH):{
      return("GL_POINT_SMOOTH");
    }
    case(GL_POINT_SMOOTH_HINT):{
      return("GL_POINT_SMOOTH_HINT");
    }
    case(GL_POLYGON_MODE):{
      return("GL_POLYGON_MODE");
    }
    case(GL_POLYGON_OFFSET_FACTOR):{
      return("GL_POLYGON_OFFSET_FACTOR");
    }
    case(GL_POLYGON_OFFSET_FILL):{
      return("GL_POLYGON_OFFSET_FILL");
    }
    case(GL_POLYGON_OFFSET_LINE):{
      return("GL_POLYGON_OFFSET_LINE");
    }
    case(GL_POLYGON_OFFSET_POINT):{
      return("GL_POLYGON_OFFSET_POINT");
    }
    case(GL_POLYGON_OFFSET_UNITS):{
      return("GL_POLYGON_OFFSET_UNITS");
    }
    case(GL_POLYGON_SMOOTH):{
      return("GL_POLYGON_SMOOTH");
    }
    case(GL_POLYGON_SMOOTH_HINT):{
      return("GL_POLYGON_SMOOTH_HINT");
    }
    case(GL_POLYGON_STIPPLE):{
      return("GL_POLYGON_STIPPLE");
    }
    case(GL_POST_COLOR_MATRIX_ALPHA_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_ALPHA_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_ALPHA_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_ALPHA_SCALE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_BLUE_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_BLUE_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_BLUE_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_BLUE_SCALE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI):{
      return("GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_GREEN_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_GREEN_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_GREEN_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_GREEN_SCALE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_RED_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_RED_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_RED_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_RED_SCALE_SGI");
    }
    case(GL_POST_CONVOLUTION_COLOR_TABLE_SGI):{
      return("GL_POST_CONVOLUTION_COLOR_TABLE_SGI");
    }
    case(GL_POST_TEXTURE_FILTER_BIAS_RANGE_SGIX):{
      return("GL_POST_TEXTURE_FILTER_BIAS_RANGE_SGIX");
    }
    case(GL_POST_TEXTURE_FILTER_SCALE_RANGE_SGIX):{
      return("GL_POST_TEXTURE_FILTER_SCALE_RANGE_SGIX");
    }
    case(GL_PROJECTION_MATRIX):{
      return("GL_PROJECTION_MATRIX");
    }
    case(GL_PROJECTION_STACK_DEPTH):{
      return("GL_PROJECTION_STACK_DEPTH");
    }
    case(GL_READ_BUFFER):{
      return("GL_READ_BUFFER");
    }
    case(GL_READ_BUFFER_NV):{
      return("GL_READ_BUFFER_NV");
    }
    case(GL_RED_BIAS):{
      return("GL_RED_BIAS");
    }
    case(GL_RED_BITS):{
      return("GL_RED_BITS");
    }
    case(GL_RED_SCALE):{
      return("GL_RED_SCALE");
    }
    case(GL_REFERENCE_PLANE_EQUATION_SGIX):{
      return("GL_REFERENCE_PLANE_EQUATION_SGIX");
    }
    case(GL_REFERENCE_PLANE_SGIX):{
      return("GL_REFERENCE_PLANE_SGIX");
    }
    case(GL_RENDER_MODE):{
      return("GL_RENDER_MODE");
    }
    case(GL_RGBA_MODE):{
      return("GL_RGBA_MODE");
    }
    case(GL_SAMPLES_SGIS):{
      return("GL_SAMPLES_SGIS");
    }
    case(GL_SAMPLE_ALPHA_TO_MASK_SGIS):{
      return("GL_SAMPLE_ALPHA_TO_MASK_SGIS");
    }
    case(GL_SAMPLE_ALPHA_TO_ONE_SGIS):{
      return("GL_SAMPLE_ALPHA_TO_ONE_SGIS");
    }
    case(GL_SAMPLE_BUFFERS_SGIS):{
      return("GL_SAMPLE_BUFFERS_SGIS");
    }
    case(GL_SAMPLE_MASK_INVERT_SGIS):{
      return("GL_SAMPLE_MASK_INVERT_SGIS");
    }
    case(GL_SAMPLE_MASK_SGIS):{
      return("GL_SAMPLE_MASK_SGIS");
    }
    case(GL_SAMPLE_MASK_VALUE_SGIS):{
      return("GL_SAMPLE_MASK_VALUE_SGIS");
    }
    case(GL_SAMPLE_PATTERN_SGIS):{
      return("GL_SAMPLE_PATTERN_SGIS");
    }
    case(GL_SCISSOR_BOX):{
      return("GL_SCISSOR_BOX");
    }
    case(GL_SCISSOR_TEST):{
      return("GL_SCISSOR_TEST");
    }
    case(GL_SELECTION_BUFFER_SIZE):{
      return("GL_SELECTION_BUFFER_SIZE");
    }
    case(GL_SHADE_MODEL):{
      return("GL_SHADE_MODEL");
    }
    case(GL_SMOOTH_LINE_WIDTH_GRANULARITY):{
      return("GL_SMOOTH_LINE_WIDTH_GRANULARITY");
    }
    case(GL_SMOOTH_LINE_WIDTH_RANGE):{
      return("GL_SMOOTH_LINE_WIDTH_RANGE");
    }
    case(GL_SMOOTH_POINT_SIZE_GRANULARITY):{
      return("GL_SMOOTH_POINT_SIZE_GRANULARITY");
    }
    case(GL_SMOOTH_POINT_SIZE_RANGE):{
      return("GL_SMOOTH_POINT_SIZE_RANGE");
    }
    case(GL_SPRITE_AXIS_SGIX):{
      return("GL_SPRITE_AXIS_SGIX");
    }
    case(GL_SPRITE_MODE_SGIX):{
      return("GL_SPRITE_MODE_SGIX");
    }
    case(GL_SPRITE_SGIX):{
      return("GL_SPRITE_SGIX");
    }
    case(GL_SPRITE_TRANSLATION_SGIX):{
      return("GL_SPRITE_TRANSLATION_SGIX");
    }
    case(GL_STENCIL_BITS):{
      return("GL_STENCIL_BITS");
    }
    case(GL_STENCIL_CLEAR_VALUE):{
      return("GL_STENCIL_CLEAR_VALUE");
    }
    case(GL_STENCIL_FAIL):{
      return("GL_STENCIL_FAIL");
    }
    case(GL_STENCIL_FUNC):{
      return("GL_STENCIL_FUNC");
    }
    case(GL_STENCIL_PASS_DEPTH_FAIL):{
      return("GL_STENCIL_PASS_DEPTH_FAIL");
    }
    case(GL_STENCIL_PASS_DEPTH_PASS):{
      return("GL_STENCIL_PASS_DEPTH_PASS");
    }
    case(GL_STENCIL_REF):{
      return("GL_STENCIL_REF");
    }
    case(GL_STENCIL_TEST):{
      return("GL_STENCIL_TEST");
    }
    case(GL_STENCIL_VALUE_MASK):{
      return("GL_STENCIL_VALUE_MASK");
    }
    case(GL_STENCIL_WRITEMASK):{
      return("GL_STENCIL_WRITEMASK");
    }
    case(GL_STEREO):{
      return("GL_STEREO");
    }
    case(GL_SUBPIXEL_BITS):{
      return("GL_SUBPIXEL_BITS");
    }
    case(GL_TEXTURE_1D):{
      return("GL_TEXTURE_1D");
    }
    case(GL_TEXTURE_2D):{
      return("GL_TEXTURE_2D");
    }
    case(GL_TEXTURE_4D_BINDING_SGIS):{
      return("GL_TEXTURE_4D_BINDING_SGIS");
    }
    case(GL_TEXTURE_4D_SGIS):{
      return("GL_TEXTURE_4D_SGIS");
    }
    case(GL_TEXTURE_BINDING_1D):{
      return("GL_TEXTURE_BINDING_1D");
    }
    case(GL_TEXTURE_BINDING_2D):{
      return("GL_TEXTURE_BINDING_2D");
    }
    case(GL_TEXTURE_BINDING_3D):{
      return("GL_TEXTURE_BINDING_3D");
    }
    case(GL_TEXTURE_COLOR_TABLE_SGI):{
      return("GL_TEXTURE_COLOR_TABLE_SGI");
    }
    case(GL_TEXTURE_COORD_ARRAY):{
      return("GL_TEXTURE_COORD_ARRAY");
    }
    case(GL_TEXTURE_COORD_ARRAY_SIZE):{
      return("GL_TEXTURE_COORD_ARRAY_SIZE");
    }
    case(GL_TEXTURE_COORD_ARRAY_STRIDE):{
      return("GL_TEXTURE_COORD_ARRAY_STRIDE");
    }
    case(GL_TEXTURE_COORD_ARRAY_TYPE):{
      return("GL_TEXTURE_COORD_ARRAY_TYPE");
    }
    case(GL_TEXTURE_GEN_Q):{
      return("GL_TEXTURE_GEN_Q");
    }
    case(GL_TEXTURE_GEN_R):{
      return("GL_TEXTURE_GEN_R");
    }
    case(GL_TEXTURE_GEN_S):{
      return("GL_TEXTURE_GEN_S");
    }
    case(GL_TEXTURE_GEN_T):{
      return("GL_TEXTURE_GEN_T");
    }
    case(GL_TEXTURE_MATRIX):{
      return("GL_TEXTURE_MATRIX");
    }
    case(GL_TEXTURE_STACK_DEPTH):{
      return("GL_TEXTURE_STACK_DEPTH");
    }
    case(GL_UNPACK_ALIGNMENT):{
      return("GL_UNPACK_ALIGNMENT");
    }
    case(GL_UNPACK_IMAGE_DEPTH_SGIS):{
      return("GL_UNPACK_IMAGE_DEPTH_SGIS");
    }
    case(GL_UNPACK_LSB_FIRST):{
      return("GL_UNPACK_LSB_FIRST");
    }
    case(GL_UNPACK_RESAMPLE_SGIX):{
      return("GL_UNPACK_RESAMPLE_SGIX");
    }
    case(GL_UNPACK_ROW_LENGTH):{
      return("GL_UNPACK_ROW_LENGTH");
    }
    case(GL_UNPACK_SKIP_PIXELS):{
      return("GL_UNPACK_SKIP_PIXELS");
    }
    case(GL_UNPACK_SKIP_ROWS):{
      return("GL_UNPACK_SKIP_ROWS");
    }
    case(GL_UNPACK_SKIP_VOLUMES_SGIS):{
      return("GL_UNPACK_SKIP_VOLUMES_SGIS");
    }
    case(GL_UNPACK_SUBSAMPLE_RATE_SGIX):{
      return("GL_UNPACK_SUBSAMPLE_RATE_SGIX");
    }
    case(GL_UNPACK_SWAP_BYTES):{
      return("GL_UNPACK_SWAP_BYTES");
    }
    case(GL_VERTEX_ARRAY):{
      return("GL_VERTEX_ARRAY");
    }
    case(GL_VERTEX_ARRAY_SIZE):{
      return("GL_VERTEX_ARRAY_SIZE");
    }
    case(GL_VERTEX_ARRAY_STRIDE):{
      return("GL_VERTEX_ARRAY_STRIDE");
    }
    case(GL_VERTEX_ARRAY_TYPE):{
      return("GL_VERTEX_ARRAY_TYPE");
    }
    case(GL_VERTEX_PRECLIP_HINT_SGIX):{
      return("GL_VERTEX_PRECLIP_HINT_SGIX");
    }
    case(GL_VERTEX_PRECLIP_SGIX):{
      return("GL_VERTEX_PRECLIP_SGIX");
    }
    case(GL_VIEWPORT):{
      return("GL_VIEWPORT");
    }
    case(GL_ZOOM_X):{
      return("GL_ZOOM_X");
    }
    case(GL_ZOOM_Y):{
      return("GL_ZOOM_Y");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_PNAME_TO_STRING__ */

#ifdef __GL_PIXEL_MAP_TO_STRING__
char * __attribute__((const)) gl_pixel_map_to_string(GLenum val){
  switch(val){
    case(GL_PIXEL_MAP_A_TO_A):{
      return("GL_PIXEL_MAP_A_TO_A");
    }
    case(GL_PIXEL_MAP_B_TO_B):{
      return("GL_PIXEL_MAP_B_TO_B");
    }
    case(GL_PIXEL_MAP_G_TO_G):{
      return("GL_PIXEL_MAP_G_TO_G");
    }
    case(GL_PIXEL_MAP_I_TO_A):{
      return("GL_PIXEL_MAP_I_TO_A");
    }
    case(GL_PIXEL_MAP_I_TO_B):{
      return("GL_PIXEL_MAP_I_TO_B");
    }
    case(GL_PIXEL_MAP_I_TO_G):{
      return("GL_PIXEL_MAP_I_TO_G");
    }
    case(GL_PIXEL_MAP_I_TO_I):{
      return("GL_PIXEL_MAP_I_TO_I");
    }
    case(GL_PIXEL_MAP_I_TO_R):{
      return("GL_PIXEL_MAP_I_TO_R");
    }
    case(GL_PIXEL_MAP_R_TO_R):{
      return("GL_PIXEL_MAP_R_TO_R");
    }
    case(GL_PIXEL_MAP_S_TO_S):{
      return("GL_PIXEL_MAP_S_TO_S");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_MAP_TO_STRING__ */

#ifdef __GL_MAP_TARGET_TO_STRING__
char * __attribute__((const)) gl_map_target_to_string(GLenum val){
  switch(val){
    case(GL_GEOMETRY_DEFORMATION_SGIX):{
      return("GL_GEOMETRY_DEFORMATION_SGIX");
    }
    case(GL_MAP1_COLOR_4):{
      return("GL_MAP1_COLOR_4");
    }
    case(GL_MAP1_INDEX):{
      return("GL_MAP1_INDEX");
    }
    case(GL_MAP1_NORMAL):{
      return("GL_MAP1_NORMAL");
    }
    case(GL_MAP1_TEXTURE_COORD_1):{
      return("GL_MAP1_TEXTURE_COORD_1");
    }
    case(GL_MAP1_TEXTURE_COORD_2):{
      return("GL_MAP1_TEXTURE_COORD_2");
    }
    case(GL_MAP1_TEXTURE_COORD_3):{
      return("GL_MAP1_TEXTURE_COORD_3");
    }
    case(GL_MAP1_TEXTURE_COORD_4):{
      return("GL_MAP1_TEXTURE_COORD_4");
    }
    case(GL_MAP1_VERTEX_3):{
      return("GL_MAP1_VERTEX_3");
    }
    case(GL_MAP1_VERTEX_4):{
      return("GL_MAP1_VERTEX_4");
    }
    case(GL_MAP2_COLOR_4):{
      return("GL_MAP2_COLOR_4");
    }
    case(GL_MAP2_INDEX):{
      return("GL_MAP2_INDEX");
    }
    case(GL_MAP2_NORMAL):{
      return("GL_MAP2_NORMAL");
    }
    case(GL_MAP2_TEXTURE_COORD_1):{
      return("GL_MAP2_TEXTURE_COORD_1");
    }
    case(GL_MAP2_TEXTURE_COORD_2):{
      return("GL_MAP2_TEXTURE_COORD_2");
    }
    case(GL_MAP2_TEXTURE_COORD_3):{
      return("GL_MAP2_TEXTURE_COORD_3");
    }
    case(GL_MAP2_TEXTURE_COORD_4):{
      return("GL_MAP2_TEXTURE_COORD_4");
    }
    case(GL_MAP2_VERTEX_3):{
      return("GL_MAP2_VERTEX_3");
    }
    case(GL_MAP2_VERTEX_4):{
      return("GL_MAP2_VERTEX_4");
    }
    case(GL_TEXTURE_DEFORMATION_SGIX):{
      return("GL_TEXTURE_DEFORMATION_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MAP_TARGET_TO_STRING__ */

#ifdef __GL_LIGHT_ENV_MODE_SGIX_TO_STRING__
char * __attribute__((const)) gl_light_env_mode_SGIX_to_string(GLenum val){
  switch(val){
    case(GL_ADD):{
      return("GL_ADD");
    }
    case(GL_MODULATE):{
      return("GL_MODULATE");
    }
    case(GL_REPLACE):{
      return("GL_REPLACE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIGHT_ENV_MODE_SGIX_TO_STRING__ */

#ifdef __GL_COLOR_MATERIAL_FACE_TO_STRING__
char * __attribute__((const)) gl_color_material_face_to_string(GLenum val){
  switch(val){
    case(GL_BACK):{
      return("GL_BACK");
    }
    case(GL_FRONT):{
      return("GL_FRONT");
    }
    case(GL_FRONT_AND_BACK):{
      return("GL_FRONT_AND_BACK");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_COLOR_MATERIAL_FACE_TO_STRING__ */

#ifdef __GL_CONVOLUTION_PARAMETER_EXT_TO_STRING__
char * __attribute__((const)) gl_convolution_parameter_EXT_to_string(GLenum val){
  switch(val){
    case(GL_CONVOLUTION_BORDER_MODE):{
      return("GL_CONVOLUTION_BORDER_MODE");
    }
    case(GL_CONVOLUTION_FILTER_BIAS):{
      return("GL_CONVOLUTION_FILTER_BIAS");
    }
    case(GL_CONVOLUTION_FILTER_SCALE):{
      return("GL_CONVOLUTION_FILTER_SCALE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CONVOLUTION_PARAMETER_EXT_TO_STRING__ */

#ifdef __GL_OCCLUSION_QUERY_EVENT_MASK_AMD_TO_STRING__
char * __attribute__((const)) gl_occlusion_query_event_mask_AMD_to_string(GLenum val){
  switch(val){
    case(GL_QUERY_DEPTH_PASS_EVENT_BIT_AMD):{
      return("GL_QUERY_DEPTH_PASS_EVENT_BIT_AMD");
    }
    case(GL_QUERY_DEPTH_FAIL_EVENT_BIT_AMD):{
      return("GL_QUERY_DEPTH_FAIL_EVENT_BIT_AMD");
    }
    case(GL_QUERY_STENCIL_FAIL_EVENT_BIT_AMD):{
      return("GL_QUERY_STENCIL_FAIL_EVENT_BIT_AMD");
    }
    case(GL_QUERY_DEPTH_BOUNDS_FAIL_EVENT_BIT_AMD):{
      return("GL_QUERY_DEPTH_BOUNDS_FAIL_EVENT_BIT_AMD");
    }
    case(GL_QUERY_ALL_EVENT_BITS_AMD):{
      return("GL_QUERY_ALL_EVENT_BITS_AMD");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_OCCLUSION_QUERY_EVENT_MASK_AMD_TO_STRING__ */

#ifdef __GL_GET_PIXEL_MAP_TO_STRING__
char * __attribute__((const)) gl_get_pixel_map_to_string(GLenum val){
  switch(val){
    case(GL_PIXEL_MAP_A_TO_A):{
      return("GL_PIXEL_MAP_A_TO_A");
    }
    case(GL_PIXEL_MAP_B_TO_B):{
      return("GL_PIXEL_MAP_B_TO_B");
    }
    case(GL_PIXEL_MAP_G_TO_G):{
      return("GL_PIXEL_MAP_G_TO_G");
    }
    case(GL_PIXEL_MAP_I_TO_A):{
      return("GL_PIXEL_MAP_I_TO_A");
    }
    case(GL_PIXEL_MAP_I_TO_B):{
      return("GL_PIXEL_MAP_I_TO_B");
    }
    case(GL_PIXEL_MAP_I_TO_G):{
      return("GL_PIXEL_MAP_I_TO_G");
    }
    case(GL_PIXEL_MAP_I_TO_I):{
      return("GL_PIXEL_MAP_I_TO_I");
    }
    case(GL_PIXEL_MAP_I_TO_R):{
      return("GL_PIXEL_MAP_I_TO_R");
    }
    case(GL_PIXEL_MAP_R_TO_R):{
      return("GL_PIXEL_MAP_R_TO_R");
    }
    case(GL_PIXEL_MAP_S_TO_S):{
      return("GL_PIXEL_MAP_S_TO_S");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_PIXEL_MAP_TO_STRING__ */

#ifdef __GL_RENDERING_MODE_TO_STRING__
char * __attribute__((const)) gl_rendering_mode_to_string(GLenum val){
  switch(val){
    case(GL_FEEDBACK):{
      return("GL_FEEDBACK");
    }
    case(GL_RENDER):{
      return("GL_RENDER");
    }
    case(GL_SELECT):{
      return("GL_SELECT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_RENDERING_MODE_TO_STRING__ */

#ifdef __GL_PATH_RENDERING_MASK_NV_TO_STRING__
char * __attribute__((const)) gl_path_rendering_mask_NV_to_string(GLenum val){
  switch(val){
    case(GL_BOLD_BIT_NV):{
      return("GL_BOLD_BIT_NV");
    }
    case(GL_ITALIC_BIT_NV):{
      return("GL_ITALIC_BIT_NV");
    }
    case(GL_GLYPH_WIDTH_BIT_NV):{
      return("GL_GLYPH_WIDTH_BIT_NV");
    }
    case(GL_GLYPH_HEIGHT_BIT_NV):{
      return("GL_GLYPH_HEIGHT_BIT_NV");
    }
    case(GL_GLYPH_HORIZONTAL_BEARING_X_BIT_NV):{
      return("GL_GLYPH_HORIZONTAL_BEARING_X_BIT_NV");
    }
    case(GL_GLYPH_HORIZONTAL_BEARING_Y_BIT_NV):{
      return("GL_GLYPH_HORIZONTAL_BEARING_Y_BIT_NV");
    }
    case(GL_GLYPH_HORIZONTAL_BEARING_ADVANCE_BIT_NV):{
      return("GL_GLYPH_HORIZONTAL_BEARING_ADVANCE_BIT_NV");
    }
    case(GL_GLYPH_VERTICAL_BEARING_X_BIT_NV):{
      return("GL_GLYPH_VERTICAL_BEARING_X_BIT_NV");
    }
    case(GL_GLYPH_VERTICAL_BEARING_Y_BIT_NV):{
      return("GL_GLYPH_VERTICAL_BEARING_Y_BIT_NV");
    }
    case(GL_GLYPH_VERTICAL_BEARING_ADVANCE_BIT_NV):{
      return("GL_GLYPH_VERTICAL_BEARING_ADVANCE_BIT_NV");
    }
    case(GL_GLYPH_HAS_KERNING_BIT_NV):{
      return("GL_GLYPH_HAS_KERNING_BIT_NV");
    }
    case(GL_FONT_X_MIN_BOUNDS_BIT_NV):{
      return("GL_FONT_X_MIN_BOUNDS_BIT_NV");
    }
    case(GL_FONT_Y_MIN_BOUNDS_BIT_NV):{
      return("GL_FONT_Y_MIN_BOUNDS_BIT_NV");
    }
    case(GL_FONT_X_MAX_BOUNDS_BIT_NV):{
      return("GL_FONT_X_MAX_BOUNDS_BIT_NV");
    }
    case(GL_FONT_Y_MAX_BOUNDS_BIT_NV):{
      return("GL_FONT_Y_MAX_BOUNDS_BIT_NV");
    }
    case(GL_FONT_UNITS_PER_EM_BIT_NV):{
      return("GL_FONT_UNITS_PER_EM_BIT_NV");
    }
    case(GL_FONT_ASCENDER_BIT_NV):{
      return("GL_FONT_ASCENDER_BIT_NV");
    }
    case(GL_FONT_DESCENDER_BIT_NV):{
      return("GL_FONT_DESCENDER_BIT_NV");
    }
    case(GL_FONT_HEIGHT_BIT_NV):{
      return("GL_FONT_HEIGHT_BIT_NV");
    }
    case(GL_FONT_MAX_ADVANCE_WIDTH_BIT_NV):{
      return("GL_FONT_MAX_ADVANCE_WIDTH_BIT_NV");
    }
    case(GL_FONT_MAX_ADVANCE_HEIGHT_BIT_NV):{
      return("GL_FONT_MAX_ADVANCE_HEIGHT_BIT_NV");
    }
    case(GL_FONT_UNDERLINE_POSITION_BIT_NV):{
      return("GL_FONT_UNDERLINE_POSITION_BIT_NV");
    }
    case(GL_FONT_UNDERLINE_THICKNESS_BIT_NV):{
      return("GL_FONT_UNDERLINE_THICKNESS_BIT_NV");
    }
    case(GL_FONT_HAS_KERNING_BIT_NV):{
      return("GL_FONT_HAS_KERNING_BIT_NV");
    }
    case(GL_FONT_NUM_GLYPH_INDICES_BIT_NV):{
      return("GL_FONT_NUM_GLYPH_INDICES_BIT_NV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PATH_RENDERING_MASK_NV_TO_STRING__ */

#ifdef __GL_CONTAINER_TYPE_TO_STRING__
char * __attribute__((const)) gl_container_type_to_string(GLenum val){
  switch(val){

    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CONTAINER_TYPE_TO_STRING__ */

#ifdef __GL_INTERLEAVED_ARRAY_FORMAT_TO_STRING__
char * __attribute__((const)) gl_interleaved_array_format_to_string(GLenum val){
  switch(val){
    case(GL_C3F_V3F):{
      return("GL_C3F_V3F");
    }
    case(GL_C4F_N3F_V3F):{
      return("GL_C4F_N3F_V3F");
    }
    case(GL_C4UB_V2F):{
      return("GL_C4UB_V2F");
    }
    case(GL_C4UB_V3F):{
      return("GL_C4UB_V3F");
    }
    case(GL_N3F_V3F):{
      return("GL_N3F_V3F");
    }
    case(GL_T2F_C3F_V3F):{
      return("GL_T2F_C3F_V3F");
    }
    case(GL_T2F_C4F_N3F_V3F):{
      return("GL_T2F_C4F_N3F_V3F");
    }
    case(GL_T2F_C4UB_V3F):{
      return("GL_T2F_C4UB_V3F");
    }
    case(GL_T2F_N3F_V3F):{
      return("GL_T2F_N3F_V3F");
    }
    case(GL_T2F_V3F):{
      return("GL_T2F_V3F");
    }
    case(GL_T4F_C4F_N3F_V4F):{
      return("GL_T4F_C4F_N3F_V4F");
    }
    case(GL_T4F_V4F):{
      return("GL_T4F_V4F");
    }
    case(GL_V2F):{
      return("GL_V2F");
    }
    case(GL_V3F):{
      return("GL_V3F");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_INTERLEAVED_ARRAY_FORMAT_TO_STRING__ */

#ifdef __GL_LIST_MODE_TO_STRING__
char * __attribute__((const)) gl_list_mode_to_string(GLenum val){
  switch(val){
    case(GL_COMPILE):{
      return("GL_COMPILE");
    }
    case(GL_COMPILE_AND_EXECUTE):{
      return("GL_COMPILE_AND_EXECUTE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIST_MODE_TO_STRING__ */

#ifdef __GL_TEX_COORD_POINTER_TYPE_TO_STRING__
char * __attribute__((const)) gl_tex_coord_pointer_type_to_string(GLenum val){
  switch(val){
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEX_COORD_POINTER_TYPE_TO_STRING__ */

#ifdef __GL_STRING_NAME_TO_STRING__
char * __attribute__((const)) gl_string_name_to_string(GLenum val){
  switch(val){
    case(GL_RENDERER):{
      return("GL_RENDERER");
    }
    case(GL_VENDOR):{
      return("GL_VENDOR");
    }
    case(GL_VERSION):{
      return("GL_VERSION");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_STRING_NAME_TO_STRING__ */

#ifdef __GL_COMMAND_OPCODES_NV_TO_STRING__
char * __attribute__((const)) gl_command_opcodes_NV_to_string(GLenum val){
  switch(val){
    case(GL_TERMINATE_SEQUENCE_COMMAND_NV):{
      return("GL_TERMINATE_SEQUENCE_COMMAND_NV");
    }
    case(GL_NOP_COMMAND_NV):{
      return("GL_NOP_COMMAND_NV");
    }
    case(GL_DRAW_ELEMENTS_COMMAND_NV):{
      return("GL_DRAW_ELEMENTS_COMMAND_NV");
    }
    case(GL_DRAW_ARRAYS_COMMAND_NV):{
      return("GL_DRAW_ARRAYS_COMMAND_NV");
    }
    case(GL_DRAW_ELEMENTS_STRIP_COMMAND_NV):{
      return("GL_DRAW_ELEMENTS_STRIP_COMMAND_NV");
    }
    case(GL_DRAW_ARRAYS_STRIP_COMMAND_NV):{
      return("GL_DRAW_ARRAYS_STRIP_COMMAND_NV");
    }
    case(GL_DRAW_ELEMENTS_INSTANCED_COMMAND_NV):{
      return("GL_DRAW_ELEMENTS_INSTANCED_COMMAND_NV");
    }
    case(GL_DRAW_ARRAYS_INSTANCED_COMMAND_NV):{
      return("GL_DRAW_ARRAYS_INSTANCED_COMMAND_NV");
    }
    case(GL_ELEMENT_ADDRESS_COMMAND_NV):{
      return("GL_ELEMENT_ADDRESS_COMMAND_NV");
    }
    case(GL_ATTRIBUTE_ADDRESS_COMMAND_NV):{
      return("GL_ATTRIBUTE_ADDRESS_COMMAND_NV");
    }
    case(GL_UNIFORM_ADDRESS_COMMAND_NV):{
      return("GL_UNIFORM_ADDRESS_COMMAND_NV");
    }
    case(GL_BLEND_COLOR_COMMAND_NV):{
      return("GL_BLEND_COLOR_COMMAND_NV");
    }
    case(GL_STENCIL_REF_COMMAND_NV):{
      return("GL_STENCIL_REF_COMMAND_NV");
    }
    case(GL_LINE_WIDTH_COMMAND_NV):{
      return("GL_LINE_WIDTH_COMMAND_NV");
    }
    case(GL_POLYGON_OFFSET_COMMAND_NV):{
      return("GL_POLYGON_OFFSET_COMMAND_NV");
    }
    case(GL_ALPHA_REF_COMMAND_NV):{
      return("GL_ALPHA_REF_COMMAND_NV");
    }
    case(GL_VIEWPORT_COMMAND_NV):{
      return("GL_VIEWPORT_COMMAND_NV");
    }
    case(GL_SCISSOR_COMMAND_NV):{
      return("GL_SCISSOR_COMMAND_NV");
    }
    case(GL_FRONT_FACE_COMMAND_NV):{
      return("GL_FRONT_FACE_COMMAND_NV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_COMMAND_OPCODES_NV_TO_STRING__ */

#ifdef __GL_PIXEL_STORE_SUBSAMPLE_RATE_TO_STRING__
char * __attribute__((const)) gl_pixel_store_subsample_rate_to_string(GLenum val){
  switch(val){
    case(GL_PIXEL_SUBSAMPLE_2424_SGIX):{
      return("GL_PIXEL_SUBSAMPLE_2424_SGIX");
    }
    case(GL_PIXEL_SUBSAMPLE_4242_SGIX):{
      return("GL_PIXEL_SUBSAMPLE_4242_SGIX");
    }
    case(GL_PIXEL_SUBSAMPLE_4444_SGIX):{
      return("GL_PIXEL_SUBSAMPLE_4444_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_STORE_SUBSAMPLE_RATE_TO_STRING__ */

#ifdef __GL_GET_TEXTURE_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_get_texture_parameter_to_string(GLenum val){
  switch(val){
    case(GL_DETAIL_TEXTURE_FUNC_POINTS_SGIS):{
      return("GL_DETAIL_TEXTURE_FUNC_POINTS_SGIS");
    }
    case(GL_DETAIL_TEXTURE_LEVEL_SGIS):{
      return("GL_DETAIL_TEXTURE_LEVEL_SGIS");
    }
    case(GL_DETAIL_TEXTURE_MODE_SGIS):{
      return("GL_DETAIL_TEXTURE_MODE_SGIS");
    }
    case(GL_DUAL_TEXTURE_SELECT_SGIS):{
      return("GL_DUAL_TEXTURE_SELECT_SGIS");
    }
    case(GL_GENERATE_MIPMAP_SGIS):{
      return("GL_GENERATE_MIPMAP_SGIS");
    }
    case(GL_POST_TEXTURE_FILTER_BIAS_SGIX):{
      return("GL_POST_TEXTURE_FILTER_BIAS_SGIX");
    }
    case(GL_POST_TEXTURE_FILTER_SCALE_SGIX):{
      return("GL_POST_TEXTURE_FILTER_SCALE_SGIX");
    }
    case(GL_QUAD_TEXTURE_SELECT_SGIS):{
      return("GL_QUAD_TEXTURE_SELECT_SGIS");
    }
    case(GL_SHADOW_AMBIENT_SGIX):{
      return("GL_SHADOW_AMBIENT_SGIX");
    }
    case(GL_SHARPEN_TEXTURE_FUNC_POINTS_SGIS):{
      return("GL_SHARPEN_TEXTURE_FUNC_POINTS_SGIS");
    }
    case(GL_TEXTURE_4DSIZE_SGIS):{
      return("GL_TEXTURE_4DSIZE_SGIS");
    }
    case(GL_TEXTURE_ALPHA_SIZE):{
      return("GL_TEXTURE_ALPHA_SIZE");
    }
    case(GL_TEXTURE_BASE_LEVEL_SGIS):{
      return("GL_TEXTURE_BASE_LEVEL_SGIS");
    }
    case(GL_TEXTURE_BLUE_SIZE):{
      return("GL_TEXTURE_BLUE_SIZE");
    }
    case(GL_TEXTURE_BORDER):{
      return("GL_TEXTURE_BORDER");
    }
    case(GL_TEXTURE_BORDER_COLOR):{
      return("GL_TEXTURE_BORDER_COLOR");
    }
    case(GL_TEXTURE_BORDER_COLOR_NV):{
      return("GL_TEXTURE_BORDER_COLOR_NV");
    }
    case(GL_TEXTURE_CLIPMAP_CENTER_SGIX):{
      return("GL_TEXTURE_CLIPMAP_CENTER_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_DEPTH_SGIX):{
      return("GL_TEXTURE_CLIPMAP_DEPTH_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_FRAME_SGIX):{
      return("GL_TEXTURE_CLIPMAP_FRAME_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_LOD_OFFSET_SGIX):{
      return("GL_TEXTURE_CLIPMAP_LOD_OFFSET_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_OFFSET_SGIX):{
      return("GL_TEXTURE_CLIPMAP_OFFSET_SGIX");
    }
    case(GL_TEXTURE_CLIPMAP_VIRTUAL_DEPTH_SGIX):{
      return("GL_TEXTURE_CLIPMAP_VIRTUAL_DEPTH_SGIX");
    }
    case(GL_TEXTURE_COMPARE_OPERATOR_SGIX):{
      return("GL_TEXTURE_COMPARE_OPERATOR_SGIX");
    }
    case(GL_TEXTURE_COMPARE_SGIX):{
      return("GL_TEXTURE_COMPARE_SGIX");
    }
    case(GL_TEXTURE_COMPONENTS):{
      return("GL_TEXTURE_COMPONENTS");
    }
    case(GL_TEXTURE_FILTER4_SIZE_SGIS):{
      return("GL_TEXTURE_FILTER4_SIZE_SGIS");
    }
    case(GL_TEXTURE_GEQUAL_R_SGIX):{
      return("GL_TEXTURE_GEQUAL_R_SGIX");
    }
    case(GL_TEXTURE_GREEN_SIZE):{
      return("GL_TEXTURE_GREEN_SIZE");
    }
    case(GL_TEXTURE_HEIGHT):{
      return("GL_TEXTURE_HEIGHT");
    }
    case(GL_TEXTURE_INTENSITY_SIZE):{
      return("GL_TEXTURE_INTENSITY_SIZE");
    }
    case(GL_TEXTURE_INTERNAL_FORMAT):{
      return("GL_TEXTURE_INTERNAL_FORMAT");
    }
    case(GL_TEXTURE_LEQUAL_R_SGIX):{
      return("GL_TEXTURE_LEQUAL_R_SGIX");
    }
    case(GL_TEXTURE_LOD_BIAS_R_SGIX):{
      return("GL_TEXTURE_LOD_BIAS_R_SGIX");
    }
    case(GL_TEXTURE_LOD_BIAS_S_SGIX):{
      return("GL_TEXTURE_LOD_BIAS_S_SGIX");
    }
    case(GL_TEXTURE_LOD_BIAS_T_SGIX):{
      return("GL_TEXTURE_LOD_BIAS_T_SGIX");
    }
    case(GL_TEXTURE_LUMINANCE_SIZE):{
      return("GL_TEXTURE_LUMINANCE_SIZE");
    }
    case(GL_TEXTURE_MAG_FILTER):{
      return("GL_TEXTURE_MAG_FILTER");
    }
    case(GL_TEXTURE_MAX_CLAMP_R_SGIX):{
      return("GL_TEXTURE_MAX_CLAMP_R_SGIX");
    }
    case(GL_TEXTURE_MAX_CLAMP_S_SGIX):{
      return("GL_TEXTURE_MAX_CLAMP_S_SGIX");
    }
    case(GL_TEXTURE_MAX_CLAMP_T_SGIX):{
      return("GL_TEXTURE_MAX_CLAMP_T_SGIX");
    }
    case(GL_TEXTURE_MAX_LEVEL_SGIS):{
      return("GL_TEXTURE_MAX_LEVEL_SGIS");
    }
    case(GL_TEXTURE_MAX_LOD_SGIS):{
      return("GL_TEXTURE_MAX_LOD_SGIS");
    }
    case(GL_TEXTURE_MIN_FILTER):{
      return("GL_TEXTURE_MIN_FILTER");
    }
    case(GL_TEXTURE_MIN_LOD_SGIS):{
      return("GL_TEXTURE_MIN_LOD_SGIS");
    }
    case(GL_TEXTURE_PRIORITY):{
      return("GL_TEXTURE_PRIORITY");
    }
    case(GL_TEXTURE_RED_SIZE):{
      return("GL_TEXTURE_RED_SIZE");
    }
    case(GL_TEXTURE_RESIDENT):{
      return("GL_TEXTURE_RESIDENT");
    }
    case(GL_TEXTURE_WIDTH):{
      return("GL_TEXTURE_WIDTH");
    }
    case(GL_TEXTURE_WRAP_Q_SGIS):{
      return("GL_TEXTURE_WRAP_Q_SGIS");
    }
    case(GL_TEXTURE_WRAP_S):{
      return("GL_TEXTURE_WRAP_S");
    }
    case(GL_TEXTURE_WRAP_T):{
      return("GL_TEXTURE_WRAP_T");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_TEXTURE_PARAMETER_TO_STRING__ */

#ifdef __GL_CLIENT_ATTRIB_MASK_TO_STRING__
char * __attribute__((const)) gl_client_attrib_mask_to_string(GLenum val){
  switch(val){
    case(GL_CLIENT_ALL_ATTRIB_BITS):{
      return("GL_CLIENT_ALL_ATTRIB_BITS");
    }
    case(GL_CLIENT_PIXEL_STORE_BIT):{
      return("GL_CLIENT_PIXEL_STORE_BIT");
    }
    case(GL_CLIENT_VERTEX_ARRAY_BIT):{
      return("GL_CLIENT_VERTEX_ARRAY_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CLIENT_ATTRIB_MASK_TO_STRING__ */

#ifdef __GL_BLENDING_FACTOR_SRC_TO_STRING__
char * __attribute__((const)) gl_blending_factor_src_to_string(GLenum val){
  switch(val){
    case(GL_DST_ALPHA):{
      return("GL_DST_ALPHA");
    }
    case(GL_DST_COLOR):{
      return("GL_DST_COLOR");
    }
    case(GL_ONE):{
      return("GL_ONE");
    }
    case(GL_ONE_MINUS_DST_ALPHA):{
      return("GL_ONE_MINUS_DST_ALPHA");
    }
    case(GL_ONE_MINUS_DST_COLOR):{
      return("GL_ONE_MINUS_DST_COLOR");
    }
    case(GL_ONE_MINUS_SRC_ALPHA):{
      return("GL_ONE_MINUS_SRC_ALPHA");
    }
    case(GL_SRC_ALPHA):{
      return("GL_SRC_ALPHA");
    }
    case(GL_SRC_ALPHA_SATURATE):{
      return("GL_SRC_ALPHA_SATURATE");
    }
    case(GL_ZERO):{
      return("GL_ZERO");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_BLENDING_FACTOR_SRC_TO_STRING__ */

#ifdef __GL_SHADER_TYPE_TO_STRING__
char * __attribute__((const)) gl_shader_type_to_string(GLenum val){
  switch(val){
    case(GL_FRAGMENT_SHADER):{
      return("GL_FRAGMENT_SHADER");
    }
    case(GL_VERTEX_SHADER):{
      return("GL_VERTEX_SHADER");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_SHADER_TYPE_TO_STRING__ */

#ifdef __GL_GET_POINTERV_PNAME_TO_STRING__
char * __attribute__((const)) gl_get_pointerv_PName_to_string(GLenum val){
  switch(val){
    case(GL_COLOR_ARRAY_POINTER):{
      return("GL_COLOR_ARRAY_POINTER");
    }
    case(GL_EDGE_FLAG_ARRAY_POINTER):{
      return("GL_EDGE_FLAG_ARRAY_POINTER");
    }
    case(GL_FEEDBACK_BUFFER_POINTER):{
      return("GL_FEEDBACK_BUFFER_POINTER");
    }
    case(GL_INDEX_ARRAY_POINTER):{
      return("GL_INDEX_ARRAY_POINTER");
    }
    case(GL_INSTRUMENT_BUFFER_POINTER_SGIX):{
      return("GL_INSTRUMENT_BUFFER_POINTER_SGIX");
    }
    case(GL_NORMAL_ARRAY_POINTER):{
      return("GL_NORMAL_ARRAY_POINTER");
    }
    case(GL_SELECTION_BUFFER_POINTER):{
      return("GL_SELECTION_BUFFER_POINTER");
    }
    case(GL_TEXTURE_COORD_ARRAY_POINTER):{
      return("GL_TEXTURE_COORD_ARRAY_POINTER");
    }
    case(GL_VERTEX_ARRAY_POINTER):{
      return("GL_VERTEX_ARRAY_POINTER");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_POINTERV_PNAME_TO_STRING__ */

#ifdef __GL_POLYGON_MODE_TO_STRING__
char * __attribute__((const)) gl_polygon_mode_to_string(GLenum val){
  switch(val){
    case(GL_FILL):{
      return("GL_FILL");
    }
    case(GL_LINE):{
      return("GL_LINE");
    }
    case(GL_POINT):{
      return("GL_POINT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_POLYGON_MODE_TO_STRING__ */

#ifdef __GL_GET_MINMAX_PARAMETER_PNAME_EXT_TO_STRING__
char * __attribute__((const)) gl_get_minmax_parameter_PName_EXT_to_string(GLenum val){
  switch(val){
    case(GL_MINMAX_FORMAT):{
      return("GL_MINMAX_FORMAT");
    }
    case(GL_MINMAX_SINK):{
      return("GL_MINMAX_SINK");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_MINMAX_PARAMETER_PNAME_EXT_TO_STRING__ */

#ifdef __GL_BUFFER_BIT_QCOM_TO_STRING__
char * __attribute__((const)) gl_buffer_bit_QCOM_to_string(GLenum val){
  switch(val){
    case(GL_COLOR_BUFFER_BIT0_QCOM):{
      return("GL_COLOR_BUFFER_BIT0_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT1_QCOM):{
      return("GL_COLOR_BUFFER_BIT1_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT2_QCOM):{
      return("GL_COLOR_BUFFER_BIT2_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT3_QCOM):{
      return("GL_COLOR_BUFFER_BIT3_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT4_QCOM):{
      return("GL_COLOR_BUFFER_BIT4_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT5_QCOM):{
      return("GL_COLOR_BUFFER_BIT5_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT6_QCOM):{
      return("GL_COLOR_BUFFER_BIT6_QCOM");
    }
    case(GL_COLOR_BUFFER_BIT7_QCOM):{
      return("GL_COLOR_BUFFER_BIT7_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT0_QCOM):{
      return("GL_DEPTH_BUFFER_BIT0_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT1_QCOM):{
      return("GL_DEPTH_BUFFER_BIT1_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT2_QCOM):{
      return("GL_DEPTH_BUFFER_BIT2_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT3_QCOM):{
      return("GL_DEPTH_BUFFER_BIT3_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT4_QCOM):{
      return("GL_DEPTH_BUFFER_BIT4_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT5_QCOM):{
      return("GL_DEPTH_BUFFER_BIT5_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT6_QCOM):{
      return("GL_DEPTH_BUFFER_BIT6_QCOM");
    }
    case(GL_DEPTH_BUFFER_BIT7_QCOM):{
      return("GL_DEPTH_BUFFER_BIT7_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT0_QCOM):{
      return("GL_STENCIL_BUFFER_BIT0_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT1_QCOM):{
      return("GL_STENCIL_BUFFER_BIT1_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT2_QCOM):{
      return("GL_STENCIL_BUFFER_BIT2_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT3_QCOM):{
      return("GL_STENCIL_BUFFER_BIT3_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT4_QCOM):{
      return("GL_STENCIL_BUFFER_BIT4_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT5_QCOM):{
      return("GL_STENCIL_BUFFER_BIT5_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT6_QCOM):{
      return("GL_STENCIL_BUFFER_BIT6_QCOM");
    }
    case(GL_STENCIL_BUFFER_BIT7_QCOM):{
      return("GL_STENCIL_BUFFER_BIT7_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT0_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT0_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT1_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT1_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT2_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT2_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT3_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT3_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT4_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT4_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT5_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT5_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT6_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT6_QCOM");
    }
    case(GL_MULTISAMPLE_BUFFER_BIT7_QCOM):{
      return("GL_MULTISAMPLE_BUFFER_BIT7_QCOM");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_BUFFER_BIT_QCOM_TO_STRING__ */

#ifdef __GL_MAP_BUFFER_USAGE_MASK_TO_STRING__
char * __attribute__((const)) gl_map_buffer_usage_mask_to_string(GLenum val){
  switch(val){
    case(GL_CLIENT_STORAGE_BIT):{
      return("GL_CLIENT_STORAGE_BIT");
    }
    case(GL_DYNAMIC_STORAGE_BIT):{
      return("GL_DYNAMIC_STORAGE_BIT");
    }
    case(GL_MAP_COHERENT_BIT):{
      return("GL_MAP_COHERENT_BIT");
    }
    case(GL_MAP_FLUSH_EXPLICIT_BIT):{
      return("GL_MAP_FLUSH_EXPLICIT_BIT");
    }
    case(GL_MAP_INVALIDATE_BUFFER_BIT):{
      return("GL_MAP_INVALIDATE_BUFFER_BIT");
    }
    case(GL_MAP_INVALIDATE_RANGE_BIT):{
      return("GL_MAP_INVALIDATE_RANGE_BIT");
    }
    case(GL_MAP_PERSISTENT_BIT):{
      return("GL_MAP_PERSISTENT_BIT");
    }
    case(GL_MAP_READ_BIT):{
      return("GL_MAP_READ_BIT");
    }
    case(GL_MAP_UNSYNCHRONIZED_BIT):{
      return("GL_MAP_UNSYNCHRONIZED_BIT");
    }
    case(GL_MAP_WRITE_BIT):{
      return("GL_MAP_WRITE_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MAP_BUFFER_USAGE_MASK_TO_STRING__ */

#ifdef __GL_TRIANGLE_LIST_SUN_TO_STRING__
char * __attribute__((const)) gl_triangle_list_SUN_to_string(GLenum val){
  switch(val){
    case(GL_RESTART_SUN):{
      return("GL_RESTART_SUN");
    }
    case(GL_REPLACE_MIDDLE_SUN):{
      return("GL_REPLACE_MIDDLE_SUN");
    }
    case(GL_REPLACE_OLDEST_SUN):{
      return("GL_REPLACE_OLDEST_SUN");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TRIANGLE_LIST_SUN_TO_STRING__ */

#ifdef __GL_NORMAL_POINTER_TYPE_TO_STRING__
char * __attribute__((const)) gl_normal_pointer_type_to_string(GLenum val){
  switch(val){
    case(GL_BYTE):{
      return("GL_BYTE");
    }
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_NORMAL_POINTER_TYPE_TO_STRING__ */

#ifdef __GL_TEXTURE_ENV_MODE_TO_STRING__
char * __attribute__((const)) gl_texture_env_mode_to_string(GLenum val){
  switch(val){
    case(GL_ADD):{
      return("GL_ADD");
    }
    case(GL_BLEND):{
      return("GL_BLEND");
    }
    case(GL_DECAL):{
      return("GL_DECAL");
    }
    case(GL_MODULATE):{
      return("GL_MODULATE");
    }
    case(GL_TEXTURE_ENV_BIAS_SGIX):{
      return("GL_TEXTURE_ENV_BIAS_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_ENV_MODE_TO_STRING__ */

#ifdef __GL_PIXEL_STORE_RESAMPLE_MODE_TO_STRING__
char * __attribute__((const)) gl_pixel_store_resample_mode_to_string(GLenum val){
  switch(val){
    case(GL_RESAMPLE_DECIMATE_SGIX):{
      return("GL_RESAMPLE_DECIMATE_SGIX");
    }
    case(GL_RESAMPLE_REPLICATE_SGIX):{
      return("GL_RESAMPLE_REPLICATE_SGIX");
    }
    case(GL_RESAMPLE_ZERO_FILL_SGIX):{
      return("GL_RESAMPLE_ZERO_FILL_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_STORE_RESAMPLE_MODE_TO_STRING__ */

#ifdef __GL_PATH_RENDERING_TOKEN_NV_TO_STRING__
char * __attribute__((const)) gl_path_rendering_token_NV_to_string(GLenum val){
  switch(val){
    case(GL_CLOSE_PATH_NV):{
      return("GL_CLOSE_PATH_NV");
    }
    case(GL_MOVE_TO_NV):{
      return("GL_MOVE_TO_NV");
    }
    case(GL_RELATIVE_MOVE_TO_NV):{
      return("GL_RELATIVE_MOVE_TO_NV");
    }
    case(GL_LINE_TO_NV):{
      return("GL_LINE_TO_NV");
    }
    case(GL_RELATIVE_LINE_TO_NV):{
      return("GL_RELATIVE_LINE_TO_NV");
    }
    case(GL_HORIZONTAL_LINE_TO_NV):{
      return("GL_HORIZONTAL_LINE_TO_NV");
    }
    case(GL_RELATIVE_HORIZONTAL_LINE_TO_NV):{
      return("GL_RELATIVE_HORIZONTAL_LINE_TO_NV");
    }
    case(GL_VERTICAL_LINE_TO_NV):{
      return("GL_VERTICAL_LINE_TO_NV");
    }
    case(GL_RELATIVE_VERTICAL_LINE_TO_NV):{
      return("GL_RELATIVE_VERTICAL_LINE_TO_NV");
    }
    case(GL_QUADRATIC_CURVE_TO_NV):{
      return("GL_QUADRATIC_CURVE_TO_NV");
    }
    case(GL_RELATIVE_QUADRATIC_CURVE_TO_NV):{
      return("GL_RELATIVE_QUADRATIC_CURVE_TO_NV");
    }
    case(GL_CUBIC_CURVE_TO_NV):{
      return("GL_CUBIC_CURVE_TO_NV");
    }
    case(GL_RELATIVE_CUBIC_CURVE_TO_NV):{
      return("GL_RELATIVE_CUBIC_CURVE_TO_NV");
    }
    case(GL_SMOOTH_QUADRATIC_CURVE_TO_NV):{
      return("GL_SMOOTH_QUADRATIC_CURVE_TO_NV");
    }
    case(GL_RELATIVE_SMOOTH_QUADRATIC_CURVE_TO_NV):{
      return("GL_RELATIVE_SMOOTH_QUADRATIC_CURVE_TO_NV");
    }
    case(GL_SMOOTH_CUBIC_CURVE_TO_NV):{
      return("GL_SMOOTH_CUBIC_CURVE_TO_NV");
    }
    case(GL_RELATIVE_SMOOTH_CUBIC_CURVE_TO_NV):{
      return("GL_RELATIVE_SMOOTH_CUBIC_CURVE_TO_NV");
    }
    case(GL_SMALL_CCW_ARC_TO_NV):{
      return("GL_SMALL_CCW_ARC_TO_NV");
    }
    case(GL_RELATIVE_SMALL_CCW_ARC_TO_NV):{
      return("GL_RELATIVE_SMALL_CCW_ARC_TO_NV");
    }
    case(GL_SMALL_CW_ARC_TO_NV):{
      return("GL_SMALL_CW_ARC_TO_NV");
    }
    case(GL_RELATIVE_SMALL_CW_ARC_TO_NV):{
      return("GL_RELATIVE_SMALL_CW_ARC_TO_NV");
    }
    case(GL_LARGE_CCW_ARC_TO_NV):{
      return("GL_LARGE_CCW_ARC_TO_NV");
    }
    case(GL_RELATIVE_LARGE_CCW_ARC_TO_NV):{
      return("GL_RELATIVE_LARGE_CCW_ARC_TO_NV");
    }
    case(GL_LARGE_CW_ARC_TO_NV):{
      return("GL_LARGE_CW_ARC_TO_NV");
    }
    case(GL_RELATIVE_LARGE_CW_ARC_TO_NV):{
      return("GL_RELATIVE_LARGE_CW_ARC_TO_NV");
    }
    case(GL_CONIC_CURVE_TO_NV):{
      return("GL_CONIC_CURVE_TO_NV");
    }
    case(GL_RELATIVE_CONIC_CURVE_TO_NV):{
      return("GL_RELATIVE_CONIC_CURVE_TO_NV");
    }
    case(GL_SHARED_EDGE_NV):{
      return("GL_SHARED_EDGE_NV");
    }
    case(GL_ROUNDED_RECT_NV):{
      return("GL_ROUNDED_RECT_NV");
    }
    case(GL_RELATIVE_ROUNDED_RECT_NV):{
      return("GL_RELATIVE_ROUNDED_RECT_NV");
    }
    case(GL_ROUNDED_RECT2_NV):{
      return("GL_ROUNDED_RECT2_NV");
    }
    case(GL_RELATIVE_ROUNDED_RECT2_NV):{
      return("GL_RELATIVE_ROUNDED_RECT2_NV");
    }
    case(GL_ROUNDED_RECT4_NV):{
      return("GL_ROUNDED_RECT4_NV");
    }
    case(GL_RELATIVE_ROUNDED_RECT4_NV):{
      return("GL_RELATIVE_ROUNDED_RECT4_NV");
    }
    case(GL_ROUNDED_RECT8_NV):{
      return("GL_ROUNDED_RECT8_NV");
    }
    case(GL_RELATIVE_ROUNDED_RECT8_NV):{
      return("GL_RELATIVE_ROUNDED_RECT8_NV");
    }
    case(GL_RESTART_PATH_NV):{
      return("GL_RESTART_PATH_NV");
    }
    case(GL_DUP_FIRST_CUBIC_CURVE_TO_NV):{
      return("GL_DUP_FIRST_CUBIC_CURVE_TO_NV");
    }
    case(GL_DUP_LAST_CUBIC_CURVE_TO_NV):{
      return("GL_DUP_LAST_CUBIC_CURVE_TO_NV");
    }
    case(GL_RECT_NV):{
      return("GL_RECT_NV");
    }
    case(GL_RELATIVE_RECT_NV):{
      return("GL_RELATIVE_RECT_NV");
    }
    case(GL_CIRCULAR_CCW_ARC_TO_NV):{
      return("GL_CIRCULAR_CCW_ARC_TO_NV");
    }
    case(GL_CIRCULAR_CW_ARC_TO_NV):{
      return("GL_CIRCULAR_CW_ARC_TO_NV");
    }
    case(GL_CIRCULAR_TANGENT_ARC_TO_NV):{
      return("GL_CIRCULAR_TANGENT_ARC_TO_NV");
    }
    case(GL_ARC_TO_NV):{
      return("GL_ARC_TO_NV");
    }
    case(GL_RELATIVE_ARC_TO_NV):{
      return("GL_RELATIVE_ARC_TO_NV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PATH_RENDERING_TOKEN_NV_TO_STRING__ */

#ifdef __GL_MESH_MODE2_TO_STRING__
char * __attribute__((const)) gl_mesh_mode2_to_string(GLenum val){
  switch(val){
    case(GL_FILL):{
      return("GL_FILL");
    }
    case(GL_LINE):{
      return("GL_LINE");
    }
    case(GL_POINT):{
      return("GL_POINT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MESH_MODE2_TO_STRING__ */

#ifdef __GL_GET_MAP_QUERY_TO_STRING__
char * __attribute__((const)) gl_get_map_query_to_string(GLenum val){
  switch(val){
    case(GL_COEFF):{
      return("GL_COEFF");
    }
    case(GL_DOMAIN):{
      return("GL_DOMAIN");
    }
    case(GL_ORDER):{
      return("GL_ORDER");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_MAP_QUERY_TO_STRING__ */

#ifdef __GL_BLENDING_FACTOR_DEST_TO_STRING__
char * __attribute__((const)) gl_blending_factor_dest_to_string(GLenum val){
  switch(val){
    case(GL_DST_ALPHA):{
      return("GL_DST_ALPHA");
    }
    case(GL_ONE):{
      return("GL_ONE");
    }
    case(GL_ONE_MINUS_DST_ALPHA):{
      return("GL_ONE_MINUS_DST_ALPHA");
    }
    case(GL_ONE_MINUS_SRC_ALPHA):{
      return("GL_ONE_MINUS_SRC_ALPHA");
    }
    case(GL_ONE_MINUS_SRC_COLOR):{
      return("GL_ONE_MINUS_SRC_COLOR");
    }
    case(GL_SRC_ALPHA):{
      return("GL_SRC_ALPHA");
    }
    case(GL_SRC_COLOR):{
      return("GL_SRC_COLOR");
    }
    case(GL_ZERO):{
      return("GL_ZERO");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_BLENDING_FACTOR_DEST_TO_STRING__ */

#ifdef __GL_GET_HISTOGRAM_PARAMETER_PNAME_EXT_TO_STRING__
char * __attribute__((const)) gl_get_histogram_parameter_PName_EXT_to_string(GLenum val){
  switch(val){

    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_HISTOGRAM_PARAMETER_PNAME_EXT_TO_STRING__ */

#ifdef __GL_MATERIAL_FACE_TO_STRING__
char * __attribute__((const)) gl_material_face_to_string(GLenum val){
  switch(val){
    case(GL_BACK):{
      return("GL_BACK");
    }
    case(GL_FRONT):{
      return("GL_FRONT");
    }
    case(GL_FRONT_AND_BACK):{
      return("GL_FRONT_AND_BACK");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MATERIAL_FACE_TO_STRING__ */

#ifdef __GL_FRAGMENT_SHADER_DEST_MOD_MASK_ATI_TO_STRING__
char * __attribute__((const)) gl_fragment_shader_dest_mod_mask_ATI_to_string(GLenum val){
  switch(val){
    case(GL_2X_BIT_ATI):{
      return("GL_2X_BIT_ATI");
    }
    case(GL_4X_BIT_ATI):{
      return("GL_4X_BIT_ATI");
    }
    case(GL_8X_BIT_ATI):{
      return("GL_8X_BIT_ATI");
    }
    case(GL_HALF_BIT_ATI):{
      return("GL_HALF_BIT_ATI");
    }
    case(GL_QUARTER_BIT_ATI):{
      return("GL_QUARTER_BIT_ATI");
    }
    case(GL_EIGHTH_BIT_ATI):{
      return("GL_EIGHTH_BIT_ATI");
    }
    case(GL_SATURATE_BIT_ATI):{
      return("GL_SATURATE_BIT_ATI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FRAGMENT_SHADER_DEST_MOD_MASK_ATI_TO_STRING__ */

#ifdef __GL_INTERNAL_FORMAT_TO_STRING__
char * __attribute__((const)) gl_internal_format_to_string(GLenum val){
  switch(val){
    case(GL_ALPHA12):{
      return("GL_ALPHA12");
    }
    case(GL_ALPHA16):{
      return("GL_ALPHA16");
    }
    case(GL_ALPHA16_ICC_SGIX):{
      return("GL_ALPHA16_ICC_SGIX");
    }
    case(GL_ALPHA4):{
      return("GL_ALPHA4");
    }
    case(GL_ALPHA8):{
      return("GL_ALPHA8");
    }
    case(GL_ALPHA_ICC_SGIX):{
      return("GL_ALPHA_ICC_SGIX");
    }
    case(GL_DEPTH_COMPONENT16_SGIX):{
      return("GL_DEPTH_COMPONENT16_SGIX");
    }
    case(GL_DEPTH_COMPONENT24_SGIX):{
      return("GL_DEPTH_COMPONENT24_SGIX");
    }
    case(GL_DEPTH_COMPONENT32_SGIX):{
      return("GL_DEPTH_COMPONENT32_SGIX");
    }
    case(GL_DUAL_ALPHA12_SGIS):{
      return("GL_DUAL_ALPHA12_SGIS");
    }
    case(GL_DUAL_ALPHA16_SGIS):{
      return("GL_DUAL_ALPHA16_SGIS");
    }
    case(GL_DUAL_ALPHA4_SGIS):{
      return("GL_DUAL_ALPHA4_SGIS");
    }
    case(GL_DUAL_ALPHA8_SGIS):{
      return("GL_DUAL_ALPHA8_SGIS");
    }
    case(GL_DUAL_INTENSITY12_SGIS):{
      return("GL_DUAL_INTENSITY12_SGIS");
    }
    case(GL_DUAL_INTENSITY16_SGIS):{
      return("GL_DUAL_INTENSITY16_SGIS");
    }
    case(GL_DUAL_INTENSITY4_SGIS):{
      return("GL_DUAL_INTENSITY4_SGIS");
    }
    case(GL_DUAL_INTENSITY8_SGIS):{
      return("GL_DUAL_INTENSITY8_SGIS");
    }
    case(GL_DUAL_LUMINANCE12_SGIS):{
      return("GL_DUAL_LUMINANCE12_SGIS");
    }
    case(GL_DUAL_LUMINANCE16_SGIS):{
      return("GL_DUAL_LUMINANCE16_SGIS");
    }
    case(GL_DUAL_LUMINANCE4_SGIS):{
      return("GL_DUAL_LUMINANCE4_SGIS");
    }
    case(GL_DUAL_LUMINANCE8_SGIS):{
      return("GL_DUAL_LUMINANCE8_SGIS");
    }
    case(GL_DUAL_LUMINANCE_ALPHA4_SGIS):{
      return("GL_DUAL_LUMINANCE_ALPHA4_SGIS");
    }
    case(GL_DUAL_LUMINANCE_ALPHA8_SGIS):{
      return("GL_DUAL_LUMINANCE_ALPHA8_SGIS");
    }
    case(GL_INTENSITY):{
      return("GL_INTENSITY");
    }
    case(GL_INTENSITY12):{
      return("GL_INTENSITY12");
    }
    case(GL_INTENSITY16):{
      return("GL_INTENSITY16");
    }
    case(GL_INTENSITY16_ICC_SGIX):{
      return("GL_INTENSITY16_ICC_SGIX");
    }
    case(GL_INTENSITY4):{
      return("GL_INTENSITY4");
    }
    case(GL_INTENSITY8):{
      return("GL_INTENSITY8");
    }
    case(GL_INTENSITY_ICC_SGIX):{
      return("GL_INTENSITY_ICC_SGIX");
    }
    case(GL_LUMINANCE12):{
      return("GL_LUMINANCE12");
    }
    case(GL_LUMINANCE12_ALPHA12):{
      return("GL_LUMINANCE12_ALPHA12");
    }
    case(GL_LUMINANCE12_ALPHA4):{
      return("GL_LUMINANCE12_ALPHA4");
    }
    case(GL_LUMINANCE16):{
      return("GL_LUMINANCE16");
    }
    case(GL_LUMINANCE16_ALPHA16):{
      return("GL_LUMINANCE16_ALPHA16");
    }
    case(GL_LUMINANCE16_ALPHA8_ICC_SGIX):{
      return("GL_LUMINANCE16_ALPHA8_ICC_SGIX");
    }
    case(GL_LUMINANCE16_ICC_SGIX):{
      return("GL_LUMINANCE16_ICC_SGIX");
    }
    case(GL_LUMINANCE4):{
      return("GL_LUMINANCE4");
    }
    case(GL_LUMINANCE4_ALPHA4):{
      return("GL_LUMINANCE4_ALPHA4");
    }
    case(GL_LUMINANCE6_ALPHA2):{
      return("GL_LUMINANCE6_ALPHA2");
    }
    case(GL_LUMINANCE8):{
      return("GL_LUMINANCE8");
    }
    case(GL_LUMINANCE8_ALPHA8):{
      return("GL_LUMINANCE8_ALPHA8");
    }
    case(GL_LUMINANCE_ALPHA_ICC_SGIX):{
      return("GL_LUMINANCE_ALPHA_ICC_SGIX");
    }
    case(GL_LUMINANCE_ICC_SGIX):{
      return("GL_LUMINANCE_ICC_SGIX");
    }
    case(GL_QUAD_ALPHA4_SGIS):{
      return("GL_QUAD_ALPHA4_SGIS");
    }
    case(GL_QUAD_ALPHA8_SGIS):{
      return("GL_QUAD_ALPHA8_SGIS");
    }
    case(GL_QUAD_INTENSITY4_SGIS):{
      return("GL_QUAD_INTENSITY4_SGIS");
    }
    case(GL_QUAD_INTENSITY8_SGIS):{
      return("GL_QUAD_INTENSITY8_SGIS");
    }
    case(GL_QUAD_LUMINANCE4_SGIS):{
      return("GL_QUAD_LUMINANCE4_SGIS");
    }
    case(GL_QUAD_LUMINANCE8_SGIS):{
      return("GL_QUAD_LUMINANCE8_SGIS");
    }
    case(GL_R3_G3_B2):{
      return("GL_R3_G3_B2");
    }
    case(GL_R5_G6_B5_A8_ICC_SGIX):{
      return("GL_R5_G6_B5_A8_ICC_SGIX");
    }
    case(GL_R5_G6_B5_ICC_SGIX):{
      return("GL_R5_G6_B5_ICC_SGIX");
    }
    case(GL_RGB10):{
      return("GL_RGB10");
    }
    case(GL_RGB10_A2):{
      return("GL_RGB10_A2");
    }
    case(GL_RGB12):{
      return("GL_RGB12");
    }
    case(GL_RGB16):{
      return("GL_RGB16");
    }
    case(GL_RGB4):{
      return("GL_RGB4");
    }
    case(GL_RGB5):{
      return("GL_RGB5");
    }
    case(GL_RGB5_A1):{
      return("GL_RGB5_A1");
    }
    case(GL_RGB8):{
      return("GL_RGB8");
    }
    case(GL_RGBA12):{
      return("GL_RGBA12");
    }
    case(GL_RGBA16):{
      return("GL_RGBA16");
    }
    case(GL_RGBA2):{
      return("GL_RGBA2");
    }
    case(GL_RGBA4):{
      return("GL_RGBA4");
    }
    case(GL_RGBA8):{
      return("GL_RGBA8");
    }
    case(GL_RGBA_ICC_SGIX):{
      return("GL_RGBA_ICC_SGIX");
    }
    case(GL_RGB_ICC_SGIX):{
      return("GL_RGB_ICC_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_INTERNAL_FORMAT_TO_STRING__ */

#ifdef __GL_DEPTH_FUNCTION_TO_STRING__
char * __attribute__((const)) gl_depth_function_to_string(GLenum val){
  switch(val){
    case(GL_ALWAYS):{
      return("GL_ALWAYS");
    }
    case(GL_EQUAL):{
      return("GL_EQUAL");
    }
    case(GL_GEQUAL):{
      return("GL_GEQUAL");
    }
    case(GL_GREATER):{
      return("GL_GREATER");
    }
    case(GL_LEQUAL):{
      return("GL_LEQUAL");
    }
    case(GL_LESS):{
      return("GL_LESS");
    }
    case(GL_NEVER):{
      return("GL_NEVER");
    }
    case(GL_NOTEQUAL):{
      return("GL_NOTEQUAL");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_DEPTH_FUNCTION_TO_STRING__ */

#ifdef __GL_FFD_TARGET_SGIX_TO_STRING__
char * __attribute__((const)) gl_ffd_target_SGIX_to_string(GLenum val){
  switch(val){
    case(GL_GEOMETRY_DEFORMATION_SGIX):{
      return("GL_GEOMETRY_DEFORMATION_SGIX");
    }
    case(GL_TEXTURE_DEFORMATION_SGIX):{
      return("GL_TEXTURE_DEFORMATION_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FFD_TARGET_SGIX_TO_STRING__ */

#ifdef __GL_PIXEL_TEX_GEN_MODE_TO_STRING__
char * __attribute__((const)) gl_pixel_tex_gen_mode_to_string(GLenum val){
  switch(val){
    case(GL_LUMINANCE):{
      return("GL_LUMINANCE");
    }
    case(GL_LUMINANCE_ALPHA):{
      return("GL_LUMINANCE_ALPHA");
    }
    case(GL_NONE):{
      return("GL_NONE");
    }
    case(GL_PIXEL_TEX_GEN_ALPHA_LS_SGIX):{
      return("GL_PIXEL_TEX_GEN_ALPHA_LS_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_ALPHA_MS_SGIX):{
      return("GL_PIXEL_TEX_GEN_ALPHA_MS_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_ALPHA_NO_REPLACE_SGIX):{
      return("GL_PIXEL_TEX_GEN_ALPHA_NO_REPLACE_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_ALPHA_REPLACE_SGIX):{
      return("GL_PIXEL_TEX_GEN_ALPHA_REPLACE_SGIX");
    }
    case(GL_RGB):{
      return("GL_RGB");
    }
    case(GL_RGBA):{
      return("GL_RGBA");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_TEX_GEN_MODE_TO_STRING__ */

#ifdef __GL_BLEND_EQUATION_MODE_EXT_TO_STRING__
char * __attribute__((const)) gl_blend_equation_mode_EXT_to_string(GLenum val){
  switch(val){
    case(GL_ALPHA_MAX_SGIX):{
      return("GL_ALPHA_MAX_SGIX");
    }
    case(GL_ALPHA_MIN_SGIX):{
      return("GL_ALPHA_MIN_SGIX");
    }
    case(GL_LOGIC_OP):{
      return("GL_LOGIC_OP");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_BLEND_EQUATION_MODE_EXT_TO_STRING__ */

#ifdef __GL_TEXTURE_STORAGE_MASK_AMD_TO_STRING__
char * __attribute__((const)) gl_texture_storage_mask_AMD_to_string(GLenum val){
  switch(val){
    case(GL_TEXTURE_STORAGE_SPARSE_BIT_AMD):{
      return("GL_TEXTURE_STORAGE_SPARSE_BIT_AMD");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_STORAGE_MASK_AMD_TO_STRING__ */

#ifdef __GL_COLOR_MATERIAL_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_color_material_parameter_to_string(GLenum val){
  switch(val){
    case(GL_AMBIENT):{
      return("GL_AMBIENT");
    }
    case(GL_AMBIENT_AND_DIFFUSE):{
      return("GL_AMBIENT_AND_DIFFUSE");
    }
    case(GL_DIFFUSE):{
      return("GL_DIFFUSE");
    }
    case(GL_EMISSION):{
      return("GL_EMISSION");
    }
    case(GL_SPECULAR):{
      return("GL_SPECULAR");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_COLOR_MATERIAL_PARAMETER_TO_STRING__ */

#ifdef __GL_CONTEXT_FLAG_MASK_TO_STRING__
char * __attribute__((const)) gl_context_flag_mask_to_string(GLenum val){
  switch(val){
    case(GL_CONTEXT_FLAG_DEBUG_BIT):{
      return("GL_CONTEXT_FLAG_DEBUG_BIT");
    }
    case(GL_CONTEXT_FLAG_DEBUG_BIT_KHR):{
      return("GL_CONTEXT_FLAG_DEBUG_BIT_KHR");
    }
    case(GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT):{
      return("GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CONTEXT_FLAG_MASK_TO_STRING__ */

#ifdef __GL_LIST_NAME_TYPE_TO_STRING__
char * __attribute__((const)) gl_list_name_type_to_string(GLenum val){
  switch(val){
    case(GL_2_BYTES):{
      return("GL_2_BYTES");
    }
    case(GL_3_BYTES):{
      return("GL_3_BYTES");
    }
    case(GL_4_BYTES):{
      return("GL_4_BYTES");
    }
    case(GL_BYTE):{
      return("GL_BYTE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    case(GL_UNSIGNED_BYTE):{
      return("GL_UNSIGNED_BYTE");
    }
    case(GL_UNSIGNED_INT):{
      return("GL_UNSIGNED_INT");
    }
    case(GL_UNSIGNED_SHORT):{
      return("GL_UNSIGNED_SHORT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIST_NAME_TYPE_TO_STRING__ */

#ifdef __GL_PIXEL_FORMAT_TO_STRING__
char * __attribute__((const)) gl_pixel_format_to_string(GLenum val){
  switch(val){
    case(GL_ALPHA):{
      return("GL_ALPHA");
    }
    case(GL_BLUE):{
      return("GL_BLUE");
    }
    case(GL_COLOR_INDEX):{
      return("GL_COLOR_INDEX");
    }
    case(GL_DEPTH_COMPONENT):{
      return("GL_DEPTH_COMPONENT");
    }
    case(GL_GREEN):{
      return("GL_GREEN");
    }
    case(GL_LUMINANCE):{
      return("GL_LUMINANCE");
    }
    case(GL_LUMINANCE_ALPHA):{
      return("GL_LUMINANCE_ALPHA");
    }
    case(GL_RED):{
      return("GL_RED");
    }
    case(GL_RGB):{
      return("GL_RGB");
    }
    case(GL_RGBA):{
      return("GL_RGBA");
    }
    case(GL_STENCIL_INDEX):{
      return("GL_STENCIL_INDEX");
    }
    case(GL_UNSIGNED_INT):{
      return("GL_UNSIGNED_INT");
    }
    case(GL_UNSIGNED_SHORT):{
      return("GL_UNSIGNED_SHORT");
    }
    case(GL_YCRCB_422_SGIX):{
      return("GL_YCRCB_422_SGIX");
    }
    case(GL_YCRCB_444_SGIX):{
      return("GL_YCRCB_444_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_FORMAT_TO_STRING__ */

#ifdef __GL_PRIMITIVE_TYPE_TO_STRING__
char * __attribute__((const)) gl_primitive_type_to_string(GLenum val){
  switch(val){
    case(GL_LINES):{
      return("GL_LINES");
    }
    case(GL_LINES_ADJACENCY):{
      return("GL_LINES_ADJACENCY");
    }
    case(GL_LINE_LOOP):{
      return("GL_LINE_LOOP");
    }
    case(GL_LINE_STRIP):{
      return("GL_LINE_STRIP");
    }
    case(GL_LINE_STRIP_ADJACENCY):{
      return("GL_LINE_STRIP_ADJACENCY");
    }
    case(GL_PATCHES):{
      return("GL_PATCHES");
    }
    case(GL_POINTS):{
      return("GL_POINTS");
    }
    case(GL_POLYGON):{
      return("GL_POLYGON");
    }
    case(GL_QUADS):{
      return("GL_QUADS");
    }
    case(GL_QUAD_STRIP):{
      return("GL_QUAD_STRIP");
    }
    case(GL_TRIANGLES):{
      return("GL_TRIANGLES");
    }
    case(GL_TRIANGLES_ADJACENCY):{
      return("GL_TRIANGLES_ADJACENCY");
    }
    case(GL_TRIANGLE_FAN):{
      return("GL_TRIANGLE_FAN");
    }
    case(GL_TRIANGLE_STRIP):{
      return("GL_TRIANGLE_STRIP");
    }
    case(GL_TRIANGLE_STRIP_ADJACENCY):{
      return("GL_TRIANGLE_STRIP_ADJACENCY");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PRIMITIVE_TYPE_TO_STRING__ */

#ifdef __GL_LIGHT_NAME_TO_STRING__
char * __attribute__((const)) gl_light_name_to_string(GLenum val){
  switch(val){
    case(GL_FRAGMENT_LIGHT0_SGIX):{
      return("GL_FRAGMENT_LIGHT0_SGIX");
    }
    case(GL_FRAGMENT_LIGHT1_SGIX):{
      return("GL_FRAGMENT_LIGHT1_SGIX");
    }
    case(GL_FRAGMENT_LIGHT2_SGIX):{
      return("GL_FRAGMENT_LIGHT2_SGIX");
    }
    case(GL_FRAGMENT_LIGHT3_SGIX):{
      return("GL_FRAGMENT_LIGHT3_SGIX");
    }
    case(GL_FRAGMENT_LIGHT4_SGIX):{
      return("GL_FRAGMENT_LIGHT4_SGIX");
    }
    case(GL_FRAGMENT_LIGHT5_SGIX):{
      return("GL_FRAGMENT_LIGHT5_SGIX");
    }
    case(GL_FRAGMENT_LIGHT6_SGIX):{
      return("GL_FRAGMENT_LIGHT6_SGIX");
    }
    case(GL_FRAGMENT_LIGHT7_SGIX):{
      return("GL_FRAGMENT_LIGHT7_SGIX");
    }
    case(GL_LIGHT0):{
      return("GL_LIGHT0");
    }
    case(GL_LIGHT1):{
      return("GL_LIGHT1");
    }
    case(GL_LIGHT2):{
      return("GL_LIGHT2");
    }
    case(GL_LIGHT3):{
      return("GL_LIGHT3");
    }
    case(GL_LIGHT4):{
      return("GL_LIGHT4");
    }
    case(GL_LIGHT5):{
      return("GL_LIGHT5");
    }
    case(GL_LIGHT6):{
      return("GL_LIGHT6");
    }
    case(GL_LIGHT7):{
      return("GL_LIGHT7");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIGHT_NAME_TO_STRING__ */

#ifdef __GL_PIXEL_COPY_TYPE_TO_STRING__
char * __attribute__((const)) gl_pixel_copy_type_to_string(GLenum val){
  switch(val){
    case(GL_COLOR):{
      return("GL_COLOR");
    }
    case(GL_DEPTH):{
      return("GL_DEPTH");
    }
    case(GL_STENCIL):{
      return("GL_STENCIL");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_COPY_TYPE_TO_STRING__ */

#ifdef __GL_FOG_POINTER_TYPE_EXT_TO_STRING__
char * __attribute__((const)) gl_fog_pointer_type_EXT_to_string(GLenum val){
  switch(val){
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FOG_POINTER_TYPE_EXT_TO_STRING__ */

#ifdef __GL_GET_CONVOLUTION_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_get_convolution_parameter_to_string(GLenum val){
  switch(val){

    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_CONVOLUTION_PARAMETER_TO_STRING__ */

#ifdef __GL_MINMAX_TARGET_EXT_TO_STRING__
char * __attribute__((const)) gl_minmax_target_EXT_to_string(GLenum val){
  switch(val){
    case(GL_MINMAX):{
      return("GL_MINMAX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MINMAX_TARGET_EXT_TO_STRING__ */

#ifdef __GL_FRAGMENT_SHADER_DEST_MASK_ATI_TO_STRING__
char * __attribute__((const)) gl_fragment_shader_dest_mask_ATI_to_string(GLenum val){
  switch(val){
    case(GL_RED_BIT_ATI):{
      return("GL_RED_BIT_ATI");
    }
    case(GL_GREEN_BIT_ATI):{
      return("GL_GREEN_BIT_ATI");
    }
    case(GL_BLUE_BIT_ATI):{
      return("GL_BLUE_BIT_ATI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FRAGMENT_SHADER_DEST_MASK_ATI_TO_STRING__ */

#ifdef __GL_FFD_MASK_SGIX_TO_STRING__
char * __attribute__((const)) gl_ffd_mask_SGIX_to_string(GLenum val){
  switch(val){

    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FFD_MASK_SGIX_TO_STRING__ */

#ifdef __GL_DATA_TYPE_TO_STRING__
char * __attribute__((const)) gl_data_type_to_string(GLenum val){
  switch(val){

    default:{
      return "";
    }
  }
}
#endif /* defined __GL_DATA_TYPE_TO_STRING__ */

#ifdef __GL_ACCUM_OP_TO_STRING__
char * __attribute__((const)) gl_accum_op_to_string(GLenum val){
  switch(val){
    case(GL_ACCUM):{
      return("GL_ACCUM");
    }
    case(GL_LOAD):{
      return("GL_LOAD");
    }
    case(GL_RETURN):{
      return("GL_RETURN");
    }
    case(GL_MULT):{
      return("GL_MULT");
    }
    case(GL_ADD):{
      return("GL_ADD");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_ACCUM_OP_TO_STRING__ */

#ifdef __GL_READ_BUFFER_MODE_TO_STRING__
char * __attribute__((const)) gl_read_buffer_mode_to_string(GLenum val){
  switch(val){
    case(GL_AUX0):{
      return("GL_AUX0");
    }
    case(GL_AUX1):{
      return("GL_AUX1");
    }
    case(GL_AUX2):{
      return("GL_AUX2");
    }
    case(GL_AUX3):{
      return("GL_AUX3");
    }
    case(GL_BACK):{
      return("GL_BACK");
    }
    case(GL_BACK_LEFT):{
      return("GL_BACK_LEFT");
    }
    case(GL_BACK_RIGHT):{
      return("GL_BACK_RIGHT");
    }
    case(GL_FRONT):{
      return("GL_FRONT");
    }
    case(GL_FRONT_LEFT):{
      return("GL_FRONT_LEFT");
    }
    case(GL_FRONT_RIGHT):{
      return("GL_FRONT_RIGHT");
    }
    case(GL_LEFT):{
      return("GL_LEFT");
    }
    case(GL_RIGHT):{
      return("GL_RIGHT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_READ_BUFFER_MODE_TO_STRING__ */

#ifdef __GL_MESH_MODE1_TO_STRING__
char * __attribute__((const)) gl_mesh_mode1_to_string(GLenum val){
  switch(val){
    case(GL_LINE):{
      return("GL_LINE");
    }
    case(GL_POINT):{
      return("GL_POINT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MESH_MODE1_TO_STRING__ */

#ifdef __GL_HINT_MODE_TO_STRING__
char * __attribute__((const)) gl_hint_mode_to_string(GLenum val){
  switch(val){
    case(GL_DONT_CARE):{
      return("GL_DONT_CARE");
    }
    case(GL_FASTEST):{
      return("GL_FASTEST");
    }
    case(GL_NICEST):{
      return("GL_NICEST");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_HINT_MODE_TO_STRING__ */

#ifdef __GL_LIGHT_ENV_PARAMETER_SGIX_TO_STRING__
char * __attribute__((const)) gl_light_env_parameter_SGIX_to_string(GLenum val){
  switch(val){
    case(GL_LIGHT_ENV_MODE_SGIX):{
      return("GL_LIGHT_ENV_MODE_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIGHT_ENV_PARAMETER_SGIX_TO_STRING__ */

#ifdef __GL_TRACE_MASK_MESA_TO_STRING__
char * __attribute__((const)) gl_trace_mask_MESA_to_string(GLenum val){
  switch(val){
    case(GL_TRACE_OPERATIONS_BIT_MESA):{
      return("GL_TRACE_OPERATIONS_BIT_MESA");
    }
    case(GL_TRACE_PRIMITIVES_BIT_MESA):{
      return("GL_TRACE_PRIMITIVES_BIT_MESA");
    }
    case(GL_TRACE_ARRAYS_BIT_MESA):{
      return("GL_TRACE_ARRAYS_BIT_MESA");
    }
    case(GL_TRACE_TEXTURES_BIT_MESA):{
      return("GL_TRACE_TEXTURES_BIT_MESA");
    }
    case(GL_TRACE_PIXELS_BIT_MESA):{
      return("GL_TRACE_PIXELS_BIT_MESA");
    }
    case(GL_TRACE_ERRORS_BIT_MESA):{
      return("GL_TRACE_ERRORS_BIT_MESA");
    }
    case(GL_TRACE_ALL_BITS_MESA):{
      return("GL_TRACE_ALL_BITS_MESA");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TRACE_MASK_MESA_TO_STRING__ */

#ifdef __GL_CLEAR_BUFFER_MASK_TO_STRING__
char * __attribute__((const)) gl_clear_buffer_mask_to_string(GLenum val){
  switch(val){
    case(GL_ACCUM_BUFFER_BIT):{
      return("GL_ACCUM_BUFFER_BIT");
    }
    case(GL_COLOR_BUFFER_BIT):{
      return("GL_COLOR_BUFFER_BIT");
    }
    case(GL_COVERAGE_BUFFER_BIT_NV):{
      return("GL_COVERAGE_BUFFER_BIT_NV");
    }
    case(GL_DEPTH_BUFFER_BIT):{
      return("GL_DEPTH_BUFFER_BIT");
    }
    case(GL_STENCIL_BUFFER_BIT):{
      return("GL_STENCIL_BUFFER_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CLEAR_BUFFER_MASK_TO_STRING__ */

#ifdef __GL_BOOLEAN_TO_STRING__
char * __attribute__((const)) gl_boolean_to_string(GLenum val){
  switch(val){
    case(GL_FALSE):{
      return("GL_FALSE");
    }
    case(GL_TRUE):{
      return("GL_TRUE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_BOOLEAN_TO_STRING__ */

#ifdef __GL_TEXTURE_COORD_NAME_TO_STRING__
char * __attribute__((const)) gl_texture_coord_name_to_string(GLenum val){
  switch(val){
    case(GL_S):{
      return("GL_S");
    }
    case(GL_T):{
      return("GL_T");
    }
    case(GL_R):{
      return("GL_R");
    }
    case(GL_Q):{
      return("GL_Q");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_COORD_NAME_TO_STRING__ */

#ifdef __GL_STENCIL_OP_TO_STRING__
char * __attribute__((const)) gl_stencil_op_to_string(GLenum val){
  switch(val){
    case(GL_DECR):{
      return("GL_DECR");
    }
    case(GL_INCR):{
      return("GL_INCR");
    }
    case(GL_INVERT):{
      return("GL_INVERT");
    }
    case(GL_KEEP):{
      return("GL_KEEP");
    }
    case(GL_REPLACE):{
      return("GL_REPLACE");
    }
    case(GL_ZERO):{
      return("GL_ZERO");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_STENCIL_OP_TO_STRING__ */

#ifdef __GL_TEXTURE_MIN_FILTER_TO_STRING__
char * __attribute__((const)) gl_texture_min_filter_to_string(GLenum val){
  switch(val){
    case(GL_FILTER4_SGIS):{
      return("GL_FILTER4_SGIS");
    }
    case(GL_LINEAR):{
      return("GL_LINEAR");
    }
    case(GL_LINEAR_CLIPMAP_LINEAR_SGIX):{
      return("GL_LINEAR_CLIPMAP_LINEAR_SGIX");
    }
    case(GL_LINEAR_CLIPMAP_NEAREST_SGIX):{
      return("GL_LINEAR_CLIPMAP_NEAREST_SGIX");
    }
    case(GL_LINEAR_MIPMAP_LINEAR):{
      return("GL_LINEAR_MIPMAP_LINEAR");
    }
    case(GL_LINEAR_MIPMAP_NEAREST):{
      return("GL_LINEAR_MIPMAP_NEAREST");
    }
    case(GL_NEAREST):{
      return("GL_NEAREST");
    }
    case(GL_NEAREST_CLIPMAP_LINEAR_SGIX):{
      return("GL_NEAREST_CLIPMAP_LINEAR_SGIX");
    }
    case(GL_NEAREST_CLIPMAP_NEAREST_SGIX):{
      return("GL_NEAREST_CLIPMAP_NEAREST_SGIX");
    }
    case(GL_NEAREST_MIPMAP_LINEAR):{
      return("GL_NEAREST_MIPMAP_LINEAR");
    }
    case(GL_NEAREST_MIPMAP_NEAREST):{
      return("GL_NEAREST_MIPMAP_NEAREST");
    }
    case(GL_PIXEL_TEX_GEN_Q_CEILING_SGIX):{
      return("GL_PIXEL_TEX_GEN_Q_CEILING_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_Q_FLOOR_SGIX):{
      return("GL_PIXEL_TEX_GEN_Q_FLOOR_SGIX");
    }
    case(GL_PIXEL_TEX_GEN_Q_ROUND_SGIX):{
      return("GL_PIXEL_TEX_GEN_Q_ROUND_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_MIN_FILTER_TO_STRING__ */

#ifdef __GL_SYNC_OBJECT_MASK_TO_STRING__
char * __attribute__((const)) gl_sync_object_mask_to_string(GLenum val){
  switch(val){
    case(GL_SYNC_FLUSH_COMMANDS_BIT):{
      return("GL_SYNC_FLUSH_COMMANDS_BIT");
    }
    case(GL_SYNC_FLUSH_COMMANDS_BIT_APPLE):{
      return("GL_SYNC_FLUSH_COMMANDS_BIT_APPLE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_SYNC_OBJECT_MASK_TO_STRING__ */

#ifdef __GL_COLOR_TABLE_TARGET_SGI_TO_STRING__
char * __attribute__((const)) gl_color_table_target_SGI_to_string(GLenum val){
  switch(val){
    case(GL_COLOR_TABLE):{
      return("GL_COLOR_TABLE");
    }
    case(GL_COLOR_TABLE_SGI):{
      return("GL_COLOR_TABLE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_COLOR_TABLE):{
      return("GL_POST_COLOR_MATRIX_COLOR_TABLE");
    }
    case(GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI):{
      return("GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI");
    }
    case(GL_POST_CONVOLUTION_COLOR_TABLE):{
      return("GL_POST_CONVOLUTION_COLOR_TABLE");
    }
    case(GL_POST_CONVOLUTION_COLOR_TABLE_SGI):{
      return("GL_POST_CONVOLUTION_COLOR_TABLE_SGI");
    }
    case(GL_PROXY_COLOR_TABLE):{
      return("GL_PROXY_COLOR_TABLE");
    }
    case(GL_PROXY_COLOR_TABLE_SGI):{
      return("GL_PROXY_COLOR_TABLE_SGI");
    }
    case(GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE):{
      return("GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE");
    }
    case(GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE_SGI):{
      return("GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE_SGI");
    }
    case(GL_PROXY_POST_CONVOLUTION_COLOR_TABLE):{
      return("GL_PROXY_POST_CONVOLUTION_COLOR_TABLE");
    }
    case(GL_PROXY_POST_CONVOLUTION_COLOR_TABLE_SGI):{
      return("GL_PROXY_POST_CONVOLUTION_COLOR_TABLE_SGI");
    }
    case(GL_PROXY_TEXTURE_COLOR_TABLE_SGI):{
      return("GL_PROXY_TEXTURE_COLOR_TABLE_SGI");
    }
    case(GL_TEXTURE_COLOR_TABLE_SGI):{
      return("GL_TEXTURE_COLOR_TABLE_SGI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_COLOR_TABLE_TARGET_SGI_TO_STRING__ */

#ifdef __GL_LOGIC_OP_TO_STRING__
char * __attribute__((const)) gl_logic_op_to_string(GLenum val){
  switch(val){
    case(GL_AND):{
      return("GL_AND");
    }
    case(GL_AND_INVERTED):{
      return("GL_AND_INVERTED");
    }
    case(GL_AND_REVERSE):{
      return("GL_AND_REVERSE");
    }
    case(GL_CLEAR):{
      return("GL_CLEAR");
    }
    case(GL_COPY):{
      return("GL_COPY");
    }
    case(GL_COPY_INVERTED):{
      return("GL_COPY_INVERTED");
    }
    case(GL_EQUIV):{
      return("GL_EQUIV");
    }
    case(GL_INVERT):{
      return("GL_INVERT");
    }
    case(GL_NAND):{
      return("GL_NAND");
    }
    case(GL_NOOP):{
      return("GL_NOOP");
    }
    case(GL_NOR):{
      return("GL_NOR");
    }
    case(GL_OR):{
      return("GL_OR");
    }
    case(GL_OR_INVERTED):{
      return("GL_OR_INVERTED");
    }
    case(GL_OR_REVERSE):{
      return("GL_OR_REVERSE");
    }
    case(GL_SET):{
      return("GL_SET");
    }
    case(GL_XOR):{
      return("GL_XOR");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LOGIC_OP_TO_STRING__ */

#ifdef __GL_INDEX_POINTER_TYPE_TO_STRING__
char * __attribute__((const)) gl_index_pointer_type_to_string(GLenum val){
  switch(val){
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_INDEX_POINTER_TYPE_TO_STRING__ */

#ifdef __GL_PIXEL_TRANSFER_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_pixel_transfer_parameter_to_string(GLenum val){
  switch(val){
    case(GL_ALPHA_BIAS):{
      return("GL_ALPHA_BIAS");
    }
    case(GL_ALPHA_SCALE):{
      return("GL_ALPHA_SCALE");
    }
    case(GL_BLUE_BIAS):{
      return("GL_BLUE_BIAS");
    }
    case(GL_BLUE_SCALE):{
      return("GL_BLUE_SCALE");
    }
    case(GL_DEPTH_BIAS):{
      return("GL_DEPTH_BIAS");
    }
    case(GL_DEPTH_SCALE):{
      return("GL_DEPTH_SCALE");
    }
    case(GL_GREEN_BIAS):{
      return("GL_GREEN_BIAS");
    }
    case(GL_GREEN_SCALE):{
      return("GL_GREEN_SCALE");
    }
    case(GL_INDEX_OFFSET):{
      return("GL_INDEX_OFFSET");
    }
    case(GL_INDEX_SHIFT):{
      return("GL_INDEX_SHIFT");
    }
    case(GL_MAP_COLOR):{
      return("GL_MAP_COLOR");
    }
    case(GL_MAP_STENCIL):{
      return("GL_MAP_STENCIL");
    }
    case(GL_POST_COLOR_MATRIX_ALPHA_BIAS):{
      return("GL_POST_COLOR_MATRIX_ALPHA_BIAS");
    }
    case(GL_POST_COLOR_MATRIX_ALPHA_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_ALPHA_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_ALPHA_SCALE):{
      return("GL_POST_COLOR_MATRIX_ALPHA_SCALE");
    }
    case(GL_POST_COLOR_MATRIX_ALPHA_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_ALPHA_SCALE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_BLUE_BIAS):{
      return("GL_POST_COLOR_MATRIX_BLUE_BIAS");
    }
    case(GL_POST_COLOR_MATRIX_BLUE_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_BLUE_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_BLUE_SCALE):{
      return("GL_POST_COLOR_MATRIX_BLUE_SCALE");
    }
    case(GL_POST_COLOR_MATRIX_BLUE_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_BLUE_SCALE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_GREEN_BIAS):{
      return("GL_POST_COLOR_MATRIX_GREEN_BIAS");
    }
    case(GL_POST_COLOR_MATRIX_GREEN_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_GREEN_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_GREEN_SCALE):{
      return("GL_POST_COLOR_MATRIX_GREEN_SCALE");
    }
    case(GL_POST_COLOR_MATRIX_GREEN_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_GREEN_SCALE_SGI");
    }
    case(GL_POST_COLOR_MATRIX_RED_BIAS):{
      return("GL_POST_COLOR_MATRIX_RED_BIAS");
    }
    case(GL_POST_COLOR_MATRIX_RED_BIAS_SGI):{
      return("GL_POST_COLOR_MATRIX_RED_BIAS_SGI");
    }
    case(GL_POST_COLOR_MATRIX_RED_SCALE):{
      return("GL_POST_COLOR_MATRIX_RED_SCALE");
    }
    case(GL_POST_COLOR_MATRIX_RED_SCALE_SGI):{
      return("GL_POST_COLOR_MATRIX_RED_SCALE_SGI");
    }
    case(GL_POST_CONVOLUTION_ALPHA_BIAS):{
      return("GL_POST_CONVOLUTION_ALPHA_BIAS");
    }
    case(GL_POST_CONVOLUTION_ALPHA_SCALE):{
      return("GL_POST_CONVOLUTION_ALPHA_SCALE");
    }
    case(GL_POST_CONVOLUTION_BLUE_BIAS):{
      return("GL_POST_CONVOLUTION_BLUE_BIAS");
    }
    case(GL_POST_CONVOLUTION_BLUE_SCALE):{
      return("GL_POST_CONVOLUTION_BLUE_SCALE");
    }
    case(GL_POST_CONVOLUTION_GREEN_BIAS):{
      return("GL_POST_CONVOLUTION_GREEN_BIAS");
    }
    case(GL_POST_CONVOLUTION_GREEN_SCALE):{
      return("GL_POST_CONVOLUTION_GREEN_SCALE");
    }
    case(GL_POST_CONVOLUTION_RED_BIAS):{
      return("GL_POST_CONVOLUTION_RED_BIAS");
    }
    case(GL_POST_CONVOLUTION_RED_SCALE):{
      return("GL_POST_CONVOLUTION_RED_SCALE");
    }
    case(GL_RED_BIAS):{
      return("GL_RED_BIAS");
    }
    case(GL_RED_SCALE):{
      return("GL_RED_SCALE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_TRANSFER_PARAMETER_TO_STRING__ */

#ifdef __GL_SPECIAL_NUMBERS_TO_STRING__
char * __attribute__((const)) gl_special_numbers_to_string(GLenum val){
  switch(val){
    case(GL_FALSE):{
      return("GL_FALSE");
    }
    case(GL_NO_ERROR):{
      return("GL_NO_ERROR");
    }
    case(GL_ZERO):{
      return("GL_ZERO");
    }
    case(GL_NONE):{
      return("GL_NONE");
    }
    case(GL_TRUE):{
      return("GL_TRUE");
    }
    case(GL_ONE):{
      return("GL_ONE");
    }
    case(GL_INVALID_INDEX):{
      return("GL_INVALID_INDEX");
    }
    case(GL_TIMEOUT_IGNORED):{
      return("GL_TIMEOUT_IGNORED");
    }
    case(GL_TIMEOUT_IGNORED_APPLE):{
      return("GL_TIMEOUT_IGNORED_APPLE");
    }
    case(GL_VERSION_ES_CL_1_0):{
      return("GL_VERSION_ES_CL_1_0");
    }
    case(GL_VERSION_ES_CM_1_1):{
      return("GL_VERSION_ES_CM_1_1");
    }
    case(GL_VERSION_ES_CL_1_1):{
      return("GL_VERSION_ES_CL_1_1");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_SPECIAL_NUMBERS_TO_STRING__ */

#ifdef __GL_CONVOLUTION_TARGET_EXT_TO_STRING__
char * __attribute__((const)) gl_convolution_target_EXT_to_string(GLenum val){
  switch(val){
    case(GL_CONVOLUTION_1D):{
      return("GL_CONVOLUTION_1D");
    }
    case(GL_CONVOLUTION_2D):{
      return("GL_CONVOLUTION_2D");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CONVOLUTION_TARGET_EXT_TO_STRING__ */

#ifdef __GL_ATTRIB_MASK_TO_STRING__
char * __attribute__((const)) gl_attrib_mask_to_string(GLenum val){
  switch(val){
    case(GL_ACCUM_BUFFER_BIT):{
      return("GL_ACCUM_BUFFER_BIT");
    }
    case(GL_ALL_ATTRIB_BITS):{
      return("GL_ALL_ATTRIB_BITS");
    }
    case(GL_COLOR_BUFFER_BIT):{
      return("GL_COLOR_BUFFER_BIT");
    }
    case(GL_CURRENT_BIT):{
      return("GL_CURRENT_BIT");
    }
    case(GL_DEPTH_BUFFER_BIT):{
      return("GL_DEPTH_BUFFER_BIT");
    }
    case(GL_ENABLE_BIT):{
      return("GL_ENABLE_BIT");
    }
    case(GL_EVAL_BIT):{
      return("GL_EVAL_BIT");
    }
    case(GL_FOG_BIT):{
      return("GL_FOG_BIT");
    }
    case(GL_HINT_BIT):{
      return("GL_HINT_BIT");
    }
    case(GL_LIGHTING_BIT):{
      return("GL_LIGHTING_BIT");
    }
    case(GL_LINE_BIT):{
      return("GL_LINE_BIT");
    }
    case(GL_LIST_BIT):{
      return("GL_LIST_BIT");
    }
    case(GL_MULTISAMPLE_BIT):{
      return("GL_MULTISAMPLE_BIT");
    }
    case(GL_MULTISAMPLE_BIT_3DFX):{
      return("GL_MULTISAMPLE_BIT_3DFX");
    }
    case(GL_PIXEL_MODE_BIT):{
      return("GL_PIXEL_MODE_BIT");
    }
    case(GL_POINT_BIT):{
      return("GL_POINT_BIT");
    }
    case(GL_POLYGON_BIT):{
      return("GL_POLYGON_BIT");
    }
    case(GL_POLYGON_STIPPLE_BIT):{
      return("GL_POLYGON_STIPPLE_BIT");
    }
    case(GL_SCISSOR_BIT):{
      return("GL_SCISSOR_BIT");
    }
    case(GL_STENCIL_BUFFER_BIT):{
      return("GL_STENCIL_BUFFER_BIT");
    }
    case(GL_TEXTURE_BIT):{
      return("GL_TEXTURE_BIT");
    }
    case(GL_TRANSFORM_BIT):{
      return("GL_TRANSFORM_BIT");
    }
    case(GL_VIEWPORT_BIT):{
      return("GL_VIEWPORT_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_ATTRIB_MASK_TO_STRING__ */

#ifdef __GL_TEXTURE_TARGET_TO_STRING__
char * __attribute__((const)) gl_texture_target_to_string(GLenum val){
  switch(val){
    case(GL_DETAIL_TEXTURE_2D_SGIS):{
      return("GL_DETAIL_TEXTURE_2D_SGIS");
    }
    case(GL_PROXY_TEXTURE_1D):{
      return("GL_PROXY_TEXTURE_1D");
    }
    case(GL_PROXY_TEXTURE_2D):{
      return("GL_PROXY_TEXTURE_2D");
    }
    case(GL_PROXY_TEXTURE_3D):{
      return("GL_PROXY_TEXTURE_3D");
    }
    case(GL_PROXY_TEXTURE_4D_SGIS):{
      return("GL_PROXY_TEXTURE_4D_SGIS");
    }
    case(GL_TEXTURE_1D):{
      return("GL_TEXTURE_1D");
    }
    case(GL_TEXTURE_2D):{
      return("GL_TEXTURE_2D");
    }
    case(GL_TEXTURE_3D):{
      return("GL_TEXTURE_3D");
    }
    case(GL_TEXTURE_4D_SGIS):{
      return("GL_TEXTURE_4D_SGIS");
    }
    case(GL_TEXTURE_BASE_LEVEL):{
      return("GL_TEXTURE_BASE_LEVEL");
    }
    case(GL_TEXTURE_BASE_LEVEL_SGIS):{
      return("GL_TEXTURE_BASE_LEVEL_SGIS");
    }
    case(GL_TEXTURE_MAX_LEVEL):{
      return("GL_TEXTURE_MAX_LEVEL");
    }
    case(GL_TEXTURE_MAX_LEVEL_SGIS):{
      return("GL_TEXTURE_MAX_LEVEL_SGIS");
    }
    case(GL_TEXTURE_MAX_LOD):{
      return("GL_TEXTURE_MAX_LOD");
    }
    case(GL_TEXTURE_MAX_LOD_SGIS):{
      return("GL_TEXTURE_MAX_LOD_SGIS");
    }
    case(GL_TEXTURE_MIN_LOD):{
      return("GL_TEXTURE_MIN_LOD");
    }
    case(GL_TEXTURE_MIN_LOD_SGIS):{
      return("GL_TEXTURE_MIN_LOD_SGIS");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_TARGET_TO_STRING__ */

#ifdef __GL_LIGHT_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_light_parameter_to_string(GLenum val){
  switch(val){
    case(GL_AMBIENT):{
      return("GL_AMBIENT");
    }
    case(GL_CONSTANT_ATTENUATION):{
      return("GL_CONSTANT_ATTENUATION");
    }
    case(GL_DIFFUSE):{
      return("GL_DIFFUSE");
    }
    case(GL_LINEAR_ATTENUATION):{
      return("GL_LINEAR_ATTENUATION");
    }
    case(GL_POSITION):{
      return("GL_POSITION");
    }
    case(GL_QUADRATIC_ATTENUATION):{
      return("GL_QUADRATIC_ATTENUATION");
    }
    case(GL_SPECULAR):{
      return("GL_SPECULAR");
    }
    case(GL_SPOT_CUTOFF):{
      return("GL_SPOT_CUTOFF");
    }
    case(GL_SPOT_DIRECTION):{
      return("GL_SPOT_DIRECTION");
    }
    case(GL_SPOT_EXPONENT):{
      return("GL_SPOT_EXPONENT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIGHT_PARAMETER_TO_STRING__ */

#ifdef __GL_LIGHT_MODEL_COLOR_CONTROL_TO_STRING__
char * __attribute__((const)) gl_light_model_color_control_to_string(GLenum val){
  switch(val){
    case(GL_SEPARATE_SPECULAR_COLOR):{
      return("GL_SEPARATE_SPECULAR_COLOR");
    }
    case(GL_SINGLE_COLOR):{
      return("GL_SINGLE_COLOR");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIGHT_MODEL_COLOR_CONTROL_TO_STRING__ */

#ifdef __GL_SAMPLE_PATTERN_SGIS_TO_STRING__
char * __attribute__((const)) gl_sample_pattern_SGIS_to_string(GLenum val){
  switch(val){
    case(GL_1PASS_SGIS):{
      return("GL_1PASS_SGIS");
    }
    case(GL_2PASS_0_SGIS):{
      return("GL_2PASS_0_SGIS");
    }
    case(GL_2PASS_1_SGIS):{
      return("GL_2PASS_1_SGIS");
    }
    case(GL_4PASS_0_SGIS):{
      return("GL_4PASS_0_SGIS");
    }
    case(GL_4PASS_1_SGIS):{
      return("GL_4PASS_1_SGIS");
    }
    case(GL_4PASS_2_SGIS):{
      return("GL_4PASS_2_SGIS");
    }
    case(GL_4PASS_3_SGIS):{
      return("GL_4PASS_3_SGIS");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_SAMPLE_PATTERN_SGIS_TO_STRING__ */

#ifdef __GL_CULL_FACE_MODE_TO_STRING__
char * __attribute__((const)) gl_cull_face_mode_to_string(GLenum val){
  switch(val){
    case(GL_BACK):{
      return("GL_BACK");
    }
    case(GL_FRONT):{
      return("GL_FRONT");
    }
    case(GL_FRONT_AND_BACK):{
      return("GL_FRONT_AND_BACK");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CULL_FACE_MODE_TO_STRING__ */

#ifdef __GL_TRANSFORM_FEEDBACK_TOKEN_NV_TO_STRING__
char * __attribute__((const)) gl_transform_feedback_token_NV_to_string(GLenum val){
  switch(val){
    case(GL_NEXT_BUFFER_NV):{
      return("GL_NEXT_BUFFER_NV");
    }
    case(GL_SKIP_COMPONENTS4_NV):{
      return("GL_SKIP_COMPONENTS4_NV");
    }
    case(GL_SKIP_COMPONENTS3_NV):{
      return("GL_SKIP_COMPONENTS3_NV");
    }
    case(GL_SKIP_COMPONENTS2_NV):{
      return("GL_SKIP_COMPONENTS2_NV");
    }
    case(GL_SKIP_COMPONENTS1_NV):{
      return("GL_SKIP_COMPONENTS1_NV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TRANSFORM_FEEDBACK_TOKEN_NV_TO_STRING__ */

#ifdef __GL_DRAW_BUFFER_MODE_TO_STRING__
char * __attribute__((const)) gl_draw_buffer_mode_to_string(GLenum val){
  switch(val){
    case(GL_AUX0):{
      return("GL_AUX0");
    }
    case(GL_AUX1):{
      return("GL_AUX1");
    }
    case(GL_AUX2):{
      return("GL_AUX2");
    }
    case(GL_AUX3):{
      return("GL_AUX3");
    }
    case(GL_BACK):{
      return("GL_BACK");
    }
    case(GL_BACK_LEFT):{
      return("GL_BACK_LEFT");
    }
    case(GL_BACK_RIGHT):{
      return("GL_BACK_RIGHT");
    }
    case(GL_FRONT):{
      return("GL_FRONT");
    }
    case(GL_FRONT_AND_BACK):{
      return("GL_FRONT_AND_BACK");
    }
    case(GL_FRONT_LEFT):{
      return("GL_FRONT_LEFT");
    }
    case(GL_FRONT_RIGHT):{
      return("GL_FRONT_RIGHT");
    }
    case(GL_LEFT):{
      return("GL_LEFT");
    }
    case(GL_NONE):{
      return("GL_NONE");
    }
    case(GL_RIGHT):{
      return("GL_RIGHT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_DRAW_BUFFER_MODE_TO_STRING__ */

#ifdef __GL_POINT_PARAMETER_NAME_SGIS_TO_STRING__
char * __attribute__((const)) gl_point_parameter_name_SGIS_to_string(GLenum val){
  switch(val){
    case(GL_DISTANCE_ATTENUATION_SGIS):{
      return("GL_DISTANCE_ATTENUATION_SGIS");
    }
    case(GL_POINT_DISTANCE_ATTENUATION):{
      return("GL_POINT_DISTANCE_ATTENUATION");
    }
    case(GL_POINT_FADE_THRESHOLD_SIZE):{
      return("GL_POINT_FADE_THRESHOLD_SIZE");
    }
    case(GL_POINT_FADE_THRESHOLD_SIZE_SGIS):{
      return("GL_POINT_FADE_THRESHOLD_SIZE_SGIS");
    }
    case(GL_POINT_SIZE_MAX):{
      return("GL_POINT_SIZE_MAX");
    }
    case(GL_POINT_SIZE_MAX_SGIS):{
      return("GL_POINT_SIZE_MAX_SGIS");
    }
    case(GL_POINT_SIZE_MIN):{
      return("GL_POINT_SIZE_MIN");
    }
    case(GL_POINT_SIZE_MIN_SGIS):{
      return("GL_POINT_SIZE_MIN_SGIS");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_POINT_PARAMETER_NAME_SGIS_TO_STRING__ */

#ifdef __GL_TEXTURE_ENV_TARGET_TO_STRING__
char * __attribute__((const)) gl_texture_env_target_to_string(GLenum val){
  switch(val){
    case(GL_TEXTURE_ENV):{
      return("GL_TEXTURE_ENV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_ENV_TARGET_TO_STRING__ */

#ifdef __GL_CLIP_PLANE_NAME_TO_STRING__
char * __attribute__((const)) gl_clip_plane_name_to_string(GLenum val){
  switch(val){
    case(GL_CLIP_DISTANCE0):{
      return("GL_CLIP_DISTANCE0");
    }
    case(GL_CLIP_DISTANCE1):{
      return("GL_CLIP_DISTANCE1");
    }
    case(GL_CLIP_DISTANCE2):{
      return("GL_CLIP_DISTANCE2");
    }
    case(GL_CLIP_DISTANCE3):{
      return("GL_CLIP_DISTANCE3");
    }
    case(GL_CLIP_DISTANCE4):{
      return("GL_CLIP_DISTANCE4");
    }
    case(GL_CLIP_DISTANCE5):{
      return("GL_CLIP_DISTANCE5");
    }
    case(GL_CLIP_DISTANCE6):{
      return("GL_CLIP_DISTANCE6");
    }
    case(GL_CLIP_DISTANCE7):{
      return("GL_CLIP_DISTANCE7");
    }
    case(GL_CLIP_PLANE0):{
      return("GL_CLIP_PLANE0");
    }
    case(GL_CLIP_PLANE1):{
      return("GL_CLIP_PLANE1");
    }
    case(GL_CLIP_PLANE2):{
      return("GL_CLIP_PLANE2");
    }
    case(GL_CLIP_PLANE3):{
      return("GL_CLIP_PLANE3");
    }
    case(GL_CLIP_PLANE4):{
      return("GL_CLIP_PLANE4");
    }
    case(GL_CLIP_PLANE5):{
      return("GL_CLIP_PLANE5");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CLIP_PLANE_NAME_TO_STRING__ */

#ifdef __GL_FOG_MODE_TO_STRING__
char * __attribute__((const)) gl_fog_mode_to_string(GLenum val){
  switch(val){
    case(GL_EXP):{
      return("GL_EXP");
    }
    case(GL_EXP2):{
      return("GL_EXP2");
    }
    case(GL_FOG_FUNC_SGIS):{
      return("GL_FOG_FUNC_SGIS");
    }
    case(GL_LINEAR):{
      return("GL_LINEAR");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FOG_MODE_TO_STRING__ */

#ifdef __GL_USE_PROGRAM_STAGE_MASK_TO_STRING__
char * __attribute__((const)) gl_use_program_stage_mask_to_string(GLenum val){
  switch(val){
    case(GL_VERTEX_SHADER_BIT):{
      return("GL_VERTEX_SHADER_BIT");
    }
    case(GL_FRAGMENT_SHADER_BIT):{
      return("GL_FRAGMENT_SHADER_BIT");
    }
    case(GL_GEOMETRY_SHADER_BIT):{
      return("GL_GEOMETRY_SHADER_BIT");
    }
    case(GL_TESS_CONTROL_SHADER_BIT):{
      return("GL_TESS_CONTROL_SHADER_BIT");
    }
    case(GL_TESS_EVALUATION_SHADER_BIT):{
      return("GL_TESS_EVALUATION_SHADER_BIT");
    }
    case(GL_COMPUTE_SHADER_BIT):{
      return("GL_COMPUTE_SHADER_BIT");
    }
    case(GL_ALL_SHADER_BITS):{
      return("GL_ALL_SHADER_BITS");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_USE_PROGRAM_STAGE_MASK_TO_STRING__ */

#ifdef __GL_FOG_POINTER_TYPE_IBM_TO_STRING__
char * __attribute__((const)) gl_fog_pointer_type_IBM_to_string(GLenum val){
  switch(val){
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FOG_POINTER_TYPE_IBM_TO_STRING__ */

#ifdef __GL_FOG_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_fog_parameter_to_string(GLenum val){
  switch(val){
    case(GL_FOG_COLOR):{
      return("GL_FOG_COLOR");
    }
    case(GL_FOG_DENSITY):{
      return("GL_FOG_DENSITY");
    }
    case(GL_FOG_END):{
      return("GL_FOG_END");
    }
    case(GL_FOG_INDEX):{
      return("GL_FOG_INDEX");
    }
    case(GL_FOG_MODE):{
      return("GL_FOG_MODE");
    }
    case(GL_FOG_OFFSET_VALUE_SGIX):{
      return("GL_FOG_OFFSET_VALUE_SGIX");
    }
    case(GL_FOG_START):{
      return("GL_FOG_START");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FOG_PARAMETER_TO_STRING__ */

#ifdef __GL_COLOR_POINTER_TYPE_TO_STRING__
char * __attribute__((const)) gl_color_pointer_type_to_string(GLenum val){
  switch(val){
    case(GL_BYTE):{
      return("GL_BYTE");
    }
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    case(GL_UNSIGNED_BYTE):{
      return("GL_UNSIGNED_BYTE");
    }
    case(GL_UNSIGNED_INT):{
      return("GL_UNSIGNED_INT");
    }
    case(GL_UNSIGNED_SHORT):{
      return("GL_UNSIGNED_SHORT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_COLOR_POINTER_TYPE_TO_STRING__ */

#ifdef __GL_SEPARABLE_TARGET_EXT_TO_STRING__
char * __attribute__((const)) gl_separable_target_EXT_to_string(GLenum val){
  switch(val){
    case(GL_SEPARABLE_2D):{
      return("GL_SEPARABLE_2D");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_SEPARABLE_TARGET_EXT_TO_STRING__ */

#ifdef __GL_PIXEL_TYPE_TO_STRING__
char * __attribute__((const)) gl_pixel_type_to_string(GLenum val){
  switch(val){
    case(GL_BITMAP):{
      return("GL_BITMAP");
    }
    case(GL_BYTE):{
      return("GL_BYTE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    case(GL_UNSIGNED_BYTE):{
      return("GL_UNSIGNED_BYTE");
    }
    case(GL_UNSIGNED_BYTE_3_3_2):{
      return("GL_UNSIGNED_BYTE_3_3_2");
    }
    case(GL_UNSIGNED_INT):{
      return("GL_UNSIGNED_INT");
    }
    case(GL_UNSIGNED_INT_10_10_10_2):{
      return("GL_UNSIGNED_INT_10_10_10_2");
    }
    case(GL_UNSIGNED_INT_8_8_8_8):{
      return("GL_UNSIGNED_INT_8_8_8_8");
    }
    case(GL_UNSIGNED_SHORT):{
      return("GL_UNSIGNED_SHORT");
    }
    case(GL_UNSIGNED_SHORT_4_4_4_4):{
      return("GL_UNSIGNED_SHORT_4_4_4_4");
    }
    case(GL_UNSIGNED_SHORT_5_5_5_1):{
      return("GL_UNSIGNED_SHORT_5_5_5_1");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_TYPE_TO_STRING__ */

#ifdef __GL_PIXEL_STORE_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_pixel_store_parameter_to_string(GLenum val){
  switch(val){
    case(GL_PACK_ALIGNMENT):{
      return("GL_PACK_ALIGNMENT");
    }
    case(GL_PACK_IMAGE_DEPTH_SGIS):{
      return("GL_PACK_IMAGE_DEPTH_SGIS");
    }
    case(GL_PACK_IMAGE_HEIGHT):{
      return("GL_PACK_IMAGE_HEIGHT");
    }
    case(GL_PACK_LSB_FIRST):{
      return("GL_PACK_LSB_FIRST");
    }
    case(GL_PACK_RESAMPLE_OML):{
      return("GL_PACK_RESAMPLE_OML");
    }
    case(GL_PACK_RESAMPLE_SGIX):{
      return("GL_PACK_RESAMPLE_SGIX");
    }
    case(GL_PACK_ROW_LENGTH):{
      return("GL_PACK_ROW_LENGTH");
    }
    case(GL_PACK_SKIP_IMAGES):{
      return("GL_PACK_SKIP_IMAGES");
    }
    case(GL_PACK_SKIP_PIXELS):{
      return("GL_PACK_SKIP_PIXELS");
    }
    case(GL_PACK_SKIP_ROWS):{
      return("GL_PACK_SKIP_ROWS");
    }
    case(GL_PACK_SKIP_VOLUMES_SGIS):{
      return("GL_PACK_SKIP_VOLUMES_SGIS");
    }
    case(GL_PACK_SUBSAMPLE_RATE_SGIX):{
      return("GL_PACK_SUBSAMPLE_RATE_SGIX");
    }
    case(GL_PACK_SWAP_BYTES):{
      return("GL_PACK_SWAP_BYTES");
    }
    case(GL_PIXEL_TILE_CACHE_SIZE_SGIX):{
      return("GL_PIXEL_TILE_CACHE_SIZE_SGIX");
    }
    case(GL_PIXEL_TILE_GRID_DEPTH_SGIX):{
      return("GL_PIXEL_TILE_GRID_DEPTH_SGIX");
    }
    case(GL_PIXEL_TILE_GRID_HEIGHT_SGIX):{
      return("GL_PIXEL_TILE_GRID_HEIGHT_SGIX");
    }
    case(GL_PIXEL_TILE_GRID_WIDTH_SGIX):{
      return("GL_PIXEL_TILE_GRID_WIDTH_SGIX");
    }
    case(GL_PIXEL_TILE_HEIGHT_SGIX):{
      return("GL_PIXEL_TILE_HEIGHT_SGIX");
    }
    case(GL_PIXEL_TILE_WIDTH_SGIX):{
      return("GL_PIXEL_TILE_WIDTH_SGIX");
    }
    case(GL_UNPACK_ALIGNMENT):{
      return("GL_UNPACK_ALIGNMENT");
    }
    case(GL_UNPACK_IMAGE_DEPTH_SGIS):{
      return("GL_UNPACK_IMAGE_DEPTH_SGIS");
    }
    case(GL_UNPACK_IMAGE_HEIGHT):{
      return("GL_UNPACK_IMAGE_HEIGHT");
    }
    case(GL_UNPACK_LSB_FIRST):{
      return("GL_UNPACK_LSB_FIRST");
    }
    case(GL_UNPACK_RESAMPLE_OML):{
      return("GL_UNPACK_RESAMPLE_OML");
    }
    case(GL_UNPACK_RESAMPLE_SGIX):{
      return("GL_UNPACK_RESAMPLE_SGIX");
    }
    case(GL_UNPACK_ROW_LENGTH):{
      return("GL_UNPACK_ROW_LENGTH");
    }
    case(GL_UNPACK_SKIP_IMAGES):{
      return("GL_UNPACK_SKIP_IMAGES");
    }
    case(GL_UNPACK_SKIP_PIXELS):{
      return("GL_UNPACK_SKIP_PIXELS");
    }
    case(GL_UNPACK_SKIP_ROWS):{
      return("GL_UNPACK_SKIP_ROWS");
    }
    case(GL_UNPACK_SKIP_VOLUMES_SGIS):{
      return("GL_UNPACK_SKIP_VOLUMES_SGIS");
    }
    case(GL_UNPACK_SUBSAMPLE_RATE_SGIX):{
      return("GL_UNPACK_SUBSAMPLE_RATE_SGIX");
    }
    case(GL_UNPACK_SWAP_BYTES):{
      return("GL_UNPACK_SWAP_BYTES");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_STORE_PARAMETER_TO_STRING__ */

#ifdef __GL_TEXTURE_ENV_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_texture_env_parameter_to_string(GLenum val){
  switch(val){
    case(GL_TEXTURE_ENV_COLOR):{
      return("GL_TEXTURE_ENV_COLOR");
    }
    case(GL_TEXTURE_ENV_MODE):{
      return("GL_TEXTURE_ENV_MODE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_ENV_PARAMETER_TO_STRING__ */

#ifdef __GL_ATTRIBUTE_TYPE_TO_STRING__
char * __attribute__((const)) gl_attribute_type_to_string(GLenum val){
  switch(val){
    case(GL_FLOAT_VEC2):{
      return("GL_FLOAT_VEC2");
    }
    case(GL_FLOAT_VEC3):{
      return("GL_FLOAT_VEC3");
    }
    case(GL_FLOAT_VEC4):{
      return("GL_FLOAT_VEC4");
    }
    case(GL_INT_VEC2):{
      return("GL_INT_VEC2");
    }
    case(GL_INT_VEC3):{
      return("GL_INT_VEC3");
    }
    case(GL_INT_VEC4):{
      return("GL_INT_VEC4");
    }
    case(GL_BOOL):{
      return("GL_BOOL");
    }
    case(GL_BOOL_VEC2):{
      return("GL_BOOL_VEC2");
    }
    case(GL_BOOL_VEC3):{
      return("GL_BOOL_VEC3");
    }
    case(GL_BOOL_VEC4):{
      return("GL_BOOL_VEC4");
    }
    case(GL_FLOAT_MAT2):{
      return("GL_FLOAT_MAT2");
    }
    case(GL_FLOAT_MAT3):{
      return("GL_FLOAT_MAT3");
    }
    case(GL_FLOAT_MAT4):{
      return("GL_FLOAT_MAT4");
    }
    case(GL_SAMPLER_1D):{
      return("GL_SAMPLER_1D");
    }
    case(GL_SAMPLER_2D):{
      return("GL_SAMPLER_2D");
    }
    case(GL_SAMPLER_3D):{
      return("GL_SAMPLER_3D");
    }
    case(GL_SAMPLER_CUBE):{
      return("GL_SAMPLER_CUBE");
    }
    case(GL_SAMPLER_1D_SHADOW):{
      return("GL_SAMPLER_1D_SHADOW");
    }
    case(GL_SAMPLER_2D_SHADOW):{
      return("GL_SAMPLER_2D_SHADOW");
    }
    case(GL_SAMPLER_2D_RECT):{
      return("GL_SAMPLER_2D_RECT");
    }
    case(GL_SAMPLER_2D_RECT_SHADOW):{
      return("GL_SAMPLER_2D_RECT_SHADOW");
    }
    case(GL_FLOAT_MAT2x3):{
      return("GL_FLOAT_MAT2x3");
    }
    case(GL_FLOAT_MAT2x3_NV):{
      return("GL_FLOAT_MAT2x3_NV");
    }
    case(GL_FLOAT_MAT2x4):{
      return("GL_FLOAT_MAT2x4");
    }
    case(GL_FLOAT_MAT2x4_NV):{
      return("GL_FLOAT_MAT2x4_NV");
    }
    case(GL_FLOAT_MAT3x2):{
      return("GL_FLOAT_MAT3x2");
    }
    case(GL_FLOAT_MAT3x2_NV):{
      return("GL_FLOAT_MAT3x2_NV");
    }
    case(GL_FLOAT_MAT3x4):{
      return("GL_FLOAT_MAT3x4");
    }
    case(GL_FLOAT_MAT3x4_NV):{
      return("GL_FLOAT_MAT3x4_NV");
    }
    case(GL_FLOAT_MAT4x2):{
      return("GL_FLOAT_MAT4x2");
    }
    case(GL_FLOAT_MAT4x2_NV):{
      return("GL_FLOAT_MAT4x2_NV");
    }
    case(GL_FLOAT_MAT4x3):{
      return("GL_FLOAT_MAT4x3");
    }
    case(GL_FLOAT_MAT4x3_NV):{
      return("GL_FLOAT_MAT4x3_NV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_ATTRIBUTE_TYPE_TO_STRING__ */

#ifdef __GL_CONVOLUTION_BORDER_MODE_EXT_TO_STRING__
char * __attribute__((const)) gl_convolution_border_mode_EXT_to_string(GLenum val){
  switch(val){
    case(GL_REDUCE):{
      return("GL_REDUCE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CONVOLUTION_BORDER_MODE_EXT_TO_STRING__ */

#ifdef __GL_FOG_COORDINATE_POINTER_TYPE_TO_STRING__
char * __attribute__((const)) gl_fog_coordinate_pointer_type_to_string(GLenum val){
  switch(val){
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FOG_COORDINATE_POINTER_TYPE_TO_STRING__ */

#ifdef __GL_REGISTER_COMBINER_PNAME_TO_STRING__
char * __attribute__((const)) gl_register_combiner_pname_to_string(GLenum val){
  switch(val){
    case(GL_COMBINE):{
      return("GL_COMBINE");
    }
    case(GL_COMBINE_RGB):{
      return("GL_COMBINE_RGB");
    }
    case(GL_COMBINE_ALPHA):{
      return("GL_COMBINE_ALPHA");
    }
    case(GL_RGB_SCALE):{
      return("GL_RGB_SCALE");
    }
    case(GL_ADD_SIGNED):{
      return("GL_ADD_SIGNED");
    }
    case(GL_INTERPOLATE):{
      return("GL_INTERPOLATE");
    }
    case(GL_CONSTANT):{
      return("GL_CONSTANT");
    }
    case(GL_CONSTANT_NV):{
      return("GL_CONSTANT_NV");
    }
    case(GL_PRIMARY_COLOR):{
      return("GL_PRIMARY_COLOR");
    }
    case(GL_PREVIOUS):{
      return("GL_PREVIOUS");
    }
    case(GL_SOURCE0_RGB):{
      return("GL_SOURCE0_RGB");
    }
    case(GL_SRC0_RGB):{
      return("GL_SRC0_RGB");
    }
    case(GL_SOURCE1_RGB):{
      return("GL_SOURCE1_RGB");
    }
    case(GL_SRC1_RGB):{
      return("GL_SRC1_RGB");
    }
    case(GL_SOURCE2_RGB):{
      return("GL_SOURCE2_RGB");
    }
    case(GL_SRC2_RGB):{
      return("GL_SRC2_RGB");
    }
    case(GL_SOURCE3_RGB_NV):{
      return("GL_SOURCE3_RGB_NV");
    }
    case(GL_SOURCE0_ALPHA):{
      return("GL_SOURCE0_ALPHA");
    }
    case(GL_SRC0_ALPHA):{
      return("GL_SRC0_ALPHA");
    }
    case(GL_SOURCE1_ALPHA):{
      return("GL_SOURCE1_ALPHA");
    }
    case(GL_SRC1_ALPHA):{
      return("GL_SRC1_ALPHA");
    }
    case(GL_SOURCE2_ALPHA):{
      return("GL_SOURCE2_ALPHA");
    }
    case(GL_SRC2_ALPHA):{
      return("GL_SRC2_ALPHA");
    }
    case(GL_SOURCE3_ALPHA_NV):{
      return("GL_SOURCE3_ALPHA_NV");
    }
    case(GL_OPERAND0_RGB):{
      return("GL_OPERAND0_RGB");
    }
    case(GL_OPERAND1_RGB):{
      return("GL_OPERAND1_RGB");
    }
    case(GL_OPERAND2_RGB):{
      return("GL_OPERAND2_RGB");
    }
    case(GL_OPERAND3_RGB_NV):{
      return("GL_OPERAND3_RGB_NV");
    }
    case(GL_OPERAND0_ALPHA):{
      return("GL_OPERAND0_ALPHA");
    }
    case(GL_OPERAND1_ALPHA):{
      return("GL_OPERAND1_ALPHA");
    }
    case(GL_OPERAND2_ALPHA):{
      return("GL_OPERAND2_ALPHA");
    }
    case(GL_OPERAND3_ALPHA_NV):{
      return("GL_OPERAND3_ALPHA_NV");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_REGISTER_COMBINER_PNAME_TO_STRING__ */

#ifdef __GL_TEXTURE_GEN_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_texture_gen_parameter_to_string(GLenum val){
  switch(val){
    case(GL_EYE_LINE_SGIS):{
      return("GL_EYE_LINE_SGIS");
    }
    case(GL_EYE_PLANE):{
      return("GL_EYE_PLANE");
    }
    case(GL_EYE_POINT_SGIS):{
      return("GL_EYE_POINT_SGIS");
    }
    case(GL_OBJECT_LINE_SGIS):{
      return("GL_OBJECT_LINE_SGIS");
    }
    case(GL_OBJECT_PLANE):{
      return("GL_OBJECT_PLANE");
    }
    case(GL_OBJECT_POINT_SGIS):{
      return("GL_OBJECT_POINT_SGIS");
    }
    case(GL_TEXTURE_GEN_MODE):{
      return("GL_TEXTURE_GEN_MODE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_GEN_PARAMETER_TO_STRING__ */

#ifdef __GL_LIGHT_MODEL_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_light_model_parameter_to_string(GLenum val){
  switch(val){
    case(GL_LIGHT_MODEL_AMBIENT):{
      return("GL_LIGHT_MODEL_AMBIENT");
    }
    case(GL_LIGHT_MODEL_COLOR_CONTROL):{
      return("GL_LIGHT_MODEL_COLOR_CONTROL");
    }
    case(GL_LIGHT_MODEL_LOCAL_VIEWER):{
      return("GL_LIGHT_MODEL_LOCAL_VIEWER");
    }
    case(GL_LIGHT_MODEL_TWO_SIDE):{
      return("GL_LIGHT_MODEL_TWO_SIDE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIGHT_MODEL_PARAMETER_TO_STRING__ */

#ifdef __GL_ERROR_CODE_TO_STRING__
char * __attribute__((const)) gl_error_code_to_string(GLenum val){
  switch(val){
    case(GL_INVALID_ENUM):{
      return("GL_INVALID_ENUM");
    }
    case(GL_INVALID_FRAMEBUFFER_OPERATION):{
      return("GL_INVALID_FRAMEBUFFER_OPERATION");
    }
    case(GL_INVALID_OPERATION):{
      return("GL_INVALID_OPERATION");
    }
    case(GL_INVALID_VALUE):{
      return("GL_INVALID_VALUE");
    }
    case(GL_NO_ERROR):{
      return("GL_NO_ERROR");
    }
    case(GL_OUT_OF_MEMORY):{
      return("GL_OUT_OF_MEMORY");
    }
    case(GL_STACK_OVERFLOW):{
      return("GL_STACK_OVERFLOW");
    }
    case(GL_STACK_UNDERFLOW):{
      return("GL_STACK_UNDERFLOW");
    }
    case(GL_TABLE_TOO_LARGE):{
      return("GL_TABLE_TOO_LARGE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_ERROR_CODE_TO_STRING__ */

#ifdef __GL_MAP_TEXTURE_FORMAT_INTEL_TO_STRING__
char * __attribute__((const)) gl_map_texture_format_INTEL_to_string(GLenum val){
  switch(val){
    case(GL_LAYOUT_DEFAULT_INTEL):{
      return("GL_LAYOUT_DEFAULT_INTEL");
    }
    case(GL_LAYOUT_LINEAR_CPU_CACHED_INTEL):{
      return("GL_LAYOUT_LINEAR_CPU_CACHED_INTEL");
    }
    case(GL_LAYOUT_LINEAR_INTEL):{
      return("GL_LAYOUT_LINEAR_INTEL");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MAP_TEXTURE_FORMAT_INTEL_TO_STRING__ */

#ifdef __GL_HISTOGRAM_TARGET_EXT_TO_STRING__
char * __attribute__((const)) gl_histogram_target_EXT_to_string(GLenum val){
  switch(val){
    case(GL_HISTOGRAM):{
      return("GL_HISTOGRAM");
    }
    case(GL_PROXY_HISTOGRAM):{
      return("GL_PROXY_HISTOGRAM");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_HISTOGRAM_TARGET_EXT_TO_STRING__ */

#ifdef __GL_TEXTURE_FILTER_FUNC_SGIS_TO_STRING__
char * __attribute__((const)) gl_texture_filter_func_SGIS_to_string(GLenum val){
  switch(val){
    case(GL_FILTER4_SGIS):{
      return("GL_FILTER4_SGIS");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_FILTER_FUNC_SGIS_TO_STRING__ */

#ifdef __GL_FEED_BACK_TOKEN_TO_STRING__
char * __attribute__((const)) gl_feed_back_token_to_string(GLenum val){
  switch(val){
    case(GL_BITMAP_TOKEN):{
      return("GL_BITMAP_TOKEN");
    }
    case(GL_COPY_PIXEL_TOKEN):{
      return("GL_COPY_PIXEL_TOKEN");
    }
    case(GL_DRAW_PIXEL_TOKEN):{
      return("GL_DRAW_PIXEL_TOKEN");
    }
    case(GL_LINE_RESET_TOKEN):{
      return("GL_LINE_RESET_TOKEN");
    }
    case(GL_LINE_TOKEN):{
      return("GL_LINE_TOKEN");
    }
    case(GL_PASS_THROUGH_TOKEN):{
      return("GL_PASS_THROUGH_TOKEN");
    }
    case(GL_POINT_TOKEN):{
      return("GL_POINT_TOKEN");
    }
    case(GL_POLYGON_TOKEN):{
      return("GL_POLYGON_TOKEN");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FEED_BACK_TOKEN_TO_STRING__ */

#ifdef __GL_LIST_PARAMETER_NAME_TO_STRING__
char * __attribute__((const)) gl_list_parameter_name_to_string(GLenum val){
  switch(val){
    case(GL_LIST_PRIORITY_SGIX):{
      return("GL_LIST_PRIORITY_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_LIST_PARAMETER_NAME_TO_STRING__ */

#ifdef __GL_TEXTURE_WRAP_MODE_TO_STRING__
char * __attribute__((const)) gl_texture_wrap_mode_to_string(GLenum val){
  switch(val){
    case(GL_CLAMP):{
      return("GL_CLAMP");
    }
    case(GL_CLAMP_TO_BORDER):{
      return("GL_CLAMP_TO_BORDER");
    }
    case(GL_CLAMP_TO_BORDER_NV):{
      return("GL_CLAMP_TO_BORDER_NV");
    }
    case(GL_CLAMP_TO_BORDER_SGIS):{
      return("GL_CLAMP_TO_BORDER_SGIS");
    }
    case(GL_CLAMP_TO_EDGE):{
      return("GL_CLAMP_TO_EDGE");
    }
    case(GL_CLAMP_TO_EDGE_SGIS):{
      return("GL_CLAMP_TO_EDGE_SGIS");
    }
    case(GL_REPEAT):{
      return("GL_REPEAT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_WRAP_MODE_TO_STRING__ */

#ifdef __GL_ENABLE_CAP_TO_STRING__
char * __attribute__((const)) gl_enable_cap_to_string(GLenum val){
  switch(val){
    case(GL_ALPHA_TEST):{
      return("GL_ALPHA_TEST");
    }
    case(GL_ASYNC_DRAW_PIXELS_SGIX):{
      return("GL_ASYNC_DRAW_PIXELS_SGIX");
    }
    case(GL_ASYNC_HISTOGRAM_SGIX):{
      return("GL_ASYNC_HISTOGRAM_SGIX");
    }
    case(GL_ASYNC_READ_PIXELS_SGIX):{
      return("GL_ASYNC_READ_PIXELS_SGIX");
    }
    case(GL_ASYNC_TEX_IMAGE_SGIX):{
      return("GL_ASYNC_TEX_IMAGE_SGIX");
    }
    case(GL_AUTO_NORMAL):{
      return("GL_AUTO_NORMAL");
    }
    case(GL_BLEND):{
      return("GL_BLEND");
    }
    case(GL_CALLIGRAPHIC_FRAGMENT_SGIX):{
      return("GL_CALLIGRAPHIC_FRAGMENT_SGIX");
    }
    case(GL_CLIP_PLANE0):{
      return("GL_CLIP_PLANE0");
    }
    case(GL_CLIP_PLANE1):{
      return("GL_CLIP_PLANE1");
    }
    case(GL_CLIP_PLANE2):{
      return("GL_CLIP_PLANE2");
    }
    case(GL_CLIP_PLANE3):{
      return("GL_CLIP_PLANE3");
    }
    case(GL_CLIP_PLANE4):{
      return("GL_CLIP_PLANE4");
    }
    case(GL_CLIP_PLANE5):{
      return("GL_CLIP_PLANE5");
    }
    case(GL_COLOR_ARRAY):{
      return("GL_COLOR_ARRAY");
    }
    case(GL_COLOR_LOGIC_OP):{
      return("GL_COLOR_LOGIC_OP");
    }
    case(GL_COLOR_MATERIAL):{
      return("GL_COLOR_MATERIAL");
    }
    case(GL_COLOR_TABLE_SGI):{
      return("GL_COLOR_TABLE_SGI");
    }
    case(GL_CULL_FACE):{
      return("GL_CULL_FACE");
    }
    case(GL_DEPTH_TEST):{
      return("GL_DEPTH_TEST");
    }
    case(GL_DITHER):{
      return("GL_DITHER");
    }
    case(GL_EDGE_FLAG_ARRAY):{
      return("GL_EDGE_FLAG_ARRAY");
    }
    case(GL_FOG):{
      return("GL_FOG");
    }
    case(GL_FOG_OFFSET_SGIX):{
      return("GL_FOG_OFFSET_SGIX");
    }
    case(GL_FRAGMENT_COLOR_MATERIAL_SGIX):{
      return("GL_FRAGMENT_COLOR_MATERIAL_SGIX");
    }
    case(GL_FRAGMENT_LIGHT0_SGIX):{
      return("GL_FRAGMENT_LIGHT0_SGIX");
    }
    case(GL_FRAGMENT_LIGHT1_SGIX):{
      return("GL_FRAGMENT_LIGHT1_SGIX");
    }
    case(GL_FRAGMENT_LIGHT2_SGIX):{
      return("GL_FRAGMENT_LIGHT2_SGIX");
    }
    case(GL_FRAGMENT_LIGHT3_SGIX):{
      return("GL_FRAGMENT_LIGHT3_SGIX");
    }
    case(GL_FRAGMENT_LIGHT4_SGIX):{
      return("GL_FRAGMENT_LIGHT4_SGIX");
    }
    case(GL_FRAGMENT_LIGHT5_SGIX):{
      return("GL_FRAGMENT_LIGHT5_SGIX");
    }
    case(GL_FRAGMENT_LIGHT6_SGIX):{
      return("GL_FRAGMENT_LIGHT6_SGIX");
    }
    case(GL_FRAGMENT_LIGHT7_SGIX):{
      return("GL_FRAGMENT_LIGHT7_SGIX");
    }
    case(GL_FRAGMENT_LIGHTING_SGIX):{
      return("GL_FRAGMENT_LIGHTING_SGIX");
    }
    case(GL_FRAMEZOOM_SGIX):{
      return("GL_FRAMEZOOM_SGIX");
    }
    case(GL_INDEX_ARRAY):{
      return("GL_INDEX_ARRAY");
    }
    case(GL_INDEX_LOGIC_OP):{
      return("GL_INDEX_LOGIC_OP");
    }
    case(GL_INTERLACE_SGIX):{
      return("GL_INTERLACE_SGIX");
    }
    case(GL_IR_INSTRUMENT1_SGIX):{
      return("GL_IR_INSTRUMENT1_SGIX");
    }
    case(GL_LIGHT0):{
      return("GL_LIGHT0");
    }
    case(GL_LIGHT1):{
      return("GL_LIGHT1");
    }
    case(GL_LIGHT2):{
      return("GL_LIGHT2");
    }
    case(GL_LIGHT3):{
      return("GL_LIGHT3");
    }
    case(GL_LIGHT4):{
      return("GL_LIGHT4");
    }
    case(GL_LIGHT5):{
      return("GL_LIGHT5");
    }
    case(GL_LIGHT6):{
      return("GL_LIGHT6");
    }
    case(GL_LIGHT7):{
      return("GL_LIGHT7");
    }
    case(GL_LIGHTING):{
      return("GL_LIGHTING");
    }
    case(GL_LINE_SMOOTH):{
      return("GL_LINE_SMOOTH");
    }
    case(GL_LINE_STIPPLE):{
      return("GL_LINE_STIPPLE");
    }
    case(GL_MAP1_COLOR_4):{
      return("GL_MAP1_COLOR_4");
    }
    case(GL_MAP1_INDEX):{
      return("GL_MAP1_INDEX");
    }
    case(GL_MAP1_NORMAL):{
      return("GL_MAP1_NORMAL");
    }
    case(GL_MAP1_TEXTURE_COORD_1):{
      return("GL_MAP1_TEXTURE_COORD_1");
    }
    case(GL_MAP1_TEXTURE_COORD_2):{
      return("GL_MAP1_TEXTURE_COORD_2");
    }
    case(GL_MAP1_TEXTURE_COORD_3):{
      return("GL_MAP1_TEXTURE_COORD_3");
    }
    case(GL_MAP1_TEXTURE_COORD_4):{
      return("GL_MAP1_TEXTURE_COORD_4");
    }
    case(GL_MAP1_VERTEX_3):{
      return("GL_MAP1_VERTEX_3");
    }
    case(GL_MAP1_VERTEX_4):{
      return("GL_MAP1_VERTEX_4");
    }
    case(GL_MAP2_COLOR_4):{
      return("GL_MAP2_COLOR_4");
    }
    case(GL_MAP2_INDEX):{
      return("GL_MAP2_INDEX");
    }
    case(GL_MAP2_NORMAL):{
      return("GL_MAP2_NORMAL");
    }
    case(GL_MAP2_TEXTURE_COORD_1):{
      return("GL_MAP2_TEXTURE_COORD_1");
    }
    case(GL_MAP2_TEXTURE_COORD_2):{
      return("GL_MAP2_TEXTURE_COORD_2");
    }
    case(GL_MAP2_TEXTURE_COORD_3):{
      return("GL_MAP2_TEXTURE_COORD_3");
    }
    case(GL_MAP2_TEXTURE_COORD_4):{
      return("GL_MAP2_TEXTURE_COORD_4");
    }
    case(GL_MAP2_VERTEX_3):{
      return("GL_MAP2_VERTEX_3");
    }
    case(GL_MAP2_VERTEX_4):{
      return("GL_MAP2_VERTEX_4");
    }
    case(GL_MULTISAMPLE_SGIS):{
      return("GL_MULTISAMPLE_SGIS");
    }
    case(GL_NORMALIZE):{
      return("GL_NORMALIZE");
    }
    case(GL_NORMAL_ARRAY):{
      return("GL_NORMAL_ARRAY");
    }
    case(GL_PIXEL_TEXTURE_SGIS):{
      return("GL_PIXEL_TEXTURE_SGIS");
    }
    case(GL_PIXEL_TEX_GEN_SGIX):{
      return("GL_PIXEL_TEX_GEN_SGIX");
    }
    case(GL_POINT_SMOOTH):{
      return("GL_POINT_SMOOTH");
    }
    case(GL_POLYGON_OFFSET_FILL):{
      return("GL_POLYGON_OFFSET_FILL");
    }
    case(GL_POLYGON_OFFSET_LINE):{
      return("GL_POLYGON_OFFSET_LINE");
    }
    case(GL_POLYGON_OFFSET_POINT):{
      return("GL_POLYGON_OFFSET_POINT");
    }
    case(GL_POLYGON_SMOOTH):{
      return("GL_POLYGON_SMOOTH");
    }
    case(GL_POLYGON_STIPPLE):{
      return("GL_POLYGON_STIPPLE");
    }
    case(GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI):{
      return("GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI");
    }
    case(GL_POST_CONVOLUTION_COLOR_TABLE_SGI):{
      return("GL_POST_CONVOLUTION_COLOR_TABLE_SGI");
    }
    case(GL_REFERENCE_PLANE_SGIX):{
      return("GL_REFERENCE_PLANE_SGIX");
    }
    case(GL_SAMPLE_ALPHA_TO_MASK_SGIS):{
      return("GL_SAMPLE_ALPHA_TO_MASK_SGIS");
    }
    case(GL_SAMPLE_ALPHA_TO_ONE_SGIS):{
      return("GL_SAMPLE_ALPHA_TO_ONE_SGIS");
    }
    case(GL_SAMPLE_MASK_SGIS):{
      return("GL_SAMPLE_MASK_SGIS");
    }
    case(GL_SCISSOR_TEST):{
      return("GL_SCISSOR_TEST");
    }
    case(GL_SPRITE_SGIX):{
      return("GL_SPRITE_SGIX");
    }
    case(GL_STENCIL_TEST):{
      return("GL_STENCIL_TEST");
    }
    case(GL_TEXTURE_1D):{
      return("GL_TEXTURE_1D");
    }
    case(GL_TEXTURE_2D):{
      return("GL_TEXTURE_2D");
    }
    case(GL_TEXTURE_4D_SGIS):{
      return("GL_TEXTURE_4D_SGIS");
    }
    case(GL_TEXTURE_COLOR_TABLE_SGI):{
      return("GL_TEXTURE_COLOR_TABLE_SGI");
    }
    case(GL_TEXTURE_COORD_ARRAY):{
      return("GL_TEXTURE_COORD_ARRAY");
    }
    case(GL_TEXTURE_GEN_Q):{
      return("GL_TEXTURE_GEN_Q");
    }
    case(GL_TEXTURE_GEN_R):{
      return("GL_TEXTURE_GEN_R");
    }
    case(GL_TEXTURE_GEN_S):{
      return("GL_TEXTURE_GEN_S");
    }
    case(GL_TEXTURE_GEN_T):{
      return("GL_TEXTURE_GEN_T");
    }
    case(GL_VERTEX_ARRAY):{
      return("GL_VERTEX_ARRAY");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_ENABLE_CAP_TO_STRING__ */

#ifdef __GL_SHADING_MODEL_TO_STRING__
char * __attribute__((const)) gl_shading_model_to_string(GLenum val){
  switch(val){
    case(GL_FLAT):{
      return("GL_FLAT");
    }
    case(GL_SMOOTH):{
      return("GL_SMOOTH");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_SHADING_MODEL_TO_STRING__ */

#ifdef __GL_MATERIAL_PARAMETER_TO_STRING__
char * __attribute__((const)) gl_material_parameter_to_string(GLenum val){
  switch(val){
    case(GL_AMBIENT):{
      return("GL_AMBIENT");
    }
    case(GL_AMBIENT_AND_DIFFUSE):{
      return("GL_AMBIENT_AND_DIFFUSE");
    }
    case(GL_COLOR_INDEXES):{
      return("GL_COLOR_INDEXES");
    }
    case(GL_DIFFUSE):{
      return("GL_DIFFUSE");
    }
    case(GL_EMISSION):{
      return("GL_EMISSION");
    }
    case(GL_SHININESS):{
      return("GL_SHININESS");
    }
    case(GL_SPECULAR):{
      return("GL_SPECULAR");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MATERIAL_PARAMETER_TO_STRING__ */

#ifdef __GL_HINT_TARGET_TO_STRING__
char * __attribute__((const)) gl_hint_target_to_string(GLenum val){
  switch(val){
    case(GL_ALLOW_DRAW_FRG_HINT_PGI):{
      return("GL_ALLOW_DRAW_FRG_HINT_PGI");
    }
    case(GL_ALLOW_DRAW_MEM_HINT_PGI):{
      return("GL_ALLOW_DRAW_MEM_HINT_PGI");
    }
    case(GL_ALLOW_DRAW_OBJ_HINT_PGI):{
      return("GL_ALLOW_DRAW_OBJ_HINT_PGI");
    }
    case(GL_ALLOW_DRAW_WIN_HINT_PGI):{
      return("GL_ALLOW_DRAW_WIN_HINT_PGI");
    }
    case(GL_ALWAYS_FAST_HINT_PGI):{
      return("GL_ALWAYS_FAST_HINT_PGI");
    }
    case(GL_ALWAYS_SOFT_HINT_PGI):{
      return("GL_ALWAYS_SOFT_HINT_PGI");
    }
    case(GL_BACK_NORMALS_HINT_PGI):{
      return("GL_BACK_NORMALS_HINT_PGI");
    }
    case(GL_BINNING_CONTROL_HINT_QCOM):{
      return("GL_BINNING_CONTROL_HINT_QCOM");
    }
    case(GL_CLIP_FAR_HINT_PGI):{
      return("GL_CLIP_FAR_HINT_PGI");
    }
    case(GL_CLIP_NEAR_HINT_PGI):{
      return("GL_CLIP_NEAR_HINT_PGI");
    }
    case(GL_CONSERVE_MEMORY_HINT_PGI):{
      return("GL_CONSERVE_MEMORY_HINT_PGI");
    }
    case(GL_CONVOLUTION_HINT_SGIX):{
      return("GL_CONVOLUTION_HINT_SGIX");
    }
    case(GL_FOG_HINT):{
      return("GL_FOG_HINT");
    }
    case(GL_FRAGMENT_SHADER_DERIVATIVE_HINT):{
      return("GL_FRAGMENT_SHADER_DERIVATIVE_HINT");
    }
    case(GL_FULL_STIPPLE_HINT_PGI):{
      return("GL_FULL_STIPPLE_HINT_PGI");
    }
    case(GL_GENERATE_MIPMAP_HINT):{
      return("GL_GENERATE_MIPMAP_HINT");
    }
    case(GL_GENERATE_MIPMAP_HINT_SGIS):{
      return("GL_GENERATE_MIPMAP_HINT_SGIS");
    }
    case(GL_LINE_QUALITY_HINT_SGIX):{
      return("GL_LINE_QUALITY_HINT_SGIX");
    }
    case(GL_LINE_SMOOTH_HINT):{
      return("GL_LINE_SMOOTH_HINT");
    }
    case(GL_MATERIAL_SIDE_HINT_PGI):{
      return("GL_MATERIAL_SIDE_HINT_PGI");
    }
    case(GL_MAX_VERTEX_HINT_PGI):{
      return("GL_MAX_VERTEX_HINT_PGI");
    }
    case(GL_MULTISAMPLE_FILTER_HINT_NV):{
      return("GL_MULTISAMPLE_FILTER_HINT_NV");
    }
    case(GL_NATIVE_GRAPHICS_BEGIN_HINT_PGI):{
      return("GL_NATIVE_GRAPHICS_BEGIN_HINT_PGI");
    }
    case(GL_NATIVE_GRAPHICS_END_HINT_PGI):{
      return("GL_NATIVE_GRAPHICS_END_HINT_PGI");
    }
    case(GL_PERSPECTIVE_CORRECTION_HINT):{
      return("GL_PERSPECTIVE_CORRECTION_HINT");
    }
    case(GL_PHONG_HINT_WIN):{
      return("GL_PHONG_HINT_WIN");
    }
    case(GL_POINT_SMOOTH_HINT):{
      return("GL_POINT_SMOOTH_HINT");
    }
    case(GL_POLYGON_SMOOTH_HINT):{
      return("GL_POLYGON_SMOOTH_HINT");
    }
    case(GL_PREFER_DOUBLEBUFFER_HINT_PGI):{
      return("GL_PREFER_DOUBLEBUFFER_HINT_PGI");
    }
    case(GL_PROGRAM_BINARY_RETRIEVABLE_HINT):{
      return("GL_PROGRAM_BINARY_RETRIEVABLE_HINT");
    }
    case(GL_RECLAIM_MEMORY_HINT_PGI):{
      return("GL_RECLAIM_MEMORY_HINT_PGI");
    }
    case(GL_SCALEBIAS_HINT_SGIX):{
      return("GL_SCALEBIAS_HINT_SGIX");
    }
    case(GL_STRICT_DEPTHFUNC_HINT_PGI):{
      return("GL_STRICT_DEPTHFUNC_HINT_PGI");
    }
    case(GL_STRICT_LIGHTING_HINT_PGI):{
      return("GL_STRICT_LIGHTING_HINT_PGI");
    }
    case(GL_STRICT_SCISSOR_HINT_PGI):{
      return("GL_STRICT_SCISSOR_HINT_PGI");
    }
    case(GL_TEXTURE_COMPRESSION_HINT):{
      return("GL_TEXTURE_COMPRESSION_HINT");
    }
    case(GL_TEXTURE_MULTI_BUFFER_HINT_SGIX):{
      return("GL_TEXTURE_MULTI_BUFFER_HINT_SGIX");
    }
    case(GL_TEXTURE_STORAGE_HINT_APPLE):{
      return("GL_TEXTURE_STORAGE_HINT_APPLE");
    }
    case(GL_TRANSFORM_HINT_APPLE):{
      return("GL_TRANSFORM_HINT_APPLE");
    }
    case(GL_VERTEX_ARRAY_STORAGE_HINT_APPLE):{
      return("GL_VERTEX_ARRAY_STORAGE_HINT_APPLE");
    }
    case(GL_VERTEX_CONSISTENT_HINT_PGI):{
      return("GL_VERTEX_CONSISTENT_HINT_PGI");
    }
    case(GL_VERTEX_DATA_HINT_PGI):{
      return("GL_VERTEX_DATA_HINT_PGI");
    }
    case(GL_VERTEX_PRECLIP_HINT_SGIX):{
      return("GL_VERTEX_PRECLIP_HINT_SGIX");
    }
    case(GL_VERTEX_PRECLIP_SGIX):{
      return("GL_VERTEX_PRECLIP_SGIX");
    }
    case(GL_WIDE_LINE_HINT_PGI):{
      return("GL_WIDE_LINE_HINT_PGI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_HINT_TARGET_TO_STRING__ */

#ifdef __GL_PERFORMANCE_QUERY_CAPS_MASK_INTEL_TO_STRING__
char * __attribute__((const)) gl_performance_query_caps_mask_INTEL_to_string(GLenum val){
  switch(val){
    case(GL_PERFQUERY_SINGLE_CONTEXT_INTEL):{
      return("GL_PERFQUERY_SINGLE_CONTEXT_INTEL");
    }
    case(GL_PERFQUERY_GLOBAL_CONTEXT_INTEL):{
      return("GL_PERFQUERY_GLOBAL_CONTEXT_INTEL");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PERFORMANCE_QUERY_CAPS_MASK_INTEL_TO_STRING__ */

#ifdef __GL_STENCIL_FUNCTION_TO_STRING__
char * __attribute__((const)) gl_stencil_function_to_string(GLenum val){
  switch(val){
    case(GL_ALWAYS):{
      return("GL_ALWAYS");
    }
    case(GL_EQUAL):{
      return("GL_EQUAL");
    }
    case(GL_GEQUAL):{
      return("GL_GEQUAL");
    }
    case(GL_GREATER):{
      return("GL_GREATER");
    }
    case(GL_LEQUAL):{
      return("GL_LEQUAL");
    }
    case(GL_LESS):{
      return("GL_LESS");
    }
    case(GL_NEVER):{
      return("GL_NEVER");
    }
    case(GL_NOTEQUAL):{
      return("GL_NOTEQUAL");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_STENCIL_FUNCTION_TO_STRING__ */

#ifdef __GL_VERTEX_POINTER_TYPE_TO_STRING__
char * __attribute__((const)) gl_vertex_pointer_type_to_string(GLenum val){
  switch(val){
    case(GL_DOUBLE):{
      return("GL_DOUBLE");
    }
    case(GL_FLOAT):{
      return("GL_FLOAT");
    }
    case(GL_INT):{
      return("GL_INT");
    }
    case(GL_SHORT):{
      return("GL_SHORT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_VERTEX_POINTER_TYPE_TO_STRING__ */

#ifdef __GL_ALPHA_FUNCTION_TO_STRING__
char * __attribute__((const)) gl_alpha_function_to_string(GLenum val){
  switch(val){
    case(GL_ALWAYS):{
      return("GL_ALWAYS");
    }
    case(GL_EQUAL):{
      return("GL_EQUAL");
    }
    case(GL_GEQUAL):{
      return("GL_GEQUAL");
    }
    case(GL_GREATER):{
      return("GL_GREATER");
    }
    case(GL_LEQUAL):{
      return("GL_LEQUAL");
    }
    case(GL_LESS):{
      return("GL_LESS");
    }
    case(GL_NEVER):{
      return("GL_NEVER");
    }
    case(GL_NOTEQUAL):{
      return("GL_NOTEQUAL");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_ALPHA_FUNCTION_TO_STRING__ */

#ifdef __GL_COLOR_TABLE_PARAMETER_PNAME_SGI_TO_STRING__
char * __attribute__((const)) gl_color_table_parameter_PName_SGI_to_string(GLenum val){
  switch(val){
    case(GL_COLOR_TABLE_BIAS):{
      return("GL_COLOR_TABLE_BIAS");
    }
    case(GL_COLOR_TABLE_BIAS_SGI):{
      return("GL_COLOR_TABLE_BIAS_SGI");
    }
    case(GL_COLOR_TABLE_SCALE):{
      return("GL_COLOR_TABLE_SCALE");
    }
    case(GL_COLOR_TABLE_SCALE_SGI):{
      return("GL_COLOR_TABLE_SCALE_SGI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_COLOR_TABLE_PARAMETER_PNAME_SGI_TO_STRING__ */

#ifdef __GL_GET_COLOR_TABLE_PARAMETER_PNAME_SGI_TO_STRING__
char * __attribute__((const)) gl_get_color_table_parameter_PName_SGI_to_string(GLenum val){
  switch(val){
    case(GL_COLOR_TABLE_ALPHA_SIZE_SGI):{
      return("GL_COLOR_TABLE_ALPHA_SIZE_SGI");
    }
    case(GL_COLOR_TABLE_BIAS_SGI):{
      return("GL_COLOR_TABLE_BIAS_SGI");
    }
    case(GL_COLOR_TABLE_BLUE_SIZE_SGI):{
      return("GL_COLOR_TABLE_BLUE_SIZE_SGI");
    }
    case(GL_COLOR_TABLE_FORMAT_SGI):{
      return("GL_COLOR_TABLE_FORMAT_SGI");
    }
    case(GL_COLOR_TABLE_GREEN_SIZE_SGI):{
      return("GL_COLOR_TABLE_GREEN_SIZE_SGI");
    }
    case(GL_COLOR_TABLE_INTENSITY_SIZE_SGI):{
      return("GL_COLOR_TABLE_INTENSITY_SIZE_SGI");
    }
    case(GL_COLOR_TABLE_LUMINANCE_SIZE_SGI):{
      return("GL_COLOR_TABLE_LUMINANCE_SIZE_SGI");
    }
    case(GL_COLOR_TABLE_RED_SIZE_SGI):{
      return("GL_COLOR_TABLE_RED_SIZE_SGI");
    }
    case(GL_COLOR_TABLE_SCALE_SGI):{
      return("GL_COLOR_TABLE_SCALE_SGI");
    }
    case(GL_COLOR_TABLE_WIDTH_SGI):{
      return("GL_COLOR_TABLE_WIDTH_SGI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_GET_COLOR_TABLE_PARAMETER_PNAME_SGI_TO_STRING__ */

#ifdef __GL_TEXTURE_GEN_MODE_TO_STRING__
char * __attribute__((const)) gl_texture_gen_mode_to_string(GLenum val){
  switch(val){
    case(GL_EYE_DISTANCE_TO_LINE_SGIS):{
      return("GL_EYE_DISTANCE_TO_LINE_SGIS");
    }
    case(GL_EYE_DISTANCE_TO_POINT_SGIS):{
      return("GL_EYE_DISTANCE_TO_POINT_SGIS");
    }
    case(GL_EYE_LINEAR):{
      return("GL_EYE_LINEAR");
    }
    case(GL_OBJECT_DISTANCE_TO_LINE_SGIS):{
      return("GL_OBJECT_DISTANCE_TO_LINE_SGIS");
    }
    case(GL_OBJECT_DISTANCE_TO_POINT_SGIS):{
      return("GL_OBJECT_DISTANCE_TO_POINT_SGIS");
    }
    case(GL_OBJECT_LINEAR):{
      return("GL_OBJECT_LINEAR");
    }
    case(GL_SPHERE_MAP):{
      return("GL_SPHERE_MAP");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_TEXTURE_GEN_MODE_TO_STRING__ */

#ifdef __GL_FRAGMENT_LIGHT_MODEL_PARAMETER_SGIX_TO_STRING__
char * __attribute__((const)) gl_fragment_light_model_parameter_SGIX_to_string(GLenum val){
  switch(val){
    case(GL_FRAGMENT_LIGHT_MODEL_AMBIENT_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_AMBIENT_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_LOCAL_VIEWER_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_LOCAL_VIEWER_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_NORMAL_INTERPOLATION_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_NORMAL_INTERPOLATION_SGIX");
    }
    case(GL_FRAGMENT_LIGHT_MODEL_TWO_SIDE_SGIX):{
      return("GL_FRAGMENT_LIGHT_MODEL_TWO_SIDE_SGIX");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FRAGMENT_LIGHT_MODEL_PARAMETER_SGIX_TO_STRING__ */

#ifdef __GL_PIXEL_TEX_GEN_PARAMETER_NAME_SGIS_TO_STRING__
char * __attribute__((const)) gl_pixel_tex_gen_parameter_name_SGIS_to_string(GLenum val){
  switch(val){
    case(GL_PIXEL_FRAGMENT_ALPHA_SOURCE_SGIS):{
      return("GL_PIXEL_FRAGMENT_ALPHA_SOURCE_SGIS");
    }
    case(GL_PIXEL_FRAGMENT_RGB_SOURCE_SGIS):{
      return("GL_PIXEL_FRAGMENT_RGB_SOURCE_SGIS");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_PIXEL_TEX_GEN_PARAMETER_NAME_SGIS_TO_STRING__ */

#ifdef __GL_FRONT_FACE_DIRECTION_TO_STRING__
char * __attribute__((const)) gl_front_face_direction_to_string(GLenum val){
  switch(val){
    case(GL_CCW):{
      return("GL_CCW");
    }
    case(GL_CW):{
      return("GL_CW");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FRONT_FACE_DIRECTION_TO_STRING__ */

#ifdef __GL_FRAGMENT_SHADER_COLOR_MOD_MASK_ATI_TO_STRING__
char * __attribute__((const)) gl_fragment_shader_color_mod_mask_ATI_to_string(GLenum val){
  switch(val){
    case(GL_COMP_BIT_ATI):{
      return("GL_COMP_BIT_ATI");
    }
    case(GL_NEGATE_BIT_ATI):{
      return("GL_NEGATE_BIT_ATI");
    }
    case(GL_BIAS_BIT_ATI):{
      return("GL_BIAS_BIT_ATI");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FRAGMENT_SHADER_COLOR_MOD_MASK_ATI_TO_STRING__ */

#ifdef __GL_CONTEXT_PROFILE_MASK_TO_STRING__
char * __attribute__((const)) gl_context_profile_mask_to_string(GLenum val){
  switch(val){
    case(GL_CONTEXT_COMPATIBILITY_PROFILE_BIT):{
      return("GL_CONTEXT_COMPATIBILITY_PROFILE_BIT");
    }
    case(GL_CONTEXT_CORE_PROFILE_BIT):{
      return("GL_CONTEXT_CORE_PROFILE_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_CONTEXT_PROFILE_MASK_TO_STRING__ */

#ifdef __GL_MEMORY_BARRIER_MASK_TO_STRING__
char * __attribute__((const)) gl_memory_barrier_mask_to_string(GLenum val){
  switch(val){
    case(GL_ALL_BARRIER_BITS):{
      return("GL_ALL_BARRIER_BITS");
    }
    case(GL_ATOMIC_COUNTER_BARRIER_BIT):{
      return("GL_ATOMIC_COUNTER_BARRIER_BIT");
    }
    case(GL_BUFFER_UPDATE_BARRIER_BIT):{
      return("GL_BUFFER_UPDATE_BARRIER_BIT");
    }
    case(GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT):{
      return("GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT");
    }
    case(GL_COMMAND_BARRIER_BIT):{
      return("GL_COMMAND_BARRIER_BIT");
    }
    case(GL_ELEMENT_ARRAY_BARRIER_BIT):{
      return("GL_ELEMENT_ARRAY_BARRIER_BIT");
    }
    case(GL_FRAMEBUFFER_BARRIER_BIT):{
      return("GL_FRAMEBUFFER_BARRIER_BIT");
    }
    case(GL_PIXEL_BUFFER_BARRIER_BIT):{
      return("GL_PIXEL_BUFFER_BARRIER_BIT");
    }
    case(GL_QUERY_BUFFER_BARRIER_BIT):{
      return("GL_QUERY_BUFFER_BARRIER_BIT");
    }
    case(GL_SHADER_GLOBAL_ACCESS_BARRIER_BIT_NV):{
      return("GL_SHADER_GLOBAL_ACCESS_BARRIER_BIT_NV");
    }
    case(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT):{
      return("GL_SHADER_IMAGE_ACCESS_BARRIER_BIT");
    }
    case(GL_SHADER_STORAGE_BARRIER_BIT):{
      return("GL_SHADER_STORAGE_BARRIER_BIT");
    }
    case(GL_TEXTURE_FETCH_BARRIER_BIT):{
      return("GL_TEXTURE_FETCH_BARRIER_BIT");
    }
    case(GL_TEXTURE_UPDATE_BARRIER_BIT):{
      return("GL_TEXTURE_UPDATE_BARRIER_BIT");
    }
    case(GL_TRANSFORM_FEEDBACK_BARRIER_BIT):{
      return("GL_TRANSFORM_FEEDBACK_BARRIER_BIT");
    }
    case(GL_UNIFORM_BARRIER_BIT):{
      return("GL_UNIFORM_BARRIER_BIT");
    }
    case(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT):{
      return("GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_MEMORY_BARRIER_MASK_TO_STRING__ */

#ifdef __GL_FEEDBACK_TYPE_TO_STRING__
char * __attribute__((const)) gl_feedback_type_to_string(GLenum val){
  switch(val){
    case(GL_2D):{
      return("GL_2D");
    }
    case(GL_3D):{
      return("GL_3D");
    }
    case(GL_3D_COLOR):{
      return("GL_3D_COLOR");
    }
    case(GL_3D_COLOR_TEXTURE):{
      return("GL_3D_COLOR_TEXTURE");
    }
    case(GL_4D_COLOR_TEXTURE):{
      return("GL_4D_COLOR_TEXTURE");
    }
    default:{
      return "";
    }
  }
}
#endif /* defined __GL_FEEDBACK_TYPE_TO_STRING__ */

