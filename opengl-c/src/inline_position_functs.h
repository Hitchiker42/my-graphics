#ifndef __INLINE_POSITION_FUNCTS_H__
#define __INLINE_POSITION_FUNCTS_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
/*
  These are all purely optional, the vec3 functions provide by gl_matrix
  can be used to do the same thing, the only difference is that these
  act explicitly on gl_positions and they get inlined.

  For functions that return a gl_position the w component is always set
  to the w component of the first argument.
*/
#define DECL_SPEC(rettype)\
  static inline rettype __attribute__((optimize(3,"fast-math")))
DECL_SPEC(float) position_distance(gl_position a, gl_position b){
  float x = a.x - b.x;
  float y = a.y - b.y;
  float z = a.z - b.z;
  return sqrt(x*x + y*y + z*z);
}
DECL_SPEC(gl_position) position_add(gl_position a, gl_position b){
  gl_position ret;
  ret.x = a.x + b.x;
  ret.y = a.y + b.y;
  ret.z = a.z + b.z;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(gl_position) position_sub(gl_position a, gl_position b){
  gl_position ret;
  ret.x = a.x - b.x;
  ret.y = a.y - b.y;
  ret.z = a.z - b.z;
  ret.w = a.w;//this seems to make the most sense
  return ret;
}
DECL_SPEC(gl_position) position_negate(gl_position a){
  gl_position ret;
  ret.x = -a.x;
  ret.y = -a.y;
  ret.z = -a.z;
  ret.w = a.w;//this seems to make the most sense
  return ret;
}
//for vector multiplication use the vec3 functions
DECL_SPEC(gl_position) position_mul(gl_position a, gl_position b){
  gl_position ret;
  ret.x = a.x*b.x;
  ret.y = a.y*b.y;
  ret.z = a.z*b.z;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(gl_position) position_scale(gl_position a, float A){
  gl_position ret;
  ret.x = A*a.x;
  ret.y = A*a.y;
  ret.z = A*a.z;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(float) position_length(gl_position a){
  return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}
DECL_SPEC(gl_position) position_normalize(gl_position a){
  gl_position ret;
  float len = sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
  float len_1 = 1/len;
  ret.x = a.x * len_1;
  ret.y = a.y * len_1;
  ret.z = a.z * len_1;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(gl_position) position_cross(gl_position a, gl_position b){
  gl_position ret;
  ret.x = a.y * b.z - a.z * b.y;
  ret.y = a.z * b.x - a.x * b.z;
  ret.z = a.x * b.y - a.y * b.x;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(float) position_dot(gl_position a, gl_position b){
  return a.x*b.x + a.y*b.y + a.z*b.z;
}
DECL_SPEC(gl_position) position_direction(gl_position a, gl_position b){
  gl_position ret;
  float x = a.x - b.x;
  float y = a.y - b.y;
  float z = a.z - b.z;
  float len = sqrt(x*x + y*y + z*z);
  float len_1 = 1/len;
  ret.x = x * len_1;
  ret.y = y * len_1;
  ret.z = z * len_1;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(gl_position) position_reflect(gl_position a, gl_position b){
  gl_position ret;
  float dot = position_dot(a,b);
  ret.x = a.x - 2*dot*b.x;
  ret.y = a.y - 2*dot*b.y;
  ret.z = a.z - 2*dot*b.z;
  ret.w = a.w;
  return ret;
}
DECL_SPEC(float) position_angle(gl_position a, gl_position b){
  /*
    dot(a,b) = (len(a)*len(b))*cos(theta)
    if a and b are normal dot(a,b) = cos(theta),
    thus theta = acos(dot(normalize(a),normalize(b)));
   */
  gl_position na = position_normalize(a);
  gl_position nb = position_normalize(b);
  return acos(na.x*nb.x + na.y*nb.y + na.z*nb.z);
}
#undef DECL_SPEC  
#ifdef __cplusplus
}
#endif
#endif /* __INLINE_POSITION_FUNCTS_H__ */
