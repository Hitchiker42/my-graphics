#ifndef __INTERNAL_H__
#define __INTERNAL_H__
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define zmalloc(sz) calloc(sz,1)
#define I_4(x) {x,0.0,0.0,0.0,                  \
                0.0,x,0.0,0.0,                  \
                0.0,0.0,x,0.0,                  \
                0.0,0.0,0.0,x}
#define I_3(x) {x,0.0,0.0,                      \
                0.0,x,0.0,                      \
                0.0,0.0,x}
//only evaluates mat once
#define set_diagonal_4(mat, x)                          \
  ({__typeof(mat) _mat = mat;                           \
    _mat[0] = _mat[5] = _mat[10] = _mat[15] = x;})
#define set_diagonal_3(mat, x)                          \
  ({__typeof(mat) _mat = mat;                           \
    _mat[0] = _mat[5] = _mat[10] = _mat[15] = x;})

#endif /* __INTERNAL_H__ */
