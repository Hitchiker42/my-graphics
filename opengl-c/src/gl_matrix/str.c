#include <stdlib.h>
#include <stdio.h>

#include "gl-matrix.h"
char* vec3_str(vec3_t vec){
  char *buf;
  asprintf(&buf, "[% f, % f, % f]", vec[0], vec[1], vec[2]);
  return buf;
}

char* mat3_str(mat3_t mat){
  char *buf;
  asprintf(&buf, "[% f, % f, % f\n % f, % f, % f\n % f, % f, % f]",
           mat[0], mat[1], mat[2], mat[3],
           mat[4], mat[5], mat[6], mat[7], mat[8]);
  return buf;
}

char* mat4_str(mat4_t mat){
  char *buf;
  asprintf(&buf,"[% f, % f, % f, % f\n % f, % f, % f, % f\n "
           "% f, % f, % f, % f\n % f, % f, % f, % f]",
           mat[0], mat[1], mat[2], mat[3],
           mat[4], mat[5], mat[6], mat[7],
           mat[8], mat[9], mat[10], mat[11],
           mat[12], mat[13], mat[14], mat[15]);
  return buf;
}

char* quat_str(quat_t quat){
  char *buf;
  asprintf(&buf, "[% f, % f, % f, % f]",
           quat[0], quat[1], quat[2], quat[3]);
  return buf;
}

char* dvec3_str(dvec3_t vec){
  char *buf;
  asprintf(&buf, "[% f, % f, % f]", vec[0], vec[1], vec[2]);
  return buf;
}

char* dmat3_str(dmat3_t mat){
  char *buf;
  asprintf(&buf, "[% f, % f, % f\n % f, % f, % f\n % f, % f, % f]",
           mat[0], mat[1], mat[2], mat[3],
           mat[4], mat[5], mat[6], mat[7], mat[8]);
  return buf;
}

char* dmat4_str(dmat4_t mat){
  char *buf;
  asprintf(&buf, "[% f, % f, % f, % f\n % f, % f, % f, % f\n "
           "% f, % f, % f, % f\n % f, % f, % f, % f]",
           mat[0], mat[1], mat[2], mat[3],
           mat[4], mat[5], mat[6], mat[7],
           mat[8], mat[9], mat[10], mat[11],
           mat[12], mat[13], mat[14], mat[15]);
  return buf;
}

char* dquat_str(dquat_t quat){
  char *buf;
  asprintf(&buf, "[% f, % f, % f, % f]",
           quat[0], quat[1], quat[2], quat[3]);
  return buf;
}
