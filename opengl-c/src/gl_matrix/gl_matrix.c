#include "internal.h"

#include "gl_matrix.h"
/*
  This is a modification to a C translation of a javascript library.
  Lots of the optimizations the code used are necessary to make
  fast javascript, since it's interpreded, but since C is compiled
  a lot of the code can be simplified to make it more readable
  without losing performance.
  
  After trying lots of different ways to write things I've found
  that using loops unrolled 4x seems to generate the best
  assembly code.
  
*/
/*
  Functions to print matrices
*/
#include "str.c"
/*
  Each of the files is a template allowing the same code to
  be used for both float and double versions
*/

#define TYPE float
#define mat4_type fmat4_t
#define mat3_type fmat3_t
#define vec4_type fvec4_t
#define vec3_type fvec3_t
#define quat_type fquat_t
#define sincos sincosf
#include "mat3_templ.c"
#include "mat4_templ.c"
#include "vec3_templ.c"
#include "quat_templ.c"
#undef TYPE
#undef sincos
#undef mat4_type
#undef mat3_type
#undef vec4_type
#undef vec3_type
#undef quat_type

#define TYPE double
#define mat4_type dmat4_t
#define mat3_type dmat3_t
#define vec4_type dvec4_t
#define vec3_type dvec3_t
#define quat_type dquat_t

#define vec3_create dvec3_create
#define vec3_set dvec3_set
#define vec3_add dvec3_add
#define vec3_subtract dvec3_subtract
#define vec3_multiply dvec3_multiply
#define vec3_negate dvec3_negate
#define vec3_scale dvec3_scale
#define vec3_normalize dvec3_normalize
#define vec3_cross  dvec3_cross 
#define vec3_length dvec3_length
#define vec3_dot dvec3_dot
#define vec3_direction  dvec3_direction 
#define vec3_reflect  dvec3_reflect 
#define vec3_lerp dvec3_lerp
#define vec3_dist dvec3_dist
#define vec3_unproject dvec3_unproject
#define vec3_str dvec3_str
#define mat3_create dmat3_create
#define mat3_set dmat3_set
#define mat3_identity dmat3_identity
#define mat3_transpose dmat3_transpose
#define mat3_to_mat4 dmat3_to_dmat4
#define mat3_str dmat3_str
#define mat4_create dmat4_create
#define mat4_set dmat4_set
#define mat4_identity dmat4_identity
#define mat4_transpose dmat4_transpose
#define mat4_determinant dmat4_determinant
#define mat4_inverse dmat4_inverse
#define mat4_create_rotation_mat dmat4_create_rotation_mat
#define mat4_to_rotation_mat dmat4_to_rotation_mat
#define mat4_to_mat3 dmat4_to_mat3
#define mat4_to_inverse_mat3 dmat4_to_inverse_mat3
#define mat4_multiply dmat4_multiply
#define mat4_multiply_vec3 dmat4_multiply_dvec3
#define mat4_multiply_vec4 dmat4_multiply_dvec4
#define mat4_translate dmat4_translate
#define mat4_scale dmat4_scale
#define mat4_rotate dmat4_rotate
#define mat4_rotateX dmat4_rotateX
#define mat4_rotateY dmat4_rotateY
#define mat4_rotateZ dmat4_rotateZ
#define mat4_shearX dmat4_shearX
#define mat4_shearY dmat4_shearY
#define mat4_shearZ dmat4_shearZ
#define mat4_frustum dmat4_frustum
#define mat4_perspective dmat4_perspective
#define mat4_ortho dmat4_ortho
#define mat4_look_at dmat4_lookAt
#define mat4_from_rotation_translation dmat4_from_rotation_translation
#define mat4_str dmat4_str
#define quat_create dquat_create
#define quat_set dquat_set
#define quat_calculateW dquat_calculateW
#define quat_dot dquat_dot
#define quat_inverse dquat_inverse
#define quat_conjugate dquat_conjugate
#define quat_length dquat_length
#define quat_normalize dquat_normalize
#define quat_multiply dquat_multiply
#define quat_multiply_vec3 dquat_multiply_dvec3
#define quat_to_mat3 dquat_to_dmat3
#define quat_to_mat4 dquat_to_dmat4
#define quat_slerp dquat_slerp
#define quat_str dquat_str
#include "mat3_templ.c"
#include "mat4_templ.c"
#include "vec3_templ.c"
#include "quat_templ.c"
#undef TYPE
#undef mat4_type
#undef mat3_type
#undef vec4_type
#undef vec3_type
#undef quat_type
