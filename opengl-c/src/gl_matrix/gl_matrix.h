#ifndef GL_MATRIX_H
#define GL_MATRIX_H

#ifdef __cplusplus
extern "C" {
#endif

#define GL_MATRIX_MAJOR_VERSION 1
#define GL_MATRIX_MINOR_VERSION 2
#define GL_MATRIX_MICRO_VERSION 3

#define GL_MATRIX_VERSION  "1.2.3"

#define GL_MATRIX_VERSION_HEX  ((GL_MATRIX_MAJOR_VERSION << 16) | \
                              (GL_MATRIX_MINOR_VERSION << 8) | \
                              (GL_MATRIX_MICRO_VERSION))

typedef float* vec3_t;
typedef float* __attribute__((aligned(16))) vec4_t;
typedef float* mat3_t;
typedef float* __attribute__((aligned(16))) mat4_t;
typedef float* quat_t;

typedef vec3_t fvec3_t;
typedef vec4_t fvec4_t;
typedef mat3_t fmat3_t;
typedef mat4_t fmat4_t;
typedef quat_t fquat_t;

typedef double* dvec3_t;
typedef double* __attribute__((aligned(16))) dvec4_t;
typedef double* dmat3_t;
typedef double* __attribute__((aligned(16))) dmat4_t;
typedef double* dquat_t;
/*
  Documentation taken (mostly) verbatium from the existing library.

  Some macros have been added to make writing some common compound
  operations eaiser.
*/
/*
 * vec3_set
 * Copies the values of one vec3_t to another
 *
 * Params:
 * vec - vec3_t containing values to copy
 * dest - vec3_t receiving copied values
 *
 * Returns:
 * dest
 */
vec3_t vec3_set(vec3_t vec, vec3_t dest);

/*
 * vec3_add
 * Performs a vector addition
 *
 * Params:
 * vec - vec3, first operand
 * vec2 - vec3, second operand
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_add(vec3_t vec, vec3_t vec2, vec3_t dest);
#define vec3_add3(v,v2,v3,dest)                 \
  vec3_add(vec3_add(v,v2,dest),v3,dest)
//vec3_axpy(float a, vec3_t x, vec3_t y) == a*x + y
#define vec3_axpy(a,x,y,dest)                   \
  vec3_scale(vec3_add(x,y,dest), a, dest);
/*
 * vec3_subtract
 * Performs a vector subtraction
 *
 * Params:
 * vec - vec3, first operand
 * vec2 - vec3, second operand
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_subtract(vec3_t vec, vec3_t vec2, vec3_t dest);

/*
 * vec3_multiply
 * Performs a vector multiplication
 *
 * Params:
 * vec - vec3, first operand
 * vec2 - vec3, second operand
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_multiply(vec3_t vec, vec3_t vec2, vec3_t dest);
#define vec3_multiply_scale(x,y,a,dest)         \
  vec3_scale(vec3_multiply(x,y,dest), a, dest);
/*
 * vec3_negate
 * Negates the components of a vec3
 *
 * Params:
 * vec - vec3_t to negate
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_negate(vec3_t vec, vec3_t dest);

/*
 * vec3_scale
 * Multiplies the components of a vec3_t by a scalar value
 *
 * Params:
 * vec - vec3_t to scale
 * val - Numeric value to scale by
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_scale(vec3_t vec, float val, vec3_t dest);

/*
 * vec3_normalize
 * Generates a unit vector of the same direction as the provided vec3
 * If vector length is 0, returns [0, 0, 0]
 *
 * Params:
 * vec - vec3_t to normalize
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_normalize(vec3_t vec, vec3_t dest);

/*
 * vec3_cross
 * Generates the cross product of two vec3s
 *
 * Params:
 * vec - vec3, first operand
 * vec2 - vec3, second operand
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_cross (vec3_t vec, vec3_t vec2, vec3_t dest);

/*
 * vec3_length
 * Caclulates the length of a vec3
 *
 * Params:
 * vec - vec3_t to calculate length of
 *
 * Returns:
 * Length of vec
 */
float vec3_length(vec3_t vec);

/*
 * vec3_dot
 * Caclulates the dot product of two vec3s
 *
 * Params:
 * vec - vec3, first operand
 * vec2 - vec3, second operand
 *
 * Returns:
 * Dot product of vec and vec2
 */
float vec3_dot(vec3_t vec, vec3_t vec2);

/*
 * vec3_direction
 * Generates a unit vector pointing from one vector to another
 *
 * Params:
 * vec - origin vec3
 * vec2 - vec3_t to point to
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_direction (vec3_t vec, vec3_t vec2, vec3_t dest);

/*
 * vec3_lerp
 * Performs a linear interpolation between two vec3
 *
 * Params:
 * vec - vec3, first vector
 * vec2 - vec3, second vector
 * lerp - interpolation amount between the two inputs
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */

vec3_t vec3_lerp(vec3_t vec, vec3_t vec2, float lerp, vec3_t dest);

/*
 * vec3_dist
 * Calculates the euclidian distance between two vec3
 *
 * Params:
 * vec - vec3, first vector
 * vec2 - vec3, second vector
 *
 * Returns:
 * distance between vec and vec2
 */
float vec3_dist(vec3_t vec, vec3_t vec2);
/*
 * vec3_reflect
 * Generate a unit vector pointing in the direction that vec reflects
 * off of the surface normal to norm (aka vec - 2*(dot(vec,norm))*norm
 *
 * Params:
 * vec - vector to reflect
 * norm - vector normal to the surface to reflect off of
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_reflect (vec3_t vec, vec3_t norm, vec3_t dest);

/*
 * vec3_unproject
 * Projects the specified vec3_t from screen space into object space
 * Based on Mesa gluUnProject implementation at: 
 * http://webcvs.freedesktop.org/mesa/Mesa/src/glu/mesa/project.c?revision=1.4&view=markup
 *
 * Params:
 * vec - vec3, screen-space vector to project
 * view - mat4, View matrix
 * proj - mat4, Projection matrix
 * viewport - vec4, Viewport as given to gl.viewport [x, y, width, height]
 * dest - Optional, vec3_t receiving unprojected result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
vec3_t vec3_unproject(vec3_t vec, mat4_t view, 
                      mat4_t proj, vec4_t viewport, vec3_t dest);

/*
 * vec3_str
 * Writes a string representation of a vector
 *
 * Params:
 * vec - vec3_t to represent as a string
 */
char* vec3_str(vec3_t vec);

/*
 * mat3_t - 3x3 Matrix
 */

/*
 * mat3_create
 * Creates a new instance of a mat3_t
 *
 * Params:
 * mat - Optional, mat3_t containing values to initialize with. If NULL the result
 * will be initialized with zeroes.
 *
 * Returns:
 * New mat3
 */
mat3_t mat3_create(mat3_t mat);

/*
 * mat3_set
 * Copies the values of one mat3_t to another
 *
 * Params:
 * mat - mat3_t containing values to copy
 * dest - mat3_t receiving copied values
 *
 * Returns:
 * dest
 */
mat3_t mat3_set(mat3_t mat, mat3_t dest);

/*
 * mat3_identity
 * Sets a mat3_t to an identity matrix
 *
 * Params:
 * dest - mat3_t to set
 *
 * Returns:
 * dest
 */
 mat3_t mat3_identity(mat3_t dest);

/*
 * mat4.transpose
 * Transposes a mat3_t (flips the values over the diagonal)
 *
 * Params:
 * mat - mat3_t to transpose
 * dest - Optional, mat3_t receiving transposed values. If NULL, result is written to mat
 *
 * Returns:
 * dest is specified, mat otherwise
 */
mat3_t mat3_transpose(mat3_t mat, mat3_t dest);

/*
 * mat3_to_mat4
 * Copies the elements of a mat3_t into the upper 3x3 elements of a mat4
 *
 * Params:
 * mat - mat3_t containing values to copy
 * dest - Optional, mat4_t receiving copied values
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
mat4_t mat3_to_mat4(mat3_t mat, mat4_t dest);

/*
 * mat3_str
 * Writes a string representation of a mat3
 *
 * Params:
 * mat - mat3_t to represent as a string
 */
char* mat3_str(mat3_t mat);

/*
 * mat4_t - 4x4 Matrix
 */

/*
 * mat4_create
 * Creates a new instance of a mat4_t
 *
 * Params:
 * mat - Optional, mat4_t containing values to initialize with
 *
 * Returns:
 * New mat4
 */
mat4_t mat4_create(mat4_t mat);

/*
 * mat4_set
 * Copies the values of one mat4_t to another
 *
 * Params:
 * mat - mat4_t containing values to copy
 * dest - mat4_t receiving copied values
 *
 * Returns:
 * dest
 */
mat4_t mat4_set(mat4_t mat, mat4_t dest);

/*
 * mat4_identity
 * Sets a mat4_t to an identity matrix
 *
 * Params:
 * dest - mat4_t to set
 *
 * Returns:
 * dest
 */
mat4_t mat4_identity(mat4_t dest);
     
/*
 * mat4_transpose
 * Transposes a mat4_t (flips the values over the diagonal)
 *
 * Params:
 * mat - mat4_t to transpose
 * dest - Optional, mat4_t receiving transposed values. If NULL, result is written to mat
 *
 * Returns:
 * dest is specified, mat otherwise
 */
mat4_t mat4_transpose(mat4_t mat, mat4_t dest);

/*
 * mat4_determinant
 * Calculates the determinant of a mat4
 *
 * Params:
 * mat - mat4_t to calculate determinant of
 *
 * Returns:
 * determinant of mat
 */
float mat4_determinant(mat4_t mat);

/*
 * mat4_inverse
 * Calculates the inverse matrix of a mat4
 *
 * Params:
 * mat - mat4_t to calculate inverse of
 * dest - Optional, mat4_t receiving inverse matrix. If NULL, result is written to mat
 *
 * Returns:
 * dest is specified, mat otherwise, NULL if matrix cannot be inverted
 */
mat4_t mat4_inverse(mat4_t mat, mat4_t dest);

/*
 * mat4_toRotationMat
 * Copies the upper 3x3 elements of a mat4_t into another mat4
 *
 * Params:
 * mat - mat4_t containing values to copy
 * dest - Optional, mat4_t receiving copied values
 *
 * Returns:
 * dest is specified, a new mat4_t otherwise
 */
mat4_t mat4_to_rotation_mat(mat4_t mat, mat4_t dest);

/*
 * mat4_toMat3
 * Copies the upper 3x3 elements of a mat4_t into a mat3
 *
 * Params:
 * mat - mat4_t containing values to copy
 * dest - Optional, mat3_t receiving copied values
 *
 * Returns:
 * dest is specified, a new mat3_t otherwise
 */
mat3_t mat4_to_mat3(mat4_t mat, mat3_t dest);

/*
 * mat4_toInverseMat3
 * Calculates the inverse of the upper 3x3 elements of a mat4_t and copies the result into a mat3
 * The resulting matrix is useful for calculating transformed normals
 *
 * Params:
 * mat - mat4_t containing values to invert and copy
 * dest - Optional, mat3_t receiving values
 *
 * Returns:
 * dest is specified, a new mat3_t otherwise, NULL if the matrix cannot be inverted
 */
mat3_t mat4_to_inverse_mat3(mat4_t mat, mat3_t dest);

/*
 * mat4_multiply
 * Performs a matrix multiplication
 *
 * Params:
 * mat - mat4, first operand
 * mat2 - mat4, second operand
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_multiply(mat4_t mat, mat4_t mat2, mat4_t dest);

/*
 * mat4_multiplyVec3
 * Transforms a vec3_t with the given matrix
 * 4th vector component is implicitly '1'
 *
 * Params:
 * mat - mat4_t to transform the vector with
 * vec - vec3_t to transform
 * dest - Optional, vec3_t receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
mat4_t mat4_multiply_vec3(mat4_t mat, vec3_t vec, mat4_t dest);

/*
 * mat4_multiplyVec4
 * Transforms a vec4 with the given matrix
 *
 * Params:
 * mat - mat4_t to transform the vector with
 * vec - vec4 to transform
 * dest - Optional, vec4 receiving operation result. If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
mat4_t mat4_multiply_vec4(mat4_t mat, vec4_t vec, mat4_t dest);

/*
 * mat4_translate
 * Translates a matrix by the given vector
 *
 * Params:
 * mat - mat4_t to translate
 * vec - vec3_t specifying the translation
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_translate(mat4_t mat, vec3_t vec, mat4_t dest);

/*
 * mat4_scale
 * Scales a matrix by the given vector
 *
 * Params:
 * mat - mat4_t to scale
 * vec - vec3_t specifying the scale for each axis
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_scale(mat4_t mat, vec3_t vec, mat4_t dest);

/*
 * mat4_rotate
 * Rotates a matrix by the given angle around the specified axis
 * If rotating around a primary axis (X,Y,Z) one of the specialized rotation functions should be used instead for performance
 *
 * Params:
 * mat - mat4_t to rotate
 * angle - angle (in radians) to rotate
 * axis - vec3_t representing the axis to rotate around 
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_rotate(mat4_t mat, float angle, vec3_t axis, mat4_t dest);

/*
 * mat4_rotateX
 * Rotates a matrix by the given angle around the X axis
 *
 * Params:
 * mat - mat4_t to rotate
 * angle - angle (in radians) to rotate
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_rotateX(mat4_t mat, float angle, mat4_t dest);

/*
 * mat4_rotateY
 * Rotates a matrix by the given angle around the Y axis
 *
 * Params:
 * mat - mat4_t to rotate
 * angle - angle (in radians) to rotate
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_rotateY(mat4_t mat, float angle, mat4_t dest);

/*
 * mat4_rotateZ
 * Rotates a matrix by the given angle around the Z axis
 *
 * Params:
 * mat - mat4_t to rotate
 * angle - angle (in radians) to rotate
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_rotateZ(mat4_t mat, float angle, mat4_t dest);

/*
 * mat4_shearX
 * Shear a matrix on the yz plane using the given shear factors
 *
 * Params:
 * mat - mat4_t to shear
 * shear_y - Shear factor in the Y direction
 * shear_z - Shear factor in the Z direction
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_shearX(mat4_t mat, float shear_y, float shear_z, mat4_t dest);

/*
 * mat4_shearY
 * Shear a matrix on the xz plane using the given shear factors
 *
 * Params:
 * mat - mat4_t to shear
 * shear_y - Shear factor in the X direction
 * shear_z - Shear factor in the Z direction
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_shearY(mat4_t mat, float shear_x, float shear_z, mat4_t dest);

/*
 * mat4_shearZ
 * Shear a matrix on the xy plane using the given shear factors
 *
 * Params:
 * mat - mat4_t to shear
 * shear_x - Shear factor in the X direction
 * shear_y - Shear factor in the Y direction
 * dest - Optional, mat4_t receiving operation result. If NULL, result is written to mat
 *
 * Returns:
 * dest if not NULL, mat otherwise
 */
mat4_t mat4_shearZ(mat4_t mat, float shear_x, float shear_y, mat4_t dest); 
/*
 * mat4_frustum
 * Generates a frustum matrix with the given bounds
 *
 * Params:
 * left, right - scalar, left and right bounds of the frustum
 * bottom, top - scalar, bottom and top bounds of the frustum
 * near, far - scalar, near and far bounds of the frustum
 * dest - Optional, mat4_t frustum matrix will be written into
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
mat4_t mat4_frustum(float left, float right, float bottom, 
                    float top, float near, float far, mat4_t dest);

/*
 * mat4_perspective
 * Generates a perspective projection matrix with the given bounds
 *
 * Params:
 * fovy - scalar, vertical field of view, in degrees
 * aspect - scalar, aspect ratio. typically viewport width/height
 * near, far - scalar, near and far bounds of the frustum
 * dest - Optional, mat4_t frustum matrix will be written into
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
mat4_t mat4_perspective(float fovy, float aspect,
                        float near, float far, mat4_t dest);

/*
 * mat4_ortho
 * Generates a orthogonal projection matrix with the given bounds
 *
 * Params:
 * left, right - scalar, left and right bounds of the frustum
 * bottom, top - scalar, bottom and top bounds of the frustum
 * near, far - scalar, near and far bounds of the frustum
 * dest - Optional, mat4_t frustum matrix will be written into
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
mat4_t mat4_ortho(float left, float right, float bottom, 
                  float top, float near, float far, mat4_t dest);

/*
 * mat4_lookAt
 * Generates a look-at matrix with the given eye position, focal point, and up axis
 *
 * Params:
 * eye - vec3, position of the viewer
 * center - vec3, point the viewer is looking at
 * up - vec3_t pointing "up"
 * dest - Optional, mat4_t frustum matrix will be written into
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
mat4_t mat4_look_at(vec3_t eye, vec3_t center, vec3_t up, mat4_t dest);

/*
 * mat4_fromRotationTranslation
 * Creates a matrix from a quaternion rotation and vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4_identity(dest);
 *     mat4_translate(dest, vec);
 *     mat4_t quatMat = mat4_create();
 *     quat_toMat4(quat, quatMat);
 *     mat4_multiply(dest, quatMat);
 *
 * Params:
 * quat - quat specifying the rotation by
 * vec - vec3_t specifying the translation
 * dest - Optional, mat4_t receiving operation result. 
 *        If NULL, result is written to a new mat4
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
mat4_t mat4_from_rotation_translation(quat_t quat, vec3_t vec, mat4_t dest);

/*
 * mat4_str
 * Writes a string representation of a mat4
 *
 * Params:
 * mat - mat4_t to represent as a string
 */
char* mat4_str(mat4_t mat);

/*
 * quat - Quaternions 
 */

/*
 * quat_create
 * Creates a new instance of a quat_t
 *
 * Params:
 * quat - Optional, quat_t containing values to initialize with
 *
 * Returns:
 * New quat_t
 */
quat_t quat_create(quat_t quat);

/*
 * quat_set
 * Copies the values of one quat_t to another
 *
 * Params:
 * quat - quat_t containing values to copy
 * dest - quat_t receiving copied values
 *
 * Returns:
 * dest
 */
quat_t quat_set(quat_t quat, quat_t dest);

/*
 * quat_calculateW
 * Calculates the W component of a quat_t from the X, Y, and Z components.
 * Assumes that quaternion is 1 unit in length. 
 * Any existing W component will be ignored. 
 *
 * Params:
 * quat - quat_t to calculate W component of
 * dest - Optional, quat_t receiving calculated values. If NULL, result is written to quat
 *
 * Returns:
 * dest if not NULL, quat otherwise
 */
quat_t quat_calculateW(quat_t quat, quat_t dest);

/*
 * quat_dot
 * Calculates the dot product of two quaternions
 *
 * Params:
 * quat - First operand
 * quat2 - Second operand
 *
 * Returns:
 * Dot product of quat and quat2
 */
float quat_dot(quat_t quat, quat_t quat2);

/*
 * quat_inverse
 * Calculates the inverse of a quat_t
 *
 * Params:
 * quat - quat_t to calculate inverse of
 * dest - Optional, quat_t receiving inverse values. If NULL, result is written to quat
 *
 * Returns:
 * dest if not NULL, quat otherwise
 */
quat_t quat_inverse(quat_t quat, quat_t dest);

/*
 * quat_conjugate
 * Calculates the conjugate of a quat_t
 *
 * Params:
 * quat - quat_t to calculate conjugate of
 * dest - Optional, quat_t receiving conjugate values. If NULL, result is written to quat
 *
 * Returns:
 * dest if not NULL, quat otherwise
 */
quat_t quat_conjugate(quat_t quat, quat_t dest);

/*
 * quat_length
 * Calculates the length of a quat_t
 *
 * Params:
 * quat - quat_t to calculate length of
 *
 * Returns:
 * Length of quat
 */
float quat_length(quat_t quat);

/*
 * quat_normalize
 * Generates a unit quaternion of the same direction as the provided quat_t
 * If quaternion length is 0, returns [0, 0, 0, 0]
 *
 * Params:
 * quat - quat_t to normalize
 * dest - Optional, quat_ receiving operation result. If NULL, result is written to quat
 *
 * Returns:
 * dest if not NULL, quat otherwise
 */
quat_t quat_normalize(quat_t quat, quat_t dest);

/*
 * quat_multiply
 * Performs a quaternion multiplication
 *
 * Params:
 * quat - quat_t, first operand
 * quat2 - quat_t, second operand
 * dest - Optional, quat_t receiving operation result. 
 *                  If NULL, result is written to quat
 *
 * Returns:
 * dest if not NULL, quat otherwise
 */
quat_t quat_multiply(quat_t quat, quat_t quat2, quat_t dest);

/*
 * quat_multiplyVec3
 * Transforms a vec3_t with the given quaternion
 *
 * Params:
 * quat - quat_t to transform the vector with
 * vec - vec3_t to transform
 * dest - Optional, vec3_t receiving operation result.
 *                  If NULL, result is written to vec
 *
 * Returns:
 * dest if not NULL, vec otherwise
 */
quat_t quat_multiply_vec3(quat_t quat, vec3_t vec, vec3_t dest);

/*
 * quat_toMat3
 * Calculates a 3x3 matrix from the given quat_t
 *
 * Params:
 * quat - quat_t to create matrix from
 * dest - Optional, mat3_t receiving operation result
 *
 * Returns:
 * dest if not NULL, a new mat3_t otherwise
 */
mat3_t quat_to_mat3(quat_t quat, mat3_t dest);

/*
 * quat_toMat4
 * Calculates a 4x4 matrix from the given quat_t
 *
 * Params:
 * quat - quat_t to create matrix from
 * dest - Optional, mat4_t receiving operation result
 *
 * Returns:
 * dest if not NULL, a new mat4_t otherwise
 */
quat_t quat_to_mat4(quat_t quat, mat4_t dest);

/*
 * quat_slerp
 * Performs a spherical linear interpolation between two quat_t
 *
 * Params:
 * quat - quat_t, first quaternion
 * quat2 - quat_t, second quaternion
 * slerp - interpolation amount between the two inputs
 * dest - Optional, quat_t receiving operation result. If NULL, result is written to quat
 *
 * Returns:
 * dest if not NULL, quat otherwise
 */
quat_t quat_slerp(quat_t quat, quat_t quat2, float slerp, quat_t dest);

/*
 * quat_str
 * Writes a string representation of a quaternion
 *
 * Params:
 * quat - quat_t to represent as a string
 */
char* quat_str(quat_t quat);

vec3_t vec3_create(vec3_t vec);

vec3_t vec3_set(vec3_t vec, vec3_t dest);

vec3_t vec3_add(vec3_t vec, vec3_t vec2, vec3_t dest);

vec3_t vec3_subtract(vec3_t vec, vec3_t vec2, vec3_t dest);

vec3_t vec3_multiply(vec3_t vec, vec3_t vec2, vec3_t dest);

vec3_t vec3_negate(vec3_t vec, vec3_t dest);

vec3_t vec3_scale(vec3_t vec, float val, vec3_t dest);

vec3_t vec3_normalize(vec3_t vec, vec3_t dest);

vec3_t vec3_cross (vec3_t vec, vec3_t vec2, vec3_t dest);

float vec3_length(vec3_t vec);

float vec3_dot(vec3_t vec, vec3_t vec2);

vec3_t vec3_direction (vec3_t vec, vec3_t vec2, vec3_t dest);

vec3_t vec3_lerp(vec3_t vec, vec3_t vec2, float lerp, vec3_t dest);

float vec3_dist(vec3_t vec, vec3_t vec2);

vec3_t vec3_unproject(vec3_t vec, mat4_t view, 
                      mat4_t proj, vec4_t viewport, vec3_t dest);

char*  vec3_str(vec3_t vec);

mat3_t mat3_create(mat3_t mat);

mat3_t mat3_set(mat3_t mat, mat3_t dest);

mat3_t mat3_identity(mat3_t dest);

mat3_t mat3_transpose(mat3_t mat, mat3_t dest);

mat4_t mat3_to_mat_4(mat3_t mat, mat4_t dest);

char*  mat3_str(mat3_t mat);

mat4_t mat4_create(mat4_t mat);

mat4_t mat4_set(mat4_t mat, mat4_t dest);

mat4_t mat4_identity(mat4_t dest);

mat4_t mat4_transpose(mat4_t mat, mat4_t dest);

float mat4_determinant(mat4_t mat);

mat4_t mat4_inverse(mat4_t mat, mat4_t dest);

mat4_t mat4_create_rotation_mat(vec3_t axis, float angle);

mat4_t mat4_to_rotation_mat(mat4_t mat, mat4_t dest);

mat3_t mat4_to_mat3(mat4_t mat, mat3_t dest);

mat3_t mat4_to_inverse_mat3(mat4_t mat, mat3_t dest);

mat4_t mat4_multiply(mat4_t mat, mat4_t mat2, mat4_t dest);

mat4_t mat4_multiply_vec3(mat4_t mat, vec3_t vec, mat4_t dest);

mat4_t mat4_multiply_vec4(mat4_t mat, vec4_t vec, mat4_t dest);

mat4_t mat4_translate(mat4_t mat, vec3_t vec, mat4_t dest);

mat4_t mat4_scale(mat4_t mat, vec3_t vec, mat4_t dest);

mat4_t mat4_rotate(mat4_t mat, float angle, vec3_t axis, mat4_t dest);

mat4_t mat4_rotateX(mat4_t mat, float angle, mat4_t dest);

mat4_t mat4_rotateY(mat4_t mat, float angle, mat4_t dest);

mat4_t mat4_rotateZ(mat4_t mat, float angle, mat4_t dest);

mat4_t mat4_frustum(float left, float right, float bottom, 
                    float top, float near, float far, mat4_t dest);

mat4_t mat4_perspective(float fovy, float aspect, 
                        float near, float far, mat4_t dest);

mat4_t mat4_ortho(float left, float right, float bottom, 
                  float top, float near, float far, mat4_t dest);

mat4_t mat4_look_at(vec3_t eye, vec3_t center, vec3_t up, mat4_t dest);

mat4_t mat4_from_rotation_translation(quat_t quat, vec3_t vec, mat4_t dest);

char*  mat4_str(mat4_t mat);

quat_t quat_create(quat_t quat);

quat_t quat_set(quat_t quat, quat_t dest);

quat_t quat_calculateW(quat_t quat, quat_t dest);

float quat_dot(quat_t quat, quat_t quat2);

quat_t quat_inverse(quat_t quat, quat_t dest);

quat_t quat_conjugate(quat_t quat, quat_t dest);

float quat_length(quat_t quat);

quat_t quat_normalize(quat_t quat, quat_t dest);

quat_t quat_multiply(quat_t quat, quat_t quat2, quat_t dest);

quat_t quat_multiply_vec3(quat_t quat, vec3_t vec, vec3_t dest);

mat3_t quat_toMat3(quat_t quat, mat3_t dest);

quat_t quat_toMat4(quat_t quat, mat4_t dest);

quat_t quat_slerp(quat_t quat, quat_t quat2, float slerp, quat_t dest);

char*  quat_str(quat_t quat);

/*
  double precision versions
 */

dvec3_t dvec3_create(dvec3_t vec);

dvec3_t dvec3_set(dvec3_t vec, dvec3_t dest);

dvec3_t dvec3_add(dvec3_t vec, dvec3_t vec2, dvec3_t dest);

dvec3_t dvec3_subtract(dvec3_t vec, dvec3_t vec2, dvec3_t dest);

dvec3_t dvec3_multiply(dvec3_t vec, dvec3_t vec2, dvec3_t dest);

dvec3_t dvec3_negate(dvec3_t vec, dvec3_t dest);

dvec3_t dvec3_scale(dvec3_t vec, double val, dvec3_t dest);

dvec3_t dvec3_normalize(dvec3_t vec, dvec3_t dest);

dvec3_t dvec3_cross (dvec3_t vec, dvec3_t vec2, dvec3_t dest);

double  dvec3_length(dvec3_t vec);

double  dvec3_dot(dvec3_t vec, dvec3_t vec2);

dvec3_t dvec3_direction (dvec3_t vec, dvec3_t vec2, dvec3_t dest);

dvec3_t dvec3_lerp(dvec3_t vec, dvec3_t vec2, double lerp, dvec3_t dest);

double  dvec3_dist(dvec3_t vec, dvec3_t vec2);

dvec3_t dvec3_unproject(dvec3_t vec, dmat4_t view, 
                      dmat4_t proj, dvec4_t viewport, dvec3_t dest);

char*   dvec3_str(dvec3_t vec);

dmat3_t dmat3_create(dmat3_t mat);

dmat3_t dmat3_set(dmat3_t mat, dmat3_t dest);

dmat3_t dmat3_identity(dmat3_t dest);

dmat3_t dmat3_transpose(dmat3_t mat, dmat3_t dest);

dmat4_t dmat3_to_mat_4(dmat3_t mat, dmat4_t dest);

char*   dmat3_str(dmat3_t mat);

dmat4_t dmat4_create(dmat4_t mat);

dmat4_t dmat4_set(dmat4_t mat, dmat4_t dest);

dmat4_t dmat4_identity(dmat4_t dest);

dmat4_t dmat4_transpose(dmat4_t mat, dmat4_t dest);

double  dmat4_determinant(dmat4_t mat);

dmat4_t dmat4_inverse(dmat4_t mat, dmat4_t dest);

dmat4_t dmat4_create_rotation_mat(dvec3_t axis, double angle);

dmat4_t dmat4_to_rotation_mat(dmat4_t mat, dmat4_t dest);

dmat3_t dmat4_to_dmat3(dmat4_t mat, dmat3_t dest);

dmat3_t dmat4_to_inverse_dmat3(dmat4_t mat, dmat3_t dest);

dmat4_t dmat4_multiply(dmat4_t mat, dmat4_t mat2, dmat4_t dest);

dmat4_t dmat4_multiply_dvec3(dmat4_t mat, dvec3_t vec, dmat4_t dest);

dmat4_t dmat4_multiply_dvec4(dmat4_t mat, dvec4_t vec, dmat4_t dest);

dmat4_t dmat4_translate(dmat4_t mat, dvec3_t vec, dmat4_t dest);

dmat4_t dmat4_scale(dmat4_t mat, dvec3_t vec, dmat4_t dest);

dmat4_t dmat4_rotate(dmat4_t mat, double angle, dvec3_t axis, dmat4_t dest);

dmat4_t dmat4_rotateX(dmat4_t mat, double angle, dmat4_t dest);

dmat4_t dmat4_rotateY(dmat4_t mat, double angle, dmat4_t dest);

dmat4_t dmat4_rotateZ(dmat4_t mat, double angle, dmat4_t dest);
dmat4_t dmat4_shearX(dmat4_t mat, double shear_y,
                    double shear_z, dmat4_t dest);
dmat4_t dmat4_shearY(dmat4_t mat, double shear_x,
                    double shear_z, dmat4_t dest);
dmat4_t dmat4_shearZ(dmat4_t mat, double shear_x,
                    double shear_y, dmat4_t dest);

dmat4_t dmat4_frustum(double left, double right, double bottom, 
                    double top, double near, double far, dmat4_t dest);

dmat4_t dmat4_perspective(double fovy, double aspect, 
                        double near, double far, dmat4_t dest);

dmat4_t dmat4_ortho(double left, double right, double bottom, 
                  double top, double near, double far, dmat4_t dest);

dmat4_t dmat4_look_at(dvec3_t eye, dvec3_t center, dvec3_t up, dmat4_t dest);

dmat4_t dmat4_from_rotation_translation(dquat_t dquat, dvec3_t vec, dmat4_t dest);

char*   dmat4_str(dmat4_t mat);

dquat_t dquat_create(dquat_t dquat);

dquat_t dquat_set(dquat_t dquat, dquat_t dest);

dquat_t dquat_calculateW(dquat_t dquat, dquat_t dest);

double  dquat_dot(dquat_t dquat, dquat_t dquat2);

dquat_t dquat_inverse(dquat_t dquat, dquat_t dest);

dquat_t dquat_conjugate(dquat_t dquat, dquat_t dest);

double  dquat_length(dquat_t dquat);

dquat_t dquat_normalize(dquat_t dquat, dquat_t dest);

dquat_t dquat_multiply(dquat_t dquat, dquat_t dquat2, dquat_t dest);

dquat_t dquat_multiply_dvec3(dquat_t dquat, dvec3_t vec, dvec3_t dest);

dmat3_t dquat_to_dmat3(dquat_t dquat, dmat3_t dest);

dquat_t dquat_to_dmat4(dquat_t dquat, dmat4_t dest);

dquat_t dquat_slerp(dquat_t dquat, dquat_t dquat2, 
                    double slerp, dquat_t dest);

char*   dquat_str(dquat_t dquat);

#ifdef __cplusplus
}
#endif

#endif
