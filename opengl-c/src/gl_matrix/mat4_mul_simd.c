#include "gl_matrix.h"
#include "x86intrin.h"
mat4_t mat4_multiply_simd(mat4_t mat1, mat4_t mat2, mat4_t dest){
  __m128 r0,r1,r2,r3;
  __m128 c0,c1,c2,c3;
  r0 = _mm_load_ps(mat1);
  r1 = _mm_load_ps(mat1+4);
  r2 = _mm_load_ps(mat1+8);
  r3 = _mm_load_ps(mat1+12);

  c0 = _mm_set_ps(mat2[0], mat2[4], mat2[8], mat2[12]);
  c1 = _mm_set_ps(mat2[1], mat2[5], mat2[9], mat2[13]);
  c2 = _mm_set_ps(mat2[2], mat2[6], mat2[10], mat2[14]);
  c3 = _mm_set_ps(mat2[3], mat2[7], mat2[11], mat2[15]);
