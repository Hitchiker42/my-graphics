/*
  Some useful structures and functions for interactive user input.
  i.e rotating/translating a scene/shape based on keypresses, or
  toggling lighting
*/
#ifndef __INTERACTION_H__
#define __INTERACTION_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
#include "context.h"
#include "drawable.h"
//Temporary untill I move the definition of gl_camera
#include "lighting.h"
//
struct keystate {
  int32_t keycode;
  int32_t scancode;
  uint16_t modmask;
  uint16_t action;
};
/*
  This is a more flexable way of specifying what to do when a key is pressed/released
  than the standard switch statement. This comes at the cost of memory, requing
  64 bytes per key to specify an action. This allows 4 functions per key, one each
  for unmodified, shift, ctrl, and alt + a data pointer for each. 
*/
typedef void(*keypress_callback)(gl_window win, int keycode, int scancode,
                                 int action, int modmask, void *data);
typedef void(*mouse_button_callback)(gl_window win, int button,
                                     int action, int modmask, void *data);
typedef void(*mouse_movement_callback)(gl_window win, int x,
                                       int y, void *data);
//NOTE: More than one callback will be invoked if multiple modifiers are
//presed, keep this in mind when writing callback functions
struct keypress_closure {
  keypress_callback callback;
  void *data;
};
union keypress_closures {
  keypress_closure closures[4];
  struct {
    keypress_callback default_callback;
    void *default_data;
    keypress_callback shift_callback;
    void *shift_data;
    keypress_callback ctrl_callback;
    void *ctrl_data;
    keypress_callback alt_callback;
    void *alt_data;
  };
};
struct mouse_button_closure {
  mouse_button_callback callback;
  void *data;
};
union mouse_button_closures {
  mouse_button_closure closures[4];
  struct {
    mouse_button_callback default_callback;
    void *default_data;
    mouse_button_callback shift_callback;
    void *shift_data;
    mouse_button_callback ctrl_callback;
    void *ctrl_data;
    mouse_button_callback alt_callback;
    void *alt_data;
  };
};
//this works for the way glfw keys are currently defined, it may
//have to change
#define KEY_TO_KEYMAP_INDEX(key) (key > 128 ? key - 128 : key)
#define KEYMAP_GET_KEY(keymap, key) (key < 128 ? keymap[key] : keymap[key-128])
#define KEYMAP_GET_KEYPTR(keymap, key) (key < 128 ? keymap+key : keymap+(key-128))
//this will only find the first of shift, control and alt
//This is dependent on the current values of the glfw moifier mask bits
#define MODIFIER_TO_INDEX(x) (ffs(x))
/*
  This needs to be a hashtable
*/
//This is more than enough to hold all the keys glfw currently defines
/*
  what this should be, or somethnig like this 
  struct gl_keymap {
    union {
      keypress_closures *keymap;
      keymap_node *sparse_keymap;
    }
    int32_t type;
    int32_t size
  }
*/
typedef keypress_closures* gl_keymap;
typedef mouse_button_closure *gl_button_map;

typedef struct keymap_node keymap_node;
typedef keymap_node *sparse_keymap;
struct keymap_node {
  keypress_closure closure;
  gl_keystate keystate;
  struct keymap_node *next;
};
//This can be set manually, for non ascii keys use the macro above to
//get the index, or via some of the functions below
extern gl_keymap default_keymap;
extern gl_keymap current_keymap;
//action argument coorsponding to text input
#define GLFW_TEXT (MAX(MAX(GLFW_RELEASE, GLFW_PRESS), GLFW_REPEAT))+1
#define KEYSTATE3(code, mod, act) {.keycode = code, .modmask = mod, .action = act}
#define KEYSTATE2(code, mod) KEYSTATE3(code, mod, 0)
#define KEYSTATE1(code) KEYSTATE3(code, 0, 0)
#define KEYSTATE(...) VFUNC(KEYSTATE, __VA_ARGS__)
#define KEYSTATE_CAST3(code, mod, act) (gl_keystate)KEYSTATE(code, mod, act)
#define KEYSTATE_CAST2(code, mod) KEYSTATE_CAST3(code, mod, 0)
#define KEYSTATE_CAST1(code) KEYSTATE_CAST3(code, 0, 0)
#define KEYSTATE_CAST(...) VFUNC(KEYSTATE_CAST, __VA_ARGS__)
//not sure where to put this so it goes here for now

#define KEY_PRESS GLFW_PRESS
#define KEY_RELEASE GLFW_RELEASE
#define KEY_REPEAT GLFW_REPEAT

struct window_data {
  gl_keymap keymap;
  gl_button_map mouse_map;
#ifdef HAVE_X11
  //  Display *X11_display;
#endif
};
void window_set_keymap(gl_window win, gl_keymap map);
void keymap_set_key(gl_keymap map, int key, int mod,
                    keypress_callback callback, void *data);

keypress_callback keypress_quit_program;
#include "interactive_transform.h"
#include "interactive_camera.h"

#ifdef __cplusplus
}
#endif
#endif /* __INTERACTION_H__ */
