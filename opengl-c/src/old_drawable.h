#ifndef __DRAWABLE_H__
#define __DRAWABLE_H__
#ifdef __cplusplus
extern "C" {
#endif
#include <stdarg.h>
/*
  TODO: figure out how to deal with ownership/refcounting of vertices.
  Problem:
   If two shapes use the same vertices how do we know when to free them,
   this isn't an issue for shape_multi or composite_drawable since their
   components have a refcount.
  Ideas:
   -If a polygon doesn't own it's vertices should it have a
     pointer to the shape that does?
   -Have a seperate refcounted struct for vertices (Probably this).
   -Add another type of drawable which is an indirect primitive shape,
     i.e a shape whoes vertices are owned by another shape.
   -Keep the refcount of the vertices in the extra field of the first
     vertex (not a bad idea)

   I could use the extra field on vertices as a refcount, but I really
   don't need to keep track of indivual vertices, just collections of them.

*/
//TODO: Move vbo_offset (and maybe some other fields) from gl_drawable to this
//prototype for refcounted vertices
/*
  This was a nice idea, but ended up being far more complex than necessary.

  A gl_drawable is a polymorphic type representing a number of different
  graphical objects. For most purposes a drawable can be thought of as
  a shape. However a drawable is somewhat more complex than a shape, due
  to optimizations in the way it is actually drawn.

  Put simply a drawable can be:
  -a primitive shape (effectively a set of vertices to draw)
  -An array of primitive shapes which share some common attributes
     allowing drawing them to be optimized
  -A composite drawable, which is a drawable composed of other drawables,
     potentially including other composite drawables.
  -An instanced drawable, which is a single drawable, which is drawn
    multiple times, along with a set of per-instance data which is
    used to modify the drawable in different ways.
*/
//Several drawable types represent primitive shapes (drawables whose components are
//vertices). They all have the first bit set (and only they have the first bit set)
enum GL_drawable_type {
  //these next 4 all represent shapes
  GL_drawable_shape = 0x01,
  GL_drawable_points = 0x03,
  GL_drawable_line = 0x05,
  GL_drawable_2d_polygon = 0x09,//specifically 2d convex polygon
  GL_drawable_3d_polygon = 0x11,//again, a convex poylgon
  GL_drawable_composite = 0x02,//A shape composed of other shapes
};
#define GL_IS_SHAPE_TYPE(type) (type & 0x01)
#define GL_IS_SHAPE(obj) (obj->type & 0x01)
#define GL_IS_POLYGON(obj) (obj->type == GL_drawable_3d_polygon ||\
                            obj->type == GL_drawable_2d_polygon)
#define TO_DRAWABLE(obj) ((gl_drawable*)obj)
#define AS_DRAWABLE(obj) ((gl_drawable*)obj)
#define TO_SHAPE(obj) ((gl_shape*)obj)
#define AS_SHAPE(obj) ((gl_shape*)obj)
#define TO_POLYGON(obj) ((gl_polygon*)obj)
#define AS_POLYGON(obj) ((gl_polygon*)obj)
char *drawable_type_to_string(enum GL_drawable_type);
/*
  The contents of a drawble are defined in a macro to avoid having
  to write them a bunch of times, and to ensure all drawables have
  the same fields.
*/
#define inline_drawable()                                               \
  gl_scene *scene;                                                      \
  gl_VBO *vbo;                                                          \
  const char *name;                                                     \
  float transform[16];                                                  \
  /* the data for the drawable, is refcounted*/                         \
  union {                                                               \
    gl_vertices *vertices;/*shape/line/points*/                         \
    gl_drawable **drawables;                                            \
    void **components;                                                  \
  };                                                                    \
  gl_material *material_properties;                                     \
  void(*bind_drawable)(gl_drawable *);                                  \
  union {                                                               \
    int num_vertices;                                                   \
    int num_drawables;                                                  \
    int num_components;                                                 \
  };                                                                    \
  uint8_t is_static;                                                    \
  uint32_t instanced;                                                   \
  int32_t refcount;/*should probably be atomic*/                        \
  uint32_t size;/*total size (in bytes) of the drawable*/               \
  enum GL_drawable_type type;
//vbo offset needs to be a pointer so when it changes it gets updated
//in all copies of the drawable as well
struct gl_drawable {
  inline_drawable();
};
union gl_drawable {
  gl_shape shape;
  gl_composite_drawable composite;
};
struct gl_vertices {
  gl_vertex *vertices;
  GLuint vertex_buffer;
  int32_t refcount;
  int32_t num_vertices;
  uint32_t vbo_offset;
  uint8_t sync;
};
struct gl_indices {
  gl_index_t *indices;
  GLuint index_buffer;
  uint32_t num_indices;
  GLenum index_type;
  uint8_t index_size;
  uint8_t index_packing;
};
//just a drawable + indices
struct gl_shape {
  gl_scene *scene;
  gl_VBO *vbo;
  const char *name;
  float transform[16];
  enum GL_drawable_type type;
  int32_t refcount;
  //end of shared fields
  gl_vertices *vertices;
  gl_indices *indices;//can be NULL
  gl_material *material_properties;
  void(*bind_drawable)(gl_drawable *);
  GLenum draw_mode;
};
//issue an opengl command to draw the given drawable, if the drawable
//needs to be synced or reallocated this is done first
void gl_draw_drawable(gl_drawable *drawable);
//Sync the data in drawable with the vbo
//Called implicitly by draw_drawable
void gl_sync_drawable(gl_drawable *drawable);
//bind the necessary uniforms/buffers necessary to draw drawable
//This should be called at the end of any user function that
//binds other components of drawable.
void bind_drawable_default(gl_drawable *drawable);
//add drawable to the vbo, if the vbo is full this will require
//orphaning it and potentially allocating a new one, all drawables
//stored in the vbo will then need to be re-synced before they are
//drawn again. However this should generally not be a hugely
//expensive function, on average
void vbo_add_drawable(gl_VBO *vbo, gl_drawable *drawable);
void vbo_add_drawables(gl_VBO *vbo, gl_drawable **drawable, int n);
//we might be able to optimze if we know the number of args
void vbo_add_drawable_va(gl_VBO *vbo, ...);
void vbo_add_drawables_va(gl_VBO *vbo, int n, ...);
//remove drawable from the vbo, return 1 if something was actually removed
int vbo_remove_drawable(gl_VBO *vbo, gl_drawable *drawable);
void drawable_set_scene(gl_drawable *drawable, gl_scene *scene);
//create a copy of drawable with refcount set to 1
//the drawable itself isn't changed in any way
//Components of drawable aren't copied, but have their refcount increased
gl_drawable *copy_gl_drawable(gl_drawable *drawable);
//recursively copy drawable
gl_drawable *copy_gl_drawable_rec(gl_drawable *drawable);
//crate a copy of drawable with refcount set to 1
//also decrement the refcount of drawable
gl_drawable *duplicate_gl_drawable(gl_drawable *drawable);
//Create a deep copy of drawable with refcount set to 1
gl_drawable *clone_gl_drawable(gl_drawable *drawable);
//up the refcount for drawable, returns drawable for convience
//(Is there really any use in this function?)
gl_drawable* ref_gl_drawable(gl_drawable *drawable);
//decrement the refcount on drawable and free it if the refcount is 0 or less
void free_gl_drawable(gl_drawable *drawable);
//free all resources assoicated with drawable and remove it from the vbo
//no questions asked (i.e ignore refcount)
void destroy_drawable(gl_drawable *drawable);

//this is a macro to avoid using va_args, since, well va_args are annoying
//type is 'not' a value from gl_drawable_type, but the name of the
//struct you want, sans the gl prefix
#define make_drawable(type, ...)                \
  make_##type(__VA_ARGS__)
/*
  TODO change make_polygon to make_polygon
*/
//allocate a primitive shape, along with memory to store its vertices
//and indices.
gl_drawable *make_polygon_indexed(int num_vertices, int num_indices);
static inline gl_drawable *make_polygon(int num_vertices){
  return make_polygon_indexed(num_vertices, num_vertices);
}
#define make_line_obj(n) make_gl_shape(n)
#define init_line_obj(v, n) init_gl_shape(v,n)
gl_drawable *init_line_obj(gl_vertex *vertices, int num_vertices);
gl_drawable *make_gl_shape(int num_vertices);
gl_drawable *init_gl_shape(gl_vertex *vertices, int num_vertices);
#define INIT_POLYGON(vertices, indices)                         \
  init_polygon(vertices, sizeof(vertices)/sizeof(vertices[0]),  \
               (gl_index_t*)indices, sizeof(indices)/sizeof(indices[0]))
#define set_draw_mode(obj, mode) (((gl_shape*)obj)->draw_mode = mode)
gl_drawable *init_polygon(gl_vertex *vertices, int num_vertices,
                          gl_index_t *indices, int num_indices);

gl_drawable *make_composite_drawable(int num_drawables);
enum {
  COMPOSITE_INIT_NONE,
  COMPOSITE_INIT_REF,
  COMPOSITE_INIT_COPY,
  COMPOSITE_INIT_DUP,
  COMPOSITE_INIT_CLONE,
};
#define INIT_COMPOSITE_DRAWABLE(action, ...)                            \
  ({gl_drawable *drawables[] = {__VA_ARGS__};                           \
    gl_drawable *composite = init_composite_drawable(drawables,         \
                                                     ARRAY_LEN(drawables), \
                                                     action);           \
    composite;})
/*
  Generate a composite drawable by copying 'proto' 'count' times, init is called
  on each generated drawable with the drawable itself and how many drawables
  have been generated so far.
*/
gl_drawable *gen_composite_drawable(gl_drawable *proto, void(*init)(gl_drawable*,int i),
                                    int count);
gl_drawable *init_composite_drawable(gl_drawable **drawables,
                                     int num_drawables, int action);
//create a composite drawable from drawables, incrementing the refcount
//of each drawable
gl_drawable *init_composite_drawable_ref(gl_drawable **drawables,
                                     int num_drawables);
//create a composite drawable from drawables, copying each
gl_drawable *init_composite_drawable_copy(gl_drawable **drawables,
                                          int num_drawables);
//create a composite drawable from drawables, duplicateing each drawable
gl_drawable *init_composite_drawable_dup(gl_drawable **drawables,
                                          int num_drawables);
//create a composite drawable from drawables, cloning each drawable
gl_drawable *init_composite_drawable_clone(gl_drawable **drawables,
                                           int num_drawables);
//Allocate an empty drawable of the specified type
gl_polygon *allocate_polygon();
gl_composite_drawable* allocate_composite_drawable();
//returns a void* to avoid having to cast the return value
void *allocate_drawable(enum GL_drawable_type type);
//create a string representation of drawable and place it into
//a newly allocated buffer
char *drawable_to_string(gl_drawable *drawable);
#ifdef __cplusplus
}
#endif
#endif /* __DRAWABLE_H__ */
