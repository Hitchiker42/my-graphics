#include "common.h"
#include "gl_util.h"
#include "GLFW/glfw3.h"
//#include "interaction.h"
#ifdef HAVE_X11
#define GLFW_EXPOSE_NATIVE_X11
#define GLFW_EXPOSE_NATIVE_GLX
#include "GLFW/glfw3native.h"
#endif
#include "interaction.h"

void default_glfw_key_callback(gl_window win, int key, int scancode,
                              int action, int modmask){
  struct window_data *data = glfwGetWindowUserPointer(win);
  //key = lookup_key(data, scancode, modmask, &sym, &mods);
  //GLFW doesn't pass different values for key for lowercase and
  //upper case keys
  if(key >= 'A' && key <= 'Z'){
    //if shift wasn't pressed change to lower case
    if(!(modmask & GLFW_MOD_SHIFT)){
      key |= 0x20;//0x20 =  'a' ^ 'A'
    }
  } else if(key >= 'a' && key <= 'z'){
    //currently glfw just passes uppercase characters, but this
    //is in case that changes
    if(modmask & GLFW_MOD_SHIFT){
      key &= ~(0x20);
    }
  }
  //unset shift for keys that can only exist in one shift state
  //(i.e you can never have shift+'a', or not shift + 'A'
  if(key > ' ' && key < 128){
    modmask &= ~(GLFW_MOD_SHIFT);
  } else if(key > 128){
    key -= 128;
  }
  gl_keymap keymap = data->keymap || current_keymap;
  keypress_closures callback = keymap[key];
  /*  char *buf = keypress_closures_to_string(&callback);
  DEBUG_PRINTF("Recieved keypress %d, with modmask %u\nwith callbacks: %s",
               key, modmask, buf);
               free(buf);*/
  if(!modmask){
    if(callback.default_callback){
      return callback.default_callback(win, key,
                                       callback.default_data, action, modmask);
    }
  } else {
    if(modmask & GLFW_MOD_SHIFT){
      if(callback.shift_callback){
        callback.shift_callback(win, key,
                                callback.shift_data, action, modmask);
      }
    }
    if(modmask & GLFW_MOD_CONTROL){
      if(callback.ctrl_callback){
        callback.ctrl_callback(win, key,
                               callback.ctrl_data, action, modmask);
      }
    }
    if(modmask & GLFW_MOD_ALT){
      if(callback.alt_callback){
        callback.alt_callback(win, key,
                              callback.alt_data, action, modmask);
      }
    }
  }
}
static void handle_error(int err_code, const char *err_string){
  fprintf(stderr,"Glfw error %d:\n%s\n",err_code, err_string);
  return;
}
    
gl_window init_gl_context(int w, int h, const char* name){
  glfwSetErrorCallback(handle_error);
  if(!glfwInit()){
    fprintf(stderr, "Error, failed to initialize glfw\n");
    exit(EXIT_FAILURE);
  }
  atexit(glfwTerminate);
  glfwWindowHint(GLFW_SAMPLES, GL_UTIL_MULTISAMPLE);//4xAA
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GL_UTIL_CONTEXT_VERSION_MAJOR);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GL_UTIL_CONTEXT_VERSION_MAJOR);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
  GLFWwindow *win = glfwCreateWindow(w, h, name, NULL, NULL);
  if(!win){
    fprintf(stderr, "Error creating window\n");
    exit(EXIT_FAILURE);
  }
  glfwMakeContextCurrent(win);
  init_glew();
  struct window_data *data = xmalloc(sizeof(struct window_data));
#ifdef HAVE_X11
  data->X11_display = glfwGetX11Display();
#endif
  glfwSetWindowUserPointer(win, data);
  glfwSetKeyCallback(win, default_glfw_key_callback);
  //  glfwSwapInterval(1);
  //  init_shapes();
  return win;
}
