#ifndef __DRAWABLE_H__
#define __DRAWABLE_H__
#include "common.h"
#include <stdarg.h>
/*
  Since we only have two different types of drawables we can easily
  store the type in the pointer, so that it's not necessary to actually 
  dereference the pointer to figure out the type.

data = glMapBufferRange(GL_ARRAY_BUFFER, *drawable->vbo_offset,
                            drawable->size, GL_MAP_WRITE_BIT);  A composite drawable has the least significant bit set, while
  a non composite drawable does not.
*/
#define IS_COMPOSITE(ptr) (((uint64_t)ptr) & 0x1ul)
#define AS_COMPOSITE(ptr) ((void*)(((uint64_t)ptr) | 0x1ul))
//AS_DRAWABLE just casts ptr to a void *, TO_DRAWABLE masks out the
//bit marking a composite drawable
#define AS_DRAWABLE(ptr) ((void*)ptr)
#define TO_DRAWABLE(ptr) ((void*)(((uint64_t)ptr) & ~(0x1ul)))
#define AS_SHAPE(ptr) ((void*)(ptr))
#define SHAPE_VERTICES(shape) (((gl_shape*)shape)->vertices->vertices)
#define VERTICES_SIZE(verts) (verts->num_vertices * sizeof(gl_vertex))
struct gl_vertices {
  gl_vertex *vertices;
  GLuint vertex_buffer;
  int32_t refcount;
  int32_t num_vertices;
  uint32_t vbo_offset;
  uint8_t sync;
  uint8_t is_static;
};
/*
  Indices are static, once they are bound to a buffer you can't
  change them, though I don't know why you would want to.
  This means than once the index data is copied to the gpu
  we can free it on the cpu.
*/
struct gl_indices {
  gl_index_t *indices;
  GLuint index_buffer;
  GLenum index_type;
  uint32_t num_indices;
  int32_t refcount;
  uint8_t index_size;
  uint8_t index_packing;
  uint8_t is_static;//if this is true, don't free this 
};
//maybe add a void* userdata pointer
typedef struct gl_drawable_header gl_drawable_header;
struct gl_drawable_header {
  gl_scene *scene;
  gl_VBO *vbo;
  const char *name;
  float transform[16];
  gl_material *material_properties;
  gl_texture *texture;
  int32_t refcount;
};
//just a drawable + indices
struct gl_shape {
  gl_scene *scene;
  gl_VBO *vbo;
  const char *name;
  float transform[16];
  gl_material *material_properties;
  gl_texture *texture;
  int32_t refcount;
  //end of shared fields
  gl_vertices *vertices;
  gl_indices *indices;//can be NULL
  void(*bind_shape)(gl_shape*);
  GLenum draw_mode;
};
struct gl_composite_drawable {
  gl_scene *scene;
  gl_VBO *vbo;
  const char *name;
  float transform[16];
  gl_material *material_properties;
  gl_texture *texture;
  int32_t refcount;
  int32_t num_drawables;
  gl_drawable **drawables;
};
union gl_drawable {
  gl_shape shape;
  gl_composite_drawable composite;
  struct gl_drawable_header header;
  struct {
    gl_scene *scene;
    gl_VBO *vbo;
    const char *name;
    float transform[16];
    gl_material *material_properties;
    gl_texture *texture;
    int32_t refcount;
  };
};

/*
  creates a shape object, a vertices object and potentially an
  indices object, if num_indices is not NULL
*/
gl_shape *make_gl_shape_indexed(int num_vertices, int num_indices);
gl_shape *make_gl_shape(int num_vertices);
gl_shape *create_gl_shape(gl_vertices *verts, gl_indices *inds);
gl_shape *init_gl_shape_indexed(gl_vertex *vertices, int num_vertices,
                        gl_index_t *indices, int num_indices,
                        int index_size);
gl_shape *init_gl_shape(gl_vertex *vertices, int num_vertices);
gl_vertices *allocate_gl_vertices(int num_vertices);
gl_vertices *init_gl_vertices(gl_vertex *verts, int num_vertices);
gl_shape *allocate_gl_shape();
gl_indices *allocate_gl_indices(int num_indices, int index_size);
gl_indices *init_gl_indices(gl_index_t *indices,
                            int num_indices, int index_size);
void init_index_buffer(gl_indices *indices);
gl_drawable* ref_drawable(gl_drawable *drawable);
gl_drawable* copy_drawable(gl_drawable *drawable);
gl_shape *copy_gl_shape(gl_shape *shape);
gl_shape *clone_gl_shape(gl_shape *shape);
gl_vertices *allocate_gl_vertices(int num_vertices);
gl_indices *allocate_gl_indices(int num_indices, int index_size);
void vbo_add_drawable(gl_VBO *vbo, gl_drawable *drawable);
void vbo_add_drawables(gl_VBO *vbo, gl_drawable **drawables, int n);
void gl_draw_drawable(gl_drawable *d);
void gl_sync_drawable(gl_drawable *drawable);
/*
  Creates a composite drawable from an array of drawables.
  action is one of COMPOSITE_REF, COMPOSITE_COPY and COMPOSITE_NONE,
  and the drawables are either reffed, copied or used directly 
  respectively.
*/
enum {
  COMPOSITE_NONE = 0x0,
  COMPOSITE_REF = 0x1,
  COMPOSITE_COPY = 0x2
};
gl_composite_drawable *make_composite_drawable(gl_drawable **shapes,
                                                int num_drawables,
                                                int action);
gl_composite_drawable *
copy_gl_composite_drawable(gl_composite_drawable *drawable, int action);
#endif
