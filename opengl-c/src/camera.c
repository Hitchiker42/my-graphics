  
/*
  Camera rotation:
  //traces a circle centered at a = center.x, b = center.y, w/radius R = |eye-center|
  //defined by the parametric equations x(t) = R*cos(t)+a and y(t) = Rsin(t)+b
  float r = distance(eye, center);
  float c = cos(theta), s = sin(theta);
  float x = r*c+eye.x, y = r*s+eye.y;
*/
//panning/zooming is pretty simple
void camera_horizontal_pan(gl_camera *camera, float delta){
  camera->center.x += delta;
}
void camera_vertical_pan(gl_camera *camera, float delta){
  camera->center.y += delta;
}
void camera_zoom(gl_camera *camera, float magnification){
  camera->eye.z*=magnification;
}
//rotation is a bit more complex
void camera_rotate(gl_camera *camera, float theta){
  float r = position_distance(camera->eye, camera->center);
  float c,s;
  sincos(theta, &c, &s);
  float x = (r * c) + camera->eye.x;
  float y = (r * s) + camera->eye.y;
  camera->eye.x = x;
  camera->eye.y = y;
}
