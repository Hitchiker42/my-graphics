#include "tree.c"
struct skip_link {
  struct skip_node *next;
  int width;//number of elements b/t current node and next
};
struct skip_node {
  void *data;
  struct skip_link *links;
  int height;//length of links array
};
//In case I change the layout/implemention of the node/link struct

#define GET_NEXT(node, i) (node->links[i].next)
#define GET_NEXT_DATA(node, i) (node->links[i].next->data)
#define GET_NEXT_WIDTH(node, i) (node->links[i].width)
typedef struct skip_list s_list;
typedef struct skip_link s_link;
typedef struct skip_node s_node;
static int rand_dist_log2(int maxdepth);
inline s_node* make_skip_node(void *data, int height){
  s_node *ret = zmalloc(sizeof(s_node));
  s_link *links = zmalloc(height * sizeof(s_link));
  ret->data = data;
  ret->links = links;
  ret->height = height;
  return ret;
}
/*
  Search for key in ls and return the node containing key, if it exists,
  otherwise the node that would come directly before key, where key
  added to the list.
*/
static inline s_node* internal_search(s_list *ls, void *key){
  int i;
  s_node *node = ls->head;
  for(i=(ls->height-1); i>=0; i--){
    while(GET_NEXT(node,i) != NULL && ls->cmp(key, GET_NEXT_DATA(node,i)) > 0){
      node = GET_NEXT(node,i);
    }
  }
  return GET_NEXT(node,0);
}
/*
  As long as we keep track of the number of elements between each link indexing
  a skip list can be done in O(log(n)) time.
*/
void *skip_list_elt(s_list *ls, int idx){
  if(idx < 0){
    idx = ls->length - idx;
  }
  if(idx < 0 || idx > ls->length){
    return NULL;
  }
  int index = 0;
  int height = ls->length-1;
  s_node *node = ls->head;
  /*
    We don't check for over/underflow on height, I'm pretty sure this is safe
    since we know idx is in range
  */
  while(index < idx){
    int inc = GET_NEXT_WIDTH(node, height);
    if(index + inc <= idx){
      index += inc;
      node = GET_NEXT(node, height);
    } else {
      height--;
    }
  }
  return node->data;
}
/*
  Returns 0 if key was already in ls, and 1 otherwise
*/
int skip_list_insert(s_list *ls, void *key){
  int new_node_height = rand_dist_log2(ls->max_height);
  s_node *new_node = make_skip_node(key, new_node_height);
  s_node *node = ls->head;
  s_node *head = ls->head;
  int height = ls->height;
  s_node *aux[ls->height];
  int *idx_aux[ls->height];
  int idx;
  for(i = (height-1); i>=0; i++){
    while(GET_NEXT(node,i) != NULL && ls->cmp(key, GET_NEXT_DATA(node,i)) > 0){
      node = GET_NEXT(node, i);
      idx += GET_NEXT_WIDTH(node, i);
    }
    aux[i] = node;
    idx_aux[i] = idx;//I'd like to get rid of this but I don't know if I can
  }
  //key was already in the list
  if(ls->cmp(key, GET_NEXT_DATA(node->data)) == 0){
    return 0;
  }
  ls->length++;
  while(new_node->height > ls->height){
    GET_NEXT(head, ls->height) = new_node;
    GET_NEXT_WIDTH(head, ls->height) = idx;
    ls->height++;
  }
  i = MIN(new_node->height, height);
  while(--i >= 0){
    int width = idx - idx_aux[i];

    GET_NEXT(new_node, i) = GET_NEXT(aux[i]);
    //The +1 is to account for the node being added
    GET_NEXT_WIDTH(new_node, i) = (GET_NEXT_WIDTH(aux[i]) - width)+1;

    GET_NEXT(aux[i]) = new_node;
    GET_NEXT_WIDTH(aux[i]) = width;
  }
}
/*
  returns a number between 1 and MIN(maxdepth, 32) with a distribution
  defined by the probability mass function: P(n) = 1/2^n
*/
static int rand_dist_log2(int maxdepth){
  int h = 1;
  uint32_t r = lrand48();
  while(r & 1 && h++ < maxdepth){
    r>>=1;
  }
  return h;
}
