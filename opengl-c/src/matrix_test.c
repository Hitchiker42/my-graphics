#include "common.h"
float translate[3] = {0x2, 0x4, 0x8};
float scale[3] = {0x10, 0x20, 0x40};
int main(){
  float mat[16];
  mat4_identity(mat);
  mat4_translate(mat, translate, NULL);
  mat4_scale(mat, scale, NULL);
  DEBUG_PRINTF("Translated then scaled:\n");
  gl_print_mat4(mat);
  mat4_identity(mat);
  mat4_scale(mat, scale, NULL);
  mat4_translate(mat, translate, NULL);
  DEBUG_PRINTF("Scaled then translated:\n");
  gl_print_mat4(mat);
  return 0;
}
