#include "common.h"
#include "context.h"
#include "shapes.h"
#include "drawable.h"
const char *vertex_shader =
  "#version 330 core\n"
  "layout(location = 0) in vec4 position;\n"
  "layout(location = 1) in vec4 color;\n"
  //unused but part of the default vertex attribute layout
  "layout(location = 2) in vec3 normals;\n"
  "layout(location = 3) in vec2 tex_coords;\n"
  "out vec4 v_color;\n"
  "uniform transform {\n"
  "  mat4 proj_view;\n"
  "  mat4 model;\n"
  "}"
  "void main(){\n"
  "  gl_position = position * proj_view * model;"
  "  v_color = color;\n"
  "}\n";
const char *fragment_shader =
  "#version 330 core\n"
  "in vec4 v_color;\n"
  "layout(location = 4) out vec4 f_color;\n"
  "void main(){\n"
  "  f_color = v_color;\n"
  "}\n";
/*
  C++ implicitly typedefs structs.
*/
#define mouse_movement_delta 5
thread_local global_context *thread_ctx;
struct userdata {
  gl_primitive_shape *current_shape;
  int current_sides;
  gl_color current_color;
  //this might be general enough to be worth putting into gl_toolkit.c
  int mouse_button_pressed;
  double x_when_pressed;
  double y_when_pressed;
  //non-null only when drawing re-sizable shape
  gl_primitive_shape *current_shape;
  gl_primitive_shape *default_triangle;
  gl_primitive_shape *default_square;
  gl_primitive_shape *default_polygon;
};
enum program_colors {
  color_blue = 0,
  color_cyan = 1,
  color_green = 2,
  color_magenta = 3,
  color_red = 4,
  color_yellow = 5
};
//top vertex of the default triangle
static gl_position triangle_start = {0.0,0.2,0.0,1.0};
//top left corner of default square
static gl_position square_start = {0.2,0.2,0.0,1.0};
//top vertex of default polygon
static gl_position polygon_start = {0.0,0.2,0.0,1.0};
static gl_color colors[] =
  {0x0000ffff /*blue*/, 0x00ffffff /*cyan*/,
   0x00ff00ff /*green*/, 0xff00ffff /*magenta*/,
   0xff0000ff /*red*/, 0xff00ffff /*yellow*/};

static inline double point_distance(double x1, double y1,
                                    double x2, double y2){
  return sqrt(pow((x1 - x2),2) + pow((y1 - y2),2));
}
static inline gl_primitive_shape *
copy_current_default_shape(struct userdata *userdata){
  if(userdata->current_sides == 3){
    return (gl_primitive_shape*)copy_gl_drawable(userdata->default_triangle);
  } else if(userdata->current_sides == 4){
    return (gl_primitive_shape*)copy_gl_drawable(userdata->default_square);
  } else {
    return (gl_primitive_shape*)copy_gl_drawable(userdata->default_polygon);
  }
}
void keypress_callback(gl_window win, int key, int action,
                       int modmask, double x, double y){
  //only act when key is released
  if(action != GL_ACTION_RELEASE){
    return;
  }
  if(key < 'a' || key > 'Z'){
    return;
  }
  struct userdata *userdata = ((struct userdata*)thread_ctx->userdata);
  if(key >= 'A'){
    switch(key){
      case 'B':
        userdata->current_color = colors[color_blue]; return;
      case 'C':
        userdata->current_color = colors[color_cyan]; return;
      case 'G':
        userdata->current_color = colors[color_green]; return;
      case 'M':
        userdata->current_color = colors[color_magenta]; return;
      case 'R':
        userdata->current_color = colors[color_red]; return;
      case 'Y':
        userdata->current_color = colors[color_yellow]; return;
    }
  } else {//key < 'A'
    switch(key){
      case 'p':
        userdata->current_sides = 10; return;
      case 'r':
        userdata->current_sides = 4; return;
      case 't':
        userdata->current_sides = 3; return;
    }
  }
}
void mouse_button_callback(gl_window win, int button, int action,
                           int modmask, double x, double y){
  if(button != GL_MOUSE_BUTTON_1){
    return;
  }
  struct userdata *userdata = ((struct userdata*)thread_ctx->userdata);
  if(action == GL_ACTION_PRESS){
    userdata->mouse_button_pressed = 1;
    return;
  } else if(action == GL_ACTION_RELEASE){
    if(userdata->current_shape == NULL){
      gl_VBO vbo = thread_ctx->scenes[0].VBO;
      gl_primitive_shape *temp = copy_current_default_shape(usardata);
      gl_position pos = {x,y,0};
      int i;
      temp->model = mat4_identity(temp->model);
      temp->model = mat4_translate(temp->model, &pos, NULL);
      for(i=0;i<userdata->current_sides;i++){
        temp->verticies->verticies.color = userdata->current_color;
      }
      vbo_add_drawable(vbo, (gl_drawable*)temp);
    } else {
      //Might need to update the shape one last time

      //We want the shape to keep being drawn, but we don't actually need to
      //access it anymore. The vertex indices are still needed to draw the
      //shape, so we can't free it, but we can just sorta forget about it
      userdata->current_shape = NULL;
    }
  }
}
mat4_t update_pos_scale(struct userdata *userdata, double x, double y){
  mat4_t transform = userdata->current_shape->model;
  float dx = userdata->x_when_pressed - x;
  float dy = userdata->y_when_pressed - y;
  float scale[3] = {dx,dy,0};
  float offset[3] = {dx/2,dy/2,0};
  transform = mat4_scale(mat4_identity(transform), scale, NULL);
  transform = mat4_translate(transform, offset, NULL);
  return transform;
}
  
void mouse_position_callback(gl_window win, double x, double y){
  struct userdata *userdata = ((struct userdata*)thread_ctx->userdata);
  if(userdata->mouse_button_pressed != 1){
    return;
  }
  if(userdata->current_shape == NULL){
    double delta = point_distance(x, y, userdata->x_when_pressed,
                                  userdata->y_when_pressed);
    if(delta < mouse_movement_delta){
      return;
    } else {
      userdata->current_shape =
        copy_current_default_shape(userdata);
      int i;
      for(i=0;i<userdata->current_sides;i++){
        userdata->current_shape->verticies->verticies.color =
          userdata->current_color;
      }
    }
  }
  //we only get here if the current shape exists
  update_pos_scale(userdata, x, y);
}
global_context *init_ctx(void){
  global_context *ctx = make_global_context(800,800,"l1");
  gl_set_key_callback(ctx->win, keypress_callback);
  gl_set_button_callback(ctx->win, mouse_button_callback);
  gl_set_mouse_pos_callback(ctx->win, mouse_position_callback);
  ctx->userdata = (struct userdata*)zmalloc(sizeof(struct userdata));
  ctx->userdata->default_triangle =
    make_colored_convex_polygon(&triangle_start, 3, NULL, 0xffffffff);
  ctx->userdata->default_square =
    make_colored_convex_polygon(&square_start, 4, NULL, 0xffffffff);
  ctx->userdata->default_polygon =
    make_colored_convex_polygon(&polygon_start, 10, NULL, 0xffffffff);
  ctx->scenes = make_simple_scene(vertex_shader, fragment_shader);
}
int main(){
  thread_ctx = init_ctx();
  gl_main_loop(thread_ctx);
}
