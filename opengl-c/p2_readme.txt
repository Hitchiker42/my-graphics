I'm not really sure what should be considered for my wildcard, I've done a lot
of extra stuff and implemented everything myself (admittly it doesn't all 
work perfectly). For some concrete things to consider:
  -Implementation of a sphere (which I'm kinda proud of)
  -Use of a single VBO per scene, updating objects via mapping the buffer,
   and using glDrawElementsBaseVertex for objects with indices.
  -Animation in my 3rd scene (it doesn't always work, and it's kinda jittery)
  -Implementing object orientation in C (It's not C++ style object 
  orientation, but I have generic functions which dispatch based on the type 
  of their argument and inheritance)
  -Writing everything myself from the ground up, in C.

There are definately some issues with my code, but I wanted to do everything
myself to help understand things better. I need to do some debugging on the
buffer mapping, since it doesn't really work, I had to put in some explicit
synchronization via calls to glFinish, which kills the performance, but I
didn't have time to figure out what the issue was.

  
