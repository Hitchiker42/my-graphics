#!/usr/bin/env perl
use v5.18.0;
use autodie;
no warnings "experimental";
my $arch = `uname -m`;
my $objfile_type = ($arch =~ m/64/ ? "elf64-x86-64" : "elf32-i386");
my $symbol_names = [];
sub bin_to_obj {
  my $binfile = shift;
  my $objfile = shift || (($binfile =~ s/\.[^.]*$//gr) . ".o");
  #will fail if binfile has spaces
  system(
    split(" ","objcopy -B i386 -I binary " .
          "--rename-section .data=.rodata,alloc,load,readonly,data,contents " .
          "-O $objfile_type $binfile $objfile"));
  for(split('\n',`nm $objfile`)){
    my @F = split(' ',$_, -1);
    push($symbol_names, $F[2]);
  }
}
my $header_name = shift;
# Convert all png and jp[e]g files to object files,
# recursively converting directories
while($_ = shift(@ARGV)){
  if(-d $_){
    opendir(my $dh, $_);
    while(my $file = readdir($dh)){
      if($file =~ m/(?:\.png$|\.jpe?g$)/){
        bin_to_obj("$_/$file");
      }
    }
  } else {
    if(m/(?:\.png$|\.jpe?g$)/){
      bin_to_obj($_);
    }
  }
}
open(my $fh, ">", $header_name);
while($_ = pop($symbol_names)){
  my $varname = (s/(?:_binary|_png|_jpe?g)//gr);
  if(m/size/){
    print($fh qq|size_t $varname = asm("$_");\n|);
  } else {
    print($fh qq|unsigned char[] $varname = asm("$_");\n|);
  }
}



# Local Variables:
# perl-indent-level: 2
# End:
