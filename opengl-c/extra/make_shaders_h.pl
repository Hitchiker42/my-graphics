#!/usr/bin/env perl
use v5.16.0;
use File::Basename;
use autodie;
my $shaders_dir = $ARGV[0];
my $outfile = $ARGV[1];
open(my $fh, ">", $outfile) || die "Can't open $outfile for writing";
opendir(my $dh, $shaders_dir) || die "Can't open directory $shaders_dir";
my $guard_name = uc(basename($outfile) =~ s/\./_/gr);
print($fh "#ifndef __${guard_name}__\n#define __${guard_name}__\n\n");
while(readdir($dh)){
    next if !(m/.*\.frag$/ || m/.*\.vert/ || m/.*\.geom/);
    open(my $shader, "<", "$shaders_dir/$_");
    #my $varname = s/\./_/gr;
    my $varname = s/\.vert/_vertex/r;
    $varname =~ s/\.frag/_fragment/;
    $varname =~ s/\.geom/_geometry/;
    $varname =~ s/\./_/g;
    $varname =~ s/-/_/g;
    print($fh "static const char ${varname}[] =");
    while(my $line = readline($shader)){
        chomp($line);
        print($fh qq(\n  "$line\\n"));
    }
    print($fh ";\n\n");
    close($shader) || die;
}
closedir($dh);
print($fh "#endif\n");
