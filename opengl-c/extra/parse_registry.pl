#!/usr/bin/env perl
use v5.18.0;
use XML::Parser;
#use autodie;
no warnings "experimental";
my $groups = {};
#my $enums = {};
my $current_group;
my $skipping = 0;
# Parser for the open gl registry xml file(s)
# Entries of note in the registry
# <type> ... <name> typename </name> </type>
# <group name="group name> groups enums into subtypes
# <enum (value="value")? name="GL_ENUM_NAME"/>
# <enums namespace="" start="" end=""> enums, with values
sub start_event {
    my ($ex,$elt,%attrs) = @_;
    given($elt){
        when("group"){
            $current_group = {name => $attrs{"name"},
                              enums => []};
        }
        when("enums"){
            if(!exists($attrs{"group"}) || exists($groups->{$attrs{"group"}})){
                $skipping = 1;
                #This doesn't seem to work for some reason
                $ex->skip_until($ex->element_index());
            } else {
                $skipping = 0;
                $current_group = {name => $attrs{"group"},
                                  enums => []};
            }
        }
        when("enum"){
            unless($skipping){

            push($current_group->{"enums"}, $attrs{"name"});
            # if(exists($attrs{"value"}) && !exists($enums->{$elt})){
            #     $enums->{$elt} = $attrs{"value"};
            # }
            }
        }
        default{
        }
    }
    return;
}
sub end_event {
    my ($ex,$elt) = @_;
    if($elt eq "group" || $elt eq "enums"){
        if(!exists($groups->{$current_group->{"name"}})){
            $groups->{$current_group->{"name"}} = $current_group->{"enums"};
        }
    }
}
sub un_camel_case {
  $_ = shift;
  s/([a-z0-9])([A-Z]{2,})/$1_$2/g;
  my $count = 0;
  while(s/([a-z0-9])([A-Z])(?=[a-z0-9]|$)/$1_\l$2/){
    if($count++ > 20){
      die "can't un_camel_case $_";
    }
  }
  return $_;
}
sub gen_c_func {
    my ($name,$enums) = @_;
    my $cname = un_camel_case(lcfirst($name));
    my $guard_name = sprintf("__GL_%s_TO_STRING__",uc($cname));
    my $cases = join("\n", map { m/_(?:EXT|ARB|OES)/ ? () : (qq|    case($_):{\n      return("$_");\n    }|) }
                     @{$enums});
    my $func = join("\n", "#ifdef ${guard_name}",
                    "char * __attribute__((const)) gl_${cname}_to_string(GLenum val){",
                    "  switch(val){", $cases,
                    "    default:{\n      return \"\";\n    }\n  }\n}",
                    "#endif /* defined ${guard_name} */\n\n");
    return $func;
}
my $registry_file = shift;
die "file $registry_file does not exist" unless (-e $registry_file);
my $output_file = shift || "output.c";
open(my $fh,">",$output_file) || die "could not open $output_file for writing";

my $parser = new XML::Parser(Handlers => {Start => \&start_event,
                                          End => \&end_event});

$parser->parsefile($registry_file);
# This is slow, but who cares
say($fh "/* Possible groups to define:\n");
for my $group (keys($groups)){
    my $cname = un_camel_case(lcfirst($group));
    my $guard_name = sprintf("__GL_%s_TO_STRING__",uc($cname));
    say($fh "$guard_name (defines gl_${cname}_to_string)");
}
say($fh "*/");

# There are a couple ways to use this data to generate strings from glEnums.
# What we will do is create a C function of the form:
# const char* gl_${group_name}_to_string(GLenum val){...}

while(my($group, $values) = each($groups)){
    print($fh gen_c_func($group, $values));
}
