#!/usr/bin/env perl
use v5.16.0;
use File::Basename;
use autodie;
my $main = $ARGV[0];
my $basedir;
if($#ARGV >= 2){
    $basedir = $ARGV[1];
} else {
    chomp($basedir = `pwd`);
}
my $builddir = "$basedir/${main}_build";
`mkdir -p ${builddir}/${main}` && die;
`cp -r $basedir/src $basedir/glfw $basedir/m4\\
    $basedir/configure.ac $basedir/Makefile.am -t $builddir/${main}` && die;
`(cd $builddir/${main} && autoreconf -i)` && die;
`(cd $builddir && tar -cf ${main}.tar $main)` && die;
open(my $fh_in, "<", "$basedir/Makefile_tar");
open(my $fh_out, ">", "$builddir/Makefile");
readline($fh_in); #discard the first line (MAIN = )
print($fh_out "MAIN = ${main}\n");
while(readline($fh_in)){print($fh_out $_);}

