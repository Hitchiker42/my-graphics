#version 450
#extension GL_ARB_separate_shader_objects : enable
out vec4 v_color;
// Just draws a triangle
vec4 positions[3] = vec4[](
  vec4(0.0, -0.5, 0.0, 1.0),
  vec4(0.5, 0.5, 0.0, 1.0),
  vec4(-0.5, 0.5, 0.0, 1.0)
);
vec4 colors[3] = vec4[](
  vec4(1.0, 0.0, 0.0, 1.0),
  vec4(0.0, 1.0, 0.0, 1.0),
  vec4(0.0, 0.0, 1.0, 1.0)
);    
void main(){
  gl_Position = positions[gl_VertexIndex];
  v_color = colors[gl_VertexIndex];
}
