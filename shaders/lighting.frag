#version 330 core
#define num_lights 10
//layout(row_major) uniform;
layout(std140) uniform;
in vec4 v_color;
in vec4 v_normals;
in vec2 v_tex_coords;
flat in int use_lighting;
layout(location = 0) out vec4 f_color;
/*
  For our purposes vec3s don't exist, they're sort of a silly type to begin with
  since the majority of the time they end up taking up the same space as a vec4
*/
uniform transform {
  mat4 model;
  mat4 proj;
  mat4 view;
  mat4 scene;
};
struct Light {
  vec4 location;
  vec4 direction;
  vec4 diffuse;
  vec4 specular;
};
struct Material {
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  vec4 default_color;
  float shininess;
};
uniform u_material_block {
  Material u_material;
};
/*
  Lights can't be created dynamically, so we always have num_lights lights,
  lights_bitmask indicates if a light is on this will only work for upto 32 lights,
  but we have room for upto 96 if you really need them
*/
uniform u_light_block {
  vec4 u_ambient;
  vec4 eye_direction;
  uint num_lights_used;
  uint lights_bitmask;
  uint transform_flag;
  uint padding;
  Light u_lights[num_lights];
};
vec4 phong_lighting(in vec4 normal, in vec4 eye_direction,
                    in Light light, in Material material){
  float lambertian = clamp(dot(normal, light.direction), 0, 1);
//Why isn't the diffuse component in the if block
  vec4 specular = vec4(0);
  vec4 diffuse = lambertian * material.diffuse * light.diffuse;
//The specular component is more complicated, so this gives the option to
//just test the diffuse component
  if(use_lighting == 1){
    return diffuse;
  }
  if(lambertian > 0){
   
     //more expensive + more accurate
    //vec4 reflection = reflect(light.direction, normal);
    //float specular_coeff = dot(reflection, eye_direction);//, material.shininess);
    vec4 H = normalize(light.direction + eye_direction);
    float specular_coeff = clamp(pow(dot(normal, H), material.shininess),0,1);
    specular = specular_coeff * material.specular * light.specular;
    /*    if(any(lessThan(diffuse+specular, vec4(0)))){
      discard;
      }*/    
    return diffuse + specular; //diffuse + specular;  
  }
}
void main(){
  if(use_lighting != 0){
    uint i;
    vec4 f_light = vec4(0);
    for(i=0u;i<num_lights_used;i++){
      if(bool(lights_bitmask & (1u << i))){
        f_light += phong_lighting(v_normals, eye_direction, u_lights[i],
                                  u_material);
      }
    }
    vec4 ambient = u_ambient * u_material.ambient;
    f_light += ambient;
    /* If there was a texture, uniform Sampler2D u_texture
       vec3 tex_color = texture2D(u_texture, tex_coords).rgb;
       f_color.rgb = tex_color * f_light;
       f_color.a = v_color.a
    */
    f_color.rgb = v_color.rgb * f_light.rgb;
    f_color.a = v_color.a;
  } else {
    f_color = v_color;
  }
}
