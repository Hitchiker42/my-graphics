#version 330 core
layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normals;
layout(location = 2) in vec4 color;
layout(location = 3) in vec2 tex_coords;
//layout(row_major) uniform;
layout(std140) uniform;
out vec4 v_pos;
uniform transform {
  mat4 model;
  mat4 proj;
  mat4 view;
  mat4 scene;
};
void main(){
  gl_Position =  proj * view * scene * model * position;
  v_pos = gl_Position;
}
