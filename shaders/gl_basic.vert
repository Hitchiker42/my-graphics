#version 330 core
layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normals;
layout(location = 2) in vec4 color;
layout(location = 3) in vec2 tex_coords;
//layout(row_major) uniform;
layout(std140) uniform;
out vec4 v_color;
uniform transform {
  mat4 model;
  mat4 proj;
  mat4 view;
  mat4 scene;
};
uniform bool cpu_mat_mul = false;
void main(){
//normals[3] is a 2 bit flag, right now it's just used to indicate
//an object shouldn't be transformed
//  if(normals[3] != 0){
//    gl_Position = position;
//  } else {
  // uint tag = floatBitsToUint(normals[3]);
  // if(tag != 0u){
  //   gl_Position = position;
  // } else {
  
//adding a conditional slows this down a bunch, but oh well
  gl_Position =  proj * view * scene * model * position;
    //  }
    //  }
  v_color = color;
}
