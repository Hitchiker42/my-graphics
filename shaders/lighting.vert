#version 330 core
layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normals;
layout(location = 2) in vec4 color;
layout(location = 3) in vec2 tex_coords;
//layout(row_major) uniform;
layout(std140) uniform;
out vec4 v_color;
out vec4 v_normals;
out vec2 v_tex_coords;
//I could use v_normals[3] for this, but I already do enough confusing things
flat out int use_lighting;
uniform transform {
  mat4 model;
  mat4 proj;
  mat4 view;
  mat4 scene;
};
void main(){
//normals[3] is a 2 bit flag, right now it's just used to indicate
//an object shouldn't be transformed
  use_lighting = 2;
  uint tag = floatBitsToUint(normals[3]);
  if(tag != 0u){
    use_lighting = 0;//I'll add a conditional for this later
    gl_Position = position;
    v_normals = normals;
  } else {
    mat4 model_view = view * scene * model;
    mat4 normal_transform = mat4(transpose(inverse(mat3(model_view))));
    gl_Position = proj * model_view * position;
    v_normals = normal_transform * normals;
    //    gl_Position = position * model * proj_view;
    //    v_normals = normals * model * proj_view;
  }
  v_tex_coords = tex_coords;
  v_color = color;
}
