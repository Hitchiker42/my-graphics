#version 330 core
//this contains an xy position an a uv texuture coordinate packed together
layout(location = 0) in vec4 pos;
out vec2 tex_coord;
void main(){
  gl_Position = vec4(pos.x, pos.y, 0, 1);
  tex_coord = vec4(pos.z, pos.w);
}
