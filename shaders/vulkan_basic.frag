#version 450
#extension GL_ARB_separate_shader_objects : enable
in vec4 v_color;
layout(location = 0) out vec4 f_color;
void main(){
  f_color = v_color;
}
