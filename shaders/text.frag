#version 330 core
uniform vec4 ucolor;
uniform sampler2D tex;
in vec2 tex_coord;
layout(location = 0) out vec4 f_color;
void main(){
//DCDCCC is probably better then FFFFFF, fix this

//the font only has one color component, which is really just a binary
//switch, if it's on use white, if it's off use the background
  f_color = vec4(1,1,1, texture(tex, tex_coord).r) * ucolor;
}
