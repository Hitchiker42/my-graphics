#version 330 core
in vec4 v_pos;
layout(location = 0) out vec4 f_color;
/*
  We decide what cooridnate each color is represented by based
  on the signs of the position coordinates.
  1. +++ = rgb = 7
  2. --- = rgb = 0

  3. +-- = bgr = 1
  4. ++- = brg = 3
  5. +-+ = grb = 5
  6. -++ = gbr = 6
  7. -+- = rbg = 2
  8. --+ = rgb = 4
*/
void main(){
  int sector = int(dot(sign(v_pos),vec4(1,2,3,0)));
  vec4 color = vec4(abs(v_pos.xyz),1);
  switch(sector){
    case 0:
      f_color = v_pos.rgba; break;
    case 1:
      f_color = v_pos.bgra; break;
    case 2:
      f_color = v_pos.brga; break;
    case 3:
      f_color = v_pos.grba; break;
    case 4:
      f_color = v_pos.gbra; break;
    case 5:
      f_color = v_pos.rgba; break;
    case 6:
      f_color = v_pos.rbga; break;
    case 7:
      f_color = v_pos.bgra; break;
  }  
}
