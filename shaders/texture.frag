#version 330 core
in vec4 v_color;
in vec2 v_tex_coord;
uniform sampler2D tex;
out vec4 f_color;
void main(){
  if(v_tex_coord.x > v_tex_coord.y){
    f_color = texture(tex, v_tex_coord) * v_color;//v_tex_coord);
  } else {
    f_color = v_color;
  }
}
