#include "vulkan_pipeline.h"
#include "vulkan_context.h"
VkBool32 init_vulkan_pipeline(vulkan_pipeline *pl,
                              vulkan_context *ctx, vulkan_swap_chain *sc){
  pl->ctx = ctx;
  pl->swap_chain = sc;
  pl->command_buffers = calloc(sc->size, sizeof(VkCommandBuffer));
  pl->framebuffers = calloc(sc->size, sizeof(VkFramebuffer));
  return VK_TRUE;
}
vulkan_pipeline* allocate_vulkan_pipeline(vulkan_context *ctx, vulkan_swap_chain *sc){
  //We could just allocate one big chunk of memory for everything
  /*
    char *mem = calloc(1, sizeof(vulkan_pipeline) +
                          (sc->size * (sizeof(VkCommandBuffer)+ sizeof(VkFrameBuffer))));
    vulkan_pipeline *pl = mem;
    pl->command_buffers = mem + sizeof(vulkan_pipeline);
    pl->frame_buffers = (char*)pl->command_buffers + sc->size * sizeof(VkCommandBuffer);
  */
  vulkan_pipeline *pl = calloc(1, sizeof(vulkan_pipeline));
  if(init_vulkan_pipeline(pl, ctx, sc)){
    return pl;
  } else {
    free(pl);
    return NULL;
  }
}
void destroy_vulkan_pipeline(vulkan_pipeline *pl){
  vkDeviceWaitIdle(pl->ctx->device);
  uint32_t size = pl->swap_chain->size;
  for(uint32_t i = 0; i < size; i++){
    vkDestroyFramebuffer(pl->ctx->device, pl->framebuffers[i], NULL);
  }
  vkFreeCommandBuffers(pl->ctx->device, pl->ctx->command_pool, size, pl->command_buffers);
  vkDestroyPipeline(pl->ctx->device, pl->pipeline, NULL);
  vkDestroyPipelineLayout(pl->ctx->device, pl->layout, NULL);
  vkDestroyRenderPass(pl->ctx->device, pl->render_pass, NULL);
}
char* file_to_string(const char *filename, size_t *sz_ptr){
  char *ret = NULL;
  FILE* file = fopen(filename, "r");
  if(!file){ return NULL; }
  if(fseek(file, 0, SEEK_END) == -1){
    goto end;
  }
  long len = ftell(file);
  fseek(file, 0, SEEK_SET);
  ret = malloc(len + 1);
  fread(ret, 1, len, file);
  ret[len] = '\0';
  if(sz_ptr){ *sz_ptr = (size_t)len; }
 end:
  fclose(file);
  return ret;
}
VkShaderModule load_shader_from_buffer(vulkan_context *ctx,
                                       const char *buf, size_t sz){
  //Ensure code is aligned to a 32 bit boundry
  assert(((uintptr_t)buf & 0x7) == 0);
  VkShaderModuleCreateInfo create_info = {
    .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
    .codeSize = sz,
    .pCode = (const uint32_t*)buf
  };
  VkShaderModule ret;
  ctx->result = vkCreateShaderModule(ctx->device, &create_info, NULL, &ret);
  return ret;
}
VkShaderModule load_shader_from_file(vulkan_context *ctx,
                                     const char* filename){
  size_t sz;
  char* code = file_to_string(filename, &sz);
  VkShaderModule ret = load_shader_from_buffer(ctx, code, sz);
  free(code);
  return ret;
}
#if 0
static VkResult init_render_pass(vulkan_pipeline *pl){
  //Specifies the format(s) of the image(s) we're rendering. These are
  //used as input/output to the fragement shader.
  VkAttachmentDescription color_attachment_info = {
    .format = pl->swap_chain->format.format,
    .samples = VK_SAMPLE_COUNT_1_BIT,
    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
  };
  //Reference to above attachment for use in subpass descriptors
  VkAttachmentReference color_attachment_ref = {
    .attachment = 0,
    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
  };
  //Descripton of the different images/buffers used in the rendering subpass
  VkSubpassDescription subpass = {
    .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
    .colorAttachmentCount = 1,
    .pColorAttachments = &color_attachment_ref,
    .inputAttachmentCount = 0,
    .pInputAttachments = NULL,
    .pResolveAttachments = NULL,
    .pDepthStencilAttachment = NULL,
    //Attatchments which cannot be modified during this render pass 
    .preserveAttachmentCount = 0,
    .pPreserveAttachments = NULL
  };
  //Create a subpass dependency to force the render pass to wait for an image
  //to actually be available from the swap chain.
  VkSubpassDependency dependency = {
    .srcSubpass = VK_SUBPASS_EXTERNAL,
    .dstSubpass = 0,
    .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .srcAccessMask = 0,
    .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .dstAccessMask = (VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                      VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT)
  };

  VkRenderPassCreateInfo render_pass_info = {
    .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
    .attachmentCount = 1,
    .pAttachments = &color_attachment_info,
    .subpassCount = 1,
    .pSubpasses = &subpass,
    .dependencyCount = 1,
    .pDependencies = &dependency
  };
  pl->ctx->result = vkCreateRenderPass(pl->ctx->device, &render_pass_info,
                                       NULL, &pl->render_pass);
  return pl->ctx->result;
}

//Not 100% sure how this is going to look in the end
VkResult init_graphics_pipeline(vulkan_pipeline *pl){
  //These are wrappers that will automatically destroy
  //the shader module on function exit
  VkShaderModule vert_shader = load_shader_from_file(pl->ctx, pl->vert_shader_path);
  VkShaderModule frag_shader = load_shader_from_file(pl->ctx, pl->frag_shader_path);

  VkPipelineShaderStageCreateInfo shader_create_info[2] = {
      {.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
       .stage = VK_SHADER_STAGE_VERTEX_BIT,
       .module = vert_shader,
       .pName = "main"},
      {.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
       .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
       .module = frag_shader,
       .pName = "main"}
  };
  //I'll probably take the vertex bindings/attributes and primitive info
  //as arguments at some point
  VkPipelineVertexInputStateCreateInfo vertex_input_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    .vertexBindingDescriptionCount = 0,
    .pVertexBindingDescriptions = NULL,
    .vertexAttributeDescriptionCount = 0,
    .pVertexAttributeDescriptions = NULL
  };
  VkPipelineInputAssemblyStateCreateInfo primitive_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    .primitiveRestartEnable = VK_TRUE
  };
  VkViewport viewport = {
    .x = 0.0f, .y = 0.0f,
    .width = (float) pl->swap_chain->extent.width,
    .height = (float) pl->swap_chain->extent.height,
    .minDepth = 0.0f, .maxDepth = 1.0f
  };
  VkRect2D scissor = {{0,0}, pl->swap_chain->extent};
  VkPipelineViewportStateCreateInfo viewport_state_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
    .viewportCount = 1,
    .pViewports = &viewport,
    .scissorCount = 1,
    .pScissors = &scissor
  };
  /*
    Most of the fields set to false / 0 are mostly used for things like shadow mapping.
    polygonMode can also be set to VK_POLYGON_MODE_POINT or VK_POLYGON_MODE_LINE
    to draw only the vertices or edges of a polygon (i.e wireframe), requires
    the fillModeNonSolid device feature.
  */
  VkPipelineRasterizationStateCreateInfo rasterizer_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
    .depthClampEnable = VK_FALSE,
    .rasterizerDiscardEnable = VK_FALSE,
    .polygonMode = VK_POLYGON_MODE_FILL,
    .cullMode = VK_CULL_MODE_BACK_BIT,
    .frontFace = VK_FRONT_FACE_CLOCKWISE,
    .lineWidth = 1.0f,
    .depthBiasEnable = VK_FALSE,
    .depthBiasConstantFactor = 0.0f,
    .depthBiasClamp = 0.0f,
    .depthBiasSlopeFactor = 0.0f
  };

  VkPipelineMultisampleStateCreateInfo multisampling_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
    .sampleShadingEnable = VK_FALSE,
    .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
    .minSampleShading = 1.0f,
    .pSampleMask = NULL,
    .alphaToCoverageEnable = VK_FALSE,
    .alphaToOneEnable = VK_FALSE
  };

  //To be used later
  //VkPipelineDepthStencilStateCreateInfo depth_stencil_info = {};

  //Enable alpha blending
#define VK_COLOR_COMPONENT_RGBA_BITS                            \
  (VK_COLOR_COMPONENT_A_BIT | VK_COLOR_COMPONENT_R_BIT |        \
   VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT)
  VkPipelineColorBlendAttachmentState fb_color_blend_info = {
    .colorWriteMask = VK_COLOR_COMPONENT_RGBA_BITS,
    .blendEnable = VK_TRUE,
    .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
    .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
    .colorBlendOp = VK_BLEND_OP_ADD,
    .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
    .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
    .alphaBlendOp = VK_BLEND_OP_ADD
  };

  VkPipelineColorBlendStateCreateInfo global_color_blend_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
    .logicOpEnable = VK_FALSE,
    .logicOp = VK_LOGIC_OP_COPY /* unused */,
    .attachmentCount = 1,
    .pAttachments = &fb_color_blend_info,
    .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f}
  };
  //Specifies components of the pipeline we can change at runtime.
  //VkPipelineDynamicStateCreateInfo dynamicState = {};

  //Will likely get moved once we start using uniforms and such
  VkPipelineLayoutCreateInfo pipeline_layout_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
    .setLayoutCount = 0,
    .pSetLayouts = NULL,
    .pushConstantRangeCount = 0,
    .pPushConstantRanges = NULL
  };
  pl->ctx->result = vkCreatePipelineLayout(pl->ctx->device, &pipeline_layout_info,
                                           NULL, &pl->pipeline_layout);
  if(pl->ctx->result != VK_SUCCESS){
    return pl->ctx->result;
  }
  /*
    Most of this is self explanatory, but the basePipelineHandle/Index are
    an optimization for use in creating a pipeline that is derived from
    an existing pipeline, which we don't have here.
  */
  VkGraphicsPipelineCreateInfo pipeline_info = {
    .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
    .stageCount = 2 /* vertex & fragment*/,
    .pStages = shader_create_info,
    .pVertexInputState = &vertex_input_info,
    .pInputAssemblyState = &primitive_info,
    .pViewportState = &viewport_state_info,
    .pRasterizationState = &rasterizer_info,
    .pMultisampleState = &multisampling_info,
    .pDepthStencilState = NULL,
    .pColorBlendState = &global_color_blend_info,
    .pDynamicState = NULL,
    .layout = pl->pipeline_layout,
    .renderPass = pl->render_pass,
    .subpass = 0,
    .basePipelineHandle = VK_NULL_HANDLE,
    .basePipelineIndex = -1
  };
  //Since you often want to create multiple graphics pipelines the create function
  //is designed to take arrays rather than single pointers. The second argument
  //(which we pass a null handle to) is for a VkPipelineCache object which is used
  //to speedup creation of similar pipelines.
  pl->ctx->result = vkCreateGraphicsPipelines(pl->ctx->device, VK_NULL_HANDLE,
                                              1, &pipeline_info, NULL,  &pl->pipeline);
  return pl->ctx->result;
}
VkResult init_framebuffers(vulkan_pipeline *pl){
  vulkan_swap_chain *sc = pl->swap_chain;
  pl->framebuffers = malloc(sizeof(VkFramebuffer) * sc->size);
  VkFramebufferCreateInfo fb_info = {
    .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
    .renderPass = pl->render_pass,
    .attachmentCount = 1,
    .pAttachments = NULL /*set per framebuffer*/,
    .width = sc->extent.width,
    .height = sc->extent.height,
    .layers = 1
  };
  for(uint32_t i = 0; i < sc->size; i++){
    fb_info.pAttachments = &sc->image_views[i];
    pl->ctx->result = vkCreateFramebuffer(pl->ctx->device, &fb_info,
                                          NULL, &pl->framebuffers[i]);
    if(pl->ctx->result != VK_SUCCESS){
      break;
    }
  }
  return pl->ctx->result;
}
VkResult init_command_buffers(vulkan_pipeline *pl){
  vulkan_swap_chain *sc = pl->swap_chain;
  VkCommandBufferAllocateInfo alloc_info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
    .commandPool = pl->ctx->command_pool,
    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
    .commandBufferCount = sc->size
  };
  pl->ctx->result = vkAllocateCommandBuffers(pl->ctx->device, &alloc_info,
                                             pl->command_buffers);
  return pl->ctx->result;
}
{
  if(ctx->result != VK_SUCCESS){
    return ctx->result;
  }
  VkCommandBufferBeginInfo begin_info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
    .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
    .pInheritanceInfo = NULL
  };
  //We need to pass a pointer to this so we can't just put it in INIT_STRUCT directly
  VkClearValue clear_value = { 0.0f, 0.0f, 0.0f, 1.0f };
  VkRenderPassBeginInfo render_pass_info = {
    .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
    .renderPass = pl->render_pass,
    .framebuffer = NULL,
    .renderArea.offset = {0, 0},
    .renderArea.extent = pl->extent,
    .clearValueCount = 1,
    .pClearValues = &clear_value
  };
  for(size_t i = 0; i < pl->swap_chain_size; i++){
    pl->ctx->result = vkBeginCommandBuffer(pl->command_buffers[i], &begin_info);
    if(ctx->result != VK_SUCCESS){ goto end; }

    //All of the vkCmd functions return void so no need for error checking.
    render_pass_info.framebuffer = pl->framebuffers[i];
    vkCmdBeginRenderPass(pl->command_buffers[i], &render_pass_info,
                         VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(pl->command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      pl->pipeline);
    //vkCmdDraw(buffer, u32 vertexCount, u32 instanceCount,
    //                  u32 firstVertex, u32 firstInstance)
    vkCmdDraw(ctx->command_buffers[i], 3, 1, 0, 0);
    vkCmdEndRenderPass(ctx->command_buffers[i]);
    ctx->result = vkEndCommandBuffer(ctx->command_buffers[i]);
    if(ctx->result != VK_SUCCESS){ goto end; }
  }
 end:
  return (ctx->result == VK_SUCCESS);
}
VkResult vulkan_info::draw_frame(){
  //Wait for the last frame to finish being displayed
  size_t cur_frame = ctx->current_frame;
  vkWaitForFences(ctx->device, 1, &ctx->fences[cur_frame],
                  VK_TRUE, UINT64_MAX);
  vkResetFences(ctx->device, 1, &ctx->fences[cur_frame]);
  uint32_t image_index;
  //Grab the next image from the swap chain. Unfortunately there is no guarantee
  //on the order images are returned relative to when they are presented.
  //Third argument is a timeout in nanoseconds, could be used to drop frames
  //for perfermance reasons if needed.
  ctx->result = vkAcquireNextImageKHR(ctx->device, ctx->swap_chain.handle,
                                     UINT64_MAX, ctx->semaphores[cur_frame * 2],
                                     VK_NULL_HANDLE, &image_index);
  //Submit request to grahpics card
  VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  VkSubmitInfo submit_info = {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .waitSemaphoreCount = 1,
    .pWaitSemaphores = &ctx->semaphores[cur_frame*2],
    .pWaitDstStageMask = wait_stages,
    .commandBufferCount = 1,
    .pCommandBuffers = &ctx->command_buffers[image_index],
    .signalSemaphoreCount = 1,
    .pSignalSemaphores = &ctx->semaphores[cur_frame*2 + 1]
  };
  ctx->result = vkQueueSubmit(ctx->graphics_queue, 1, &submit_info,
                               ctx->fences[cur_frame]);
  if(ctx->result != VK_SUCCESS){ return false; }

  //Present the image to the screen
  VkPresentInfoKHR present_info = {
    .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
    .waitSemaphoreCount = 1,
    .pWaitSemaphores = &semaphores[cur_frame*2 + 1]
  };
  ctx->result = vkQueuePresentKHR(present_queue, &present_info);
  //Since max_frames_queued is a constant this should get optimized
  ctx->current_frame = (cur_frame + 1) % ctx->max_frames_queued;
  return (ctx->result == VK_SUCCESS);
}
//Stuff below should be part of a seperate structure.
VkResult init_command_pool(vulkan_pipeline *pl){
  VkCommandPoolCreateInfo create_info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
    .queueFamilyIndex = pl->ctx->graphics_queue_index,
    .flags = 0
  };
  pl->ctx->result = vkCreateCommandPool(pl->ctx->device, &create_info,
                                        NULL, &pl->ctx->command_pool);
  return pl->ctx->result;
}
#endif
