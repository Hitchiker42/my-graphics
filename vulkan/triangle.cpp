#include "vulkan_util.hpp"
static constexpr int default_width = 1280;
static constexpr int default_height = 720;
bool handle_event(SDL_Event *evt, vulkan_info *info){
  switch(evt->type){
    case(SDL_QUIT):
      return true;
    default:
      return false;
  }
}
int main(int argc, char *argv[]){
  SDL_Window *win;
  if(SDL_Init(SDL_INIT_VIDEO) < 0){
    fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
  win = SDL_CreateWindow("Vulkan 3D Model Viewer",
                         SDL_WINDOWPOS_UNDEFINED,
                         SDL_WINDOWPOS_UNDEFINED,
                         default_width, default_height,
                         SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
  if(!win){
    fprintf(stderr, "Couldn't open SDL window.\n");
    exit(1);
  }
  vulkan_info info(win, nullptr);
  if(info.result != VK_SUCCESS){
    fprintf(stderr, "Error initializing Vulkan\n");
    exit(1);
  }
  SDL_Event evt;
  while(1){
    while(SDL_PollEvent(&evt)){
      if(handle_event(&evt, &info)){
        goto end;
      }
    }
  }
 end:
  SDL_DestroyWindow(win);
#ifdef DEBUG
  return 0;
#else
  exit(0);
#endif
}
