#ifndef __GLM_H__
#define __GLM_H__
/*
  Header file to include all needed glm headers, should have less overhead than 
  glm/glm.hpp and can be precompiled since it won't need to be modified often.
*/
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/geometry.hpp>
#if (GLM_CONFIG_ANONYMOUS_STRUCT != GLM_ENABLE)
#warning "Warning glm anyonyous structs not available"
#endif
#endif /* __GLM_H__ */
