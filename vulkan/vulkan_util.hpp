#ifndef __VULKAN_UTIL_HPP__
#define __VULKAN_UTIL_HPP__
#include "common.h"
#if 0
#include "glm.h"
//I want to move this to a seperate file, but that will involve making a bunch
//of paramaters of the graphics pipline modifiable, which is a bunch of work
//I don't want to do until I can at least get some basic objects to render.
struct vertex {
  glm::vec4 pos;
  glm::vec4 norm;
  union {
    struct {
      uint8_t u;
      uint8_t v;
    } tex_coords;
    union {
      uint32_t argb;
      struct {
        uint8_t b;
        uint8_t g;
        uint8_t r;
        uint8_t a;
      }
    } color;
  };
  static constexpr VkVertexInputBindingDescription binding = {
    0, sizeof(vertex), VK_VERTEX_INPUT_RATE_VERTEX
  };
  static constexpr VkVertexInputAttributeDescription attributes[] = {
    { 0, binding.binding, VK_FORMAT_R32G32B32A32_SFLOAT, 0 },
    { 1, binding.binding, VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(vertex, norm) },
    { 2, binding.binding, VK_FORMAT_B8G8R8A8_UINT_PACK32, offsetof(vertex, color) }
  };
  static constexpr VkPipelineVertexInputStateCreateInfo vi_info = {
    VK_STRUCTURE_TYPE_PIPELINE_INPUT_STATE_CREATE_INFO,
    nullptr, 0, 1, &binding, 3, &attributes
  };
};
struct mesh {
  std::vector<vertex> vertices;
  std::vector<int> indices;
};
struct drawable {
  vulkan_info *info;
  mesh vertex_data;//Will likely change to a shared pointer at some point
  VkBuffer vbo;
  VkDeviceMemory vbo_memory;
};
#endif
inline constexpr VkFlags VK_COLOR_COMPONENT_RGBA_BITS =
  (VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
   VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);
//Specify calling conventions for vulkan callbacks, this is only needed
//because windows is stupid and can't just have one sane calling convention.
#define VK_DECL(rettype) VKAPI_ATTR rettype VKAPI_CALL
VK_DECL(VkBool32)
debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
               VkDebugUtilsMessageTypeFlagsEXT message_type,
               const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
               void* userdata);
const char* vk_result_to_string(VkResult res);
struct vulkan_info {
  //Static constants
  static constexpr size_t max_frames_queued = 3;
  static constexpr const char *default_vert_shader_path = "vert.spv";
  static constexpr const char *default_frag_shader_path = "frag.spv";

  SDL_Window *win = nullptr; //window that created this instance
                             //according to SDL this will eventually be unnecessary.
  PFN_vkGetInstanceProcAddr get_instance_proc_addr_ptr = nullptr;
  //Utilizes cpp stringification and vulkan creating and naming types for pointers
  //to each function.
#define call_instance_proc(vkinfo, name, ...)                            \
  vkinfo->call_instance_proc_fn<CAT(PFN_, name)>(#name MAYBE_VA_ARGS(__VA_ARGS__))
#define call_instance_proc_this(name, ...)                              \
  this->call_instance_proc_fn<CAT(PFN_, name)>(#name MAYBE_VA_ARGS(__VA_ARGS__))

  //It's important to initalize all handles to NULL so we can call
  //their destructors even if we never initialize them.
  mutable VkResult result = VK_SUCCESS; //Result of last vulkan call using this structure.
  VkInstance instance = VK_NULL_HANDLE;
  VkSurfaceKHR surface = VK_NULL_HANDLE; //We get this from SDL
  VkPhysicalDevice physical_device = VK_NULL_HANDLE;
  VkDevice device = VK_NULL_HANDLE; //logical device
  //IDEA: combine these with the queue_family indices to make a single struct
  //to deal with queues (this depends on how complicated queue management will get).
  VkQueue graphics_queue = VK_NULL_HANDLE;
  VkQueue present_queue = VK_NULL_HANDLE;
  //TODO: combine these with the swap chain into a seperate graphics pipeline object
  VkRenderPass render_pass = VK_NULL_HANDLE;
  VkPipelineLayout pipeline_layout = VK_NULL_HANDLE;//layout of data used in shaders.
  VkPipeline pipeline = VK_NULL_HANDLE;
  VkBuffer vbo = VK_NULL_HANDLE;
  size_t current_frame = 0;
  std::array<VkSemaphore, max_frames_queued*2> semaphores = {{}};
  std::array<VkFence, max_frames_queued> fences = {{}};
#if (defined DEBUG) || !(defined NDEBUG)
  VkDebugUtilsMessengerEXT debug_messenger = VK_NULL_HANDLE;
#endif
  struct queue_family_indices {
    int graphics_index = -1;
    int present_index = -1;
    static constexpr size_t count() { return 2; }
  };
  queue_family_indices queue_indices;
  struct swap_chain_info {
    VkSwapchainKHR handle = VK_NULL_HANDLE;
    std::vector<VkImage> images;
    std::vector<VkImageView> image_views;
    VkExtent2D extent;
    VkSurfaceCapabilitiesKHR capabilities;
    VkSurfaceFormatKHR surface_format;
    VkPresentModeKHR present_mode;
    VkFormat format() const {
      return surface_format.format;
    }
    size_t size() const {
      return images.size();
    }
    void cleanup(VkDevice device){
      for(size_t i = 0; i < size(); i++){
        vkDestroyImageView(device, image_views[i], nullptr);
      }
      vkDestroySwapchainKHR(device, handle, nullptr);
    }
  };
  swap_chain_info swap_chain;
  //I may move these into the swap chain struct
  std::vector<VkFramebuffer> framebuffers;
  VkCommandPool command_pool;
  std::vector<VkCommandBuffer> command_buffers;

  const char *vert_shader_path = default_vert_shader_path;
  const char *frag_shader_path = default_frag_shader_path;
  /*
    The constructor just creates the instance and gets a pointer to
    the vkGetInstaceProcAddr function, all other initalization is done
    in actual functions (with a general init function provided for convenience)
  */
  bool init_sdl_instance(std::vector<const char*> &extensions,
                         VkApplicationInfo *appinfo);
  vulkan_info(SDL_Window *win, VkApplicationInfo *appinfo = nullptr)
    : win{win} {
    std::vector<const char*> extensions;
    init_sdl_instance(extensions, appinfo);
  }
  vulkan_info(SDL_Window *win, std::vector<const char*> &extensions,
              VkApplicationInfo *appinfo = nullptr)
    : win{win} {
    if(!init_sdl_instance(extensions, appinfo)){
      fprintf(stderr, "Failed to initailize vulkan: %s.\n", result_str());
    }
  }
  //Cleanup the pipeline, this may be called multiple times
  //This doesn't destroy the swap_chain since we can reuse that
  //when creating a new one, and init_swap_chain will cleaup the old one.
  void cleanup_pipeline(){ //may rename to cleanup swap_chain
    vkDeviceWaitIdle(device);
    for(auto &fb : framebuffers){
      vkDestroyFramebuffer(device, fb, nullptr);
    }
    vkFreeCommandBuffers(device, command_pool, (uint32_t)command_buffers.size(),
                         command_buffers.data());
    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipeline_layout, nullptr);
    vkDestroyRenderPass(device, render_pass, nullptr);
  }

  void cleanup(){
    vkDeviceWaitIdle(device);
    for(size_t i = 0; i < max_frames_queued; i++){
      vkDestroySemaphore(device, semaphores[2*i], nullptr);
      vkDestroySemaphore(device, semaphores[2*i + 1], nullptr);
      vkDestroyFence(device, fences[i], nullptr);
    }
    vkDestroyCommandPool(device, command_pool, nullptr);
    for(auto &fb : framebuffers){
      vkDestroyFramebuffer(device, fb, nullptr);
    }
    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipeline_layout, nullptr);
    vkDestroyRenderPass(device, render_pass, nullptr);
    swap_chain.cleanup(device);
    vkDestroySurfaceKHR(instance, surface, nullptr);
    vkDestroyDevice(device, nullptr);
#if (defined DEBUG) || !(defined NDEBUG)
    call_instance_proc(this, vkDestroyDebugUtilsMessengerEXT,
                       instance, debug_messenger, nullptr);
#endif
    vkDestroyInstance(instance, nullptr);
  }
  //Init functions
  bool init(const char *vert_shader_path = default_vert_shader_path,
            const char *frag_shader_path = default_frag_shader_path){
    this->vert_shader_path = vert_shader_path;
    this->frag_shader_path = frag_shader_path;
    return (init_debug_messenger() &&
            init_device() &&
            init_command_pool() &&
            init_pipeline() &&
            init_sync_objects());
  }
  bool init_pipeline(){
    return (init_swap_chain() &&
            init_render_pass() &&
            init_graphics_pipeline() &&
            init_framebuffers() &&
            init_command_buffers());    
  }
  bool init_debug_messenger(); //Does nothing if NDEBUG is defined
  bool init_physical_device();
  bool init_logical_device();
  bool init_swap_chain();
  bool init_render_pass();
  bool init_graphics_pipeline();
  bool init_framebuffers();
  bool init_command_pool();
  bool init_command_buffers();
  bool init_sync_objects(){
    for(size_t i = 0; i < max_frames_queued; i++){
      if(!(init_semaphore(&semaphores[2*i]) &&
           init_semaphore(&semaphores[2*i + 1]) && init_fence(&fences[i]))){
        return false;
      }
    }
    return true;
  }
  bool init_device(){
    return (init_physical_device() && init_logical_device());
  }
  bool init_semaphore(VkSemaphore *sem) const {
    static constexpr VkSemaphoreCreateInfo create_info =
      {VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0};
    this->result = vkCreateSemaphore(this->device, &create_info, nullptr, sem);
    return (this->result == VK_SUCCESS);
  }
  bool init_fence(VkFence *fence, bool signaled = true) const {
    static VkFenceCreateInfo create_info =
      {VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr, 0};
    create_info.flags = (signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0);
    this->result = vkCreateFence(this->device, &create_info, nullptr, fence);
    return (this->result == VK_SUCCESS);
  }
  //Regular member functions
  bool draw_frame();
  bool check_swap_chain(VkResult res){
    if(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR){
      return recreate_swap_chain();
    } else {
      return (res == VK_SUCCESS);
    }
  }
  bool check_swap_chain(){
    this->result = vkGetSwapchainStatusKHR(this->device, this->swap_chain.handle);
    return check_swap_chain(this->result);
  }
  bool recreate_swap_chain(){
    cleanup_pipeline();
    return init_pipeline();
  }
  PFN_vkVoidFunction get_instance_proc_addr(const char *name){
    return get_instance_proc_addr_ptr(instance, name);
  }
  const char* result_str() const {
    return vk_result_to_string(result);
  }
  template<typename T, typename ... Ts,
           std::enable_if_t<std::is_void_v<std::invoke_result_t<T, Ts...>>, int> = 0>
  bool call_instance_proc_fn(const char *name, Ts&&... Args){
    T pfn = (T)(get_instance_proc_addr_ptr(instance, name));
    if(!pfn){
      return false;
    } else {
      pfn(std::forward<Ts>(Args)...);
      return true;
    }
  }
  template<typename T, typename ... Ts,
           std::enable_if_t<std::is_same_v<VkResult,
                                           std::invoke_result_t<T, Ts...>>, int> = 0>
  bool call_instance_proc_fn(const char *name, Ts&&... Args){
    T pfn = (T)(get_instance_proc_addr_ptr(instance, name));
    if(!pfn){
      this->result = VK_ERROR_EXTENSION_NOT_PRESENT;
    } else {
      this->result = pfn(std::forward<Ts>(Args)...);
    }
    return (this->result == VK_SUCCESS);
  }
  //informational functions
  void print_extensions();
};
#endif /* __VULKAN_UTIL_HPP__ */
