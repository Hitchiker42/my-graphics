#ifndef __VULKAN_UTIL_H__
#define __VULKAN_UTIL_H__
#include "common.h"
#include "vulkan_context.h"
#include "vulkan_pipeline.h"
struct vulkan_info {
  vulkan_context ctx;
  vulkan_swap_chain swap_chain;
  vulkan_pipeline pipeline;
  VkResult& result(){
    return ctx.result;
  }
  const VkResult& result() const{
    return ctx.result;
  }
  bool check_result() const {
    return (result() == VK_SUCCESS);
  }
  static vulkan_info* create_vulkan_info(SDL_Window *win,
                                         VkApplicationInfo *app_info = nullptr){
    return create_vulkan_info(win, nullptr, 0, app_info);
  }
  static vulkan_info* create_vulkan_info(SDL_Window *win, const char **extensions,
                                         size_t n_extensions,
                                         VkApplicationInfo *app_info = nullptr){
    vulkan_info *info = (vulkan_info*)malloc(sizeof(vulkan_info));
    if(init_vulkan_context(&info->ctx, win,
                           extensions, n_extensions, app_info) != VK_SUCCESS){
      goto error;
    }
    if(!reset_swap_chain(&info->swap_chain)){
      goto error;
    }
    if(!init_vulkan_pipeline(&info->pipeline, &info->ctx,
                             &info->swap_chain)){
      goto error;
    }
    return info;
  error:
    destroy_vulkan_info(info);
    free(info);
    return nullptr;
  }
  static void destroy_vulkan_info(vulkan_info* info){
    destroy_swap_chain(&info->swap_chain);
    destroy_vulkan_pipeline(&info->pipeline);
    destroy_vulkan_context(&info->ctx);
  }
  vulkan_info() = delete;
};
#endif /* __VULKAN_UTIL_H__ */

/* Local Variables: */
/* mode: c++ */
/* End: */
