#include "vulkan_util.hpp"
struct swap_chain_details {
  VkSurfaceCapabilitiesKHR capabilities;
  std::vector<VkSurfaceFormatKHR> formats;
  std::vector<VkPresentModeKHR> present_modes;
};
static bool check_for_instance_layers(const char** layers, size_t layer_count);
static bool check_for_device_layers(VkDevice device,
                                    const char** layers, size_t layer_count);
bool vulkan_info::init_sdl_instance(std::vector<const char*> &extensions,
                                    VkApplicationInfo *appinfo){
  uint32_t count;
  size_t initial_extensions = extensions.size();
  bool err = SDL_Vulkan_GetInstanceExtensions(this->win, &count, nullptr);
  if(err){
    this->result = VK_ERROR_INITIALIZATION_FAILED;
    return false;
  }
  extensions.resize(initial_extensions + count);
  err = SDL_Vulkan_GetInstanceExtensions(this->win, &count,
                                         extensions.data() + initial_extensions);
  if(err){
    this->result = VK_ERROR_INITIALIZATION_FAILED;
    return false;
  }
  /*
    Enable validation layers when building in debug mode. I may make layers
    an additional argument at some point.
  */
#ifdef DEBUG
  const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
  size_t layer_count = 1;
  if(!check_for_instance_layers(layers, layer_count)){
    this->result = VK_ERROR_LAYER_NOT_PRESENT;
    return false;
  }
  extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME); // enable debug messages
#else
  const char **layers = nullptr;
  size_t layer_count = 0;
#endif
  VkInstanceCreateInfo create_info = {};

  create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  create_info.pApplicationInfo = appinfo;
  create_info.enabledLayerCount = static_cast<uint32_t>(layer_count);
  create_info.ppEnabledLayerNames = layers;
  create_info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
  create_info.ppEnabledExtensionNames = extensions.data();;

  this->result = vkCreateInstance(&create_info, nullptr, &this->instance);
  if(result == VK_SUCCESS){
    this->get_instance_proc_addr_ptr =
      (PFN_vkGetInstanceProcAddr)SDL_Vulkan_GetVkGetInstanceProcAddr();
    if(!SDL_Vulkan_CreateSurface(this->win, this->instance, &this->surface)){
        //mayber use VK_ERROR_SURFACE_LOST_KHR?
      this->result = VK_ERROR_INITIALIZATION_FAILED;
    }
  }
  return (this->result != VK_SUCCESS);
}
/*
  Layers built into the vulkan SDK:
  Layer Name                            Layer Type
  VK_LAYER_LUNARG_api_dump              utility
    print API calls and their parameters and values
  VK_LAYER_LUNARG_assistant_layer       utility
    highlight potential application issues that are not specifically prohibited
    by the Vulkan spec, but which can still create problems
  VK_LAYER_LUNARG_core_validation       validation
    print and validate the descriptor set, pipeline state, and dynamic state;
    validate the interfaces between SPIR-V modules and the graphics pipeline;
    track and validate GPU memory and its binding to objects and command
    buffers; validate texture formats and render target formats
  VK_LAYER_LUNARG_device_simulation     utility
    allows modification of an actual device's reported features,
    limits, and capabilities
  VK_LAYER_LUNARG_monitor               utility
    outputs the frames-per-second of the target application
    in the applications title bar
  VK_LAYER_LUNARG_object_tracker        validation
    track all Vulkan objects and flag invalid objects and object memory leaks
  VK_LAYER_LUNARG_parameter_validation  validation
    validate API parameter values
  VK_LAYER_LUNARG_screenshot            utility
    outputs specified frames to an image file as they are presented
  VK_LAYER_GOOGLE_threading             validation
    check validity of multi-threaded API usage
  VK_LAYER_GOOGLE_unique_objects        utility
    wrap all Vulkan objects in a unique pointer at create time and
    unwrap them at use time

  In addition the meta-layer VK_LAYER_LUNARG_standard_validation is provided which
    currently acts the same as the following layers in order:
     VK_LAYER_GOOGLE_threading
     VK_LAYER_LUNARG_parameter_validation
     VK_LAYER_LUNARG_object_tracker
     VK_LAYER_LUNARG_core_validation
     VK_LAYER_GOOGLE_unique_objects
  It's generally best to just specify this for validation purposes.

*/
/*
  This only works for types which contain a char* or char[] as their first member.
  really it only needs to work for VkExtensionProperties and VkLayerProperties.
*/
template<typename T>
static bool check_for_properties(const char** names, size_t names_count,
                                 T *available, size_t available_count){
  auto strcmp_pred = [](const char *a, const char *b){ return strcmp(a,b) < 0; };
  auto layer_pred = [](const T& a, const T& b){
    return strcmp(reinterpret_cast<const char*>(&a),
                  reinterpret_cast<const char*>(&b)) < 0;
  };
  //It's probably more efficent to just do the O(N^2) search, but I dunno,
  //I just want to avoid that.
  std::sort(available, available + available_count, layer_pred);
  std::sort(names, names + names_count, strcmp_pred);
  size_t i = 0, j = 0;
  while(j < available_count && i < names_count){
    int result = strcmp(names[i],
                        reinterpret_cast<char*>(&available[j]));
    if(result == 0){
      i++;
    } else if(result < 0){
      return false;
    }
    j++;
  }
  return (i == names_count);
}
static bool check_for_layers(const char** names, size_t names_count,
                             VkLayerProperties *available, size_t available_count){
  return check_for_properties(names, names_count, available, available_count);
}
static bool check_for_extensions(const char** names, size_t names_count,
                                 VkExtensionProperties *available,
                                 size_t available_count){
  return check_for_properties(names, names_count, available, available_count);
}
static bool check_for_instance_layers(const char** layers, size_t layer_count){
  uint32_t count;
  vkEnumerateInstanceLayerProperties(&count, nullptr);
  std::vector<VkLayerProperties> available(count);
  vkEnumerateInstanceLayerProperties(&count, available.data());
  return check_for_layers(layers, layer_count, available.data(), available.size());
}
static bool check_for_device_layers(VkPhysicalDevice device,
                                    const char** layers, size_t layer_count){
  uint32_t count;
  vkEnumerateDeviceLayerProperties(device,  &count, nullptr);
  std::vector<VkLayerProperties> available(count);
  vkEnumerateDeviceLayerProperties(device, &count, available.data());
  return check_for_layers(layers, layer_count, available.data(), available.size());
}
static bool check_for_instance_extensions(const char** extns, size_t extn_count){
  uint32_t count;
  vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);
  std::vector<VkExtensionProperties> available(count);
  vkEnumerateInstanceExtensionProperties(nullptr, &count, available.data());
  return check_for_extensions(extns, extn_count, available.data(), available.size());
}
static bool check_for_device_extensions(VkPhysicalDevice device,
                                        const char** extns, size_t extn_count){
  uint32_t count;
  vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);
  std::vector<VkExtensionProperties> available(count);
  vkEnumerateDeviceExtensionProperties(device, nullptr, &count, available.data());
  return check_for_extensions(extns, extn_count, available.data(), available.size());
}
using queue_indices = vulkan_info::queue_family_indices;
  //Called if some indices are found but not all and we need to test
  //a different device
static void reset_queue_indices(queue_indices* idxs){
  idxs->graphics_index = -1;
  idxs->present_index = -1;
}
static bool queue_indices_complete(queue_indices* idxs){
  return (idxs->graphics_index >= 0 && idxs->present_index >= 0);
}
static bool check_queue_family(vulkan_info *info, queue_indices *idxs,
                               VkPhysicalDevice device,
                               const VkQueueFamilyProperties& queue_family, int idx){
  if(queue_family.queueCount > 0){
    if(queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT){
      idxs->graphics_index = idx;
    }
    VkBool32 present_support = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(device, idx, info->surface, &present_support);
    if(present_support){
      idxs->present_index = idx;
    }
  }
  return queue_indices_complete(idxs);
}
static bool check_device_queues(vulkan_info *info, queue_indices *idxs,
                                VkPhysicalDevice device){
  uint32_t count = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
  std::vector<VkQueueFamilyProperties> queue_families(count);
  vkGetPhysicalDeviceQueueFamilyProperties(device, &count, queue_families.data());
  for(uint32_t i = 0; i < count; i++){
    if(check_queue_family(info, idxs, device, queue_families[i], i)){
      return true;
    }
  }
  return false;
}
static bool check_swap_chain(vulkan_info *info, VkPhysicalDevice device){
  uint32_t format_count = 0, present_mode_count = 0;
  vkGetPhysicalDeviceSurfaceFormatsKHR(device, info->surface, &format_count, nullptr);
  vkGetPhysicalDeviceSurfacePresentModesKHR(device, info->surface, &format_count, nullptr);
  return (format_count > 0 && present_mode_count > 0);
}
static bool check_device_features(VkPhysicalDevice device){
  /*
    Some potentially useful features:
    geometryShader, tessellationShader: enable support for these shader types,
    fillModeNonSolid: Allow point and wireframe polygon rendering
    wideLines, largePoints: Allow point size / line width values other than 1.0
    sampleRateShading: Allow multisampling
   */
  VkPhysicalDeviceFeatures device_features;
  vkGetPhysicalDeviceFeatures(device, &device_features);
  //For now only require multisampling.
  return (device_features.sampleRateShading);
}
static constexpr size_t Num_Device_Extensions = 1;
//Can't be const due to the fact I need to sort it.
static std::array<const char*, Num_Device_Extensions> Device_Extensions = {{
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
}};
//Returns true if the given device is suitable for our purposes
//Could also return a score to sort possible devices.
static int is_device_suitable(vulkan_info *info, queue_indices *idxs,
                              VkPhysicalDevice device){
#if 0
  //Get properties and features
  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceProperties(device, &device_properties);
  VkPhysicalDeviceFeatures device_features;
  vkGetPhysicalDeviceFeatures(device, &device_features);
#endif
  //Check that the device supports all the queue_families we want.
  reset_queue_indices(idxs);
  if(!check_device_queues(info, idxs, device)){
    return false;
  }
  if(!check_for_device_extensions(device, Device_Extensions.data(),
                                  Device_Extensions.size())){
    return false;
  }
  if(!check_device_features(device)){
    return false;
  }
  if(!check_swap_chain(info, device)){
    return false;
  }
#ifdef DEBUG
  const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
  size_t layer_count = 1;
  if(!check_for_device_layers(device, layers, layer_count)){
    return false;
  }
#endif
  return true;
#if 0
  //Kinda pointless but at least it gives me something to score.
  if(idxs->graphics_index == idxs->present_index){
    score = 2;
  } else {
    score = 1;
  }
  //More checks here
  return score;
#endif
}
bool vulkan_info::init_physical_device(){
  uint32_t count = 0;
  vkEnumeratePhysicalDevices(this->instance, &count, nullptr);
  if(count == 0){
    return false;
  }
  std::vector<VkPhysicalDevice> devices(count);
  vkEnumeratePhysicalDevices(this->instance, &count, devices.data());
  for(uint32_t i = 0; i < count; i++){
    if(is_device_suitable(this, &this->queue_indices, devices[i])){
      return true;
    }
  }
  return false;
#if 0
  int best = 0;
  queue_indices idxs;
  for(uint32_t i = 0; i < count; i++){
    int score = is_device_suitable(this, &idxs, devices[i]);
    if(score > best){
      best = score;
      this->physical_device = devices[i];
      this->queue_indices = idxs;
    }
  }
  return (best > 0);
#endif
}
bool vulkan_info::init_logical_device(){
  VkDeviceQueueCreateInfo queue_create_info[this->queue_indices.count()] = {{}};
  uint32_t queue_create_info_count = 1;
  float queue_priority = 1.0f;

  queue_create_info[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  queue_create_info[0].queueFamilyIndex = this->queue_indices.graphics_index;
  queue_create_info[0].queueCount = 1;
  queue_create_info[0].pQueuePriorities = &queue_priority /*technically an array*/;;
  if(this->queue_indices.graphics_index != this->queue_indices.present_index){
    queue_create_info_count++;
    queue_create_info[1] = queue_create_info[0];
    queue_create_info[1].queueFamilyIndex = this->queue_indices.present_index;
  }
  //Also need to reuse this from is_device_suitable
  VkPhysicalDeviceFeatures device_features = {};
  device_features.sampleRateShading = VK_TRUE;
#ifdef DEBUG
  const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
  size_t layer_count = 1;
#else
  const char **layers = nullptr;
  size_t layer_count = 0;
#endif

  VkDeviceCreateInfo device_create_info = {};

  device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  device_create_info.pQueueCreateInfos = queue_create_info;
  device_create_info.queueCreateInfoCount = 1;
  device_create_info.enabledLayerCount = layer_count;
  device_create_info.ppEnabledLayerNames = layers;
  device_create_info.pEnabledFeatures = &device_features;
  device_create_info.enabledExtensionCount = static_cast<uint32_t>(Device_Extensions.size());
  device_create_info.ppEnabledExtensionNames = Device_Extensions.data();
  this->result = vkCreateDevice(this->physical_device, &device_create_info,
                                nullptr, &this->device);
  if(this->result != VK_SUCCESS){
    return false;
  }
  //Initialize queues here since it'd be overkill to put that in a seperate function.
  vkGetDeviceQueue(this->device, this->queue_indices.graphics_index,
                   0, &this->graphics_queue);
  vkGetDeviceQueue(this->device, this->queue_indices.present_index,
                   0, &this->present_queue);
  return true;
}
bool vulkan_info::init_command_pool(){
  VkCommandPoolCreateInfo create_info = {};

  create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  create_info.queueFamilyIndex = this->queue_indices.graphics_index;
  create_info.flags = 0;;
  this->result = vkCreateCommandPool(this->device, &create_info,
                                     nullptr, &this->command_pool);
  return (this->result == VK_SUCCESS);
}
/*
  I may move these swap chain functions into the graphics pipeline file.
*/
bool operator ==(const VkSurfaceFormatKHR& lhs, const VkSurfaceFormatKHR& rhs){
  return lhs.format == rhs.format && lhs.colorSpace == rhs.colorSpace;
}
static void choose_swap_chain_format(vulkan_info *info){
  static constexpr VkSurfaceFormatKHR prefered_format =
    {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
  uint32_t format_count = 0;
  vkGetPhysicalDeviceSurfaceFormatsKHR(info->physical_device, info->surface,
                                       &format_count, nullptr);
  std::vector<VkSurfaceFormatKHR> formats(format_count);
  vkGetPhysicalDeviceSurfaceFormatsKHR(info->physical_device, info->surface,
                                       &format_count, formats.data());
  if (formats.size() == 1 && formats[0].format == VK_FORMAT_UNDEFINED) {
    info->swap_chain.surface_format = prefered_format;
    return;
  }
  for (const auto& format : formats) {
    if(format == prefered_format){
      info->swap_chain.surface_format = format;
      return;
    }
  }
  //Otherwise just use whatever's first, We could seach for the best available later.
  info->swap_chain.surface_format = formats[0];
  return;
}
static void choose_swap_chain_present_mode(vulkan_info *info){
  uint32_t present_mode_count = 0;
  vkGetPhysicalDeviceSurfacePresentModesKHR(info->physical_device, info->surface,
                                            &present_mode_count, nullptr);
  std::vector<VkPresentModeKHR> present_modes(present_mode_count);
  vkGetPhysicalDeviceSurfacePresentModesKHR(info->physical_device, info->surface,
                                            &present_mode_count, present_modes.data());
  //Double buffering, always available
  info->swap_chain.present_mode = VK_PRESENT_MODE_FIFO_KHR;
  //Look for triple buffering
  auto it = std::find(present_modes.begin(), present_modes.end(),
                      VK_PRESENT_MODE_MAILBOX_KHR);
  if(it != present_modes.end()){
    info->swap_chain.present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
  }
}
static void choose_swap_chain_extent(vulkan_info *info){
  const VkSurfaceCapabilitiesKHR& capabilities = info->swap_chain.capabilities;
  if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
    info->swap_chain.extent = capabilities.currentExtent;
  } else {
    VkExtent2D &actual_extent = info->swap_chain.extent;
    SDL_Vulkan_GetDrawableSize(info->win, (int*)&actual_extent.width,
                               (int*)&actual_extent.height);
    actual_extent.width =
      std::clamp(actual_extent.width, capabilities.minImageExtent.width,
                 capabilities.maxImageExtent.width);
    actual_extent.height =
      std::clamp(actual_extent.height, capabilities.minImageExtent.height,
                 capabilities.maxImageExtent.height);
    return;
  }
}
static bool init_image_views(vulkan_info *info);
//Initialize the swap chain, This may be called multiple times since the swap
//chain may need to be regenerated (i.e if the window changes size)
bool vulkan_info::init_swap_chain(){
  VkSwapchainKHR old_swap_chain = this->swap_chain.handle;

  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(this->physical_device, this->surface,
                                            &this->swap_chain.capabilities);
  choose_swap_chain_format(this);
  choose_swap_chain_present_mode(this);
  choose_swap_chain_extent(this);

  uint32_t image_count = this->swap_chain.capabilities.minImageCount+1;
  if(this->swap_chain.capabilities.minImageCount ==
     this->swap_chain.capabilities.maxImageCount){
    image_count -= 1;
  }
  VkSwapchainCreateInfoKHR create_info = {};

  create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  create_info.surface = this->surface;
  create_info.minImageCount = image_count;
  create_info.imageFormat = this->swap_chain.surface_format.format;
  create_info.imageColorSpace = this->swap_chain.surface_format.colorSpace;
  create_info.imageExtent = this->swap_chain.extent;
  create_info.imageArrayLayers = 1;
  create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  create_info.preTransform = this->swap_chain.capabilities.currentTransform;
  create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  create_info.presentMode = this->swap_chain.present_mode;
  create_info.clipped = VK_TRUE;
  create_info.oldSwapchain = old_swap_chain;;
  uint32_t indices[] =
    {(uint32_t)this->queue_indices.graphics_index,
     (uint32_t)this->queue_indices.present_index};
  if(indices[0] == indices[1]){
    create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
  } else {
    create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
    create_info.queueFamilyIndexCount = 2;
    create_info.pQueueFamilyIndices = indices;
  }
  this->result = vkCreateSwapchainKHR(this->device, &create_info, nullptr,
                                      &this->swap_chain.handle);
  //Even if creating the swap_chain fails free the old one (if it exists)
  if(old_swap_chain != VK_NULL_HANDLE){
    //Destroy any old images views (if they exist)
    for(size_t i = 0; i < this->swap_chain.size(); i++){
      vkDestroyImageView(this->device, this->swap_chain.image_views[i], nullptr);
    }
    vkDestroySwapchainKHR(this->device, old_swap_chain, nullptr);
  }
  if(this->result != VK_SUCCESS){
    return false;
  }
  return init_image_views(this);
}
static bool init_image_views(vulkan_info *info){
  uint32_t image_count;
  vkGetSwapchainImagesKHR(info->device, info->swap_chain.handle,
                          &image_count, nullptr);
  info->swap_chain.images.resize(image_count);
  vkGetSwapchainImagesKHR(info->device, info->swap_chain.handle,
                          &image_count, info->swap_chain.images.data());
  info->swap_chain.image_views.resize(image_count);
  VkImageViewCreateInfo create_info = {};

  create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  create_info.image = VK_NULL_HANDLE;
  create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
  create_info.format = info->swap_chain.format();
  create_info.components =
    { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };
  create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  create_info.subresourceRange.baseMipLevel = 0;
  create_info.subresourceRange.levelCount = 1;
  create_info.subresourceRange.baseArrayLayer = 0;
  create_info.subresourceRange.layerCount = 1;
  for(size_t i = 0; i < info->swap_chain.size(); i++){
    create_info.image = info->swap_chain.images[i];
    info->result = vkCreateImageView(info->device, &create_info,
                                     nullptr, &info->swap_chain.image_views[i]);
    if(info->result != VK_SUCCESS){
      return false;
    }
  }
  return true;
}
