#include "vulkan_util.hpp"
//RAII wrapper for VkShaderModule, since they don't need to be kept after
//we use them to create the pipeline.
struct vk_shader_module_wrapper {
  vulkan_info *info;
  VkShaderModule module = VK_NULL_HANDLE;
  //info gives the device and is used to set the return value of vkCreateShaderModule
  vk_shader_module_wrapper(vulkan_info *info,
                           VkShaderModuleCreateInfo *create_info)
    : info{info} {
    info->result = vkCreateShaderModule(info->device, create_info,
                                        nullptr, &module);
  }
  ~vk_shader_module_wrapper(){
    vkDestroyShaderModule(info->device, module, nullptr);
  }
  vk_shader_module_wrapper(vk_shader_module_wrapper&& other)
    : info{other.info}, module{other.module} {
      other.module = VK_NULL_HANDLE;
  }
  //Ensure we don't accidently make a copy
  vk_shader_module_wrapper(vk_shader_module_wrapper& other) = delete;
  vk_shader_module_wrapper& operator=(vk_shader_module_wrapper& other) = delete;
};
char* file_to_string(const char *filename, size_t *sz_ptr){
  char *ret = nullptr;
  FILE* file = fopen(filename, "r");
  long len;
  if(!file){ return NULL; }
  if(fseek(file, 0, SEEK_END) == -1){
    goto end;
  }
  len = ftell(file);
  fseek(file, 0, SEEK_SET);
  ret = (char*)malloc(len + 1);
  fread(ret, 1, len, file);
  ret[len] = '\0';
  if(sz_ptr){ *sz_ptr = (size_t)len; }
 end:
  fclose(file);
  return ret;
}
vk_shader_module_wrapper load_shader_from_buffer(vulkan_info *info,
                                                 const char *buf, size_t sz){
  //Ensure code is aligned to a 32 bit boundry
  assert(((uintptr_t)buf) == 0);
  VkShaderModuleCreateInfo create_info = {};
  
  create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  create_info.codeSize = sz;
  create_info.pCode = (const uint32_t*)buf;
  return vk_shader_module_wrapper(info, &create_info);
}
vk_shader_module_wrapper load_shader_from_file(vulkan_info *info,
                                               const char* filename){
  size_t sz;
  char *code = file_to_string(filename, &sz);
  auto ret = load_shader_from_buffer(info, code, sz);
  free(code);
  return std::move(ret);
}
bool vulkan_info::init_render_pass(){
  //Specifies the format(s) of the image(s) we're rendering. These are
  //used as input/output to the fragement shader.
  VkAttachmentDescription color_attachment_info = {};
  //The multisample count will need to be part of the struct eventually
  
  color_attachment_info.format = this->swap_chain.format();
  color_attachment_info.samples = VK_SAMPLE_COUNT_1_BIT;
  color_attachment_info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  color_attachment_info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  color_attachment_info.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  color_attachment_info.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  color_attachment_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  color_attachment_info.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
  //Reference to above attachment for use in subpass descriptors
  VkAttachmentReference color_attachment_ref = {};
  
  color_attachment_ref.attachment = 0;
  color_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
  //Descripton of the different images/buffers used in the rendering subpass
  VkSubpassDescription subpass = {};
  
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &color_attachment_ref;
  subpass.inputAttachmentCount = 0;
  subpass.pInputAttachments = nullptr;
  //there are more fields but I'm ignoring them for now.

  //Create a subpass dependency to force the render pass to wait for an image
  //to actually be available from the swap chain.
  VkSubpassDependency dependency = {};
  
  dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
  dependency.dstSubpass = 0;
  dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependency.srcAccessMask = 0;
  dependency.dstStageMask = (VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT |
                             VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                             VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

  VkRenderPassCreateInfo render_pass_info = {};
  
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  render_pass_info.attachmentCount = 1;
  render_pass_info.pAttachments = &color_attachment_info;
  render_pass_info.subpassCount = 1;
  render_pass_info.pSubpasses = &subpass;
  render_pass_info.dependencyCount = 1;
  render_pass_info.pDependencies = &dependency;
  this->result = vkCreateRenderPass(this->device, &render_pass_info,
                                    nullptr, &this->render_pass);
  return (this->result == VK_SUCCESS);
}

//Not 100% sure how this is going to look in the end
bool vulkan_info::
init_graphics_pipeline(){
  //These are wrappers that will automatically destroy
  //the shader module on function exit
  auto vert_shader = load_shader_from_file(this, this->vert_shader_path);
  auto frag_shader = load_shader_from_file(this, this->frag_shader_path);

  VkPipelineShaderStageCreateInfo shader_create_info[2] = {};
  
  shader_create_info[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shader_create_info[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
  shader_create_info[0].module = vert_shader.module;
  shader_create_info[0].pName = "main";
  //pSpecializationInfo = nullptr /*map used to set shader constants*/
  
  shader_create_info[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shader_create_info[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  shader_create_info[1].module = frag_shader.module;
  shader_create_info[1].pName = "main";
  //This is obviously really important (it describes what info each vertex
  //contains), we're not using it just yet though.
  VkPipelineVertexInputStateCreateInfo vertex_input_info = {};
  
  vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  vertex_input_info.vertexBindingDescriptionCount = 0;
  vertex_input_info.pVertexBindingDescriptions = nullptr;
  vertex_input_info.vertexAttributeDescriptionCount = 0;
  vertex_input_info.pVertexAttributeDescriptions = nullptr;
  VkPipelineInputAssemblyStateCreateInfo primitive_info = {};
  
  primitive_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  primitive_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  primitive_info.primitiveRestartEnable = VK_TRUE;;//I like primitive restart.

  VkViewport viewport = {};
  
  viewport.x = 0.0f, viewport.y = 0.0f;
  viewport.width = (float) this->swap_chain.extent.width;
  viewport.height = (float) this->swap_chain.extent.height;
  viewport.minDepth = 0.0f, viewport.maxDepth = 1.0f;
  VkRect2D scissor = {{0,0}, this->swap_chain.extent};
  VkPipelineViewportStateCreateInfo viewport_state_info = {};
  
  viewport_state_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewport_state_info.viewportCount = 1;
  viewport_state_info.pViewports = &viewport;
  viewport_state_info.scissorCount = 1;
  viewport_state_info.pScissors = &scissor;

  VkPipelineRasterizationStateCreateInfo rasterizer_info = {};
  /*
    Most of the fields set to false / 0 are mostly used for things like shadow mapping.
    polygonMode can also be set to VK_POLYGON_MODE_POINT or VK_POLYGON_MODE_LINE
    to draw only the vertices or edges of a polygon (i.e wireframe), requires
    the fillModeNonSolid device feature.
  */
  
  rasterizer_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rasterizer_info.depthClampEnable = VK_FALSE;
  rasterizer_info.rasterizerDiscardEnable = VK_FALSE;
  rasterizer_info.polygonMode = VK_POLYGON_MODE_FILL;
  rasterizer_info.cullMode = VK_CULL_MODE_BACK_BIT;
  rasterizer_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
  rasterizer_info.lineWidth = 1.0f;
  rasterizer_info.depthBiasEnable = VK_FALSE;
  rasterizer_info.depthBiasConstantFactor = 0.0f;
  rasterizer_info.depthBiasClamp = 0.0f;
  rasterizer_info.depthBiasSlopeFactor = 0.0f;

  VkPipelineMultisampleStateCreateInfo multisampling_info = {};
  
  multisampling_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  multisampling_info.sampleShadingEnable = VK_FALSE;
  multisampling_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
  multisampling_info.minSampleShading = 1.0f;
  multisampling_info.pSampleMask = nullptr;
  multisampling_info.alphaToCoverageEnable = VK_FALSE;
  multisampling_info.alphaToOneEnable = VK_FALSE;

  //To be used later
  //VkPipelineDepthStencilStateCreateInfo depth_stencil_info = {};

  //Enable alpha blending
  VkPipelineColorBlendAttachmentState fb_color_blend_info = {};
  
  fb_color_blend_info.colorWriteMask = VK_COLOR_COMPONENT_RGBA_BITS;
  fb_color_blend_info.blendEnable = VK_TRUE;
  fb_color_blend_info.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
  fb_color_blend_info.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  fb_color_blend_info.colorBlendOp = VK_BLEND_OP_ADD;
  fb_color_blend_info.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  fb_color_blend_info.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  fb_color_blend_info.alphaBlendOp = VK_BLEND_OP_ADD;

  VkPipelineColorBlendStateCreateInfo global_color_blend_info = {};
  
  global_color_blend_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  global_color_blend_info.logicOpEnable = VK_FALSE;
  global_color_blend_info.logicOp = VK_LOGIC_OP_COPY /* unused */;
  global_color_blend_info.attachmentCount = 1;
  global_color_blend_info.pAttachments = &fb_color_blend_info;
  for(int i = 0; i < 4; i++){
    global_color_blend_info.blendConstants[i] = 0.0f;
  }

  //Specifies components of the pipeline we can change at runtime.
  //VkPipelineDynamicStateCreateInfo dynamicState = {};

  //Will likely get moved once we start using uniforms and such
  VkPipelineLayoutCreateInfo pipeline_layout_info = {};
  
  pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipeline_layout_info.setLayoutCount = 0;
  pipeline_layout_info.pSetLayouts = nullptr;
  pipeline_layout_info.pushConstantRangeCount = 0;
  pipeline_layout_info.pPushConstantRanges = nullptr;
  this->result = vkCreatePipelineLayout(this->device, &pipeline_layout_info,
                                        nullptr, &this->pipeline_layout);
  if(this->result != VK_SUCCESS){
    return false;
  }
  VkGraphicsPipelineCreateInfo pipeline_info = {};
  /*
    Most of this is self explanatory, but the basePipelineHandle/Index are
    an optimization for use in creating a pipeline that is derived from
    an existing pipeline, which we don't have here.
  */
  
  pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipeline_info.stageCount = 2 /* vertex & fragment*/;
  pipeline_info.pStages = shader_create_info;
  pipeline_info.pVertexInputState = &vertex_input_info;
  pipeline_info.pInputAssemblyState = &primitive_info;
  pipeline_info.pViewportState = &viewport_state_info;
  pipeline_info.pRasterizationState = &rasterizer_info;
  pipeline_info.pMultisampleState = &multisampling_info;
  pipeline_info.pDepthStencilState = nullptr;
  pipeline_info.pColorBlendState = &global_color_blend_info;
  pipeline_info.pDynamicState = nullptr;
  pipeline_info.layout = this->pipeline_layout;
  pipeline_info.renderPass = this->render_pass;
  pipeline_info.subpass = 0;
  pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
  pipeline_info.basePipelineIndex = -1;
  //Since you often want to create multiple graphics pipelines the create function
  //is designed to take arrays rather than single pointers. The second argument
  //(which we pass a null handle to) is for a VkPipelineCache object which is used
  //to speedup creation of similar pipelines.
  this->result = vkCreateGraphicsPipelines(this->device, VK_NULL_HANDLE,
                                           1, &pipeline_info, nullptr,  &this->pipeline);
  return (this->result == VK_SUCCESS);
}
//May move these next functions, since there aren't really related to the pipeline.
bool vulkan_info::init_framebuffers(){
  this->framebuffers.resize(this->swap_chain.size());
  VkFramebufferCreateInfo fb_info = {};
  
  fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  fb_info.renderPass = this->render_pass;
  fb_info.attachmentCount = 1;
  fb_info.pAttachments = nullptr /*set per framebuffer*/;
  fb_info.width = this->swap_chain.extent.width;
  fb_info.height = this->swap_chain.extent.height;
  fb_info.layers = 1;
  for(size_t i = 0; i < this->swap_chain.size(); i++){
    fb_info.pAttachments = &this->swap_chain.image_views[i];
    this->result = vkCreateFramebuffer(this->device, &fb_info,
                                       nullptr, &framebuffers[i]);
    if(this->result != VK_SUCCESS){
      return false;
    }
  }
  return true;
}
bool vulkan_info::init_command_buffers(){
  /*
    Quick note on command buffers.
    There are two types of command buffers primary and secondary, secondary
    buffers can be embeded into primary command buffers but not used on their
    own. We're only using primary buffers here.
  */
  this->command_buffers.resize(this->framebuffers.size());
  VkCommandBufferAllocateInfo alloc_info = {};
  
  alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  alloc_info.commandPool = this->command_pool;
  alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  alloc_info.commandBufferCount = (uint32_t) command_buffers.size();
  this->result = vkAllocateCommandBuffers(device, &alloc_info, this->command_buffers.data());
  if(this->result != VK_SUCCESS){ return false; }

  VkCommandBufferBeginInfo begin_info = {};
  
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
  begin_info.pInheritanceInfo = nullptr;
  VkClearValue clear_value = { 0.0f, 0.0f, 0.0f, 1.0f };
  VkRenderPassBeginInfo render_pass_info = {};
  
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  render_pass_info.renderPass = this->render_pass;
  render_pass_info.framebuffer = nullptr;
  render_pass_info.renderArea.offset = ((VkOffset2D){0, 0});
  render_pass_info.renderArea.extent = this->swap_chain.extent;
  render_pass_info.clearValueCount = 1;
  render_pass_info.pClearValues = &clear_value;
  for(size_t i = 0; i < this->command_buffers.size(); i++){
    this->result = vkBeginCommandBuffer(this->command_buffers[i], &begin_info);
    if(this->result != VK_SUCCESS){ goto end; }

    //All of the vkCmd functions return void so no need for error checking.
    render_pass_info.framebuffer = this->framebuffers[i];
    vkCmdBeginRenderPass(this->command_buffers[i], &render_pass_info,
                         VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(this->command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      this->pipeline);
    //vkCmdDraw(buffer, u32 vertexCount, u32 instanceCount,
    //                  u32 firstVertex, u32 firstInstance)
    vkCmdDraw(this->command_buffers[i], 3, 1, 0, 0);
    vkCmdEndRenderPass(this->command_buffers[i]);
    this->result = vkEndCommandBuffer(this->command_buffers[i]);
    if(this->result != VK_SUCCESS){ goto end; }
  }
 end:
  return (this->result == VK_SUCCESS);
}
bool vulkan_info::draw_frame(){
  //Wait for the last frame to finish being displayed
  size_t cur_frame = this->current_frame;
  vkWaitForFences(this->device, 1, &this->fences[cur_frame],
                  VK_TRUE, UINT64_MAX);
  vkResetFences(this->device, 1, &this->fences[cur_frame]);
  uint32_t image_index;
  //Grab the next image from the swap chain. Unfortunately there is no guarantee
  //on the order images are returned relative to when they are presented. 
  //Third argument is a timeout in nanoseconds, could be used to drop frames
  //for perfermance reasons if needed.
  this->result = vkAcquireNextImageKHR(this->device, this->swap_chain.handle,
                                     UINT64_MAX, this->semaphores[cur_frame * 2],
                                     VK_NULL_HANDLE, &image_index);
  //Submit request to grahpics card
  VkSubmitInfo submit_info = {};
  VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.waitSemaphoreCount = 1;
  submit_info.pWaitSemaphores = &this->semaphores[cur_frame*2];
  submit_info.pWaitDstStageMask = wait_stages;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &this->command_buffers[image_index];
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores = &this->semaphores[cur_frame*2 + 1];
  this->result = vkQueueSubmit(this->graphics_queue, 1, &submit_info,
                               this->fences[cur_frame]);
  if(this->result != VK_SUCCESS){ return false; }

  //Present the image to the screen
  VkPresentInfoKHR present_info = {};
  
  present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  present_info.waitSemaphoreCount = 1;
  present_info.pWaitSemaphores = &semaphores[cur_frame*2 + 1];
  this->result = vkQueuePresentKHR(present_queue, &present_info);
  //Since max_frames_queued is a constant this should get optimized
  this->current_frame = (cur_frame + 1) % this->max_frames_queued;
  return (this->result == VK_SUCCESS);
}
