#ifndef __VULKAN_CONTEXT_H__
#define __VULKAN_CONTEXT_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
/*
  Structure to encapsulate an sdl window, vulkan instance, 
  vulkan physical/logical device and device info.

  TODO: Maybe add a refcount
*/
struct vulkan_context {
  //The result of any vulkan call made using any of the members of this
  //struct is stored in this variable
  VkResult result;
  VkInstance instance;
  VkPhysicalDevice physical_device;
  VkDevice device;
  VmaAllocator allocator; //manages device memory
#if (defined DEBUG) || !(defined NDEBUG)
  VkDebugUtilsMessengerEXT debug_messenger;
#endif
  union {
    int queue_indices[2];
    struct {
      int graphics_queue_index;
      int present_queue_index;
    };
  };
  //Queues are created along with the logical device, so they belong here.
  VkQueue graphics_queue;
  VkQueue present_queue;
  //Memory pool from which command buffers are allocated.
  VkCommandPool command_pool;
  //Memory pool use to allocate temporary command buffers
  VkCommandPool temp_command_pool;
  //WSI info  
  SDL_Window* win;
  //A pointer to the vkGetInstanceProcAddr function obtained from SDL
  PFN_vkGetInstanceProcAddr get_instance_proc_addr_ptr;
  VkSurfaceKHR surface; //Not sure if this belongs here
};
/*
  Structure to encapsulate a swap chain, including information about its
  format, which is also the format of the screen we're rendering to.
*/
struct vulkan_swap_chain {
  vulkan_context *ctx;
  //swap chain
  VkSwapchainKHR handle;
  uint32_t size; //Has to fit in uint32 since that's what vulkan uses.
  VkExtent2D extent;
  VkSurfaceCapabilitiesKHR capabilities; 
  VkSurfaceFormatKHR format;
  VkPresentModeKHR present_mode;
  VkImage* images;
  VkImageView* image_views;
};
vulkan_context* allocate_vulkan_context();
VkResult init_vulkan_context(vulkan_context *ctx, SDL_Window *win,
                             const char **extensions,
                             size_t n_extns, VkApplicationInfo *app_info);
//Calls the previous 2 functions, returns NULL if initialization fails,
//Maybe it should take a VkResult* to allow finding the reason it failed.
vulkan_context* create_vulkan_context(SDL_Window *win, const char **extensions,
                                      size_t n_extns, VkApplicationInfo *app_info);
//destroys the structure fields and frees the struct
void destroy_vulkan_context(vulkan_context* ctx);
//Allocates and initializes a swap_chain, returns NULL on failure with
//ctx->result indicating the reason for failure.
vulkan_swap_chain* create_swap_chain(vulkan_context *ctx);
//(Re)initializes swap chain
VkBool32 reset_swap_chain(vulkan_swap_chain *sc);
//Reset the state of the swap_chain, i.e when the window is resized
//returns VK_TRUE on success and VK_FALSE with on failure
VkBool32 reset_swap_chain(vulkan_swap_chain *sc);
void destroy_swap_chain(vulkan_swap_chain* ctx);
#ifdef __cplusplus
}
#endif
#endif /* __VULKAN_CONTEXT_H__ */
