#ifndef _MACROS_H_
#define _MACROS_H_
/**
   Macros usable in C or C++
*/
//Some Macros
#define SET_BIT(val, bit)                       \
  ((val) |= (1ul << (bit)))
#define UNSET_BIT(val, bit)                     \
  ((val) &= ~(1ul << (bit)))
#define FLIP_BIT(val, bit)                      \
  ((val) ^= (1ul << (bit)))


//will fail for 0
//To exclude 0 use (num && (!(num & (num-1))))
#define IS_POW_OF_2(x) (!(x & (x-1)))

//This will fail for 0
#if (defined __GNUC__)
#define NEXT_POW_OF_2(num)                                      \
  (1UL << ((sizeof(long)*CHAR_BIT) - __builtin_clzl(num)))
#else //sorry windows, but you kinda suck when it comes to intrinsics.
#define NEXT_POW_OF_2(x)                                \
  (sizeof(x) <= 4 ?                                     \
   (x |= x >> 1; x |= x >> 2; x |= x >> 4;              \
    x |= x >> 8; x |= x >> 16; ++x) :                   \
   (x |= x >> 1; x |= x >> 2; x |= x >> 4;              \
    x |= x >> 8; x |= x >> 16; x |= x >> 32; ++x))
#endif

//Optimize allocation of temp memory by using alloca for small allocations
#define SMALL_ALLOC_LIMIT (64 << 10) //64K
#define SMALL_ALLOC(size) (size < SMALL_ALLOC_LIMIT ? alloca(size) : malloc(size))
#define SMALL_ALLOC_FREE(ptr, size) (size < SMALL_ALLOC_LIMIT ? (void)0 : free(ptr))

//If num is a power of 2 return it, otherwise return
//the nearest power of 2 larger than num.
#define NEAREST_POW_OF_2(num)                   \
  (IS_POW_OF_2(num) ? num : NEXT_POW_OF_2(num))

#define DOWNCASE_ASCII(c) (c > 0x40 && c < 0x5B ? c | 0x20 : c)
#define UPCASE_ASCII(c) (c > 0x60 && c < 0x7B ? c & (~0x20) : c)
#define ARRAY_LENGTH(arr) (sizeof(arr) / sizeof(arr[0]))

#ifndef __cplusplus
#ifdef __GNUC__
#define SWAP(x,y)                               \
  __extension__                                 \
  ({__typeof(x) __temp = x;                     \
    x = y;                                      \
    y = __temp;})
#define ARR_SWAP(arr,i,j)                       \
  __extension__                                 \
  ({__typeof(arr[i]) __temp = arr[i];           \
    arr[i] = arr[j];                            \
    arr[j] = __temp;})
#define MIN(_x,_y)                              \
  __extension__                                 \
  ({__typeof(_x) x = _x;                        \
    __typeof(_y) y = _y;                        \
    x<y ? x : y;})
#define MAX(_x,_y)                              \
  __extension__                                 \
  ({__typeof(_x) x = _x;                        \
    __typeof(_y) y = _y;                        \
    x>y ? x : y;})
#else
#define MIN(_x,_y) (_x < _y ? _x : _y)
#define MAX(_x,_y) (_x > _y ? _x : _y)
#endif //__GNUC__
#define CLAMP(x, lo, hi) MIN(hi, MAX(lo, x))
#endif

/*
  CPP Funtimes
*/
#define CAT(a,b) PRIMITIVE_CAT(a, b)
#define PRIMITIVE_CAT(a,b) a ## b
#define CAT2(a,b) PRIMITIVE_CAT2(a,b)
#define PRIMITIVE_CAT2(a,b) a ## b
#define CAT3(a, b, c) PRIMITIVE_CAT3(a, b, c)
#define PRIMITIVE_CAT3(a, b, c) a ## b ##c
#define CAT4(a, b, c, d) PRIMITIVE_CAT4(a, b, c, d)
#define PRIMITIVE_CAT4(a, b, c, d) a ## b ## c ## d
//these concatenate their arguments but insert an underscore between them
//they're mainly for use in constructing function names in macros
#define CAT_2(a,b) PRIMITIVE_CAT_2(a, b)
#define PRIMITIVE_CAT_2(a,b) a ## _ ## b
#define CAT_3(a, b, c) PRIMITIVE_CAT_3(a, b, c)
#define PRIMITIVE_CAT_3(a, b, c) a ## _ ## b ## _ ## c
#define CAT_4(a, b, c, d) PRIMITIVE_CAT_4(a, b, c, d)
#define PRIMITIVE_CAT_4(a, b, c, d) a ## _ b ## _ ## c ## _ ## d

#define __NARG__(...)  __NARG_I_(0 MAYBE_VA_ARGS(__VA_ARGS__), __RSEQ_N())
#define __HAVE_ARG__(...) __NARG_I_(0 MAYBE_VA_ARGS(__VA_ARGS__), __RSEQ_1())
#define __NARG_I_(...) __ARG_N(__VA_ARGS__)
#define __ARG_N(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9,_10,     \
                _11,_12,_13,_14,_15,_16,_17,_18,_19,_20,        \
                _21,_22,_23,_24,_25,_26,_27,_28,_29,_30,        \
                _31,_32,_33,_34,_35,_36,_37,_38,_39,_40,        \
                _41,_42,_43,_44,_45,_46,_47,_48,_49,_50,        \
                _51,_52,_53,_54,_55,_56,_57,_58,_59,_60,        \
                _61,_62,_63,n,...) n
#define __RSEQ_N() 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53,  \
                   52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42,  \
                   41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31,  \
                   30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20,  \
                   19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9,   \
                   8, 7, 6, 5, 4, 3, 2, 1, 0
#define __RSEQ_1() 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,    \
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,   \
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,   \
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,   \
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,   \
                    1, 1, 1, 1, 1, 1, 1, 1, 0
//Macro recursion
/*
  This is a bit complicated (I still don't totally understand it), but it does work.
  See below for examples of how to use it.
*/
#define EVAL0(...) __VA_ARGS__
#define EVAL1(...) EVAL0 (EVAL0 (EVAL0 (__VA_ARGS__)))
#define EVAL2(...) EVAL1 (EVAL1 (EVAL1 (__VA_ARGS__)))
#define EVAL3(...) EVAL2 (EVAL2 (EVAL2 (__VA_ARGS__)))
#define EVAL4(...) EVAL3 (EVAL3 (EVAL3 (__VA_ARGS__)))
#define EVAL(...)  EVAL4 (EVAL4 (EVAL4 (__VA_ARGS__)))

#define RECUR_END(...)
#define RECUR_OUT

#define RECUR_GET_END() 0, RECUR_END
#define RECUR_NEXT0(test, next, ...) next RECUR_OUT
#define RECUR_NEXT1(test, next) RECUR_NEXT0 (test, next, 0)
#define RECUR_NEXT(test, next)  RECUR_NEXT1 (RECUR_GET_END test, next)
/*
  Map a macro over a set of args.
*/
#define MACRO_MAP0(f, x, peek, ...) f(x) RECUR_NEXT (peek, MACRO_MAP1) (f, peek, __VA_ARGS__)
#define MACRO_MAP1(f, x, peek, ...) f(x) RECUR_NEXT (peek, MACRO_MAP0) (f, peek, __VA_ARGS__)
#define MACRO_MAP(f, ...) EVAL (MACRO_MAP1(f, __VA_ARGS__ ,(), 0))

//Initialize a structure, Similar to designated initializers but without a leading
//'.' before the field name.
//i.e struct point(int x,y,z); point pt; INIT_STRUCT(pt, x = 1, y = 2, z = 3);
//is the same as: point pt = {.x = 1, .y = 2, .z = 3};

#define INIT_STRUCT(name, ...) EVAL(_INIT_STRUCT1(name, __VA_ARGS__, (), 0))

#define _INIT_STRUCT0(name, field_val, peek, ...)                       \
  name.field_val; RECUR_NEXT(peek, _INIT_STRUCT1) (name, peek, __VA_ARGS__)
#define _INIT_STRUCT1(name, field_val, peek, ...)                       \
  name.field_val; RECUR_NEXT(peek, _INIT_STRUCT0) (name, peek, __VA_ARGS__)

#define INIT_STRUCT_PTR(name, ...) EVAL(_INIT_STRUCT_PTR1(name, __VA_ARGS__, (), 0))

#define _INIT_STRUCT_PTR0(name, field_val, peek, ...)                       \
  name->field_val; RECUR_NEXT(peek, _INIT_STRUCT_PTR1) (name, peek, __VA_ARGS__)
#define _INIT_STRUCT_PTR1(name, field_val, peek, ...)                       \
  name->field_val; RECUR_NEXT(peek, _INIT_STRUCT_PTR0) (name, peek, __VA_ARGS__)

#define PROTECT(...) __VA_ARGS__
//Macro for portably using possibly empty __VA_ARGS__
//Likely to eventually be replaced by C++20 VA_OPT
#ifndef MAYBE_VA_ARGS
#if __cplusplus > 202000L //C++20
#define MAYBE_VA_ARGS(...) __VA_OPT__(,) __VA_ARGS__
#elif (defined __GNUC__)
#define MAYBE_VA_ARGS(...) ,##__VA_ARGS__
#else
#define MAYBE_VA_ARGS(...) ,__VA_ARGS__
#endif
#endif

#if (defined DEBUG)
#define DEBUG_PRINTF(fmt, ...) fprintf(stderr, fmt MAYBE_VA_ARGS(__VA_ARGS__))
#else
#define DEBUG_PRINTF(...)
#endif

//portable version of gcc's builtin_unreachable
#if (defined __GNUC__)
#define unreachable() __builtin_unreachable()
#elif (defined _MSC_VER)
//MVSC has assume, which is like assert but used by the compiler.
#define unreachable() __assume(0)
#else
//Will only work when debugging is enabled but it's better than nothing.
#define unreachable() assert(false);
#endif

//some macros for cleaner attribute syntax
#ifdef __cplusplus
#define ATTRIBUTE(...) [[__VA_ARGS__]]
#define attribute_unused ATTRIBUTE(maybe_unused)
#define attribute_noreturn ATTRIBUTE(noreturn)
#define attribute_fallthrough ATTRIBUTE(fallthrough)
#if __cplusplus > 202000L //C++20
#define attribute_likely(...) [[likely]] __VA_ARGS__
#define attribute_unlikely(...) [[unlikely]] __VA_ARGS__
#elif (defined __GNUC__)
#define attribute_likely(...) __builtin_expect(__VA_ARGS__, 1)
#define attribute_unlikely(...) __builtin_expect(__VA_ARGS__, 0)
#else
#define attribute_likely(...) __VA_ARGS__
#define attribute_unlikely(...) __VA_ARGS__
#endif
#endif

//Shut up a super annoying g++ warning.
#ifdef __cplusplus
#if (defined __GNUC__) && (defined __GNUC_MINOR__) && (defined __GNUC_PATCHLEVEL__)
#if __GNUC__ >= 8
#pragma GCC diagnostic ignored "-Wclass-memaccess"
#endif
#endif
#endif

#endif
