#ifndef __VULKAN_PIPELINE_H__
#define __VULKAN_PIPELINE_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"
enum VkFormatSimple {
  VK_FORMAT_FLOAT = VK_FORMAT_R32_SFLOAT,
  VK_FORMAT_VEC2 = VK_FORMAT_R32G32_SFLOAT,
  VK_FORMAT_VEC3 = VK_FORMAT_R32G32B32_SFLOAT,
  VK_FORMAT_VEC4 = VK_FORMAT_R32G32B32A32_SFLOAT,
  VK_FORMAT_INT =  VK_FORMAT_R32_SINT,
  VK_FORMAT_IVEC2 = VK_FORMAT_R32G32_SINT,
  VK_FORMAT_IVEC3 = VK_FORMAT_R32G32B32_SINT,
  VK_FORMAT_IVEC4 = VK_FORMAT_R32G32B32A32_SINT,
  //  VK_FORMAT_RGBA = VK_FORMAT_R8G8B8A8_UNORM_PACK32,
  VK_FORMAT_ABGR = VK_FORMAT_A8B8G8R8_UNORM_PACK32
};
struct vulkan_pipeline {
  vulkan_context *ctx;
  //this also has a handle to ctx, so we could get rid of the pointer above,
  //but I think it's worth keeping for the convenience
  vulkan_swap_chain *swap_chain;
  VkPipelineLayout layout;
  VkPipeline pipeline;
  VkRenderPass render_pass;
  VkCommandBuffer* command_buffers;
  VkFramebuffer* framebuffers;

  //I could replace these with the actual VkShaderModules, but I'm not
  //sure if I should.
  const char *vert_shader_path;
  const char *frag_shader_path;
};
//Allocates a pipeline struct as well as space for all needed arrays, also
//sets the ctx and swap_chain fields. 
vulkan_pipeline* allocate_vulkan_pipeline(vulkan_context *ctx, vulkan_swap_chain *sc);
//Sets the ctx and swap_chain fields and allocates space for
//command_buffers and framebuffers
VkBool32 init_vulkan_pipeline(vulkan_pipeline *pipeline,
                              vulkan_context *ctx, vulkan_swap_chain *sc);
void destroy_vulkan_pipeline(vulkan_pipeline *pl);
VkShaderModule load_shader_from_file(vulkan_context *ctx,
                                     const char* filename);
VkShaderModule load_shader_from_buffer(vulkan_context *ctx,
                                       const char *buf, size_t sz);
#ifdef __cplusplus
}
#endif
#endif /* __VULKAN_PIPELINE_H__ */
