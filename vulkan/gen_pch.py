# Really simple, but meson doesn't allow inline python, so we need
# a seperate file even for something this simple.
import os, shutils
os.mkdir("pch")
shutil.copyfile("common.h", "pch/common.h")
