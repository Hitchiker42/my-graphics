#include "vulkan_context.h"
//The functions that return a VkResult return the value of ctx->result,
//that is to say that the return value is just a convenience.
static VkResult init_sdl_vk_instance(vulkan_context* ctx, const char **extensions,
                                     size_t n_extns, VkApplicationInfo *app_info);
static VkResult init_physical_device(vulkan_context* ctx);
static VkResult init_logical_device(vulkan_context* ctx);
static VkResult init_command_pools(vulkan_context* ctx);
static VkResult init_allocator(vulkan_context* ctx);
static VkBool32 check_instance_layers(const char** layers, size_t layer_count);
static VkBool32 check_device_layers(VkPhysicalDevice device, const char** layers,
                                   size_t layer_count);
static VkBool32 check_instance_extensions(const char** extns, size_t extn_count);
static VkBool32 check_device_extensions(VkPhysicalDevice device, const char** extns,
                                       size_t extn_count);
//In vulkan_debug.c
extern VkResult init_debug_messenger(vulkan_context* ctx);

vulkan_context* allocate_vulkan_context(){
  vulkan_context *ctx = calloc(1, sizeof(vulkan_context));
#if (VK_NULL_HANDLE != 0)
  ctx->instance = ctx->physical_device = ctx->device = VK_NULL_HANDLE;
#endif
  return ctx;
}
VkResult init_vulkan_context(vulkan_context *ctx, SDL_Window *win,
                             const char **extensions, size_t n_extns,
                             VkApplicationInfo *app_info){
  ctx->win = win;
  if(init_sdl_vk_instance(ctx, extensions, n_extns, app_info) != VK_SUCCESS){
    return ctx->result;
  }
  if(init_physical_device(ctx) != VK_SUCCESS){ return ctx->result; }
  if(init_logical_device(ctx) != VK_SUCCESS){ return ctx->result; }
  if(init_command_pools(ctx) != VK_SUCCESS){ return ctx->result; }
  if(init_allocator(ctx) != VK_SUCCESS){ return ctx->result; }
  return ctx->result;
}
//Calls the previous 2 functions, returns NULL if initialization fails
vulkan_context* create_vulkan_context(SDL_Window *win, const char **extensions,
                                      size_t n_extns, VkApplicationInfo *app_info){
  vulkan_context *ctx = allocate_vulkan_context();
  if(ctx){
    VkResult res = init_vulkan_context(ctx, win, extensions, n_extns, app_info);
    if(res != VK_SUCCESS){
      destroy_vulkan_context(ctx);
      ctx = NULL;
    }
  }
  return ctx;
}
//destroys the structure fields and frees the struct
void destroy_vulkan_context(vulkan_context* ctx){
  //Make sure any resources using the context have been freed
  vmaDestroyAllocator(ctx->allocator);
  vkDestroyCommandPool(ctx->device, ctx->command_pool, NULL);
  vkDestroyCommandPool(ctx->device, ctx->temp_command_pool, NULL); 
  vkDestroyDevice(ctx->device, NULL);
  vkDestroyInstance(ctx->instance, NULL);
}
static VkResult init_sdl_vk_instance(vulkan_context* ctx, const char **base_extensions,
                                     size_t n_extns, VkApplicationInfo *app_info){
  uint32_t count;
  if(SDL_Vulkan_GetInstanceExtensions(ctx->win, &count, NULL)){
    return (ctx->result = VK_ERROR_INITIALIZATION_FAILED);
  }
#ifdef DEBUG
  count++;
#endif
  const char **extensions = malloc(n_extns + count);
  if(extensions){ //memcpy with a null pointer is techncially undefined
    memcpy(extensions, base_extensions, n_extns * sizeof(const char *));
  }
  if(SDL_Vulkan_GetInstanceExtensions(ctx->win, &count, extensions + n_extns)){
    ctx->result = VK_ERROR_INITIALIZATION_FAILED;
    goto end;
  }
  count += n_extns; //set count to total number of extensions
#ifdef DEBUG
  const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
  size_t layer_count = 1;
  if(!check_instance_layers(layers, layer_count)){
    ctx->result = VK_ERROR_LAYER_NOT_PRESENT;
    goto end;
  }
  extensions[count++] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
#else
  const char* layers[] = {};
  size_t layer_count = 0;
#endif
  VkInstanceCreateInfo create_info = {
    .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
    .pApplicationInfo = app_info,
    .enabledLayerCount = (uint32_t)layer_count,
    .ppEnabledLayerNames = layers,
    .enabledExtensionCount = count,
    .ppEnabledExtensionNames = extensions
  };
  ctx->result = vkCreateInstance(&create_info, NULL, &ctx->instance);
  if(ctx->result == VK_SUCCESS){
    ctx->get_instance_proc_addr_ptr = SDL_Vulkan_GetVkGetInstanceProcAddr();
    if(!SDL_Vulkan_CreateSurface(ctx->win, ctx->instance, &ctx->surface)){
      ctx->result = VK_ERROR_INITIALIZATION_FAILED;
    }
  }
 end:
  free(extensions);
  return ctx->result;
}
/*
  Both VkLayerProperties and VkExtensionProperties have a character array as their
  first member so they can be implicitly converted into char*s and passed to strcmp
  for an actual string array we need to use a wrapper than takes char** pointers.
*/
static int pstrcmp(const void *a, const void *b){
  return strcmp(*(char *const *)a, *(char *const *)b);
}
static int void_strcmp(const void *a, const void *b){
  return strcmp((const char*)a, (const char*)b);
}
#define do_prop_check(wanted, wanted_count, available, available_count) \
  qsort(wanted, wanted_count, sizeof(wanted[0]), pstrcmp);              \
  qsort(available, available_count, sizeof(available[0]), void_strcmp); \
  size_t i = 0, j = 0;                                                  \
  while (j < available_count && i < wanted_count) {                     \
    int result = void_strcmp(wanted[i], &available[j]);                 \
    if (result == 0) {                                                  \
      i++;                                                              \
    } else if (result < 0) {                                            \
      break;                                                            \
    }                                                                   \
    j++;                                                                \
  }                                                                     \
  free(available);                                                      \
  return (i == wanted_count);
//These check functions could all be changed to use alloca for better performance.
static VkBool32 check_instance_layers(const char** layers, size_t layer_count){
  uint32_t count;
  vkEnumerateInstanceLayerProperties(&count, NULL);
  VkLayerProperties* available = calloc(count, sizeof(VkLayerProperties));
  vkEnumerateInstanceLayerProperties(&count, available);
  do_prop_check(layers, layer_count, available, count);
}
static VkBool32 check_device_layers(VkPhysicalDevice device,
                                        const char** layers, size_t layer_count){
  uint32_t count;
  vkEnumerateDeviceLayerProperties(device, &count, NULL);
  VkLayerProperties *available = calloc(count, sizeof(VkLayerProperties));
  vkEnumerateDeviceLayerProperties(device, &count, available);
  do_prop_check(layers, layer_count, available, count);
}
static VkBool32 check_instance_extensions(const char** extns, size_t extn_count){
  uint32_t count;
  vkEnumerateInstanceExtensionProperties(NULL, &count, NULL);
  VkExtensionProperties* available = calloc(count, sizeof(VkExtensionProperties));
  vkEnumerateInstanceExtensionProperties(NULL, &count, available);
  do_prop_check(extns, extn_count, available, count);
}
static VkBool32 check_device_extensions(VkPhysicalDevice device,
                                            const char** extns, size_t extn_count){
  uint32_t count;
  vkEnumerateDeviceExtensionProperties(device, NULL, &count, NULL);
  VkExtensionProperties *available = calloc(count, sizeof(VkExtensionProperties));
  vkEnumerateDeviceExtensionProperties(device, NULL, &count, available);
  do_prop_check(extns, extn_count, available, count);
}
static void reset_queue_indices(vulkan_context *ctx){
  ctx->graphics_queue_index = ctx->present_queue_index = -1;
}
static int queue_indices_complete(vulkan_context *ctx){
  return (ctx->graphics_queue_index >= 0 && ctx->present_queue_index >= 0);
}
static VkBool32 check_queue_family(vulkan_context *ctx,
                                   const VkQueueFamilyProperties* queue_family, int idx){
  if(queue_family->queueCount > 0){
    if(queue_family->queueFlags & VK_QUEUE_GRAPHICS_BIT){
      ctx->graphics_queue_index = idx;
      VkBool32 present_support = VK_FALSE;
      vkGetPhysicalDeviceSurfaceSupportKHR(ctx->physical_device, idx,
                                           ctx->surface, &present_support);
      if(present_support){ ctx->present_queue_index = idx; }
      return VK_TRUE;
    }
  }
  return VK_FALSE;
}
static VkBool32 check_device_queues(vulkan_context *ctx){
  uint32_t count = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(ctx->physical_device, &count, NULL);
  VkQueueFamilyProperties* queue_families = alloca(count*sizeof(VkQueueFamilyProperties));
  vkGetPhysicalDeviceQueueFamilyProperties(ctx->physical_device, &count, queue_families);
  for(uint32_t i = 0; i < count; i++){
    if(check_queue_family(ctx, &queue_families[i], i)){
      return VK_TRUE;
    }
  }
  return VK_FALSE;
}
static VkBool32 check_swap_chain(vulkan_context *ctx){
  uint32_t format_count = 0, present_mode_count = 0;
  vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->physical_device, ctx->surface,
                                       &format_count, NULL);
  vkGetPhysicalDeviceSurfacePresentModesKHR(ctx->physical_device, ctx->surface,
                                            &present_mode_count, NULL);
  return (format_count > 0 && present_mode_count > 0);
}
static VkBool32 check_device_properties(VkPhysicalDevice device){
  /*
    Some potentially useful features:
    geometryShader, tessellationShader: enable support for these shader types,
    fillModeNonSolid: Allow point and wireframe polygon rendering
    wideLines, largePoints: Allow point size / line width values other than 1.0
    sampleRateShading: Allow multisampling
   */
  VkPhysicalDeviceFeatures device_features;
  vkGetPhysicalDeviceFeatures(device, &device_features);
  //For now only require multisampling.
  return (device_features.sampleRateShading);
}
static const char* Device_Extensions[] = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};
static const size_t Num_Device_Extensions = ARRAY_LENGTH(Device_Extensions);
static const VkPhysicalDeviceFeatures Device_Features = {
  .sampleRateShading = VK_TRUE
};
static VkBool32 check_device_features(VkPhysicalDevice device){
  VkPhysicalDeviceFeatures device_features;
  vkGetPhysicalDeviceFeatures(device, &device_features);
  size_t num_features = sizeof(VkPhysicalDeviceFeatures) / sizeof(VkBool32);
  VkBool32* wanted = (VkBool32*)&Device_Features;
  VkBool32* available = (VkBool32*)&device_features;
  for(size_t i = 0; i < num_features; i++){
    if((wanted[i] & available[i]) != wanted[i]){
      return VK_FALSE;
    }
  }
  return VK_TRUE;
}
//Returns true if the device located at ctx->physical_device is suitable.
//Could also return a score to sort possible devices.
static int is_device_suitable(vulkan_context *ctx){
#if 0
  //Get properties and features
  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceProperties(device, &device_properties);
#endif
  //Check that the device supports all the queue_families we want.
  reset_queue_indices(ctx);
  if(!check_device_queues(ctx)){
    return 0;
  }
  if(!check_device_extensions(ctx->physical_device, Device_Extensions,
                                  Num_Device_Extensions)){
    return 0;
  }
  if(!check_device_features(ctx->physical_device)){
    return 0;
  }
  if(!check_swap_chain(ctx)){
    return 0;
  }
#ifdef DEBUG
  const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
  size_t layer_count = 1;
  if(!check_device_layers(ctx->physical_device, layers, layer_count)){
    return 0;
  }
#endif
  return 1;
#if 0
  //Kinda pointless but at least it gives me something to score.
  if(ctx->graphics_queue_index == ctx->present_queue_index){
    score = 2;
  } else {
    score = 1;
  }
  //More checks here
  return score;
#endif
}
VkResult init_physical_device(vulkan_context *ctx){
  uint32_t count = 0;
  vkEnumeratePhysicalDevices(ctx->instance, &count, NULL);
  if(count == 0){ return VK_ERROR_INITIALIZATION_FAILED; }
  VkPhysicalDevice* devices = alloca(count * sizeof(VkPhysicalDevice));
  vkEnumeratePhysicalDevices(ctx->instance, &count, devices);
  for(uint32_t i = 0; i < count; i++){
    ctx->physical_device = devices[0];
    if(is_device_suitable(ctx)){ return VK_SUCCESS;}
  }
  return VK_ERROR_FEATURE_NOT_PRESENT;//could also use EXTENSION/LAYER not present
#if 0
  //This Needs to be redone, I need to store the best device so far somewhere
  int best = 0;
  for(uint32_t i = 0; i < count; i++){
    ctx->physcial_device = devices[i];
    int score = is_device_suitable(ctx);
    if(score > best){
      best = score;
    }
  }
  return (best > 0);
#endif
}
VkResult init_logical_device(vulkan_context *ctx){
  VkDeviceQueueCreateInfo queue_create_infos[ARRAY_LENGTH(ctx->queue_indices)];
  uint32_t queue_create_info_count = 1;
  float queue_priority = 1.0f;
  queue_create_infos[0] = (VkDeviceQueueCreateInfo){
    .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
    .queueFamilyIndex = ctx->graphics_queue_index,
    .queueCount = 1,
    .pQueuePriorities = &queue_priority
  };
  if(ctx->graphics_queue_index != ctx->present_queue_index){
    queue_create_info_count++;
    queue_create_infos[1] = queue_create_infos[0];
    queue_create_infos[1].queueFamilyIndex = ctx->present_queue_index;
  }
  //Also need to reuse this from is_device_suitable
  VkPhysicalDeviceFeatures device_features = {
    .sampleRateShading = VK_TRUE
  };
#ifdef DEBUG
  const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
  size_t layer_count = 1;
#else
  const char **layers = nullptr;
  size_t layer_count = 0;
#endif

  VkDeviceCreateInfo device_create_info = {
    .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
    .pQueueCreateInfos = queue_create_infos,
    .queueCreateInfoCount = queue_create_info_count,
    .enabledLayerCount = layer_count,
    .ppEnabledLayerNames = layers,
    .pEnabledFeatures = &device_features,
    .enabledExtensionCount = (uint32_t)Num_Device_Extensions,
    .ppEnabledExtensionNames = Device_Extensions
  };
  ctx->result = vkCreateDevice(ctx->physical_device, &device_create_info,
                               NULL, &ctx->device);
  if(ctx->result == VK_SUCCESS){
    vkGetDeviceQueue(ctx->device, ctx->graphics_queue_index,
                     0, &ctx->graphics_queue);
    vkGetDeviceQueue(ctx->device, ctx->present_queue_index,
                     0, &ctx->present_queue);
  }
  return ctx->result;
}
static VkResult init_command_pools(vulkan_context *ctx){
  VkCommandPoolCreateInfo create_info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
    .flags = 0,
    .queueFamilyIndex = ctx->graphics_queue_index
  };
  ctx->result = vkCreateCommandPool(ctx->device, &create_info, NULL,
                                    &ctx->command_pool);
  if(ctx->result != VK_SUCCESS){ return ctx->result; }
  create_info.flags |= VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
  ctx->result = vkCreateCommandPool(ctx->device, &create_info, NULL,
                                    &ctx->temp_command_pool);
  return ctx->result;
}
static VkResult init_allocator(vulkan_context *ctx){
  VmaAllocatorCreateInfo create_info = {
    .flags = VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT,
    .physicalDevice = ctx->physical_device,
    .device = ctx->device
  };
  return (ctx->result = vmaCreateAllocator(&create_info, &ctx->allocator));
}
static const VkSurfaceFormatKHR prefered_surface_format =
  {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
static void choose_swap_chain_format(vulkan_swap_chain *sc){
  //Assume we'll get our prefered format, it simplifies later code.
  sc->format = prefered_surface_format;
  uint32_t format_count = 0;
  vkGetPhysicalDeviceSurfaceFormatsKHR(sc->ctx->physical_device,
                                       sc->ctx->surface, &format_count, NULL);
  VkSurfaceFormatKHR* formats = malloc(format_count * sizeof(VkSurfaceFormatKHR));
  vkGetPhysicalDeviceSurfaceFormatsKHR(sc->ctx->physical_device, sc->ctx->surface,
                                       &format_count, formats);
  //Vulkan indicates any format is fine by returning 1 format set to VK_FORMAT_UNDEFINED
  if (format_count == 1 && formats[0].format == VK_FORMAT_UNDEFINED) {
    goto end;
  }
  for (uint32_t i = 0; i < format_count; i++){
    if(formats[i].format == prefered_surface_format.format &&
       formats[i].colorSpace == prefered_surface_format.colorSpace){
      goto end;
    }
  }
  //Otherwise just use whatever's first, We could seach for the best available later.
  sc->format = formats[0];
 end:
  free(formats);
  return;
}
static void choose_swap_chain_present_mode(vulkan_swap_chain *sc){
  uint32_t mode_count = 0;
  vkGetPhysicalDeviceSurfacePresentModesKHR(sc->ctx->physical_device,
                                            sc->ctx->surface,
                                            &mode_count, NULL);
  VkPresentModeKHR* present_modes = malloc(mode_count * sizeof(VkPresentModeKHR));
  vkGetPhysicalDeviceSurfacePresentModesKHR(sc->ctx->physical_device,
                                            sc->ctx->surface,
                                            &mode_count, present_modes);
  //Double buffering, always available
  sc->present_mode = VK_PRESENT_MODE_FIFO_KHR;
  //Look for triple buffering
  for(uint32_t i  = 0; i < mode_count; i++){
    if(present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR){
      sc->present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
    }
  }
  free(present_modes);
  return;
}
static void choose_swap_chain_extent(vulkan_swap_chain *sc){
  if (sc->capabilities.currentExtent.width != UINT32_MAX){
    sc->extent = sc->capabilities.currentExtent;
  } else {
    SDL_Vulkan_GetDrawableSize(sc->ctx->win, (int*)&sc->extent.width, (int*)&sc->extent.height);
    sc->extent.width = CLAMP(sc->extent.width, sc->capabilities.minImageExtent.width,
                             sc->capabilities.maxImageExtent.width);
    sc->extent.height = CLAMP(sc->extent.height,sc->capabilities.minImageExtent.height,
                              sc->capabilities.maxImageExtent.height);
  }
}
static VkBool32 init_image_views(vulkan_swap_chain *sc);
static void alloc_image_views(vulkan_swap_chain *sc);
//Initialize the swap chain, This may be called multiple times since the swap
//chain may need to be regenerated (i.e if the window changes size)
VkBool32 reset_swap_chain(vulkan_swap_chain *sc){
  VkSwapchainKHR old_swap_chain = sc->handle;
  uint32_t old_size = sc->size;
  
  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(sc->ctx->physical_device, sc->ctx->surface,
                                            &sc->capabilities);
  //It may not be necessary to query these when reseting a swap chain
  //(vs initially setting it up) but it's easiest to do it this way.
  choose_swap_chain_format(sc);
  choose_swap_chain_present_mode(sc);
  choose_swap_chain_extent(sc);
  
  //maxImageCount is set to 0 if there is no limit.
  sc->size = MIN(sc->capabilities.minImageCount+1,
                 (sc->capabilities.maxImageCount || UINT32_MAX));
  VkSwapchainCreateInfoKHR create_info = {
    .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
    .surface = sc->ctx->surface,
    .minImageCount = sc->size,
    .imageFormat = sc->format.format,
    .imageColorSpace = sc->format.colorSpace,
    .imageExtent = sc->extent,
    .imageArrayLayers = 1,
    .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
    .preTransform = sc->capabilities.currentTransform,
    .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
    .presentMode = sc->present_mode,
    .clipped = VK_TRUE,
    .oldSwapchain = old_swap_chain
  };
  if(sc->ctx->queue_indices[0] == sc->ctx->queue_indices[1]){
    create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
  } else {
    create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
    create_info.queueFamilyIndexCount = 2;
    create_info.pQueueFamilyIndices = sc->ctx->queue_indices;
  }
  sc->ctx->result = vkCreateSwapchainKHR(sc->ctx->device, &create_info, NULL,
                                         &sc->handle);
  if(sc->ctx->result != VK_SUCCESS){ return VK_FALSE; }
  if(old_size < sc->size){
    alloc_image_views(sc);
  }
  return init_image_views(sc);
}
static void alloc_image_views(vulkan_swap_chain *sc){
  free(sc->images);
  free(sc->image_views);
  vkGetSwapchainImagesKHR(sc->ctx->device, sc->handle, &sc->size, NULL);
  sc->images = malloc(sc->size * sizeof(VkImage));
  vkGetSwapchainImagesKHR(sc->ctx->device, sc->handle,
                          &sc->size, sc->images);
  sc->image_views = malloc(sc->size * sizeof(VkImageView));
}
static VkBool32 init_image_views(vulkan_swap_chain *sc){
  VkImageViewCreateInfo create_info = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    .image = VK_NULL_HANDLE,
    .viewType = VK_IMAGE_VIEW_TYPE_2D,
    .format = sc->format.format,
    .components =
    { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY },
    .subresourceRange = {
      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .baseMipLevel = 0,
      .levelCount = 1,
      .baseArrayLayer = 0,
      .layerCount = 1
    }
  };
  for(uint32_t i = 0; i < sc->size; i++){
    create_info.image = sc->images[i];
    sc->ctx->result = vkCreateImageView(sc->ctx->device, &create_info,
                                        NULL, &sc->image_views[i]);
    if(sc->ctx->result != VK_SUCCESS){
      return VK_FALSE;
    }
  }
  return VK_FALSE;
}
vulkan_swap_chain* create_swap_chain(vulkan_context *ctx){
  vulkan_swap_chain* ret = calloc(1, sizeof(vulkan_swap_chain));
  ret->ctx = ctx;
  if(reset_swap_chain(ret)){
    return ret;
  } else{
    free(ret);
    return NULL;
  }
}
void destroy_swap_chain(vulkan_swap_chain *sc){
  for(uint32_t i = 0; i < sc->size; i++){
    vkDestroyImageView(sc->ctx->device, sc->image_views[i], NULL);
  }
  vkDestroySwapchainKHR(sc->ctx->device, sc->handle, NULL);
}
