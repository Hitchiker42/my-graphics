#include "vulkan_context.h"
#define get_instance_proc(ctx, name)                                    \
  ((CAT(PFN_, name))ctx->get_instance_proc_addr_ptr(ctx->instance, #name))
#define call_instance_proc(ctx, name, ...)                              \
  CAT(PFN_, name) CAT(pfn_, __LINE__) = get_instance_proc(ctx, name);   \
  if(!CAT(pfn_, __LINE__)){                                             \
    ctx->result = VK_ERROR_EXTENSION_NOT_PRESENT;                       \
  } else {                                                              \
    ctx->result = CAT(pfn_, __LINE__)(__VA_ARGS__);                     \
  }
#define call_instance_proc_void(ctx, name, ...)                         \
  CAT(PFN_, name) CAT(pfn_, __LINE__) = get_instance_proc(ctx, name);   \
  if(!CAT(pfn_, __LINE__)){                                             \
    ctx->result = VK_ERROR_EXTENSION_NOT_PRESENT;                       \
  } else {                                                              \
    CAT(pfn_, __LINE__)(MAYBE_VA_ARGS(__VA_ARGS__));                    \
    ctx->result = VK_SUCCESS;                                           \
  }
typedef VkDebugUtilsMessageSeverityFlagBitsEXT message_severity_flag;
typedef VkDebugUtilsMessageTypeFlagsEXT message_type_flag;
static message_severity_flag vk_debug_verbose =
  VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
static message_severity_flag vk_debug_info =
  VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
static message_severity_flag vk_debug_warning =
  VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
static message_severity_flag vk_debug_error =
  VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

static message_type_flag vk_debug_type_general =
  VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
static message_type_flag vk_debug_type_validation =
  VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
static message_type_flag vk_debug_type_performance =
  VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

static const char* severity_string(message_severity_flag severity){
  if(severity == vk_debug_verbose){
    return "Verbose";
  } else if(severity == vk_debug_info){
    return "Info";
  } else if(severity == vk_debug_warning){
    return "Warning";
  } else if(severity == vk_debug_error){
    return "Error";
  } else {
    return "Unknown";
  }
}
static const char* type_string(message_type_flag type){
  if(type == vk_debug_type_general){
    return "General";
  } else if(type == vk_debug_type_validation){
    return "Validation";
  } else if(type == vk_debug_type_performance){
    return "Performance";
  } else {
    return "Unknown";
  }
}
static const char* result_str(VkResult result){
#define do_case(value) case value: return #value;
  switch(result){
    do_case(VK_SUCCESS);
    do_case(VK_NOT_READY);
    do_case(VK_TIMEOUT);
    do_case(VK_EVENT_SET);
    do_case(VK_EVENT_RESET);
    do_case(VK_INCOMPLETE);
    do_case(VK_ERROR_OUT_OF_HOST_MEMORY);
    do_case(VK_ERROR_OUT_OF_DEVICE_MEMORY);
    do_case(VK_ERROR_INITIALIZATION_FAILED);
    do_case(VK_ERROR_DEVICE_LOST);
    do_case(VK_ERROR_MEMORY_MAP_FAILED);
    do_case(VK_ERROR_LAYER_NOT_PRESENT);
    do_case(VK_ERROR_EXTENSION_NOT_PRESENT);
    do_case(VK_ERROR_FEATURE_NOT_PRESENT);
    do_case(VK_ERROR_INCOMPATIBLE_DRIVER);
    do_case(VK_ERROR_TOO_MANY_OBJECTS);
    do_case(VK_ERROR_FORMAT_NOT_SUPPORTED);
    do_case(VK_ERROR_FRAGMENTED_POOL);
    do_case(VK_ERROR_OUT_OF_POOL_MEMORY);
    do_case(VK_ERROR_INVALID_EXTERNAL_HANDLE);
    do_case(VK_ERROR_SURFACE_LOST_KHR);
    do_case(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
    do_case(VK_SUBOPTIMAL_KHR);
    do_case(VK_ERROR_OUT_OF_DATE_KHR);
    do_case(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
    do_case(VK_ERROR_VALIDATION_FAILED_EXT);
    do_case(VK_ERROR_INVALID_SHADER_NV);
    do_case(VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT);
    do_case(VK_ERROR_FRAGMENTATION_EXT);
    do_case(VK_ERROR_NOT_PERMITTED_EXT);
    do_case(VK_ERROR_INVALID_DEVICE_ADDRESS_EXT);
    default:
      return "Unknown";
  }
#undef do_case
}
const char* vk_result_to_string(VkResult res){
  return result_str(res);
}
VK_DECL(VkBool32)
debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
               VkDebugUtilsMessageTypeFlagsEXT message_type,
               const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
               void* userdata){
  const char *severity = severity_string(message_severity);
  const char *type = type_string(message_severity);
  fprintf(stderr, "%s(%s): %s\n", severity, type, callback_data->pMessage);
  //returning true indicates and error.
  return VK_FALSE;
}
VkResult init_debug_messenger(vulkan_context *ctx){
#if (defined DEBUG)
  VkDebugUtilsMessengerCreateInfoEXT createInfo = {
    .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
    .messageSeverity = (vk_debug_verbose | vk_debug_warning | vk_debug_error),
    .messageType = (vk_debug_type_general | vk_debug_type_validation |
                    vk_debug_type_performance),
    .pfnUserCallback = debug_callback,
    .pUserData = ctx
  };
  call_instance_proc(ctx, vkCreateDebugUtilsMessengerEXT, ctx->instance,
                     &createInfo, NULL, &ctx->debug_messenger);
#else
  ctx->result = VK_SUCCESS;
#endif
  return ctx->result;
}
/*
  Debugging / informational stuff
*/
void print_extensions(vulkan_context *ctx){
  uint32_t count = 0;
  vkEnumerateInstanceExtensionProperties(NULL, &count, NULL);
  VkExtensionProperties *extns = malloc(count * sizeof(VkExtensionProperties));
  vkEnumerateInstanceExtensionProperties(NULL, &count, extns);
  printf("Available Instance Extensions:\n");
  for(uint32_t i = 0; i < count; i++){
    printf("\t%s\n", extns[i].extensionName);
  }
  if(ctx->physical_device != VK_NULL_HANDLE){
    uint32_t count2 = 0;
    vkEnumerateDeviceExtensionProperties(ctx->physical_device, NULL, &count2, NULL);
    if(count2 > count){
      extns = realloc(extns, count2 * sizeof(VkExtensionProperties));
    }
    vkEnumerateDeviceExtensionProperties(ctx->physical_device, NULL, &count2, extns);
    printf("Available Device Extensions:\n");
    for(uint32_t i = 0; i < count; i++){
      printf("\t%s\n", extns[i].extensionName);
    }
  }
}
