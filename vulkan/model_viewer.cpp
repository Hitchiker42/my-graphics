#include "vulkan_util.h"
static constexpr int default_width = 1280;
static constexpr int default_height = 720;
bool handle_event(SDL_Event *evt, vulkan_info *info){
  switch(evt->type){
    case(SDL_QUIT):
      return true;
    default:
      return false;
  }
}
int main(int argc, char *argv[]){
  SDL_Window *win;
  if(SDL_Init(SDL_INIT_VIDEO) < 0){
    fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
  win = SDL_CreateWindow("Vulkan 3D Model Viewer",
                         SDL_WINDOWPOS_UNDEFINED,
                         SDL_WINDOWPOS_UNDEFINED,
                         default_width, default_height,
                         SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
  if(!win){
    fprintf(stderr, "Couldn't open SDL window.\n");
    exit(1);
  }
  vulkan_info *info = vulkan_info::create_vulkan_info(win);
  if(!info){
    fprintf(stderr, "Error initializing Vulkan\n");
    exit(1);
  }
  SDL_Event evt;
  while(1){
    while(SDL_PollEvent(&evt)){
      if(handle_event(&evt, info)){
        goto end;
      }
    }
  }
 end:
  vulkan_info::destroy_vulkan_info(info);
  SDL_DestroyWindow(win);
  exit(0);
}
#if 0  
  
  

bool init_render_pass(vulkan_info *info){
  //Specifies the format(s) of the image(s) we're rendering. These are
  //used as input/output to the fragement shader.
  VkAttachmentDescription color_attachment_info = {};
  //The multisample count will need to be part of the struct eventually
  
  color_attachment_info.format = info->swap_chain.format.format;
  color_attachment_info.samples = VK_SAMPLE_COUNT_1_BIT;
  color_attachment_info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  color_attachment_info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  color_attachment_info.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  color_attachment_info.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  color_attachment_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  color_attachment_info.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;;
  //Reference to above attachment for use in subpass descriptors
  VkAttachmentReference color_attachment_ref = {};
  
  color_attachment_ref.attachment = 0;
  color_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;;
  //Descripton of the different images/buffers used in the rendering subpass
  VkSubpassDescription subpass = {};
  
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &color_attachment_ref;
  subpass.inputAttachmentCount = 0;
  subpass.pInputAttachments = nullptr;
  //there are more fields but I'm ignoring them for now.

  //Create a subpass dependency to force the render pass to wait for an image
  //to actually be available from the swap chain.
  VkSubpassDependency dependency = {};
  
  dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
  dependency.dstSubpass = 0;
  dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependency.srcAccessMask = 0;
  dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependency.dstAccessMask = (VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                              VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

  VkRenderPassCreateInfo render_pass_info = {};
  
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  render_pass_info.attachmentCount = 1;
  render_pass_info.pAttachments = &color_attachment_info;
  render_pass_info.subpassCount = 1;
  render_pass_info.pSubpasses = &subpass;
  render_pass_info.dependencyCount = 1;
  render_pass_info.pDependencies = &dependency;;
  info->result() = vkCreateRenderPass(info->ctx.device, &render_pass_info,
                                    nullptr, &info->pipeline.render_pass);
  return (info->result() == VK_SUCCESS);
}

//Not 100% sure how this is going to look in the end
bool init_graphics_pipeline(const char *vert_shader_path,
                            const char *frag_shader_path){
  //These are wrappers that will automatically destroy
  //the shader module on function exit
  VkShaderModule vert_shader = load_shader_from_file(info, vert_shader_path);
  VkShaderModule frag_shader = load_shader_from_file(info, frag_shader_path);

  VkPipelineShaderStageCreateInfo shader_create_info[2] = {};
  
  shader_create_info[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shader_create_info[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
  shader_create_info[0].module = vert_shader.module;
  shader_create_info[0].pName = "main";
  //pSpecializationInfo = nullptr /*map used to set shader constants*/
  
  shader_create_info[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shader_create_info[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  shader_create_info[1].module = frag_shader.module;
  shader_create_info[1].pName = "main";
  //This is obviously really important (it describes what info each vertex
  //contains), we're not using it just yet though.
  VkPipelineVertexInputStateCreateInfo vertex_input_info = {};
  
  vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  vertex_input_info.vertexBindingDescriptionCount = 0;
  vertex_input_info.pVertexBindingDescriptions = nullptr;
  vertex_input_info.vertexAttributeDescriptionCount = 0;
  vertex_input_info.pVertexAttributeDescriptions = nullptr;
  VkPipelineInputAssemblyStateCreateInfo primitive_info = {};
  
  primitive_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  primitive_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  primitive_info.primitiveRestartEnable = VK_TRUE;

  VkViewport viewport = {};
  
  viewport.x = 0.0f, viewport.y = 0.0f;
  viewport.width = (float) info->swap_chain.extent.width;
  viewport.height = (float) info->swap_chain.extent.height;
  viewport.minDepth = 0.0f, maxDepth = 1.0f;;
  VkRect2D scissor = {{0,0}, info->swap_chain.extent};
  
  VkPipelineViewportStateCreateInfo viewport_state_info = {};
  
  viewport_state_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewport_state_info.viewportCount = 1;
  viewport_state_info.pViewports = &viewport;
  viewport_state_info.scissorCount = 1;
  viewport_state_info.pScissors = &scissor;

  VkPipelineRasterizationStateCreateInfo rasterizer_info = {};
  /*
    Most of the fields set to false / 0 are mostly used for things like shadow mapping.
    polygonMode can also be set to VK_POLYGON_MODE_POINT or VK_POLYGON_MODE_LINE
    to draw only the vertices or edges of a polygon (i.e wireframe), requires
    the fillModeNonSolid device feature.
  */
  
  rasterizer_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rasterizer_info.depthClampEnable = VK_FALSE;
  rasterizer_info.rasterizerDiscardEnable = VK_FALSE;
  rasterizer_info.polygonMode = VK_POLYGON_MODE_FILL;
  rasterizer_info.cullMode = VK_CULL_MODE_BACK_BIT;
  rasterizer_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
  rasterizer_info.lineWidth = 1.0f;
  rasterizer_info.depthBiasEnable = VK_FALSE;
  rasterizer_info.depthBiasConstantFactor = 0.0f;
  rasterizer_info.depthBiasClamp = 0.0f;
  rasterizer_info.depthBiasSlopeFactor = 0.0f;

  VkPipelineMultisampleStateCreateInfo multisampling_info = {};
  
  multisampling_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  multisampling_info.sampleShadingEnable = VK_FALSE;
  multisampling_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
  multisampling_info.minSampleShading = 1.0f;
  multisampling_info.pSampleMask = nullptr;
  multisampling_info.alphaToCoverageEnable = VK_FALSE;
  multisampling_info.alphaToOneEnable = VK_FALSE;

  //To be used later
  //VkPipelineDepthStencilStateCreateInfo depth_stencil_info = {};

  //Enable alpha blending
  VkPipelineColorBlendAttachmentState fb_color_blend_info = {};
  
  fb_color_blend_info.colorWriteMask = VK_COLOR_COMPONENT_RGBA_BITS;
  fb_color_blend_info.blendEnable = VK_TRUE;
  fb_color_blend_info.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
  fb_color_blend_info.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  fb_color_blend_info.colorBlendOp = VK_BLEND_OP_ADD;
  fb_color_blend_info.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  fb_color_blend_info.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  fb_color_blend_info.alphaBlendOp = VK_BLEND_OP_ADD;;

  VkPipelineColorBlendStateCreateInfo global_color_blend_info = {};
  
  global_color_blend_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  global_color_blend_info.logicOpEnable = VK_FALSE;
  global_color_blend_info.logicOp = VK_LOGIC_OP_COPY /* unused */;
  global_color_blend_info.attachmentCount = 1;
  global_color_blend_info.pAttachments = &fb_color_blend_info;
  global_color_blend_info.blendConstants[0] = 0.0f, blendConstants[1] = 0.0f;
  global_color_blend_info.blendConstants[2] = 0.0f, blendConstants[3] = 0.0f;

  //Specifies components of the pipeline we can change at runtime.
  //VkPipelineDynamicStateCreateInfo dynamicState = {};

  //Will likely get moved once we start using uniforms and such
  VkPipelineLayoutCreateInfo pipeline_layout_info = {};
  
  pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipeline_layout_info.setLayoutCount = 0;
  pipeline_layout_info.pSetLayouts = nullptr;
  pipeline_layout_info.pushConstantRangeCount = 0;
  pipeline_layout_info.pPushConstantRanges = nullptr;;
  info->result = vkCreatePipelineLayout(info->ctx.device, &pipeline_layout_info,
                                        nullptr, &info->pipeline.pipeline);
  VkGraphicsPipelineCreateInfo pipeline_info = {};
  if(info->result != VK_SUCCESS){
    goto end;
  }

  /*
    Most of this is self explanatory, but the basePipelineHandle/Index are
    an optimization for use in creating a pipeline that is derived from
    an existing pipeline, which we don't have here.
  */
  
  pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipeline_info.stageCount = 2 /* vertex & fragment*/;
  pipeline_info.pStages = shader_create_info;
  pipeline_info.pVertexInputState = &vertex_input_info;
  pipeline_info.pInputAssemblyState = &primitive_info;
  pipeline_info.pViewportState = &viewport_state_info;
  pipeline_info.pRasterizationState = &rasterizer_info;
  pipeline_info.pMultisampleState = &multisampling_info;
  pipeline_info.pDepthStencilState = nullptr;
  pipeline_info.pColorBlendState = &global_color_blend_info;
  pipeline_info.pDynamicState = nullptr;
  pipeline_info.layout = info->pipeline_layout;
  pipeline_info.renderPass = info->render_pass;
  pipeline_info.subpass = 0;
  pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
  pipeline_info.basePipelineIndex = -1;;
  //Since you often want to create multiple graphics pipelines the create function
  //is designed to take arrays rather than single pointers. The second argument
  //(which we pass a null handle to) is for a VkPipelineCache object which is used
  //to speedup creation of similar pipelines.
  info->result() = vkCreateGraphicsPipelines(info->ctx.device, VK_NULL_HANDLE,
                                             1, &pipeline_info, nullptr,
                                             &info->pipeline.pipeline);
 end:
  vkDestroyShaderModule(info->ctx.device, vert_shader, nullptr);
  vkDestroyShaderModule(info->ctx.device, frag_shader, nullptr);
  return (info->result() == VK_SUCCESS);
}
//May move these next functions, since there aren't really related to the pipeline.
bool init_framebuffers(){
  info->pipeline.framebuffers.resize(info->swap_chain.size());
  VkFramebufferCreateInfo fb_info = {};
  
  fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  fb_info.renderPass = info->render_pass;
  fb_info.attachmentCount = 1;
  fb_info.pAttachments = nullptr /*set per framebuffer*/;
  fb_info.width = info->swap_chain.extent.width;
  fb_info.height = info->swap_chain.extent.height;
  fb_info.layers = 1;;
  for(size_t i = 0; i < info->swap_chain.size(); i++){
    fb_info.pAttachments = &info->swap_chain.image_views[i];
    info->result = vkCreateFramebuffer(info->device, &fb_info,
                                       nullptr, &framebuffers[i]);
    if(info->result != VK_SUCCESS){
      return false;
    }
  }
  return true;
}
bool vulkan_info::init_command_buffers(){
  /*
    Quick note on command buffers.
    There are two types of command buffers primary and secondary, secondary
    buffers can be embeded into primary command buffers but not used on their
    own. We're only using primary buffers here.
  */
  info->command_buffers.resize(info->framebuffers.size());
  VkCommandBufferAllocateInfo alloc_info = {};
  
  alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  alloc_info.commandPool = info->command_pool;
  alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  alloc_info.commandBufferCount = (uint32_t) command_buffers.size();;
  info->result = vkAllocateCommandBuffers(device, &alloc_info, info->command_buffers.data());
  if(info->result != VK_SUCCESS){ return false; }

  VkCommandBufferBeginInfo begin_info = {};
  
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
  begin_info.pInheritanceInfo = nullptr;;
  //We need to pass a pointer to this so we can't just put it in INIT_STRUCT directly
  VkClearValue clear_value = { 0.0f, 0.0f, 0.0f, 1.0f };
  VkRenderPassBeginInfo render_pass_info = {};
  
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  render_pass_info.renderPass = info->render_pass;
  render_pass_info.framebuffer = nullptr;
  render_pass_info.renderArea.offset = ((VkOffset2D){0, 0});
  render_pass_info.renderArea.extent = info->swap_chain.extent;
  render_pass_info.clearValueCount = 1;
  render_pass_info.pClearValues = &clear_value;;
  for(size_t i = 0; i < info->command_buffers.size(); i++){
    info->result = vkBeginCommandBuffer(info->command_buffers[i], &begin_info);
    if(info->result != VK_SUCCESS){ goto end; }

    //All of the vkCmd functions return void so no need for error checking.
    render_pass_info.framebuffer = info->framebuffers[i];
    vkCmdBeginRenderPass(info->command_buffers[i], &render_pass_info,
                         VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(info->command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      info->pipeline);
    //vkCmdDraw(buffer, u32 vertexCount, u32 instanceCount,
    //                  u32 firstVertex, u32 firstInstance)
    vkCmdDraw(info->command_buffers[i], 3, 1, 0, 0);
    vkCmdEndRenderPass(info->command_buffers[i]);
    info->result = vkEndCommandBuffer(info->command_buffers[i]);
    if(info->result != VK_SUCCESS){ goto end; }
  }
 end:
  return (info->result == VK_SUCCESS);
}
bool vulkan_info::draw_frame(){
  //Wait for the last frame to finish being displayed
  size_t cur_frame = info->current_frame;
  vkWaitForFences(info->device, 1, &info->fences[cur_frame],
                  VK_TRUE, UINT64_MAX);
  vkResetFences(info->device, 1, &info->fences[cur_frame]);
  uint32_t image_index;
  //Grab the next image from the swap chain. Unfortunately there is no guarantee
  //on the order images are returned relative to when they are presented. 
  //Third argument is a timeout in nanoseconds, could be used to drop frames
  //for perfermance reasons if needed.
  info->result = vkAcquireNextImageKHR(info->device, info->swap_chain.handle,
                                     UINT64_MAX, info->semaphores[cur_frame * 2],
                                     VK_NULL_HANDLE, &image_index);
  //Submit request to grahpics card
  VkSubmitInfo submit_info = {};
  VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.waitSemaphoreCount = 1;
  submit_info.pWaitSemaphores = &info->semaphores[cur_frame*2];
  submit_info.pWaitDstStageMask = wait_stages;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &info->command_buffers[image_index];
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores = &info->semaphores[cur_frame*2 + 1];;
  info->result = vkQueueSubmit(info->graphics_queue, 1, &submit_info,
                               info->fences[cur_frame]);
  if(info->result != VK_SUCCESS){ return false; }

  //Present the image to the screen
  VkPresentInfoKHR present_info = {};
  
  present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  present_info.waitSemaphoreCount = 1;
  present_info.pWaitSemaphores = &semaphores[cur_frame*2 + 1];;
  info->result = vkQueuePresentKHR(present_queue, &present_info);
  //Since max_frames_queued is a constant this should get optimized
  info->current_frame = (cur_frame + 1) % info->max_frames_queued;
  return (info->result == VK_SUCCESS);
}
#endif
