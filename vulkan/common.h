#ifndef __COMMON_H__
#define __COMMON_H__
//C headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>

#ifdef __cplusplus
//C++ headers
#include <algorithm>
#include <array>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <numeric>
#include <map>
#include <type_traits>
#include <string>
#include <string_view>
#include <vector>
#include <utility>
#include <unordered_map>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#endif
//library headers
#include <vulkan/vulkan.h>
#include <SDL.h>
#include <SDL_vulkan.h>
#include "vk_mem_alloc.h"

#include "macros.h"

//I rely on this in a couple places, it wouldn't be hard to change though
static_assert(VK_NULL_HANDLE == 0, "VK_NULL_HANDLE no longer equals 0");
//Specify calling conventions for vulkan callbacks, this is only needed
//because windows is stupid and can't just have one sane calling convention.
#define VK_DECL(rettype) VKAPI_ATTR rettype VKAPI_CALL
//Structure typedefs for C
typedef struct vulkan_context vulkan_context;
typedef struct vulkan_pipeline vulkan_pipeline;
typedef struct vulkan_swap_chain vulkan_swap_chain;

//Set a consistent value for the DEBUG macro
#if (defined DEBUG) || !(defined NDEBUG)
#undef DEBUG
#define DEBUG 1
#endif

#endif /* __COMMON_H__ */
